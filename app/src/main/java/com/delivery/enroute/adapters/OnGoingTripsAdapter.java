package com.delivery.enroute.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.delivery.enroute.R;
import com.delivery.enroute.activities.TrackTripActivity;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.utils.Constants;

import java.util.ArrayList;

import spencerstudios.com.bungeelib.Bungee;

public class OnGoingTripsAdapter  extends RecyclerView.Adapter<OnGoingTripsAdapter.InnerViewHolder>{

    private Context context;
    private ArrayList<Trip> trips;

    public OnGoingTripsAdapter(ArrayList<Trip> tripArrayList) {
        trips = tripArrayList;
    }

    @NonNull
    @Override
    public InnerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_ongoing_trip,viewGroup,false);
        context = viewGroup.getContext();
        return new InnerViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull InnerViewHolder holder, int position) {
        Trip trip = trips.get(position);

        holder.trackBtn.setText("Track");

        holder.label1.setText(Constants.shortenPickupsDestinations(context, trip));
        holder.label2.setText(Constants.formatToFriendlyDateTime(trip.requestDatetime));


        switch (trip.status){
            case "PENDING":
                holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_remove_circle_outline_black_24dp));
                ViewCompat.setBackgroundTintList(
                        holder.leftIcon,
                        ColorStateList.valueOf(context.getResources().getColor(R.color.dark_yellow)));
                break;
            case "ONGOING":
                holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_check_circle_black_24dp));
                ViewCompat.setBackgroundTintList(
                        holder.leftIcon,
                        ColorStateList.valueOf(context.getResources().getColor(R.color.dark_green)));
                break;
            case "NOT_FOUND":
                holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ongoing));
                ViewCompat.setBackgroundTintList(
                        holder.leftIcon,
                        ColorStateList.valueOf(context.getResources().getColor(R.color.dark_red)));
                break;
            case "CANCELLED_R":
                holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_indeterminate_check_box_black_24dp));
                ViewCompat.setBackgroundTintList(
                        holder.leftIcon,
                        ColorStateList.valueOf(context.getResources().getColor(R.color.dark_red)));
                break;
            case "CANCELLED":
                holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_indeterminate_check_box_black_24dp));
                ViewCompat.setBackgroundTintList(
                        holder.leftIcon,
                        ColorStateList.valueOf(context.getResources().getColor(R.color.dark_red)));
                break;

        }
    }

    @Override
    public int getItemCount() {
        return trips.size();
    }

    class InnerViewHolder extends RecyclerView.ViewHolder {

        private Button trackBtn;
        ImageView leftIcon;
        TextView label1, label2;
        View layout;

        InnerViewHolder(@NonNull View itemView) {
            super(itemView);

            label1 = itemView.findViewById(R.id.label_1);
            label2 = itemView.findViewById(R.id.label_2);
            leftIcon = itemView.findViewById(R.id.left_icon);

            layout = itemView.findViewById(R.id.layout);

            trackBtn = itemView.findViewById(R.id.trackBtn);
            trackBtn.setOnClickListener(trackTrip);

            //layout.setOnClickListener(trackTrip);


        }


        private View.OnClickListener trackTrip = view -> {
            Intent intent = new Intent(context, TrackTripActivity.class);

            Trip trip =  trips.get(getAdapterPosition());
            intent.putExtra(Constants.TRIP, trip.requestId);
            context.startActivity(intent);
            Bungee.slideUp(context);
        };


    }


}
