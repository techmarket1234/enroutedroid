package com.delivery.enroute.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.delivery.enroute.R;
import com.delivery.enroute.models.Destination;

import java.util.ArrayList;

public class ReceiptDestinationAdapter extends RecyclerView.Adapter<ReceiptDestinationAdapter.InnerViewHolder>  {

    private ArrayList<String> destinationArrayList;
    private Context context;

    public ReceiptDestinationAdapter(ArrayList<String> destinationArrayList) {
        this.destinationArrayList = destinationArrayList;
    }

    @NonNull
    @Override
    public InnerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_point,viewGroup,false);
        context = viewGroup.getContext();
        return new InnerViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull InnerViewHolder holder, int position) {
        String destination = destinationArrayList.get(position);
        holder.title.setText("DELIVERY POINT");


        if (destinationArrayList.size() == 1)
            holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.delivery_raw));
        else{
            switch (position){
                case 0:
                    // pc.marker.remove();
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.deliver_raw_1));
                    break;
                case 1:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.deliver_raw_2));
                    break;
                case 2:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.deliver_raw_3));
                    break;
                case 3:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.deliver_raw_4));
                    break;
                case 4:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.deliver_raw_5));
                    break;
                case 5:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.deliver_raw_6));
                    break;
                default:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.deliver_raw_1));
                    break;
            }
        }


       holder.title.setText(destination);
    }


    @Override
    public int getItemCount() {
        return destinationArrayList.size();
    }

    class InnerViewHolder extends RecyclerView.ViewHolder{

        TextView title;
        ImageView leftIcon;

        public InnerViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);

            leftIcon = itemView.findViewById(R.id.left_icon);
        }
    }
}
