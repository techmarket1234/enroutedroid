package com.delivery.enroute.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.delivery.enroute.R;
import com.delivery.enroute.models.Pickup;

import java.util.ArrayList;

public class OnGoingPickUpAdapter extends RecyclerView.Adapter<OnGoingPickUpAdapter.InnerViewHolder>  {

    private ArrayList<Pickup> pickupArrayList;
    private Context context;

    public OnGoingPickUpAdapter(ArrayList<Pickup> pickupArrayList) {
        this.pickupArrayList = pickupArrayList;
    }

    @NonNull
    @Override
    public InnerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_onging_trip_status,viewGroup,false);
        context = viewGroup.getContext();
        return new InnerViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull InnerViewHolder holder, int position) {
        Pickup pickup = pickupArrayList.get(position);
        holder.description.setText(pickup.place);

        String pickupPersonName = pickup.pickUpName != null ? pickup.pickUpName : "";
        String pickuppersonPhone = pickup.pickUpPersonPhone != null ? pickup.pickUpPersonPhone : "";
        String sender = pickupPersonName + " " + pickuppersonPhone;

        holder.sender_details.setText(sender);

        if (pickupArrayList.size() == 1)
         holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.pick_up_icon));
        else {
            switch (position){
                case 0:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.pickup_raw_1));
                    break;
                case 1:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.pickup_raw_2));
                    break;
                case 2:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.pickup_raw_3));
                    break;
                case 3:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.pickup_raw_4));
                    break;
                case 4:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.pickup_raw_5));
                    break;
                case 5:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.pickup_raw_6));
                    break;
                default:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.pickup_raw_1));
                    break;

            }
        }

        if (pickup.isPickedup){
            holder.status.setText("ITEM PICKED UP");
            holder.status.setTextColor(context.getResources().getColor(R.color.green500));
            return;
        }

        if (pickup.isArrived){
            holder.status.setText("RIDER HAS ARRIVED");
            holder.status.setTextColor(context.getResources().getColor(R.color.dark_yellow));
            return;
        }

        holder.status.setText("ON GOING");
        holder.status.setTextColor(context.getResources().getColor(R.color.dark_red));

    }


    @Override
    public int getItemCount() {
        return pickupArrayList.size();
    }

    class InnerViewHolder extends RecyclerView.ViewHolder{

        TextView title, description, status, sender_details;
        ImageView leftIcon;

        public InnerViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            description = itemView.findViewById(R.id.description);
            status = itemView.findViewById(R.id.status);
            sender_details = itemView.findViewById(R.id.sender_details);
            leftIcon = itemView.findViewById(R.id.left_icon);
        }
    }


}
