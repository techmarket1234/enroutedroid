package com.delivery.enroute.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.delivery.enroute.R;
import com.delivery.enroute.models.Destination;
import com.delivery.enroute.utils.Constants;

import java.util.ArrayList;

public class OnGoingDestinationAdapter extends RecyclerView.Adapter<OnGoingDestinationAdapter.InnerViewHolder> {

    private ArrayList<Destination> destinationArrayList;
    private Context context;

    public OnGoingDestinationAdapter(ArrayList<Destination> destinationArrayList) {
        this.destinationArrayList = destinationArrayList;
    }

    @NonNull
    @Override
    public InnerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_onging_trip_status,viewGroup,false);
        context = viewGroup.getContext();
        return new InnerViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull InnerViewHolder holder, int position) {
        Destination destination = destinationArrayList.get(position);
        holder.title.setText("DELIVERY POINT");
        holder.description.setText(destination.place);

        String recipieintName = destination.recipientName != null ? destination.recipientName : "";
        String recipientPhone = destination.recipientNumber != null ? destination.recipientNumber : "";

        String sender = recipieintName + " " + recipientPhone;
        holder.sender_details.setText(sender);

        if (destinationArrayList.size() == 1)
            holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.delivery_raw));
        else{
            switch (position){
                case 0:
                    // pc.marker.remove();
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.deliver_raw_1));
                    break;
                case 1:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.deliver_raw_2));
                    break;
                case 2:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.deliver_raw_3));
                    break;
                case 3:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.deliver_raw_4));
                    break;
                case 4:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.deliver_raw_5));
                    break;
                case 5:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.deliver_raw_6));
                    break;
                default:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.deliver_raw_1));
                    break;
            }
        }

        if (destination.isDelivered){
            holder.status.setText("ITEM DELIVERED");
            holder.status.setTextColor(context.getResources().getColor(R.color.green500));
            return;
        }

        if (destination.isArrived){
            holder.status.setText("RIDER HAS ARRIVED");
            holder.status.setTextColor(context.getResources().getColor(R.color.dark_yellow));
            return;
        }

        holder.status.setText("ON GOING");
        holder.status.setTextColor(context.getResources().getColor(R.color.dark_red));
    }

    @Override
    public int getItemCount() {
        return destinationArrayList.size();
    }

    class InnerViewHolder extends RecyclerView.ViewHolder{

        TextView title, description, status, sender_details;
        ImageView leftIcon;

        public InnerViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            description = itemView.findViewById(R.id.description);
            status = itemView.findViewById(R.id.status);
            sender_details = itemView.findViewById(R.id.sender_details);
            leftIcon = itemView.findViewById(R.id.left_icon);
        }
    }
}
