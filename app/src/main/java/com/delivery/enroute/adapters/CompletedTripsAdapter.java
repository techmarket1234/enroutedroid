package com.delivery.enroute.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SwipeLayout;
import com.delivery.enroute.R;
import com.delivery.enroute.activities.ReceiptActivity;
import com.delivery.enroute.listeners.OnRedoRequestTrip;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.utils.Constants;

import java.util.ArrayList;

import spencerstudios.com.bungeelib.Bungee;

public class CompletedTripsAdapter  extends RecyclerView.Adapter<CompletedTripsAdapter.InnerViewHolder> {

    Context context;
    private ArrayList<Trip> trips;
    private OnRedoRequestTrip onRedoRequestTrip;

    public CompletedTripsAdapter(ArrayList<Trip> tripsArrayList, OnRedoRequestTrip onRedoRequestTrip) {
        this.trips = tripsArrayList;
        this.onRedoRequestTrip = onRedoRequestTrip;
    }

    @NonNull
    @Override
    public InnerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_completed_trips,viewGroup,false);
        context = viewGroup.getContext();
        return new InnerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InnerViewHolder holder, int position) {


        Trip trip = trips.get(position);


        StringBuilder ppbuilder = new StringBuilder();

        for(String pickUpString : trip.pickupNames){

            String abbrvPickUps = pickUpString;
            if (pickUpString.length() > 4){
                abbrvPickUps = pickUpString.substring(0, Math.min(pickUpString.length(), 4));
            }

            ppbuilder.append(abbrvPickUps);
            ppbuilder.append("/");
        }
        String pickupStringResult = ppbuilder.toString().substring(0, ppbuilder.toString().length() - 1);

        StringBuilder destStringBuilder = new StringBuilder();
        for(String destString : trip.destinationNames){
            String abbrvPickUps = destString;
            if (destString.length() > 4){
                abbrvPickUps = destString.substring(0, Math.min(destString.length(), 4));
            }
            destStringBuilder.append(abbrvPickUps);
            destStringBuilder.append("/");
        }

        String destStringResult = destStringBuilder.toString().substring(0, destStringBuilder.toString().length() - 1);

        holder.label1.setText(context.getResources().getString(R.string.pick_dest, pickupStringResult, destStringResult));
        holder.label2.setText(Constants.formatToFriendlyDateTime(trip.requestDatetime));
    }

    @Override
    public int getItemCount() {
        return trips.size();
    }

    class InnerViewHolder extends RecyclerView.ViewHolder implements SwipeLayout.SwipeListener {

        ImageView leftIcon;
        TextView label1, label2;
//        TextView title, description, status, sender_details;
//        ImageView leftIcon;
        RelativeLayout viewBackground, viewForeground;
        Button receipt;
        SwipeLayout swipeLayout;



        public InnerViewHolder(@NonNull View itemView) {
            super(itemView);


            label1 = itemView.findViewById(R.id.label_1);
            label2 = itemView.findViewById(R.id.label_2);
            leftIcon = itemView.findViewById(R.id.left_icon);


            receipt = itemView.findViewById(R.id.receipt);
            receipt.setOnClickListener(view -> {
               showReceipt();
            });

            swipeLayout = itemView.findViewById(R.id.place_layout);
            swipeLayout.addSwipeListener(this);
            swipeLayout.setOnClickListener(view -> {
                showReceipt();
            });

//            title = itemView.findViewById(R.id.title);
//            description = itemView.findViewById(R.id.description);
//            status = itemView.findViewById(R.id.status);
//            sender_details = itemView.findViewById(R.id.sender_details);
//            leftIcon = itemView.findViewById(R.id.left_icon);
//            viewBackground = itemView.findViewById(R.id.view_background);
//            viewForeground = itemView.findViewById(R.id.view_foreground);
        }

        private void showReceipt(){
            Intent intent = new Intent(context, ReceiptActivity.class);
            Constants.TEMPORAL_TRIP_HOLDER = trips.get(getAdapterPosition());
            context.startActivity(intent);
            Bungee.slideUp(context);
        }

        @Override
        public void onStartOpen(SwipeLayout layout) {

        }

        @Override
        public void onOpen(SwipeLayout layout) {

            onRedoRequestTrip.requestTrip(trips.get(getAdapterPosition()));
            swipeLayout.close(true);

//            Dialogs.confirmWithSweet(context).setTitleText("Confirm").setContentText("Do you want to request another trip ?").setConfirmClickListener(sweetAlertDialog -> {
//                sweetAlertDialog.dismiss();
//
//
//            }).show();
        }

        @Override
        public void onStartClose(SwipeLayout layout) {

        }

        @Override
        public void onClose(SwipeLayout layout) {

        }

        @Override
        public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

        }

        @Override
        public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

        }
    }

}
