package com.delivery.enroute.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.delivery.enroute.R;
import com.delivery.enroute.models.CheckOut;
import com.delivery.enroute.utils.Constants;

import java.util.ArrayList;

public class CheckOutListAdapter extends RecyclerView.Adapter<CheckOutListAdapter.InnerViewHolder> {

    private ArrayList<CheckOut> checkOutArrayList;
    private Context context;

    public CheckOutListAdapter(ArrayList<CheckOut> checkOuts) {
        this.checkOutArrayList = checkOuts;
    }

    @NonNull
    @Override
    public InnerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup  , int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_checkout,viewGroup,false);
        context = viewGroup.getContext();
        return new InnerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InnerViewHolder holder, int i) {
        CheckOut checkOut = checkOutArrayList.get(i);
        holder.title.setText(checkOut.itemTitle);
        String name = checkOut.personName != null && !"null".equals(checkOut.personName)  ? checkOut.personName : "";
        String phone = checkOut.phone != null && !"null".equals(checkOut.phone) ? checkOut.phone : "";
        String senderDetails = name + " " + phone;
        holder.senderDetails.setText(senderDetails);
        holder.description.setText(checkOut.name);

        holder.leftIcon.setImageDrawable(context.getResources().getDrawable(checkOut.leftIcon));

        holder.title.setText(checkOut.type.equals(Constants.PICK_UP) ? "PICK UP LOCATION" : "DELIVERY LOCATION");

    }

    @Override
    public int getItemCount() {
        return checkOutArrayList.size();
    }

    class InnerViewHolder extends RecyclerView.ViewHolder{

        TextView title, description, senderDetails;
        ImageView leftIcon;

        InnerViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            description = itemView.findViewById(R.id.description);
            senderDetails = itemView.findViewById(R.id.sender_details);
            leftIcon = itemView.findViewById(R.id.left_icon);

        }
    }
}
