package com.delivery.enroute.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.delivery.enroute.R;
import com.delivery.enroute.models.Item;

import java.util.List;

public class ItemCategoryAdapter extends BaseAdapter {

    private List<Item> rows;
    public Resources res;
    private Context context;
    private LayoutInflater inflater;

    public ItemCategoryAdapter(Context context, List<Item> rows) {
        this.rows = rows;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return rows.size();
    }

    @Override
    public Item getItem(int i) {
        return rows.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        @SuppressLint("ViewHolder") View row = inflater.inflate(R.layout.item_category, parent, false);
        Item item = getItem(position);


        ImageView icon = row.findViewById(R.id.icon_spinner);
        icon.setImageDrawable(context.getResources().getDrawable(item.icon));

        TextView label = row.findViewById(R.id.text_spinner);

        label.setText(item.getItemName());

      /*  String item_sel=label.getItemName().toString();

        SharedPreferences.Editor editor2 = prefs.edit();
        editor2.putString("the_item_picked", item_sel); //InputString: from the response from JSon

        editor2.commit();*/

        return row;
    }
}
