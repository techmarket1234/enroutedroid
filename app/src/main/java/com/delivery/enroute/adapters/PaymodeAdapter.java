package com.delivery.enroute.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.delivery.enroute.R;
import com.delivery.enroute.models.Item;

import java.util.List;

public class PaymodeAdapter extends BaseAdapter {

    private String [] paymodes = {"MTN","TIGO / AIRTEL", "VODAFONE"};
    private int mSelectedItem;

    public void setmSelectedItem(int mSelectedItem) {
        this.mSelectedItem = mSelectedItem;
    }

    public PaymodeAdapter() {
    }

    @Override
    public int getCount() {
        return paymodes.length;
    }

    @Override
    public String getItem(int i) {
        return paymodes[i];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Context context = viewGroup.getContext();
        @SuppressLint("ViewHolder")
        View row = View.inflate(context,R.layout.row_pay_mode, null);
        TextView label = row.findViewById(R.id.network_label);
        String item = getItem(i);
        label.setText(item);

        if (i == mSelectedItem) {
            // set your color
            label.setBackground(context.getResources().getDrawable(R.mipmap.edittext_bg));
        }

        return row;
    }

}
