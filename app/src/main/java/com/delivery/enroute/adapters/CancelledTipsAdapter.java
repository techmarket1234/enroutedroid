package com.delivery.enroute.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.delivery.enroute.R;
import com.delivery.enroute.activities.TrackTripActivity;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.Dialogs;

import java.util.ArrayList;

import spencerstudios.com.bungeelib.Bungee;

public class CancelledTipsAdapter extends RecyclerView.Adapter<CancelledTipsAdapter.InnerViewHolder> {


    private Context context;
    private ArrayList<Trip> trips;

    public CancelledTipsAdapter(ArrayList<Trip> trips) {
        this.trips = trips;
    }

    @NonNull
    @Override
    public InnerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_cancelled_trip,viewGroup,false);
        context = viewGroup.getContext();
        return new InnerViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull InnerViewHolder holder, int position) {

        Trip trip = trips.get(position);

        holder.trackBtn.setText("Retry");

        StringBuilder ppbuilder = new StringBuilder();

        for(String pickUpString : trip.pickupNames){

            String abbrvPickUps = pickUpString;
            if (pickUpString.length() > 4){
                abbrvPickUps = pickUpString.substring(0, Math.min(pickUpString.length(), 4));
            }

            ppbuilder.append(abbrvPickUps);
            ppbuilder.append("/");
        }
        String pickupStringResult = ppbuilder.toString().substring(0, ppbuilder.toString().length() - 1);

        StringBuilder destStringBuilder = new StringBuilder();
        for(String destString : trip.destinationNames){
            String abbrvPickUps = destString;
            if (destString.length() > 4){
                abbrvPickUps = destString.substring(0, Math.min(destString.length(), 4));
            }
            destStringBuilder.append(abbrvPickUps);
            destStringBuilder.append("/");
        }

        String destStringResult = destStringBuilder.toString().substring(0, destStringBuilder.toString().length() - 1);

        holder.label1.setText(context.getResources().getString(R.string.pick_dest, pickupStringResult, destStringResult));
        holder.label2.setText(Constants.formatToFriendlyDateTime(trip.requestDatetime));

    }

    @Override
    public int getItemCount() {
        return trips.size();
    }

    class InnerViewHolder extends RecyclerView.ViewHolder{

        private Button trackBtn;
        ImageView leftIcon;
        View layout;
        TextView label1, label2;

        InnerViewHolder(@NonNull View itemView) {
            super(itemView);

            label1 = itemView.findViewById(R.id.label_1);
            label2 = itemView.findViewById(R.id.label_2);
            leftIcon = itemView.findViewById(R.id.left_icon);

            layout = itemView.findViewById(R.id.layout);
            layout.setOnClickListener(trackTrip);


            trackBtn = itemView.findViewById(R.id.trackBtn);
            trackBtn.setOnClickListener(trackTrip);


        }

        private View.OnClickListener trackTrip = view -> {

            Dialogs.confirm(context, "Do you want to request same trip again ?").setTitle("Request Trip").setPositiveButton("Yes", view1 -> {

               Trip trip =  trips.get(getAdapterPosition());
               Trip.requestAgain((Activity) context, trip);

            }).show();
//            Intent intent = new Intent(context, TrackTripActivity.class);
//
//            Trip trip =  trips.get(getAdapterPosition());
//            intent.putExtra(Constants.TRIP, trip.requestId);
//            context.startActivity(intent);
//            Bungee.slideUp(context);
        };
    }
}
