package com.delivery.enroute.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.delivery.enroute.R;
import com.delivery.enroute.models.CancelOrderReason;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CancelOrderReasonsAdapter extends RecyclerView.Adapter<CancelOrderReasonsAdapter.InnerViewHolder> {

    private Context context;
    private ArrayList<CancelOrderReason> cancelOrderReasonArrayList;

    public CancelOrderReasonsAdapter(ArrayList<CancelOrderReason> cancelOrderReasonArrayList) {
        this.cancelOrderReasonArrayList = cancelOrderReasonArrayList;
    }

    public List<String> getSelectedCancellationReasons() {
        List<String> selectedCancelledReasons = new ArrayList<>();
        for (CancelOrderReason cancelOrderReason : cancelOrderReasonArrayList) {
            if (cancelOrderReason.selected){
                selectedCancelledReasons.add(cancelOrderReason.text);
            }
        }
        return selectedCancelledReasons;
    }

    @NonNull
    @Override
    public InnerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cancel_order_reason,parent,false);
        context = parent.getContext();
        return new CancelOrderReasonsAdapter.InnerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InnerViewHolder holder, int position) {
        CancelOrderReason reason = cancelOrderReasonArrayList.get(position);
        holder.label.setText(reason.text);
    }

    @Override
    public int getItemCount() {
        return cancelOrderReasonArrayList.size();
    }

    class InnerViewHolder extends RecyclerView.ViewHolder{

        ImageView icon;
        TextView label;
        View layout;

        public InnerViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.left_icon);
            label = itemView.findViewById(R.id.label);
            layout = itemView.findViewById(R.id.item_reason_layout);
            layout.setOnClickListener(view -> {
                CancelOrderReason reason = cancelOrderReasonArrayList.get(getAdapterPosition());
                reason.selected = !reason.selected;

                icon.setImageDrawable(context.getResources().getDrawable((reason.selected) ? R.drawable.ic_check_circle_black_24dp : R.drawable.ic_remove_circle_outline_black_24dp));

            });
        }
    }
}
