package com.delivery.enroute.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.delivery.enroute.R;
import com.delivery.enroute.listeners.RowTappedLister;
import com.delivery.enroute.models.PlaceComplete;

import java.util.ArrayList;

public class PlacesCompleteAdapter extends RecyclerView.Adapter<PlacesCompleteAdapter.InnerViewHolder> {

    private Context context;
    private ArrayList<PlaceComplete> placeCompletes;
    private RowTappedLister<PlaceComplete> rowTappedLister;

    public PlacesCompleteAdapter(ArrayList<PlaceComplete> placeCompletes) {
        this.placeCompletes = placeCompletes;
    }

    public void setRowTappedLister(RowTappedLister<PlaceComplete> rowTappedLister) {
        this.rowTappedLister = rowTappedLister;
    }

    @NonNull
    @Override
    public InnerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_place,viewGroup,false);
        context = viewGroup.getContext();
        return new InnerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InnerViewHolder innerViewHolder, int i) {
        PlaceComplete placeComplete = placeCompletes.get(i);
        innerViewHolder.name.setText(placeComplete.name);
        innerViewHolder.description.setText(placeComplete.description);
        innerViewHolder.leftIcon.setImageDrawable(context.getResources().getDrawable(placeComplete.leftIcon));

        if (!placeComplete.showRemoveBtn){
            innerViewHolder.removeBtn.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return placeCompletes.size();
    }

    class InnerViewHolder extends RecyclerView.ViewHolder{

        TextView name, description;
        View placeLayout;
        ImageView leftIcon;
        ImageView removeBtn;

        InnerViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            placeLayout = itemView.findViewById(R.id.place_layout);
            description = itemView.findViewById(R.id.description);
            leftIcon = itemView.findViewById(R.id.left_icon);
            removeBtn = itemView.findViewById(R.id.remove_btn);

            placeLayout.setOnClickListener(view -> {
                if (rowTappedLister != null && getAdapterPosition() < placeCompletes.size()){
                    rowTappedLister.onRowTapped(placeCompletes.get(getAdapterPosition()));
                }
            });
        }
    }
}
