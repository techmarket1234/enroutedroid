package com.delivery.enroute.adapters;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.delivery.enroute.R;
import com.delivery.enroute.listeners.RowTappedLister;
import com.delivery.enroute.models.Vendor;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

public class VendorListAdapter extends RecyclerView.Adapter<VendorListAdapter.InnerViewHolder> {



    private ArrayList<Vendor> vendorArrayList;
    private  RowTappedLister<Vendor> vendorRowTappedLister;
    private ImageLoader imageLoader = ImageLoader.getInstance();

    public VendorListAdapter(ArrayList<Vendor> vendorArrayList) {
        this.vendorArrayList = vendorArrayList;
    }

    public void setVendorRowTappedLister(RowTappedLister<Vendor> vendorRowTappedLister) {
        this.vendorRowTappedLister = vendorRowTappedLister;
    }

    @NonNull
    @Override
    public InnerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_vender,viewGroup,false);
        return new InnerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InnerViewHolder holder, int position) {
        Vendor vendor = vendorArrayList.get(position);
        holder.vendorTitle.setText(vendor.vendorName);
        holder.vendorDescription.setText(vendor.vendorDescription);

        if(vendor.vendorLogo != null && !vendor.vendorLogo.equals(""))
            imageLoader.displayImage(vendor.vendorLogo, holder.vendorLogo);

    }

    @Override
    public int getItemCount() {
        return vendorArrayList.size();
    }

    class InnerViewHolder extends RecyclerView.ViewHolder{

        ImageView vendorLogo;
        TextView vendorTitle, vendorDescription;
        View layout;

        public InnerViewHolder(@NonNull View itemView) {
            super(itemView);

            vendorLogo = itemView.findViewById(R.id.logo);
            vendorDescription = itemView.findViewById(R.id.vendor_description);
            vendorTitle = itemView.findViewById(R.id.vendor_title);
            layout = itemView.findViewById(R.id.layout);
            layout.setOnClickListener(view -> {
                if (vendorRowTappedLister != null){
                    vendorRowTappedLister.onRowTapped(vendorArrayList.get(getAdapterPosition()));
                }
            });
        }
    }
}
