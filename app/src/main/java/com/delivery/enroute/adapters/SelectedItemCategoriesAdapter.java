package com.delivery.enroute.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.delivery.enroute.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class SelectedItemCategoriesAdapter extends RecyclerView.Adapter<SelectedItemCategoriesAdapter.InnerViewHolder> {

    private ArrayList<String> items;

    public SelectedItemCategoriesAdapter(ArrayList<String> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public InnerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_simple_text_custom,viewGroup,false);
        return new InnerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InnerViewHolder holder, int position) {
        String item = items.get(position);

        if (item != null)
            holder.item.setText(item);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class InnerViewHolder extends RecyclerView.ViewHolder{

        TextView item;

        public InnerViewHolder(@NonNull View itemView) {
            super(itemView);

            item = itemView.findViewById(R.id.text1);
        }
    }
}
