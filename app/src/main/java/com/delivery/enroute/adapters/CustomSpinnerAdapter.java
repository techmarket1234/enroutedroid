package com.delivery.enroute.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.delivery.enroute.R;
import com.delivery.enroute.models.Item;

import java.util.List;

/**
 * Created by learnzone.info on 29/03/17.
 */
public class CustomSpinnerAdapter extends BaseAdapter {
    SharedPreferences prefs;
    private List<Item> rows;
    public Resources res;
    private Context context;
    private LayoutInflater inflater;

    public CustomSpinnerAdapter(Context context, List<Item> rows) {
        this.rows = rows;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    //customizing the dropdown view of spinner
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View row = inflater.inflate(R.layout.item_category, parent, false);
        TextView label = row.findViewById(R.id.text_spinner);
        ImageView icon = row.findViewById(R.id.icon_spinner);
        icon.setImageResource(rows.get(position).getIcon());
        String text = rows.get(position).getItemName();
        label.setText(text);
        label.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        return row;
    }

    @Override
    public int getCount() {
        return rows.size();
    }

    @Override
    public Item getItem(int i) {
        return rows.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    //customizing the selected view of spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        @SuppressLint("ViewHolder") View row = inflater.inflate(R.layout.item_category, parent, false);
        TextView label = row.findViewById(R.id.text_spinner);
        Item item = getItem(position);
        label.setText(item.getItemName());

      /*  String item_sel=label.getItemName().toString();

        SharedPreferences.Editor editor2 = prefs.edit();
        editor2.putString("the_item_picked", item_sel); //InputString: from the response from JSon

        editor2.commit();*/

        return row;
    }

}