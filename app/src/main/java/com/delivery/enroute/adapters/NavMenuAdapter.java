package com.delivery.enroute.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.delivery.enroute.MainActivity;
import com.delivery.enroute.R;
import com.delivery.enroute.listeners.RowTappedLister;
import com.delivery.enroute.models.Item;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.TutorialManager;

import java.util.ArrayList;

import me.toptas.fancyshowcase.FancyShowCaseView;

public class NavMenuAdapter extends  RecyclerView.Adapter<NavMenuAdapter.InnerViewHolder> {

    private ArrayList<Item> items = Item.getMenuItems();
    private RowTappedLister<Item> itemRowTappedLister;
    private Context context;


    public void setItemRowTappedLister(RowTappedLister<Item> itemRowTappedLister) {
        this.itemRowTappedLister = itemRowTappedLister;
    }

    @NonNull
    @Override
    public InnerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_nav_menu_item,parent,false);
        context = parent.getContext();
        //TutorialManager.getInstance().clearAllTutorials(context);
        return new InnerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InnerViewHolder holder, int position) {
        Item item = items.get(position);

        holder.icon.setImageDrawable(context.getResources().getDrawable(item.icon));
        holder.itemName.setText(item.itemName);

       holder.itemRowLayout.setOnClickListener(view1 -> {
            if (itemRowTappedLister != null){
                itemRowTappedLister.onRowTapped(item);
            }
        });


      Log.d(Constants.TAG, "onBindViewHolder: holder.itemRowLayout id => " + holder.itemRowLayout.getId());


       if (item.changeIconColor && position == 6){
           holder.itemName.setTextColor(context.getResources().getColor(item.color));
           holder.icon.setColorFilter(ContextCompat.getColor(context, item.color));
       }



    }


    @Override
    public int getItemCount() {
        return items.size();
    }



    static class InnerViewHolder extends RecyclerView.ViewHolder{

        ImageView icon;
        TextView itemName;
        View itemRowLayout;

        InnerViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
            itemName = itemView.findViewById(R.id.item_name);
            itemRowLayout = itemView.findViewById(R.id.item_row_layout);

            //Item item = items.get(getAdapterPosition());

        }
    }
}
