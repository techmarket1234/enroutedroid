package com.delivery.enroute.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.delivery.enroute.R;
import com.delivery.enroute.layoutdialogs.ProductEnlargeFragment;
import com.delivery.enroute.listeners.RowTappedLister;
import com.delivery.enroute.models.Product;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

public class ProductsAdapter  extends RecyclerView.Adapter<ProductsAdapter.InnerViewHolder> {

    private ArrayList<Product> productArrayList;
    private RowTappedLister<Product> rowTappedLister;
    private ImageLoader imageLoader = ImageLoader.getInstance();
    Context context;

    public ProductsAdapter(ArrayList<Product> productArrayList) {
        this.productArrayList = productArrayList;
    }

    public void setRowTappedLister(RowTappedLister<Product> rowTappedLister) {
        this.rowTappedLister = rowTappedLister;
    }

    @NonNull
    @Override
    public InnerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_product,viewGroup,false);
        context = viewGroup.getContext();
        return new InnerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InnerViewHolder holder, int position) {

        Product product = productArrayList.get(position);
        holder.title.setText(product.productName);
        holder.description.setText(product.productDescription);
        String formattedPrice = "GHS " + product.productPrice;
        holder.productPrice.setText(formattedPrice);

        if (product.productImage != null && !product.productImage.equals("")){

            imageLoader.loadImage(product.productImage, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    // Do whatever you want with Bitmap
                    holder.productImage.setImageBitmap(loadedImage);
                    holder.productImage.setBackground(context.getResources().getDrawable(android.R.color.transparent));
                    holder.productImage.setOnClickListener(view1 -> {
                        ProductEnlargeFragment.newInstance(product.productName,"GHS "+  product.productPrice, loadedImage).show(((AppCompatActivity)context).getSupportFragmentManager(), "ZOOM_PRODUCT");
                    });
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return productArrayList.size();
    }

    class InnerViewHolder extends RecyclerView.ViewHolder{

        TextView title, description, productPrice;
        ImageView productImage;
        View layout;

        public InnerViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            description = itemView.findViewById(R.id.description);
            productPrice = itemView.findViewById(R.id.product_price);

            productImage = itemView.findViewById(R.id.product_image);
            layout = itemView.findViewById(R.id.layout);
            layout.setOnClickListener(view -> {
                if(rowTappedLister != null){
                    rowTappedLister.onRowTapped(productArrayList.get(getAdapterPosition()));
                }
            });
        }
    }
}
