package com.delivery.enroute.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.delivery.enroute.R;
import com.delivery.enroute.listeners.RowTappedLister;
import com.delivery.enroute.models.PaymentItem;

import java.util.ArrayList;

public class PaymentOptionsAdapter extends RecyclerView.Adapter<PaymentOptionsAdapter.InnerViewHolder> {

    private ArrayList<PaymentItem> paymentItemArrayList = PaymentItem.getNonCashItems();
    private Context context;
    private RowTappedLister<PaymentItem> rowTappedLister;
    private boolean showLabel;

    public PaymentOptionsAdapter(RowTappedLister<PaymentItem> rowTappedLister) {
        this.rowTappedLister = rowTappedLister;
    }

    public void setShowLabel(boolean showLabel) {
        this.showLabel = showLabel;
    }

    @NonNull
    @Override
    public InnerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_pay_option_linear,viewGroup,false);
        context = viewGroup.getContext();
        return new InnerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InnerViewHolder holder, int position) {
        PaymentItem paymentItem = paymentItemArrayList.get(position);
        holder.item.setText(paymentItem.itemName);
        holder.icon.setImageDrawable(context.getResources().getDrawable(paymentItem.icon));
//        if (!showLabel){
////            holder.item.setVisibility(View.GONE);
////        }else {
////            holder.item.setVisibility(View.VISIBLE);
////        }
    }

    @Override
    public int getItemCount() {
        return paymentItemArrayList.size();
    }

    class InnerViewHolder extends RecyclerView.ViewHolder{
        ImageView icon;
        TextView item;
        View layout;
        public InnerViewHolder(@NonNull View itemView) {
            super(itemView);

            icon = itemView.findViewById(R.id.icon);
            item = itemView.findViewById(R.id.item);

            layout = itemView.findViewById(R.id.layout);
            layout.setOnClickListener(view -> {
                if (rowTappedLister != null)
                    rowTappedLister.onRowTapped(paymentItemArrayList.get(getAdapterPosition()));
            });
        }
    }
}
