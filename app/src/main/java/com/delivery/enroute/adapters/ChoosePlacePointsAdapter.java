package com.delivery.enroute.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.delivery.enroute.R;
import com.delivery.enroute.listeners.RowTappedLister;
import com.delivery.enroute.models.PlaceComplete;
import com.delivery.enroute.utils.Constants;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ChoosePlacePointsAdapter extends RecyclerView.Adapter<ChoosePlacePointsAdapter.InnerViewHolder> {

    private ArrayList<PlaceComplete> placeCompletes;
    private Context context;
    private RowTappedLister<PlaceComplete> rowMoreOptionTapped, rowRemoveItemTapped, rowTapped;
    private boolean changeDescriptionColor;
    private int changeColorAtIndex;


    public ChoosePlacePointsAdapter(ArrayList<PlaceComplete> placeCompletes) {
        this.placeCompletes = placeCompletes;
    }

    public void setRowMoreOptionTapped(RowTappedLister<PlaceComplete> rowMoreOptionTapped) {
        this.rowMoreOptionTapped = rowMoreOptionTapped;
    }

    public void setRowRemoveItemTapped(RowTappedLister<PlaceComplete> rowRemoveItemTapped) {
        this.rowRemoveItemTapped = rowRemoveItemTapped;
    }

    public void setRowTapped(RowTappedLister<PlaceComplete> rowTapped) {
        this.rowTapped = rowTapped;
    }

    public void changeDesciptionColor(boolean changeDescriptionColor, int atIndex){
        this.changeDescriptionColor = changeDescriptionColor;
        changeColorAtIndex = atIndex;
    }



    @NonNull
    @Override
    public InnerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // row_place
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chosen_place,parent,false);
        context = parent.getContext();
        return new InnerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InnerViewHolder holder, int position) {
        PlaceComplete placeComplete = placeCompletes.get(position);
        placeComplete.index = position;
        holder.name.setText(placeComplete.name);
        holder.description.setText(placeComplete.description);


        holder.leftIcon.setImageDrawable(context.getResources().getDrawable(placeComplete.leftIcon));

        if (placeComplete.showMoreBtn){
            holder.moreOptions.setVisibility(View.VISIBLE);
        }

        holder.removeBtn.setVisibility(View.INVISIBLE);

        if (changeDescriptionColor && changeColorAtIndex == position){
            holder.description.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        }

    }

    @Override
    public int getItemCount() {
        return placeCompletes.size();
    }

    class InnerViewHolder extends RecyclerView.ViewHolder implements SwipeLayout.SwipeListener {

        TextView name, description;
        ImageView removeBtn, leftIcon, moreOptions;
        SwipeLayout layout;

        InnerViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            description = itemView.findViewById(R.id.description);

            layout = itemView.findViewById(R.id.place_layout);
            layout.addSwipeListener(this);
//            layout.setOnClickListener(view -> {
//                if (rowTapped != null) rowTapped.onRowTapped(placeCompletes.get(getAdapterPosition()));
//            });

            leftIcon = itemView.findViewById(R.id.left_icon);
            removeBtn = itemView.findViewById(R.id.remove_btn);

            //removeBtn.setOnClickListener(removeBtnTapped);
            moreOptions = itemView.findViewById(R.id.more_options_btn);

            moreOptions.setOnClickListener(view -> rowMoreOptionTapped.onRowTapped(placeCompletes.get(getAdapterPosition())));


        }

//        private View.OnClickListener removeBtnTapped = view -> {
//
//
//        };

        @Override
        public void onStartOpen(SwipeLayout layout) {
            Log.d(Constants.TAG, "onStartOpen: ");
        }

        @Override
        public void onOpen(SwipeLayout layout) {
            Log.d(Constants.TAG, "onOpen: getAdapterPosition() " + getAdapterPosition());

            Log.d(Constants.TAG, "onOpen: placeCompletes.size() " + placeCompletes.size());

            if (rowRemoveItemTapped != null)
                rowRemoveItemTapped.onRowTapped(placeCompletes.get(getAdapterPosition()));

            layout.close(true);

        }

        @Override
        public void onStartClose(SwipeLayout layout) {
            Log.d(Constants.TAG, "onStartClose: ");
        }

        @Override
        public void onClose(SwipeLayout layout) {
            Log.d(Constants.TAG, "onClose: ");
        }

        @Override
        public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

        }

        @Override
        public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

        }
    }
}
