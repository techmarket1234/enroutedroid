package com.delivery.enroute.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.delivery.enroute.R;
import com.delivery.enroute.models.Pickup;

import java.util.ArrayList;

public class RecieptPickupAdapter extends RecyclerView.Adapter<RecieptPickupAdapter.InnerViewHolder> {

    private ArrayList<String> pickupArrayList;
    private Context context;

    public RecieptPickupAdapter(ArrayList<String> pickupArrayList) {
        this.pickupArrayList = pickupArrayList;
    }

    @NonNull
    @Override
    public InnerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_point,viewGroup,false);
        context = viewGroup.getContext();
        return new InnerViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull InnerViewHolder holder, int position) {
        String pickup = pickupArrayList.get(position);


        if (pickupArrayList.size() == 1)
            holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.pick_up_icon));
        else {
            switch (position){
                case 0:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.pickup_raw_1));
                    break;
                case 1:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.pickup_raw_2));
                    break;
                case 2:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.pickup_raw_3));
                    break;
                case 3:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.pickup_raw_4));
                    break;
                case 4:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.pickup_raw_5));
                    break;
                case 5:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.pickup_raw_6));
                    break;
                default:
                    holder.leftIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.pickup_raw_1));
                    break;

            }
        }

        holder.title.setText(pickup);

    }


    @Override
    public int getItemCount() {
        return pickupArrayList.size();
    }

    class InnerViewHolder extends RecyclerView.ViewHolder{

        TextView title;
        ImageView leftIcon;

        public InnerViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            leftIcon = itemView.findViewById(R.id.left_icon);
        }
    }
}
