package com.delivery.enroute.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.delivery.enroute.R;
import com.delivery.enroute.models.WalletTransaction;

import java.util.ArrayList;

public class WalletTransactionAdapter extends RecyclerView.Adapter<WalletTransactionAdapter.InnerViewHolder> {

    private Context context;
    private ArrayList<WalletTransaction> walletTransactions;

    public WalletTransactionAdapter(ArrayList<WalletTransaction> walletTransactions) {
        this.walletTransactions = walletTransactions;
    }

    @NonNull
    @Override
    public InnerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_wallet_trans,viewGroup,false);
        context = viewGroup.getContext();
        return new InnerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InnerViewHolder holder, int position) {
        WalletTransaction walletTransaction = walletTransactions.get(position);

        String formattedDate = String.valueOf(android.text.format.DateFormat.format("yyyy-MM-dd hh:mm a", walletTransaction.createdAt));
        holder.dateCreated.setText(formattedDate);

        holder.amount.setText(context.getResources().getString(R.string.ghs_amount, String.valueOf(walletTransaction.amount)));
        String desc = walletTransaction.narration + " made by " + walletTransaction.accountNumber;
        holder.description.setText(desc);
    }

    @Override
    public int getItemCount() {
        return walletTransactions.size();
    }

    class InnerViewHolder extends RecyclerView.ViewHolder{

      TextView dateCreated, amount, description;

    InnerViewHolder(@NonNull View itemView) {
        super(itemView);

        dateCreated = itemView.findViewById(R.id.date_created);
        amount = itemView.findViewById(R.id.amount);
        description = itemView.findViewById(R.id.description);

    }
}
}
