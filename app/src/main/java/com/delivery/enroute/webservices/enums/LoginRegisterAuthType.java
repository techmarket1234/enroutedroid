package com.delivery.enroute.webservices.enums;

import java.io.Serializable;
import java.security.cert.CollectionCertStoreParameters;

public enum LoginRegisterAuthType {
    PHONE_LOGIN, PHONE_REGISTRATION, GOOGLE_LOGIN,GOOGLE_REGISTRATION,FACEBOOK_REGISTRATION, FACEBOOK_LOGIN;
}
