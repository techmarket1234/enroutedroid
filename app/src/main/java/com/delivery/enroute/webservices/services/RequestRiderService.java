package com.delivery.enroute.webservices.services;

import android.app.IntentService;
import android.content.Intent;

import com.delivery.enroute.models.Trip;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.webservices.enums.Collections;
import com.delivery.enroute.webservices.trips.RequestBuilder;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Timer;
import java.util.TimerTask;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class RequestRiderService extends IntentService {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    public RequestRiderService() {
        super("RequestRiderService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        final Trip request = RequestBuilder.request;

        if (request == null) return;

        // push initial request ............
       final DocumentReference docRef = db.collection(Collections.REQUESTS.toString()).document();
        docRef.set(request);

        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                // increase the radius after every waiting time
//                request.radius = request.radius + Constants.STEP_REQUEST_RADIUS;
//                docRef.update("radius",request.radius);

            }
        }, 0, Constants.REQUEST_WAITING_TIME);//put here time 1000 milliseconds=1 second 600000
    }

}
