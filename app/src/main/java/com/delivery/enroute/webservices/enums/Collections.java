package com.delivery.enroute.webservices.enums;

public enum Collections {
    USERS, REQUESTS, TRANSACTIONLEDGER, COUPONS;
}
