package com.delivery.enroute.webservices.trips;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.webservices.callbacks.WebServiceCallbacks;
import com.delivery.enroute.webservices.enums.Collections;
import com.delivery.enroute.webservices.errors.WebServiceErrors;
import com.delivery.enroute.webservices.services.RequestRiderService;

import com.google.firebase.auth.FirebaseAuth;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

public class RequestRider {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private WebServiceCallbacks callbacks;
    private SharedPreferences sp;
    private RequestRider requestRider;
    private RequestBuilder builder;


    private RequestRider(RequestBuilder builder){
        this.builder = builder;
    }

    public static RequestRider newInstance(RequestBuilder builder){
        return new RequestRider(builder);
    }

    /*
    * Send request to rider ...
    * */

    public RequestRider sendRequest(Context context){
        if (builder == null) {
            if (callbacks != null)
                callbacks.OnFail(new WebServiceErrors(402, "Build a request"));
            return this;
        }

        // push requests to riders that are 10 m away, and increase by every 10 meters

        Intent intent = new Intent(context, RequestRiderService.class);
        context.startService(intent);

        //
        return this;


    }


}
