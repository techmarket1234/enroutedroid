package com.delivery.enroute.webservices.services;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.delivery.enroute.R;
import com.delivery.enroute.activities.ChatActivity;
import com.delivery.enroute.models.Chat;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.NotificationHelper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class ChatService extends IntentService {

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef;
    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    String riderName;
    String riderId;
    String riderPhone;

    public ChatService() {
        super("ChatService");
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onHandleIntent(Intent intent) {

        riderId = intent.getStringExtra(Constants.RIDER_ID);
        riderName = intent.getStringExtra(Constants.RIDER_NAME);
//        String riderName = intent.getStringExtra(Constants.RIDER_NAME);
        riderPhone = intent.getStringExtra(Constants.RIDER_PHONE);

        if (riderId == null) return;

        myRef = database.getReference("messages/"+ firebaseUser.getUid() + riderId);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                Chat lastChat = new Chat();
                lastChat.createdTimestamp = 0L;
                DatabaseReference lastChidNode = null;
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    Chat chat = postSnapshot.getValue(Chat.class);

                    if (chat == null) continue;

                    if (chat.getCreatedTimestampLong() > lastChat.getCreatedTimestampLong()){
                        lastChidNode = postSnapshot.getRef();
                        lastChat = chat;
                    }
                    Log.e(Constants.TAG, new Gson().toJson(chat));
                }

                if ("rider".equals(lastChat.sentBy) && !lastChat.seen){

                    Intent resultIntent = new Intent(ChatService.this ,  ChatActivity.class);
                    resultIntent.putExtra(Constants.RIDER_ID, lastChat.riderId);
                    resultIntent.putExtra(Constants.RIDER_NAME, lastChat.riderName != null ? lastChat.riderName : riderName);
                    resultIntent.putExtra(Constants.RIDER_PHONE, lastChat.riderPhone != null ? lastChat.riderPhone : riderPhone);

                    NotificationHelper.newInstance(ChatService.this).createNotification(riderName, lastChat.messageText, resultIntent);

                    if (lastChidNode != null){
                        lastChat.seen = true;
                        lastChidNode.setValue(lastChat);
                    }

                    //sendBroadCast(lastChat);

                    Log.d(Constants.TAG, "onDataChange: ");
                }
////
////                // comment this from here
//                sendNotification(lastChat);
//                sendBroadCast(lastChat);

//                chats.remove(lastChat);
//                chats.add(lastChat);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(Constants.TAG, "onCancelled: " + databaseError);
            }
        });

    }

    private void sendBroadCast(Chat chat){
        Intent localIntent =
                new Intent(Constants.BROADCAST_ACTION_CHAT)
                        // Puts the status into the Intent
                        .putExtra(Constants.CHAT, chat);

        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);

    }

}
