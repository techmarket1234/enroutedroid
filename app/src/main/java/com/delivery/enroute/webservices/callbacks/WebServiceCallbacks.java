package com.delivery.enroute.webservices.callbacks;


import com.delivery.enroute.webservices.errors.WebServiceErrors;

public interface  WebServiceCallbacks {
    <T> void OnSuccess(T result);
     void OnFail(WebServiceErrors reason);
}


