package com.delivery.enroute.webservices.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.JobIntentService;

import com.delivery.enroute.activities.TrackTripActivity;
import com.delivery.enroute.enums.Collections;
import com.delivery.enroute.models.Destination;
import com.delivery.enroute.models.Pickup;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.NotificationHelper;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class OnGoingTripIntentService extends IntentService {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    Trip trip;

    public OnGoingTripIntentService() { super("OnGoingTripIntentService"); }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

       if (intent != null){
           trip = (Trip) intent.getSerializableExtra(Constants.TRIP);

           monitorPickupPoints();
           // real time listening
           monitorDestinationPoints();
       }
    }

    //    @Override
//    protected void onHandleWork(@NonNull Intent intent) {
//       trip = (Trip) intent.getSerializableExtra(Constants.TRIP);
//
//        monitorPickupPoints();
//        // real time listening
//        monitorDestinationPoints();
//
//    }

    private void monitorDestinationPoints() {

        // real time listening to the server
        db.collection(Collections.DESTINATIONS.toString())
                .whereEqualTo("requestId", trip.requestId)
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) return;

                    if (queryDocumentSnapshots == null) return;

                    List<Destination> destinationList =  queryDocumentSnapshots.toObjects(Destination.class);

                    if(destinationList.size() == 1){
                        // single destination
                        Destination destination = destinationList.get(0);
                        if(destination.isArrived && !destination.isArrivedNotified){

                            String message = "Rider " + trip.riderPlateNumber + " has arrived at " + destination.place;

                            Intent resultIntent = new Intent(this , TrackTripActivity.class);
                            resultIntent.putExtra(Constants.TRIP, trip.requestId);

                            if (!"COMPLETED".equals(trip.status))
                            NotificationHelper.newInstance(this).createNotification("Arrived", message, resultIntent);

                            destination.isArrivedNotified = true;
                            db.collection(Collections.PICKUPPOINTS.toString()).document(destination.requestId).update("isArrivedNotified",destination.isArrivedNotified);

                        }else if(destination.isDelivered && !destination.isDeliveredNotified){

                            String message = "Rider " + trip.riderPlateNumber + " has delivered your item at " + destination.place;

                            Intent resultIntent = new Intent(this , TrackTripActivity.class);
                            resultIntent.putExtra(Constants.TRIP, trip.requestId);

                            if (!"COMPLETED".equals(trip.status))
                            NotificationHelper.newInstance(this).createNotification("Delivered", message, resultIntent);

                            destination.isDeliveredNotified = true;
                            db.collection(Collections.PICKUPPOINTS.toString()).document(destination.requestId).update("isDeliveredNotified",destination.isDeliveredNotified);

                        }

                    }else{

                        int countDestination = 0;
                        Destination lastDestinationPoint =  null;

                        for(Destination destination : destinationList){
                            if (destination.isDelivered){
                                countDestination = countDestination + 1;

                            }else if(destination.isArrived){
                                lastDestinationPoint = destination;
                            }
                        }

                        if (lastDestinationPoint != null && !lastDestinationPoint.isArrivedNotified){
                            Intent resultIntent = new Intent(this , TrackTripActivity.class);
                            resultIntent.putExtra(Constants.TRIP, trip.requestId);

                            String message = "Your rider has arrived at " + lastDestinationPoint.place;
                            Log.d(Constants.TAG, "monitorDestinationPoints: notified " +  message);

                            if (!"COMPLETED".equals(trip.status))
                            NotificationHelper.newInstance(this).createNotification("RIDER HAS ARRIVED", message, resultIntent);

                            lastDestinationPoint.isArrivedNotified = true;
                            db.collection(Collections.PICKUPPOINTS.toString()).document(lastDestinationPoint.requestId).update("isArrivedNotified",lastDestinationPoint.isArrivedNotified);


                        }
                        else if(countDestination > 0){

                            String message = countDestination + " item(s) have been delivered";

                            Intent resultIntent = new Intent(this , TrackTripActivity.class);
                            resultIntent.putExtra(Constants.TRIP, trip.requestId);

                            Log.d(Constants.TAG, "monitorDestinationPoints: notified " +  message);

                            if (!"COMPLETED".equals(trip.status))
                            NotificationHelper.newInstance(this).createNotification("Item Delivered", message, resultIntent);

                        }

                    }
        });

    }


    private void monitorPickupPoints(){

        db.collection(Collections.PICKUPPOINTS.toString()).whereEqualTo("requestId", trip.requestId)
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) return;

                    if (queryDocumentSnapshots == null) return;


                    List<Pickup> pickupList =  queryDocumentSnapshots.toObjects(Pickup.class);

                    if (pickupList.size() == 1){
                        // single pick up
                        Pickup pickup = pickupList.get(0);
                        if (pickup.isPickedup && !pickup.isPickedupNotified){

                            String message = " Your ";
                            message += (pickup.itemCategory != null && !"".equals(pickup.itemCategory)) ? pickup.itemCategory : " item ";
                            message += " has been picked up";

                            Intent resultIntent = new Intent(this , TrackTripActivity.class);
                            resultIntent.putExtra(Constants.TRIP, trip.requestId);

                            if (!"COMPLETED".equals(trip.status))
                            NotificationHelper.newInstance(this).createNotification("ITEM PICKED UP", message, resultIntent);

                            pickup.isPickedupNotified = true;
                            db.collection(Collections.PICKUPPOINTS.toString()).document(pickup.requestId).update("isPickedupNotified",pickup.isArrivedNotified);

                        }else if(pickup.isArrived && !pickup.isArrivedNotified){

                            Intent resultIntent = new Intent(this , TrackTripActivity.class);
                            resultIntent.putExtra(Constants.TRIP, trip.requestId);

                            String message = "Your rider  has arrived at " + pickup.place;

                            if (!"COMPLETED".equals(trip.status))
                             NotificationHelper.newInstance(this).createNotification("RIDER HAS ARRIVED", message, resultIntent);

                            pickup.isArrivedNotified = true;
                            db.collection(Collections.PICKUPPOINTS.toString()).document(pickup.requestId).update("isArrivedNotified",pickup.isArrivedNotified);

                        }

                    }else{

                        // multiple pick ups

                        int countPickups = 0;
                        Pickup lastPickupArrived =  null;
                        for(Pickup pickup : pickupList){

                            if (pickup.isPickedup){
                                countPickups = countPickups + 1;
                            }
                            else if (pickup.isArrived){
                                lastPickupArrived = pickup;
                            }

                        }


                        if(lastPickupArrived != null && !lastPickupArrived.isArrivedNotified){

                            Intent resultIntent = new Intent(this , TrackTripActivity.class);
                            resultIntent.putExtra(Constants.TRIP, trip.requestId);


                            String message = "Your rider  has arrived at " + lastPickupArrived.place;
                            Log.d(Constants.TAG, "monitorPickupPoints: notified " +  message);

                           if (!"COMPLETED".equals(trip.status)){
                               NotificationHelper.newInstance(this).createNotification("ITEM PICKED UP", message, resultIntent);
                           }

                            lastPickupArrived.isArrivedNotified = true;
                            db.collection(Collections.PICKUPPOINTS.toString()).document(lastPickupArrived.requestId).update("isArrivedNotified",lastPickupArrived.isArrivedNotified);




                        }
                        else if (countPickups > 0){

                            String message = countPickups + " item(s) have been picked up";

                            Intent resultIntent = new Intent(this , TrackTripActivity.class);
                            resultIntent.putExtra(Constants.TRIP, trip.requestId);

                            Log.d(Constants.TAG, "monitorPickupPoints: notified " +  message);

                            if (!"COMPLETED".equals(trip.status)){
                                NotificationHelper.newInstance(this).createNotification("RIDER HAS ARRIVED", message, resultIntent);
                            }

                        }

                    }



                });
    }
}
