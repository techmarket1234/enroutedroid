package com.delivery.enroute.webservices.errors;

public class WebServiceErrors {
    public int statusCode;
    public String message;

    public WebServiceErrors(int statusCode, String message){
        this.statusCode = statusCode;
        this.message = message;
    }

}
