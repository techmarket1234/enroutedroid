package com.delivery.enroute.webservices;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;


import com.delivery.enroute.models.User;
import com.delivery.enroute.singletons.ApplicaitonUser;
import com.delivery.enroute.webservices.callbacks.WebServiceCallbacks;
import com.delivery.enroute.webservices.enums.AuthTypes;
import com.delivery.enroute.webservices.enums.Collections;
import com.delivery.enroute.webservices.enums.LoginRegisterAuthType;
import com.delivery.enroute.webservices.errors.WebServiceErrors;
import com.delivery.enroute.utils.Constants;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;

import static com.delivery.enroute.utils.Constants.RC_SIGN_IN;
import static com.delivery.enroute.webservices.enums.LoginRegisterAuthType.PHONE_REGISTRATION;


public class Authenticate {


    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private Context context;
    private WebServiceCallbacks callbacks;
    private SharedPreferences sp;
    private CallbackManager mCallbackManager;

    private Authenticate(){}

    /**
     * Your class should implement webServiceCallbacks before calling newInstance
     * @param context of the class calling it
     * @return instance of the Authenticate class
     */
    public static Authenticate newInstance(Context context) {
        Authenticate authenticate = new Authenticate();
        authenticate.context = context;
        authenticate.sp = context.getSharedPreferences(Constants.APP,Context.MODE_PRIVATE);
        FacebookSdk.sdkInitialize(context);
        authenticate.mCallbackManager = CallbackManager.Factory.create();
        return  authenticate;
    }

    /////////////////////////// REGISTRATIONS /////////////////////////////////

    /**
     * register user with email and password
     * @param email of the user
     * @param password of the user
     * @return instance of the Authenticate class, so that setCallback() can be called on it
     */
    public Authenticate registerWithEmailAndPassword(final String name, final String email,final String password){

        // register user in authentication table ..............

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener((Activity) context, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {

                    Log.d(Constants.TAG, "createUserWithEmail:success");
                    FirebaseUser user = mAuth.getCurrentUser();

                    if (user == null)return;

                    setUserDisplayName(user, name);

                    User appUser = new User();
                    appUser.id = user.getUid();
                    appUser.fullName = name;
                    appUser.email = email;
                    appUser.authType = AuthTypes.EMAIL_PASSWORD.toString();

                    // create user in the users collection of the fireStore db

                    createUser(appUser);

                } else {
                    // If sign in fails, display a message to the user.

                    if (callbacks != null){
                        if ((task.getException() != null)){
                            Log.d(Constants.TAG, "createUserWithEmail:failure", task.getException());
                            callbacks.OnFail( new WebServiceErrors(500, task.getException().getMessage()) );
                        }

                    }

                }

                // ...
            }
        });

        return this;

    }


    /***
     * onSuccess callback returns
     * 100 - meaning code sent to user, pending verification,
     * User - meaning webservice has automatically verified the code and user is logged in
     *
     * onFail callback returns
     * 400 - invalid phone number,
     * 500 - SMS quota exceeded (SMS finished),
     * 550 - sign up failed,
     *
     *
     *
     * @param phoneNumber begin with +233 , this is phone number of the user
     *
     * @return an instance of Authenticate class so that the setCallback() can be called
     */
    public Authenticate registerWithPhoneNumber(String name, String phoneNumber){
        // save name in a preference file for use if user is authenticated
        sp.edit().putString(Constants.USER_DISPLAY_NAME, name).apply();
        sp.edit().putString(Constants.PHONE_AUT_TYPE, PHONE_REGISTRATION.toString()).apply();
        sp.edit().putString(Constants.PHONE_AUTH_PHONE_NUMBER, phoneNumber).apply();
        sendSMSVerificationCode(phoneNumber);
        return this;
    }

    public Authenticate resendPhoneVerificationCode(){
        String phoneNumber = sp.getString(Constants.PHONE_AUTH_PHONE_NUMBER, null);
        if (phoneNumber == null) return this;
        sendSMSVerificationCode(phoneNumber);
        return this;
    }


    /***
     *
     *  * - onSuccess callback returns
     *      * 100 - meaning code sent to user, pending verification,
     *      * User - meaning webservice has automatically verified the code and user is logged in,
     *
     * - onFail callback returns,
     * 402 - verification not sent,
     * 550 - sign up failed,
     * 400 - verification code entered is invalid,
     * @param code
     * @return
     */
    public Authenticate verifySMSCode(String code){
        if (sp.getString(Constants.VERIFICATION_ID, null) == null && callbacks != null){
            callbacks.OnFail(new WebServiceErrors(402, "Verification code not sent yet!"));
            return this;
        }

        String verificationId = sp.getString(Constants.VERIFICATION_ID, "");

        if (verificationId == null) return this;

        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
        return this;
    }


    /**
     * After this method call finalizeAuthWithGoogle() in onActivityForResult
     * @return
     */
    public Authenticate registerWithGoogle(String username, String phone){
        sp.edit().putString(Constants.GOOGLE_AUTH_TYPE,LoginRegisterAuthType.GOOGLE_REGISTRATION.toString())
                .putString(Constants.USER_DISPLAY_NAME, username)
                .putString(Constants.PHONE_AUTH_PHONE_NUMBER, phone)
                .apply();

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(Constants.default_web_client_id)
                .requestEmail()
                .build();

        // [START build_client]
        // Build a GoogleSignInClient with the options specified by gso.
        GoogleSignInClient  mGoogleSignInClient = GoogleSignIn.getClient(context, gso);
        // [END build_client]


        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        ((Activity)context).startActivityForResult(signInIntent, RC_SIGN_IN);

        return this;

    }

    public Authenticate registerWithFacebook(String username, String phone){
        sp.edit().putString(Constants.FACE_BOOK_AUTH_TYPE,LoginRegisterAuthType.FACEBOOK_REGISTRATION.toString())
                .putString(Constants.PHONE_AUTH_PHONE_NUMBER, phone)
                .putString(Constants.USER_DISPLAY_NAME, username).apply();


        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d(Constants.TAG, "Login");
                        handleFacebookAccessToken(loginResult.getAccessToken());

                    }

                    @Override
                    public void onCancel() {
                        Log.d(Constants.TAG, "facebook:onCancel");
                        if (callbacks != null)
                            callbacks.OnFail(new WebServiceErrors(501, "Cancel"));
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d(Constants.TAG, "facebook:onError" + exception.getMessage());
                        if (callbacks != null)
                            callbacks.OnFail(new WebServiceErrors(505, exception.getMessage()));
                    }
                });

        LoginManager.getInstance().logInWithReadPermissions((Activity) context, Arrays.asList("public_profile", "user_friends"));


        return this;

//
//        CallbackManager mCallbackManager = CallbackManager.Factory.create();
//        loginButton.setReadPermissions("email", "public_profile");
//        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                Log.d(Constants.TAG, "facebook:onSuccess:" + loginResult);
//
//            }
//
//            @Override
//            public void onCancel() {
//
//                // ...
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//
//                // ...
//            }
//        });
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(Constants.TAG, "handleFacebookAccessToken:" + token.getToken());

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener((Activity) context, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(Constants.TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            if (user == null) return;

                            String googleAuthType = sp.getString(Constants.FACE_BOOK_AUTH_TYPE, null);
                            Log.d(Constants.TAG, "onComplete: " + googleAuthType);
                            switch (LoginRegisterAuthType.valueOf(googleAuthType)) {

                                case FACEBOOK_REGISTRATION:


                                    User appUser = new User();
                                    appUser.id = user.getUid();
                                    appUser.fullName = user.getDisplayName();
                                    appUser.email = user.getEmail();
                                    appUser.phoneNumber = sp.getString(Constants.PHONE_AUTH_PHONE_NUMBER, "");
                                    appUser.authType = AuthTypes.GOOGLE.toString();

                                    String preferredName = sp.getString(Constants.USER_DISPLAY_NAME, null);
                                    if (preferredName != null)
                                        setUserDisplayName(user, preferredName);

                                    // create user
                                    createUser(appUser);

                                    break;

                                case FACEBOOK_LOGIN:
                                    // retrieve the the user's credentials
                                    getUser(user.getUid());
                                    break;
                            }

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(Constants.TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(context, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                           if (callbacks != null){
                               callbacks.OnFail(new WebServiceErrors(500, "" +task.getException()));
                           }
                        }


                    }
                });
    }

    /**
     * call this method in onActivityResult
     * @param requestCode - request code from the activity result,
     * @param data - this is the Intent passed by the on activity result,
     * @return instance of Authenticate, call setCallbacks on it, for authentication result
     * onSuccess returns instance of the user
     * onFaile returns 500 statusCode with corresponding message
     */
    public Authenticate finalizeAuthWithGoogle(int requestCode, Intent data){

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                if (account == null) return this;
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(Constants.TAG, "Google sign in failed", e);
                // ...
                if(callbacks != null)
                    callbacks.OnFail(new WebServiceErrors(500, "Google sign in failed"));
            }
        }

        return this;

    }



    ////////// END OF REGISTRATIONS ///////////////////////////////////////////


    ///////////// LOGIN BEGINS /////////////////////////////////////////////

    /**
     * After this method call finalizeAuthWithGoogle() in onActivityForResult
     * @return
     */
    public Authenticate loginWithGoogle(){
        sp.edit().putString(Constants.GOOGLE_AUTH_TYPE,LoginRegisterAuthType.GOOGLE_LOGIN.toString()).apply();
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(Constants.default_web_client_id)
                .requestEmail()
                .build();

        // [START build_client]
        // Build a GoogleSignInClient with the options specified by gso.
        GoogleSignInClient  mGoogleSignInClient = GoogleSignIn.getClient(context, gso);
        // [END build_client]


        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        ((Activity)context).startActivityForResult(signInIntent, RC_SIGN_IN);

        return this;


    }

    public Authenticate loginWithFacebook(){
        sp.edit().putString(Constants.FACE_BOOK_AUTH_TYPE,LoginRegisterAuthType.FACEBOOK_LOGIN.toString()).apply();


        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d("Success", "Login");
                        handleFacebookAccessToken(loginResult.getAccessToken());

                    }

                    @Override
                    public void onCancel() {
                        Log.d(Constants.TAG, "facebook:onCancel");
                        if (callbacks != null)
                            callbacks.OnFail(new WebServiceErrors(501, "Cancel"));
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d(Constants.TAG, "facebook:onError" + exception.getMessage());
                        if (callbacks != null)
                            callbacks.OnFail(new WebServiceErrors(505, exception.getMessage()));
                    }
                });

        LoginManager.getInstance().logInWithReadPermissions((Activity) context, Arrays.asList("public_profile", "user_friends"));


        return this;

    }


    /**
     *
     * @param phoneNumber of the user logging in
     * @return
     * onFail callback returns
     * 400 - invalid phone number,
     * 500 - SMS quota exceeded (SMS finished),
     * 550 - sign in failed,
     */

    public Authenticate loginWithPhoneNumber(String phoneNumber){
        sp.edit().putString(Constants.PHONE_AUT_TYPE, LoginRegisterAuthType.PHONE_LOGIN.toString()).apply();
        sendSMSVerificationCode(phoneNumber);
        return this;
    }

    /**
     *
     * @param email
     * @param password
     * @return an instance of Authenticate class so that the setCallback() can be called
     * onSuccess callback returns an instance of the User model
     * onFail call back returns statusCod = 500 , and corresponding reason
     */

    public Authenticate loginWithEmailAndPassword(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener((Activity) context, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(Constants.TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user == null) return;


                            String uid = user.getUid();

                            // Fetch the user's record from the database
                            getUser(uid);


                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(Constants.TAG, "signInWithEmail:failure", task.getException());
                            if (task.getException() == null) return;
                            if (callbacks != null){
                                callbacks.OnFail(new WebServiceErrors(500, task.getException().getMessage()));
                            }
                        }

                        // ...
                    }
                });
        return  this;
    }


    /**
     *onSuccess Callback returns an instance of the user
     * onFail callback returns an instance of WebService errors with status code 500, and the corresponding message
     * @param uid is the userID of the person's info to fetch
     * @return an instance of Authenticate class so that the setCallback() can be called
     */
    private void getUser(final String uid){
        db.collection(Collections.USERS.toString()).document(uid).get().addOnCompleteListener(task -> {
            if (!task.isSuccessful()){
                if (task.getException() == null) return;
                if (callbacks == null) return;

                callbacks.OnFail(new WebServiceErrors(500, task.getException().getMessage()));
                return;

            }

            if (task.getResult() == null)
                return;

            User user = task.getResult().toObject(User.class);

            if (user == null){
                if (callbacks == null) return;
                callbacks.OnFail(new WebServiceErrors(501, "You haven't registered with us. Kindly register"));
                return;
            }

            ApplicaitonUser applicaitonUser = ApplicaitonUser.getInstance(context);
            applicaitonUser.setAppUser(context,user);

            db.collection(Collections.USERS.toString()).document(uid).update("lastLogin",FieldValue.serverTimestamp());

            if (callbacks != null)
            callbacks.OnSuccess(user);
        });

    }


    /**
     * create an new user record in the db
     * @param user is the user to create
     * onFail callback returns an instance of WebService errors with status code 500, and the corresponding message
     * onSuccess Callback returns  an instance of the user
     * @return an instance of Authenticate class so that the setCallback() can be called
     */
    private Authenticate createUser(final User user){

        // check if user is already registered, sign user in instead
        db.collection(Collections.USERS.toString()).whereEqualTo("id", user.id).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (!task.isSuccessful()){
                    if (callbacks != null)
                        callbacks.OnFail(new WebServiceErrors(700,"Check connection and try again"));
                    return;
                }

                QuerySnapshot snapshots = task.getResult();
                if (snapshots == null){
                    if (callbacks != null)
                        callbacks.OnFail(new WebServiceErrors(700,"Check connection and try again"));
                    return;
                }

                if (!snapshots.isEmpty()){
                   // if user already exists
                    getUser(user.id);
                    return;
                }


                user.dateAdded = FieldValue.serverTimestamp();
                user.lastLogin = FieldValue.serverTimestamp();

                ApplicaitonUser applicaitonUser = ApplicaitonUser.getInstance(context);
                applicaitonUser.setAppUser(context,user);

                db.collection(Collections.USERS.toString()).document(user.id).set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful() && task.getException() != null && callbacks != null)
                            callbacks.OnFail(new WebServiceErrors(500,task.getException().getMessage()));

                        if (callbacks != null)
                            callbacks.OnSuccess(user); // true mean its successful
                    }
                });
            }
        });



        return this;
    }


    /**
     * onSuccess callback returns
     * 100 - meaning code sent to user, pending verification
     * User - meaning webservice has automatically verified the code and user is logged in
     *
     * onFail callback returns
     * 400 - invalid phone number
     * 500 - SMS quota exceeded (SMS finished)
     *
     * @param phone begin with +233 , this is phone number of the user
     *
     * @return an instance of Authenticate class so that the setCallback() can be called
     */
    private Authenticate sendSMSVerificationCode(String phone){
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phone,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                (Activity) context,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks

        return this;
    }



    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential credential) {
            // This callback will be invoked in two situations:
            // 1 - Instant verification. In some cases the phone number can be instantly
            //     verified without needing to send or enter a verification code.
            // 2 - Auto-retrieval. On some devices Google Play services can automatically
            //     detect the incoming verification SMS and perform verification without
            //     user action.
            Log.d(Constants.TAG, "onVerificationCompleted:" + credential);

            // this mean the phone number has been automatically verified

            signInWithPhoneAuthCredential(credential);
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            // This callback is invoked in an invalid request for verification is made,
            // for instance if the the phone number format is not valid.
            Log.w(Constants.TAG, "onVerificationFailed", e);

            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                // Invalid request
                if (callbacks != null)
                    callbacks.OnFail(new WebServiceErrors(400, e.getMessage()));
                // ...
            } else if (e instanceof FirebaseTooManyRequestsException) {
                // The SMS quota for the project has been exceeded
                // ...
                if (callbacks != null)
                    callbacks.OnFail(new WebServiceErrors(500, e.getMessage()));
            }

            if (callbacks != null)
            callbacks.OnFail(new WebServiceErrors(505, e.getMessage()));

            // Show a message and update the UI
            // ...
        }

        @Override
        public void onCodeSent(@NonNull String verificationId,
                               @NonNull PhoneAuthProvider.ForceResendingToken token) {
            // The SMS verification code has been sent to the provided phone number, we
            // now need to ask the user to enter the code and then construct a credential
            // by combining the code with a verification ID.
            Log.d(Constants.TAG, "onCodeSent:" + verificationId);

            // Save verification ID and resending token so we can use them later

            SharedPreferences.Editor editor = sp.edit();
            editor.putString(Constants.VERIFICATION_ID, verificationId);
            editor.apply();

            callbacks.OnSuccess(100); // 100 - code sent and pending verification

            // ...
        }
    };


    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener((Activity) context, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(Constants.TAG, "signInWithCredential:success");

                            if (task.getResult() == null)return;

                            FirebaseUser user = task.getResult().getUser();
                            if (user == null) return;

                            String phoneAuthType = sp.getString(Constants.PHONE_AUT_TYPE, null);
                            Log.d(Constants.TAG, "login onComplete: " + phoneAuthType);
                            switch (LoginRegisterAuthType.valueOf(phoneAuthType)){
                                case PHONE_REGISTRATION:
                                    // create account with the person name and phone number
                                    String userDisplayName = sp.getString(Constants.USER_DISPLAY_NAME,"");
                                    setUserDisplayName(user, userDisplayName);


                                    User appUser = new User();
                                    appUser.id = user.getUid();
                                    appUser.fullName = userDisplayName;
                                    appUser.phoneNumber = sp.getString(Constants.PHONE_AUTH_PHONE_NUMBER, "");
                                    appUser.authType = AuthTypes.PHONE.toString();

                                    // create user
                                    createUser(appUser);

                                    break;
                                case PHONE_LOGIN:
                                    // retrieve the the user's credentials
                                     getUser(user.getUid());
                                    break;
                            }


                            // ...
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(Constants.TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                if (callbacks != null)
                                callbacks.OnFail(new WebServiceErrors(400,task.getException().getMessage()));
                            }
                        }
                    }
                });
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct){
        Log.d(Constants.TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener((Activity) context, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(Constants.TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user == null) return;

                            String googleAuthType = sp.getString(Constants.GOOGLE_AUTH_TYPE, null);
                            switch (LoginRegisterAuthType.valueOf(googleAuthType)){

                                case GOOGLE_REGISTRATION:


                                    User appUser = new User();
                                    appUser.id = user.getUid();
                                    appUser.fullName = user.getDisplayName();
                                    appUser.email = user.getEmail();
                                    appUser.phoneNumber = sp.getString(Constants.PHONE_AUTH_PHONE_NUMBER, "");
                                    appUser.authType = AuthTypes.GOOGLE.toString();

                                    String preferredName = sp.getString(Constants.USER_DISPLAY_NAME, null);
                                    if (preferredName != null)
                                        setUserDisplayName(user, preferredName);

                                    // create user
                                    createUser(appUser);

                                    break;

                                case GOOGLE_LOGIN:
                                    // retrieve the the user's credentials
                                    getUser(user.getUid());
                                    break;
                            }

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(Constants.TAG, "signInWithCredential:failure", task.getException());

                        }

                        // ...
                    }
                });

    }

    /**
     * This sets the user display name on the firebase authentication table
     * @param user- refers to the firebase user after authentication
     * @param name - refers to the name you want to set
     */
    private void setUserDisplayName(final FirebaseUser user,final String name){
       new Thread(() -> {

           UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(name).build();
           user.updateProfile(profileUpdates);

       }).start();
    }




    /**
     * call this method to get response after executing server side code
     * OnSuccess(<T>result) return dynamic result and depends on the method which called it
     * @param callbacks thus an implementation of on success and unFailure
     * @return an instance of authenticate
     */
    public void setCallbacks(WebServiceCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    public void finalizeAuthWithFacebook(int requestCode, int resultCode, Intent data) {
        mCallbackManager.onActivityResult(requestCode,resultCode, data);
    }


//    login with email and password

//    public void re


}
