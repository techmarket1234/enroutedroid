package com.delivery.enroute.webservices.services;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.delivery.enroute.R;
import com.delivery.enroute.activities.TrackTripActivity;
import com.delivery.enroute.enums.Collections;
import com.delivery.enroute.enums.TripStatus;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.models.TripStatusNotified;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.NotificationHelper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Nullable;

import static com.delivery.enroute.utils.Constants.TAG;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class TripListenerIntentService extends JobIntentService {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

    List<String> requestList = new ArrayList<>();

    public TripListenerIntentService() {
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {

//        onGoingMediaPlayer = MediaPlayer.create(this, R.raw.default_sound);
//        completedMediaPlayer = MediaPlayer.create(this, R.raw.default_sound);
//        cancelledMediaPlayer = MediaPlayer.create(this, R.raw.short_notify);

        String requestId = intent.getStringExtra(Constants.TRIP);

        requestList.add(requestId);

        for(String tripId : requestList)
            db.collection(Collections.REQUEST.toString()).document(tripId)
                    .addSnapshotListener((documentSnapshot, e) -> {
                        if (e != null) return;

                        if (documentSnapshot == null) return;

                        Trip trip = documentSnapshot.toObject(Trip.class);

                        if (trip == null) return;


                        boolean notifyCustomer = true;
                        String title = "", content = "";

                        TripStatusNotified mTripStatusNotified = trip.tripStatusNotified;

                        switch (trip.status) {

                            case "PROCESSING":

                                if (trip.tripStatusNotified.PROCESSING) {
                                    notifyCustomer = false;
                                }else{
                                    title = "Looking For rider";
                                    content = getResources().getString(R.string.app_name) + " is looking for the closest rider for you";
                                    mTripStatusNotified.PROCESSING = true;
                                }

                                break;
                            case "PENDING":
                                if (trip.tripStatusNotified.PENDING) {
                                    notifyCustomer = false;
                                }else{
                                    title = "Looking For rider";
                                    content = getResources().getString(R.string.app_name) + " is looking for the closest rider for you";
                                    mTripStatusNotified.PENDING = true;
                                }
                                break;
                            case "ONGOING":
                                if (trip.tripStatusNotified.ONGOING) {
                                    notifyCustomer = false;
                                }else{
                                    title = "Rider Found";
                                    content = trip.riderName + " (" + trip.riderPlateNumber + ") has accepted your trip";
                                    mTripStatusNotified.ONGOING = true;
                                }
                                break;
                            case "NOT_FOUND":
                                if (trip.tripStatusNotified.NOT_FOUND) {
                                    notifyCustomer = false;
                                }else{
                                    title = "Finding rider";
                                    content = "Please be patient. a delivery partner will contact you shortly";
                                    mTripStatusNotified.NOT_FOUND = true;
                                }

                                break;
                            case "DELAYED":
                                if (trip.tripStatusNotified.DELAYED) {
                                    notifyCustomer = false;
                                }else{
                                    title = "Delayed";
                                    content = "please be patient. a delivery partner will contact you shortly";
                                    mTripStatusNotified.DELAYED = true;
                                }
                                break;
                            case "CANCELLED":
                                if (trip.tripStatusNotified.CANCELLED) {
                                    notifyCustomer = false;
                                }else{
                                    title = "Cancelled";
                                    content = "Your trip was cancelled. We're sorry for any inconvenience";
                                    mTripStatusNotified.CANCELLED = true;
                                }

                                break;
                            case "CANCELLED_R":
                                if (trip.tripStatusNotified.CANCELLED_R) {
                                    notifyCustomer = false;
                                }else{
                                    title = "Cancelled";
                                    content = "Sorry!, your trip was cancelled.";
                                    mTripStatusNotified.CANCELLED_R = true;
                                }
                                break;
                            case "COMPLETED":
                                if (trip.tripStatusNotified.COMPLETED) {
                                    notifyCustomer = false;
                                }else{
                                    title = "Trip Completed";
                                    content = "Delivery completed. Kindly pay " + trip.riderName + " GHS " + trip.actualCost;
                                    mTripStatusNotified.COMPLETED = true;
                                }
                                break;
                        }

                        if (notifyCustomer) {
                            if (!trip.status.equals("PENDING")) {
                                Intent resultIntent = new Intent(this, TrackTripActivity.class);
                                resultIntent.putExtra(Constants.TRIP, trip.requestId);
                                NotificationHelper.newInstance(this).createNotification(title, content, resultIntent);
                            }

                            db.collection(Collections.REQUEST.toString()).document(tripId).update("tripStatusNotified", mTripStatusNotified);

                        }

                    });

    }



}
