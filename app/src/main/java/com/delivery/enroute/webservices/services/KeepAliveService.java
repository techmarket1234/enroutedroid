package com.delivery.enroute.webservices.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.delivery.enroute.MainActivity;
import com.delivery.enroute.activities.TrackTripActivity;
import com.delivery.enroute.singletons.SockIOConnection;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.LoadingGif;

import org.json.JSONObject;

import io.socket.client.Ack;
import io.socket.client.Socket;
import spencerstudios.com.bungeelib.Bungee;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class KeepAliveService extends IntentService {

    private Socket mSocket;

    public KeepAliveService() {
        super("KeepAliveService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        mSocket = SockIOConnection.getSocketInstance();

        if (mSocket == null) return;

        mSocket.on("connection::check::isConnected", args1 -> {
            // JSONObject obj2 = (JSONObject) args[0];

            Log.d(Constants.TAG, "onHandleIntent: connection::check::isConnected ");

            JSONObject obj = new JSONObject();
            mSocket.emit("connection::check::isConnected::received", obj, (Ack) args2 -> {
                JSONObject objRet = (JSONObject)args2[0];
                Log.d(Constants.TAG, "connection::check::isConnected::received" + objRet);
            });

        });



    }

}
