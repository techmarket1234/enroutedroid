package com.delivery.enroute;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.delivery.enroute.enums.Collections;
import com.delivery.enroute.models.Destination;
import com.delivery.enroute.models.Pickup;
import com.delivery.enroute.models.Pos;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.utils.Constants;


import com.delivery.enroute.utils.NotificationHelper;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Map;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;

public class TestActivity extends AppCompatActivity {

    EditText input;

    ArrayList<Pickup> pickupArrayList = new ArrayList<>();
    ArrayList<Destination> destinationsArrayList = new ArrayList<>();

    private View.OnClickListener requestRider = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            // Build Trip

            // Create a new user with a first and last name




            ///  add references to pickups and destination // do this at the point where the user selects pick ups for a destination

//            pickup1.destinations.add(destination2.documentReference);
//            pickup2.destinations.add(destination2.documentReference);
//            pickup3.destinations.add(destination1.documentReference);
//
//            destination1.pickups.add(pickup3.documentReference);
//            destination2.pickups.add(pickup1.documentReference);
//            destination2.pickups.add(pickup2.documentReference);

            //db.collection("DRIVERS").document("ridersDocumentId").update("currentLocation", riderCurrentLocation);

//            End of Destination


            //                ################ Pickup points #############

//                    Trip #######################################

            DocumentReference requestRef = db.collection(Collections.REQUEST.toString()).document();
            Trip request = new Trip();
            request.estimatedCost = (int) 10.0;
            request.isAccepted = false;
            request.requestId = requestRef.getId();

            for (Pickup pp: pickupArrayList) {
                request.pickups.add(pp.documentReference);
                pp.documentReference.set(pp);
            }

            for (Destination dd : destinationsArrayList){
                request.destinations.add(dd.documentReference);
                dd.documentReference.set(dd);
            }

            if (FirebaseAuth.getInstance().getCurrentUser() != null){
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                request.senderId = user.getUid();
                request.senderName = user.getDisplayName();
            }

            request.senderPhoneNumber = "+233541243508"; // pick this from the request form
            request.status = "PENDING";

            requestRef.set(request).addOnSuccessListener(runnable -> {
                Toast.makeText(TestActivity.this, "request successful", Toast.LENGTH_SHORT).show();

                Log.d(Constants.TAG, "call: about to call server - " + requestRef.getId());

                JSONObject obj = new JSONObject();
                try {
                    obj.put("requestId", requestRef.getId());

                    mSocket.emit("request::created", obj, (Ack) args -> {
                        JSONObject objRet = (JSONObject)args[0];
                        Log.d(Constants.TAG, "call: server received it - " + objRet);
                    });

                    mSocket.on("request::not-exists", args -> {
                        JSONObject obj2 = (JSONObject) args[0];
                        Log.d(Constants.TAG, "request::not-exists - " + obj2);

                    });

                    // resend request reference
                    mSocket.emit("request::created", obj);

                    //runOnUiThread(() -> Toast.makeText(TestActivity.this, "" + obj, Toast.LENGTH_SHORT).show());


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            });

        }
    };



    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("https://realtime.elcentino.com");
        } catch (URISyntaxException e) {
            Log.d(Constants.TAG, "instance initializer: URISyntaxException" + e.getMessage());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        //findViewById(R.id.request).setOnClickListener(requestRider);
        //finish();
       // startActivity(ne);
        findViewById(R.id.button).setOnClickListener(view -> {
           //new NotificationHelper(this).createNotification("Enrout", "Finally, don't give up");
        });
//
       // startService(new Intent(this, TestIntentService.class));
//
//        ImageLoader imageLoader = ImageLoader.getInstance();
//        imageLoader.loadImage(trip.riderImage, new SimpleImageLoadingListener() {
//            @Override
//            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                // Do whatever you want with Bitmap
//                if (loadedImage != null){
//                    riderImage.setImageBitmap(loadedImage);
//                }
//            }
//        });

    }



    private void sendNotification( boolean sound, boolean vibrate) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Notification notification = new Notification();

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);

        if (sound) {
            notification.defaults |= Notification.DEFAULT_SOUND;

        }
        if (vibrate) {
            notification.defaults |= Notification.DEFAULT_VIBRATE;
        }

        notificationBuilder.setDefaults(notification.defaults);
        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Enrout")
                .setContentText("You Have A Request")
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);



        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

        //  notification.flags = Notification.FLAG_INSISTENT;

//        playSound(getApplicationContext());
//        mp.setLooping(true);


    }


    FirebaseFirestore db = FirebaseFirestore.getInstance();


    // this is the method that invokes the function gets the number of trips completed

    private void thisMethodWantsToKnowIfLoopIsDone(){

        countTheNumberOfTripsCompleted(new SetOnDataReturnedListener() {
            @Override
            public void onDataReturned(int numberOfItems) {

                // number of trips completed = numberOfItems


            }
        });

    }


    // put the code that access the server in a for loop into a method
    public void countTheNumberOfTripsCompleted(final SetOnDataReturnedListener listener){

            // whiles you're looping

            db.collection("yourCollection").whereEqualTo("yourField", "COMPLETED").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {

                    /// all your code after the server has returned data here

                    // so example, we want to count the number of trips completed
                    QuerySnapshot queryDocumentSnapshots =  task.getResult();
                    if (queryDocumentSnapshots == null) return;  // this code is just to guard against null pointers

                    int numberOfTripsCompleted = queryDocumentSnapshots.size();
                    listener.onDataReturned(numberOfTripsCompleted); // set the number of trips completed to the listener

                }
            });


    }


    // create an interface that will get notified when the server returns data
    // you can pass the data you're expecting to receive when loop is done, into the method

    public interface SetOnDataReturnedListener {
        void onDataReturned(int numberOfItems);
    }



    private View.OnClickListener test = view -> {

        db.collection("REQUEST").document("PzYG0k6esdlmhMXAFuqg").addSnapshotListener((documentSnapshot, e) -> {
            if (documentSnapshot == null) return;

            Map<String,Object> data = documentSnapshot.getData();

            Log.d(Constants.TAG, ": data : " + data);

        });
    };

}
