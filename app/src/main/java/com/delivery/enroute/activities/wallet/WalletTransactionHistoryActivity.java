package com.delivery.enroute.activities.wallet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.delivery.enroute.R;
import com.delivery.enroute.adapters.WalletTransactionAdapter;
import com.delivery.enroute.models.WalletTransaction;
import com.delivery.enroute.singletons.ApplicaitonUser;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.webservices.enums.Collections;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.Source;

import java.util.ArrayList;
import java.util.List;

import spencerstudios.com.bungeelib.Bungee;

public class WalletTransactionHistoryActivity extends AppCompatActivity {

    ProgressBar loadTransProgressBar;
    ArrayList<WalletTransaction> walletTransactionArrayList = new ArrayList<>();
    WalletTransactionAdapter walletTransactionAdapter = new WalletTransactionAdapter(walletTransactionArrayList);
    View noTransDecorator;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    ApplicaitonUser applicaitonUser;
    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_transaction_history);

        loadTransProgressBar = findViewById(R.id.loadTransProgressBar);

        applicaitonUser =ApplicaitonUser.getInstance(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
//            replace the home (back) button with the menu icon
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
//            set the Home / Back Button
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

        noTransDecorator = findViewById(R.id.empty_trans);
        noTransDecorator.setVisibility(View.GONE);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(walletTransactionAdapter);

        loadTransactions();

    }

    private void loadTransactions(){
        new Thread(() -> {
            if (firebaseUser == null) return;
            db.collection(Collections.TRANSACTIONLEDGER.toString())
                    .whereEqualTo("senderId", firebaseUser.getUid())
                    .whereEqualTo("type","DEPOSIT")
                    .whereEqualTo("transType","CREDIT")
                    .orderBy("createdAt", Query.Direction.DESCENDING)
                    .get().addOnCompleteListener(task -> {
               if (!task.isSuccessful()){

                   if(task.getException() != null)
                   Log.d(Constants.TAG, "loadTransactions: " + task.getException().getMessage());

                   indicateNoTransFound();
                   return;
               }

                if (task.getResult() == null){
                    indicateNoTransFound();
                    return;
                }


                if (task.getResult().isEmpty()){
                    indicateNoTransFound();
                    return;
                }

               List<WalletTransaction> walletTransactions = task.getResult().toObjects(WalletTransaction.class);

               walletTransactionArrayList.addAll(walletTransactions);
               indicateTransFound();
               walletTransactionAdapter.notifyDataSetChanged();



            });
        }).start();
    }

    private void indicateNoTransFound(){
        loadTransProgressBar.setVisibility(View.GONE);
        noTransDecorator.setVisibility(View.VISIBLE);
    }

    private void indicateTransFound(){
        loadTransProgressBar.setVisibility(View.GONE);
        noTransDecorator.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                super.onBackPressed();
                Bungee.slideRight(this);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);
    }
}
