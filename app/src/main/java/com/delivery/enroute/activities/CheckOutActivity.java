package com.delivery.enroute.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.delivery.enroute.MainActivity;
import com.delivery.enroute.R;
import com.delivery.enroute.activities.wallet.PayWithMomoActivity;
import com.delivery.enroute.adapters.CheckOutListAdapter;
import com.delivery.enroute.adapters.PaymentMethodsAdapter;
import com.delivery.enroute.enums.Collections;
import com.delivery.enroute.enums.PaymentTypes;
import com.delivery.enroute.enums.TripTypeCode;
import com.delivery.enroute.layoutdialogs.PromoCodeDialog;
import com.delivery.enroute.listeners.OnUserBalanceFeteched;
import com.delivery.enroute.listeners.UnPaidTripListener;
import com.delivery.enroute.models.CheckOut;
import com.delivery.enroute.models.Coupon;
import com.delivery.enroute.models.CurrentLocation;
import com.delivery.enroute.models.Destination;
import com.delivery.enroute.models.PaymentItem;
import com.delivery.enroute.models.Pickup;
import com.delivery.enroute.models.Pos;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.models.TripStatusNotified;
import com.delivery.enroute.models.User;
import com.delivery.enroute.singletons.ApplicaitonUser;
import com.delivery.enroute.singletons.SockIOConnection;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.Dialogs;
import com.delivery.enroute.utils.LoadingGif;
import com.delivery.enroute.utils.MSlideUp;
import com.delivery.enroute.utils.NotificationHelper;
import com.delivery.enroute.utils.TutorialManager;
import com.delivery.enroute.utils.volley.RequestObject;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.mancj.slideup.SlideUp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.socket.client.Ack;
import io.socket.client.Socket;
import me.toptas.fancyshowcase.FancyShowCaseView;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import spencerstudios.com.bungeelib.Bungee;

public class CheckOutActivity extends AppCompatActivity {

    Trip trip;
    TextView estimatedTimeTv, estimatedCostTv, suggestedWalletFee, cashLabel, surge_message;
    RecyclerView selected_pick_up_points, selected_destination_points;
    SlideUp selectPaymentMethodLayout;


    private static final int RSS_JOB_ID_ONGOING = 1009;


    View cash_view;
    View checkout_layout;

    MaterialProgressBar materialProgressBar, loadFee;
    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

    ImageView cashIcon;

    private EditText pFM_Fee;


    ArrayList<CheckOut> pickPtsCheckOutArrayList = new ArrayList<>();
    ArrayList<CheckOut> destinationsCheckOutArrayList = new ArrayList<>();

    // adapters to render list on checkout page
    CheckOutListAdapter pickPtCheckOutListAdapter = new CheckOutListAdapter(pickPtsCheckOutArrayList);
    CheckOutListAdapter destCheckOutListAdapter = new CheckOutListAdapter(destinationsCheckOutArrayList);

    // pick up and destination array list
    ArrayList<Pickup> pickupArrayList = new ArrayList<>();
    ArrayList<Destination> destinationArrayList = new ArrayList<>();

    boolean isMultiplePickUps, isMultipleDestinations;

    private double walletFee = 0.00d;

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    private ApplicaitonUser applicaitonUser;

    FusedLocationProviderClient fusedLocationClient;

    Socket mSocket;

    Button dispatchBtn;
    
    boolean computePriceAgain;

    ArrayList<LatLng> points;
    ArrayList<ForServer> pickUpForServer = new ArrayList<>();
    ArrayList<ForServer> destinationForServer = new ArrayList<>();

    double pfmValue;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);

        applicaitonUser = ApplicaitonUser.getInstance(this);

        new Thread(() -> {
            try{
                mSocket = SockIOConnection.getSocketInstance();
            }catch (Exception e){
                Log.d(Constants.TAG, "onResume: Exception " + e.getMessage());
                Constants.callErrorPage(this, "onResume: Exception " + e.getMessage());
            }
        }).start();

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        /*
        *   Initial required params
        * */

        trip = (Trip) getIntent().getSerializableExtra(Constants.TRIP);

        boolean isTripInTemporaryHolder = getIntent().getBooleanExtra(Constants.tripInTemporaryHolder, false);
        if (isTripInTemporaryHolder){
            trip = Constants.TEMPORAL_TRIP_HOLDER;
        }

        trip.couponApplied = false;
        trip.coupon = null;
        trip.paymentMethod = "CASH";
        trip.useBox = false;

        trip.customerType = "ANDROID";

        trip.riderName = null;
        trip.riderImage = null;
        trip.riderId = null;
        trip.riderPlateNumber = null;
        trip.riderPhoneNumber = null;
        trip.isAccepted = false;
        trip.isArrived = false;

        trip.couponName = null;
        trip.couponValueInPercentage = 0;
        trip.couponPreviousPrice = 0;

        Constants.TEMP_COUPON = null;

        ArrayList<CheckOut> mPickPtsCheckOutArrayList =   (ArrayList<CheckOut>)  getIntent().getSerializableExtra(Constants.CHECKOUT_PICK_UPS);
        ArrayList<CheckOut> mDestinationsCheckOutArrayList =   (ArrayList<CheckOut>)  getIntent().getSerializableExtra(Constants.CHECKOUT_DESTINATIONS);

        pickPtsCheckOutArrayList.addAll(mPickPtsCheckOutArrayList);
        destinationsCheckOutArrayList.addAll(mDestinationsCheckOutArrayList);

        Log.d(Constants.TAG, "onCreate: pickPtsCheckOutArrayList " + new Gson().toJson(pickPtsCheckOutArrayList));
        Log.d(Constants.TAG, "onCreate: destinationsCheckOutArrayList " + new Gson().toJson(destinationsCheckOutArrayList));

        isMultiplePickUps = getIntent().getBooleanExtra(Constants.IS_MULTIPLE_PICK_UPS, false);
        isMultipleDestinations = getIntent().getBooleanExtra(Constants.IS_MULTIPLE_DESTINATIONS, false);

        ArrayList<Pickup> mPickupArrayList = (ArrayList<Pickup>) getIntent().getSerializableExtra(Constants.PICK_UPS_ARRAY_LIST);
        ArrayList<Destination> mDestinationsArrayList = (ArrayList<Destination>) getIntent().getSerializableExtra(Constants.DESTINATIONS_ARRAY_LIST);

        pickupArrayList.addAll(mPickupArrayList);
        destinationArrayList.addAll(mDestinationsArrayList);

        if (pickupArrayList.size() < 1){
            NotificationHelper.newInstance(this).createNotification("Invalid pick ups","Choose at least one pick up", null);
            compeleteTrip();
            finish();
        }

        if (destinationArrayList.size() < 1){
            NotificationHelper.newInstance(this).createNotification("Invalid destinations","Choose at least one destination", null);
            compeleteTrip();
            finish();
        }

        if (pickupArrayList.size() != pickPtsCheckOutArrayList.size()){
            NotificationHelper.newInstance(this).createNotification("Invalid pick ups","Choose at least one pick up", null);
            compeleteTrip();
            finish();
        }

        if (destinationArrayList.size() !=  destinationsCheckOutArrayList.size()){
            NotificationHelper.newInstance(this).createNotification("Invalid destinations","Choose at least one destination", null);
            compeleteTrip();
            finish();
        }

        Log.d(Constants.TAG, "onCreate: pickupArrayList size = " + pickupArrayList.size());
        Log.d(Constants.TAG, "onCreate: destinationArrayList size = " + destinationArrayList.size());

        /*
            End of initial required prams
        * */

        if (trip == null){
            finish();
        }

        initLayouts();

//        final DateFormat dateFormat = new SimpleDateFormat("mm", Locale.getDefault());
//        String value = dateFormat.format(new Date(TimeUnit.SECONDS.toMillis((int)trip.estimatedTimeInSeconds)));
//        int timeInmin = Integer.parseInt(value);
//
//        estimatedTimeTv.setText(getString(R.string._n_m_mins, timeInmin, timeInmin + 1));

        estimatedTimeTv.setVisibility(View.GONE);

//      if (timeInmin > 60)
//          else
//      estimatedTimeTv.setText(getString(R.string._n_m_mins, trip.estimatedTimeInSeconds, t));

        setPoints();

       new Thread(this::calDistanceTime).start();


        Constants.checkOutstandingPayment(this, new UnPaidTripListener() {
            @Override
            public void unpaidTrip(Trip trip, boolean allowUserToRequestAnotherTrip) {

            }

            @Override
            public void notFound() {

            }
        });


        new Handler().postDelayed(() ->{
            boolean shouldShowTutorial = TutorialManager.getInstance().showTutorial(CheckOutActivity.this, pFM_Fee.getId());
            if (shouldShowTutorial){
                new FancyShowCaseView.Builder(this)
                        .focusOn(pFM_Fee)
                        .title("Specify amount here, if you want rider to purchase item for you")
                        .closeOnTouch(true)
                        .titleSize(20, 1)
                        .roundRectRadius(80)
                        .build()
                        .show();
            }

        },1000);

        // trip type codes
        if (isMultiplePickUps)
            trip.typeCode = TripTypeCode.MP.toString();
        if (isMultipleDestinations)
            trip.typeCode = TripTypeCode.MD.toString();

        trip.senderName = firebaseUser.getDisplayName();
        trip.senderPhoneNumber = firebaseUser.getPhoneNumber();

    }

    private void compeleteTrip(){
        startActivity(new Intent(this, MainActivity.class));
        Bungee.slideRight(this);
    }

    private void setPoints(){
        points = new ArrayList<>();
        pickUpForServer.clear();
        destinationForServer.clear();

        for (Pickup pickup : pickupArrayList) {
            points.add(new LatLng(pickup.pos.geopoint.latitude, pickup.pos.geopoint.longitude));
            pickUpForServer.add(new ForServer(pickup.pickUpName, pickup.pos.geopoint.latitude, pickup.pos.geopoint.longitude));
        }
        for (Destination destination : destinationArrayList) {
            points.add(new LatLng(destination.pos.geopoint.latitude, destination.pos.geopoint.longitude));
            destinationForServer.add(new ForServer(destination.place, destination.pos.geopoint.latitude, destination.pos.geopoint.longitude));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        suggestedWalletFee.setText("loading ...");
        Constants.getWalletBalance(this, user -> {
            suggestedWalletFee.setText(getString(R.string.ghs_amount, String.format(Locale.getDefault(),"%.2f", user.currentBalance)));
        });

    }


    // calculate total distance and time

    private void calDistanceTime(){

        Log.d(Constants.TAG, "drawRoute: called " + points);

        trip.estimatedTimeInSeconds = 0.0d;
        trip.estimatedDistanceInMeters = 0.0d;

        runOnUiThread(() -> {
            showHideProgressLoader(true);
            dispatchBtn.setEnabled(false);
        });

        if (points.size() <= 1) {
            runOnUiThread(() -> {
                showHideProgressLoader(false);
                dispatchBtn.setEnabled(true);
            });
            return;
        }

        if (points.size() == 2) {
            // single trip
            LatLng firstPt = points.get(0);
            LatLng lastPt = points.get(points.size() - 1);

            // get directions and time
            String serverKey = getResources().getString(R.string.the_google_server_key);
            String mode = "driving";
            String url = "https://maps.googleapis.com/maps/api/directions/json?key=" + serverKey + "&mode="+mode+"&origin="+firstPt.latitude+","+firstPt.longitude+"&destination="+lastPt.latitude+","+lastPt.longitude;

            Log.d(Constants.TAG, "url : " + url);
            new RequestObject(url, null, response -> {

                try {
                     String status = response.getString("status");
                     if (!"OK".equals(status)) {
                         runOnUiThread(() -> {
                             showHideProgressLoader(false);
                             showTryAgainAlert();
                             dispatchBtn.setEnabled(false);
                         });
                         return;
                     }

                     JSONArray routes = response.getJSONArray("routes");
                     JSONObject route = routes.getJSONObject(0);
                     JSONArray legs = route.getJSONArray("legs");
                     JSONObject leg = legs.getJSONObject(0);
                     JSONObject distanceObj = leg.getJSONObject("distance");
                     JSONObject durationObj = leg.getJSONObject("duration");

                     String durationText = durationObj.getString("text");
                     String distanceText = distanceObj.getString("text");

                    trip.estimatedDistanceInMeters = distanceObj.getDouble("value");
                    trip.estimatedTimeInSeconds = durationObj.getDouble("value");

                    trip.estimatedDistanceInText = distanceText;
                    trip.estimatedTimeInText = durationText;


                    Log.d(Constants.TAG, " checkout estimatedDistanceInMeters : " + trip.estimatedDistanceInMeters);
                    Log.d(Constants.TAG, " checkout estimatedTimeInSeconds : " + trip.estimatedTimeInSeconds);
                    computePrice();


                } catch (JSONException e) {
                    Log.d(Constants.TAG, "calDistanceTime: JSONException:  " + e.getMessage());
                    e.printStackTrace();
                }

            }, error -> {

                runOnUiThread(() -> {
                    showHideProgressLoader(false);
                    showTryAgainAlert();
                    dispatchBtn.setEnabled(false);
                });

            }).send(CheckOutActivity.this);

        }else{

            // multiple trip

            LatLng firstPt = points.get(0);
            LatLng lastPt = points.get(points.size() - 1);

            // get directions and time
            String serverKey = getResources().getString(R.string.the_google_server_key);
            String mode = "driving";
            StringBuilder url = new StringBuilder("https://maps.googleapis.com/maps/api/directions/json?key=" + serverKey + "&mode=" + mode + "&origin=" + firstPt.latitude + "," + firstPt.longitude + "&destination=" + lastPt.latitude + "," + lastPt.longitude);

            url.append("&waypoints=");
            for (int index = 0; index < points.size() ; index++) {
                LatLng point = points.get(index);
                if (index != 0 && index != (points.size() - 1)){
                    url.append("via:").append(point.latitude).append(",").append(point.longitude).append("|");
                }
            }

           String finalUrl = url.substring(0, url.length() - 1);

            Log.d(Constants.TAG, "final : " + finalUrl);

            new RequestObject(finalUrl, null, response -> {

                String status = null;
                try {
                    status = response.getString("status");
                    if (!"OK".equals(status)) {
                        Log.d(Constants.TAG, "status: NOT OK ");
                        runOnUiThread(() -> {
                            showHideProgressLoader(false);
                            showTryAgainAlert();
                            dispatchBtn.setEnabled(false);
                        });
                        return;
                    }

                    JSONArray routes = response.getJSONArray("routes");
                    JSONObject route = routes.getJSONObject(0);
                    JSONArray legs = route.getJSONArray("legs");
                    JSONObject leg = legs.getJSONObject(0);
                    JSONObject distanceObj = leg.getJSONObject("distance");
                    JSONObject durationObj = leg.getJSONObject("duration");

                    String durationText = durationObj.getString("text");
                    String distanceText = distanceObj.getString("text");

                    trip.estimatedDistanceInMeters = distanceObj.getDouble("value");
                    trip.estimatedTimeInSeconds = durationObj.getDouble("value");

                    trip.estimatedDistanceInText = distanceText;
                    trip.estimatedTimeInText = durationText;

                    Log.d(Constants.TAG, " checkout estimatedDistanceInMeters : " + trip.estimatedDistanceInMeters);
                    Log.d(Constants.TAG, " checkout estimatedTimeInSeconds : " + trip.estimatedTimeInSeconds);
                    computePrice();

                } catch (JSONException e) {
                    Log.d(Constants.TAG, "JSONException: " + e.getMessage());
                    e.printStackTrace();
                }



            }, error -> {
                Log.d(Constants.TAG, "Volley error : " + error);
                runOnUiThread(() -> {
                    showHideProgressLoader(false);
                    showTryAgainAlert();
                    dispatchBtn.setEnabled(false);
                });

            }).send(CheckOutActivity.this);



        }

    }

    private void showTryAgainAlert() {
        Dialogs.alertWithSweet(this, "Sorry!, There was a problem with connection. Please try again")
                .setConfirmClickListener(sweetAlertDialog -> {
                    sweetAlertDialog.dismiss();
                    new Thread(this::calDistanceTime).start();
                });
    }


    private void initLayouts() {

        materialProgressBar = findViewById(R.id.progress_bar);
        loadFee = findViewById(R.id.load_fee);
        cash_view = findViewById(R.id.cash_view);

        estimatedTimeTv = findViewById(R.id.estimated_time);
        estimatedCostTv = findViewById(R.id.estimated_cost_fee);
        suggestedWalletFee = findViewById(R.id.wallet_suggested_amount);

        estimatedCostTv.setText(getString(R.string.ghs_amount, String.format(Locale.getDefault(),"%.2f", (double)trip.estimatedCost)));

        selected_pick_up_points = findViewById(R.id.selected_pick_up_points);
        selected_pick_up_points.setLayoutManager(new LinearLayoutManager(this));
        selected_pick_up_points.setHasFixedSize(true);
        selected_pick_up_points.setAdapter(pickPtCheckOutListAdapter);

        selected_destination_points = findViewById(R.id.selected_destination_points);
        selected_destination_points.setLayoutManager(new LinearLayoutManager(this));
        selected_destination_points.setHasFixedSize(true);
        selected_destination_points.setAdapter(destCheckOutListAdapter);

        pickPtCheckOutListAdapter.notifyDataSetChanged();
        destCheckOutListAdapter.notifyDataSetChanged();

        findViewById(R.id.close_checkout_page).setOnClickListener(view -> {
            super.onBackPressed();
            Bungee.slideRight(this);
        });

        /*
         *   select payment method ===============================
         * */

        selectPaymentMethodLayout = MSlideUp.controlView(findViewById(R.id.select_payment_method));
        findViewById(R.id.close_payment_method).setOnClickListener(view -> {
            selectPaymentMethodLayout.hide();
        });
        GridView paymentGridView = findViewById(R.id.gridView_payment_methods);
        PaymentMethodsAdapter paymentMethodsAdapter = new PaymentMethodsAdapter(PaymentItem.getItems(), this);
        paymentGridView.setAdapter(paymentMethodsAdapter);
        paymentGridView.setOnItemClickListener((adapterView, view, i, l) -> {
            PaymentItem item = PaymentItem.getItems().get(i);

            if (item.itemName.equals(PaymentTypes.WALLET.toString())){
                estimatedCostTv.setText(getString(R.string.ghs_amount, String.format(Locale.getDefault(),"%.2f", walletFee)));
            }else{
                estimatedCostTv.setText(getString(R.string.ghs_amount, String.format(Locale.getDefault(),"%.2f", trip.estimatedCost)));
            }

            trip.paymentMethod = item.itemName;

            selectPaymentMethodLayout.hide();
            //itemRowTappedLister.onRowTapped(item);
            // dismiss();
            cashLabel.setText(item.itemName);
            cashIcon.setImageDrawable(getResources().getDrawable(item.icon));


        });

        /*
         *   end of select payment method ############################################
         * */


//        apply coupon ---------------------
        TextView applyCouponBtn = findViewById(R.id.apply_code_btn);

        CheckBox useBox = findViewById(R.id.useBox);
        useBox.setOnCheckedChangeListener((compoundButton, b) -> {
            trip.useBox = b;
        });

        applyCouponBtn.setOnClickListener(view -> {
            Log.d(Constants.TAG, "apply coupon tapped: ");
            //todo apply coupon
            PromoCodeDialog promoCodeDialog = new PromoCodeDialog();
            promoCodeDialog.setPromoCodeAppliedListener(coupon -> {
//                trip.coupon = db.collection(com.delivery.enroute.webservices.enums.Collections.COUPONS.toString()).document(coupon.docId);
//                trip.couponApplied = true;
//                trip.couponName = coupon.name;
//                trip.couponValueInPercentage = coupon.value;

                Toast.makeText(this, "Promo code applied", Toast.LENGTH_SHORT).show();
                displaySlashedPriceWithCoupon();

            });

            promoCodeDialog.show(getSupportFragmentManager(),"show promo dialog");
        });


        ImageButton reducePFM = findViewById(R.id.reduce_pfm);
        ImageButton increacePFm = findViewById(R.id.increase_pfm);

        pFM_Fee = findViewById(R.id.pfm_fee);

        if(trip.isPickFromVendor){
            pFM_Fee.setEnabled(false);
            pFM_Fee.setText(String.valueOf(trip.payForMe));

            increacePFm.setEnabled(false);
            reducePFM.setEnabled(false);
        }else{
            pFM_Fee.setEnabled(true);
            increacePFm.setEnabled(true);
            reducePFM.setEnabled(true);
            pFM_Fee.setText("0.0");
        }

        reducePFM.setOnClickListener(view -> {
            pfmValue =  Double.parseDouble(String.valueOf(pFM_Fee.getText()));
            pfmValue--;
            if (pfmValue < 0)return;
            pFM_Fee.setText(String.valueOf(pfmValue));
        });
        increacePFm.setOnClickListener(view -> {
            pfmValue =  Double.parseDouble(String.valueOf(pFM_Fee.getText()));
            pfmValue++;
            pFM_Fee.setText(String.valueOf(pfmValue));
            ///compareWalletWithPayForMe(pfmValue);
        });

        dispatchBtn = findViewById(R.id.dispatch_btn);
        dispatchBtn.setOnClickListener(view -> {
            // todo check if pay for me exceeds amount in wallet
             pfmValue =  Double.parseDouble(String.valueOf(pFM_Fee.getText()));
            if (pfmValue > 0){
                compareWalletWithPayForMe(pfmValue, balance -> {
                   ;
                    if  (((pfmValue * 0.03) + pfmValue) >= balance){
                    // don't allow
                        String pfmMessage = "WALLET balance not enough for 'Pay For Me'. Do you wish to top up ?";

                        if(trip.isPickFromVendor){
                            pfmMessage = "Kindly pay for the item before you proceed";
                        }
                        SweetAlertDialog sweetAlertDialog = Dialogs.alertWithSweet(CheckOutActivity.this, pfmMessage);
                        sweetAlertDialog.setConfirmText("Top up");
                        sweetAlertDialog.setConfirmClickListener(sweetAlertDialog1 -> {
                            sweetAlertDialog1.dismiss();

                            Intent intent = new Intent(this, PayWithMomoActivity.class);
                            intent.putExtra(Constants.PAYMENT_FIXED_AMOUNT,  pfmValue);
                            if (trip.isPickFromVendor){
                                intent.putExtra(Constants.SHOULD_DISABLE_AMOUNT_FIELD, true);
                            }
                            startActivity(intent);
                            Bungee.slideLeft(this);

                        });
                        if(!trip.isPickFromVendor){
                            sweetAlertDialog.setCancelText("Cancel");
                            sweetAlertDialog.setCancelClickListener(sweetAlertDialog1 -> {
                                super.onBackPressed();
                                Bungee.slideRight(this);
//                                sweetAlertDialog1.dismiss();
//                                pfmValue = 0;
//                                pFM_Fee.setText(String.valueOf(pfmValue));
//                                requestLocationPermission();
                            });
                        }

                    }else{
                    // allow
                        requestLocationPermission();
                    }
                });
            }else{
                requestLocationPermission();
            }



        });

        findViewById(R.id.more_option_cash_fee).setOnClickListener(view -> {

            selectPaymentMethodLayout.show();
        });

        cashLabel = findViewById(R.id.cash_label);
        cashIcon = findViewById(R.id.cash_icon);

        surge_message = findViewById(R.id.surge_message);
        surge_message.setVisibility(View.GONE);



//        popoverView = new PopoverView(this, R.layout.popover_showed_view);
//        popoverView.setContentSizeForViewInPopover(new Point(320, 200));

        checkout_layout = findViewById(R.id.checkout_layout);

//        PopupWindow popupWindow = new PopupWindow(this);
//        popupWindow.setBackgroundDrawable(new BorderDrawable(checkout_layout, popupWindow, Color.argb(0,0,0,0)));

        // after 6 info
        findViewById(R.id.after_6_info).setOnClickListener(view -> {
            Log.d(Constants.TAG, "initLayouts: after_6_info tapped ");
            Constants.showTutorial(this, estimatedCostTv, null, null);

            //popoverView.setDelegate(this);
          //  popoverView.showPopoverFromRectInViewGroup(checkout_layout, PopoverView.getFrameForView(view), PopoverView.PopoverArrowDirectionUp, true);

        });


    }

    private void compareWalletWithPayForMe(double pfmValue, OnUserBalanceFeteched onUserBalanceFeteched) {


        if (firebaseUser != null){
            LoadingGif.show(this);
            db.collection(Collections.USERS.toString()).document(firebaseUser.getUid()).get().addOnSuccessListener(documentSnapshot -> {
                LoadingGif.hide();
                if (documentSnapshot == null) return;
                User user = documentSnapshot.toObject(User.class);
                if (user == null) return;

                    if (applicaitonUser != null){
                        applicaitonUser.currentBalance = Constants.convertTo2dp(user.currentBalance);

                        onUserBalanceFeteched.balanceFetched(applicaitonUser.currentBalance);

                    }


            }).addOnFailureListener(e -> {
                LoadingGif.hide();
            });
        }
//        if (pfmValue > applicaitonUser.currentBalance){
//            Dialogs.alertWithSweet(CheckOutActivity.this, "Top up your wallet to enable pay for me " + applicaitonUser.currentBalance);
//            return false;
//        }
//        pFM_Fee.setText(String.valueOf(pfmValue));
//        return true;
    }

    private void showHideProgressLoader(boolean show){
        if (show){
            loadFee.setVisibility(View.VISIBLE);
            materialProgressBar.setVisibility(View.VISIBLE);
            cash_view.setVisibility(View.GONE);
        }else{
            loadFee.setVisibility(View.GONE);
            materialProgressBar.setVisibility(View.GONE);
            cash_view.setVisibility(View.VISIBLE);
        }

    }

    private void computePrice() {

        // is already in a thread

            runOnUiThread(() -> {

               // String timeString = String.format(Locale.getDefault(),"%02d:%02d:%02d", hours, minutes, seconds);

                estimatedTimeTv.setVisibility(View.VISIBLE);
                estimatedTimeTv.setText(getString(R.string._n_m_mins, trip.estimatedTimeInText));
                YoYo.with(Techniques.BounceInUp)
                        .duration(2000)
                        .repeat(1)
                        .playOn(estimatedTimeTv);
            });

            Gson mGson = new Gson();

            String url = Constants.BaseUrl + "/universal/calculate-distances";
            HashMap<String, String> params = new HashMap<>();
            params.put("pickups",mGson.toJson(pickUpForServer));
            params.put("destinations",mGson.toJson(destinationForServer));
            params.put("type",trip.typeCode);
            params.put("totalTime",String.valueOf( trip.estimatedTimeInSeconds / 60));
            params.put("totalDistance", String.valueOf(trip.estimatedDistanceInMeters / 1000));

            Log.d(Constants.TAG, "url " + url);
            Log.d(Constants.TAG, "params " + params);

            RequestObject requestObject = new RequestObject(Request.Method.POST, url, params, response -> {

                runOnUiThread(() -> {
                    showHideProgressLoader(false);
                    dispatchBtn.setEnabled(true);
                });

                Log.d(Constants.TAG, "response: " + response);

                try {

                    boolean status = response.getBoolean("status");
                    double fare = response.getDouble("fare");
                    boolean isSurgeCharge = response.getBoolean("isSurgeCharge");
                    String message = response.getString("message");

                    if (status){
                        trip.estimatedCost = fare;
                        
                        if (computePriceAgain){
                            Toast.makeText(this, "Please dispatch trip again", Toast.LENGTH_SHORT).show();
                        }

                        computePriceAgain = false;

                        // calculate the cost
                        //trip.estimatedCost = CostCalculator.getInstance(baseFare, costPerMin, costPerKm).calculate(trip, pickPtsCheckOutArrayList, destinationsCheckOutArrayList);
                        walletFee  = trip.estimatedCost - (trip.estimatedCost * 0.01);

                        if (trip.couponApplied)
                            displaySlashedPriceWithCoupon();
                        else
                            estimatedCostTv.setText(getString(R.string.ghs_amount, String.format(Locale.getDefault(),"%.2f", (double)trip.estimatedCost)));
                       // suggestedWalletFee.setText(getString(R.string.ghs_amount, String.format(Locale.getDefault(),"%.2f", walletFee)));

                        walletFee  = trip.estimatedCost - (trip.estimatedCost * 0.01);


                        if (trip.couponApplied)
                            displaySlashedPriceWithCoupon();
                        else
                            estimatedCostTv.setText(getString(R.string.ghs_amount, String.format(Locale.getDefault(),"%.2f", (double)trip.estimatedCost)));
                        //suggestedWalletFee.setText(getString(R.string.ghs_amount, String.format(Locale.getDefault(),"%.2f", walletFee)));

                        if (isSurgeCharge){
                            surge_message.setText(message);
                            surge_message.setVisibility(View.VISIBLE);
                        }else{
                            surge_message.setVisibility(View.GONE);
                        }


                        return;
                    }

                    runOnUiThread(() -> {
                        showHideProgressLoader(false);
                        showTryAgainAlert();
                        dispatchBtn.setEnabled(true);
                    });

                    Toast.makeText(this, "Check your connection and try again", Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    Log.d(Constants.TAG, "JSONException: " + e.getMessage());
                    e.printStackTrace();
                }


            }, error -> {

                runOnUiThread(() -> {
                    showHideProgressLoader(false);
                    showTryAgainAlert();
                    dispatchBtn.setEnabled(true);
                });

                Toast.makeText(this, "Check your connection and try again", Toast.LENGTH_SHORT).show();

                Log.d(Constants.TAG, "error: " + error);

            });

//            requestObject.setRetryPolicy(new DefaultRetryPolicy(
//                    0,
//                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestObject.send(this);


//        db.collection("SETTINGS").get().addOnSuccessListener(queryDocumentSnapshots -> {
//            showHideProgressLoader(false);
//            dispatchBtn.setEnabled(true);
//
//            // load ends here
//            List<Setting> settingList =  queryDocumentSnapshots.toObjects(Setting.class);
//            Setting setting = settingList.get(0);
//
//            baseFare = setting.baseFare;
//            costPerKm = setting.costPerKm;
//            costPerMin = setting.costPerMin;
//
//            trip.scheduleId = setting.scheduleId;
//
//            // calculate the cost
//            trip.estimatedCost = CostCalculator.getInstance(baseFare, costPerMin, costPerKm).calculate(trip, pickPtsCheckOutArrayList, destinationsCheckOutArrayList);
//            walletFee  = trip.estimatedCost - (trip.estimatedCost * 0.01);
//
//            Log.d(Constants.TAG, "renderAndShowCheckout: distance - " + trip.estimatedDistanceInMeters);
//
//            if (trip.couponApplied)
//                displaySlashedPriceWithCoupon();
//            else
//                estimatedCostTv.setText(getString(R.string.ghs_amount, String.format(Locale.getDefault(),"%.2f", (double)trip.estimatedCost)));
//            suggestedWalletFee.setText(getString(R.string.ghs_amount, String.format(Locale.getDefault(),"%.2f", walletFee)));
//
//
//        }).addOnFailureListener(e -> {
//            // load ends here
//            showHideProgressLoader(false);
//            Dialogs.alertWithSweet(CheckOutActivity.this, e.getMessage());
//        });


    }




    // display slashed new cost after applying coupon code
    private void displaySlashedPriceWithCoupon() {
        double newCostToDisplay = trip.estimatedCost - (trip.estimatedCost * Constants.TEMP_COUPON.value);
        estimatedCostTv.setText(getResources().getString(R.string.ghs_amount,  String.format(Locale.getDefault(),"%.2f", newCostToDisplay)));
    }

    @Override
    public void onBackPressed() {

        if (selectPaymentMethodLayout.isVisible()){
            selectPaymentMethodLayout.hide();
            return;
        }

        super.onBackPressed();
        Bungee.slideRight(this);
    }
    

    private static String[] MY_PERMISSIONS = {
            "android.permission.ACCESS_FINE_LOCATION",
            "android.permission.ACCESS_COARSE_LOCATION",
            "android.permission.INTERNET"
    };

    private static final int REQUEST_LOC_PERM = 5;



    private void requestLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int checkCallPhonePermission = ContextCompat.checkSelfPermission(CheckOutActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);
            if (checkCallPhonePermission != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, MY_PERMISSIONS, REQUEST_LOC_PERM);
            } else {
                getUserLastKnowLocation();
            }
        } else {
            getUserLastKnowLocation();
        }
    }


    private void getUserLastKnowLocation() {

        // check if user's location is turned on, and turn it on

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }

        fusedLocationClient.getLastLocation().addOnSuccessListener(location -> {

            if (location == null) {
                Dialogs.alertWithSweet(this, "Enrout is unable to get your location");
                return;
            }

            Pos pos = new Pos();
            pos.geohash = Constants.getGeoHash(new LatLng(location.getLatitude(), location.getLongitude()));
            pos.geopoint = new Pos.PosLatLng(location.getLatitude(), location.getLongitude());

            trip.pos = pos;
            trip.currentLocation = new CurrentLocation(location.getLongitude(), location.getLatitude());

            trip.userCurrentLan = location.getLatitude();
            trip.userCurrentLng = location.getLongitude();

            dispatchRequestNow();

        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOC_PERM) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this,"Kindly grant permission to request trip",Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(this, MY_PERMISSIONS, REQUEST_LOC_PERM);
            }else
            {
                getUserLastKnowLocation();
            }
        }
    }

        /*
     *   This method is for dispatch the request
     * */
    private void dispatchRequestNow() {
        
        if ((int) trip.estimatedCost <= 0){
            computePriceAgain = true;

            new Thread(this::calDistanceTime).start();

            Toast.makeText(this, "Computing price .. please hold on", Toast.LENGTH_SHORT).show();
            return;
        }


        // check if user is using opted to use currentBalance
        if (trip.paymentMethod.equals(PaymentTypes.WALLET.toString())){
            trip.estimatedCost = (int) walletFee;
        }

        // assign pay for me value

        trip.payForMe = pfmValue;

        if (trip.payForMe > 0){
            trip.isPayForMe = true;
        }



        // add all the destination

        // set destinations
//        for (Destination destination : destinationArrayLists) {
//            DocumentReference destinationDocRef = db.collection(Collections.DESTINATIONS.toString()).document();
//        }

        // set pick ups


//            Log.d(Constants.TAG, "initViews: pickups built = " + new Gson().toJson(pickupArrayList));
//
//            Log.d(Constants.TAG, "initViews: destinations built = " + new Gson().toJson(destinationArrayLists));

        //LoadingGif.show(MainActivity.this);
//            db.collection(Collections)

        //
        Log.d(Constants.TAG, "dispatchRequestNow checkout : pickupArrayList  " + pickupArrayList);
        Log.d(Constants.TAG, "dispatchRequestNow checkout : destinationArrayList  " + pickupArrayList);

        DocumentReference requestDocumentReference = db.collection(Collections.REQUEST.toString()).document(UUID.randomUUID().toString());
        trip.requestId = requestDocumentReference.getId();

        Log.d(Constants.TAG, "dispatchRequestNow: requestId " + trip.requestId);

        ArrayList<DocumentReference> pickUpDocRefs = new ArrayList<>();
        ArrayList<String> pickupNames = new ArrayList<>();
        ArrayList<String> itemCategories = new ArrayList<>();
        ArrayList<String> itemIntructions = new ArrayList<>();
        ArrayList<String> stringPickupRef = new ArrayList<>();

        for(Pickup pickup : pickupArrayList){
            pickupNames.add(pickup.place);
            itemCategories.add(pickup.itemCategory);
            itemIntructions.add(pickup.instruction);

            pickup.documentReference = db.collection(Collections.PICKUPPOINTS.toString()).document(UUID.randomUUID().toString());

            pickup.referenceId = pickup.documentReference.getId();
            pickup.requestId = trip.requestId;

            pickUpDocRefs.add(pickup.documentReference);
            stringPickupRef.add(pickup.documentReference.getId());
        }

        ArrayList<DocumentReference> destDocRefs = new ArrayList<>();

        ArrayList<String> destNames = new ArrayList<>();
        ArrayList<String> stringDestRef = new ArrayList<>();


        for (Destination destination : destinationArrayList){
            destNames.add(destination.place);
            destination.documentReference = db.collection(Collections.DESTINATIONS.toString()).document(UUID.randomUUID().toString());
            destination.referenceId = destination.documentReference.getId();
            destination.requestId = trip.requestId;
            destDocRefs.add(destination.documentReference);
            stringDestRef.add(destination.documentReference.getId());
        }

        // set the pick up points to the db
        for(Pickup pickup : pickupArrayList){
            pickup.documentReference.set(pickup);
        }

        //  set the destination points to the db
        for(Destination destination : destinationArrayList){
            destination.documentReference.set(destination);
        }

        trip.pickups = pickUpDocRefs;
        trip.destinations = destDocRefs;
        trip.pickupNames = pickupNames;
        trip.pickupsRef = stringPickupRef;
        trip.destinationsRef = stringDestRef;
        trip.destinationNames = destNames;
        trip.selectedItemCategories = itemCategories;
        trip.selectedItemInstructions = itemIntructions;
        trip.senderId = applicaitonUser.id != null ? applicaitonUser.id : FirebaseAuth.getInstance().getUid();
        trip.requestDatetime = null;
        trip.hasPaid = false;
        trip.rated = false;

        /// set all notification for trip status to false
        trip.tripStatusNotified = new TripStatusNotified();

        trip.tripStatusNotified.PROCESSING = false;
        trip.tripStatusNotified.PENDING = false;
        trip.tripStatusNotified.ONGOING = false;
        trip.tripStatusNotified.NOT_FOUND = false;
        trip.tripStatusNotified.DELAYED = false;
        trip.tripStatusNotified.CANCELLED = false;
        trip.tripStatusNotified.CANCELLED_R = false;
        trip.tripStatusNotified.COMPLETED = false;

        trip.taskCount = pickupArrayList.size() + destinationArrayList.size();
        trip.taskCompleted = 0;
        trip.device = "ANDROID";
        trip.couponPreviousPrice = trip.estimatedCost;


        Log.d(Constants.TAG, "dispatchRequestNow: taskCount " + trip.taskCount);

        Log.d(Constants.TAG, "dispatchRequestNow: application Id " + applicaitonUser.id);

        LoadingGif.show(CheckOutActivity.this);

        new Thread(() -> {

            //  Log.d(Constants.TAG, "initViews: request built = " + new Gson().toJson(request));
            // apply coupon
            if(Constants.TEMP_COUPON != null){
                Coupon coupon = Constants.TEMP_COUPON;
                trip.coupon = db.collection(com.delivery.enroute.webservices.enums.Collections.COUPONS.toString()).document(coupon.docId);
                trip.couponApplied = true;
                trip.couponName = coupon.name;
                trip.couponValueInPercentage = coupon.value;
                trip.estimatedCost = trip.estimatedCost - (trip.estimatedCost * coupon.value);
                saveApplyCoupon(Constants.TEMP_COUPON);
            }


            requestDocumentReference.set(trip).addOnCompleteListener(task -> {

                if (!task.isSuccessful()){
                    LoadingGif.hide();
                    Toast.makeText(this, "Sorry, please try again", Toast.LENGTH_SHORT).show();
                    return;
                }

                HashMap<String, Object> params = new HashMap<>();
                params.put("pickups", trip.pickups);
                params.put("destinations", trip.destinations);
                params.put("requestDatetime", FieldValue.serverTimestamp());
                requestDocumentReference.update(params).addOnCompleteListener(task1 -> {

                    if (!task1.isSuccessful()){
                        LoadingGif.hide();
                        Toast.makeText(this, "Sorry please try again", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    emitTripToServerV2(trip);

                });


            });
            
        }).start();


        // refreshState();



        // todo take user to track trip activity with small animation
//        Intent intent = new Intent(MainActivity.this, TrackTripActivity.class);
//        startActivity(intent);
//        Bungee.slideUp(MainActivity.this);

    }

    private  void emitTripToServerV2(Trip trip){

        HashMap<String, String> param = new HashMap<>();
        param.put("requestId", trip.requestId);

        Log.d(Constants.TAG, "emitTripToServerV2: requestId: " + trip.requestId);

        String url = Constants.BaseUrl + "/universal/request-matrix/search-request/v2";
        RequestObject requestObject = new RequestObject(Request.Method.POST, url, param, response -> {


            try {
                boolean status = response.getBoolean("status");
                String message = response.getString("message");

                if(status){

                    runOnUiThread(() -> {

                        //OnGoingTripIntentService.enqueueWork(this,OnGoingTripIntentService.class, RSS_JOB_ID_ONGOING, serviceIntent);

                        Intent intent = new Intent(CheckOutActivity.this, TrackTripActivity.class);
                        intent.putExtra(Constants.TRIP, trip.requestId);
                        intent.putExtra(Constants.IS_FROM_CHECKOUT, true);

                        LoadingGif.hide();
                        startActivity(intent);
                        Bungee.slideUp(CheckOutActivity.this);

                    });

                }else{
                    Dialogs.alertWithSweet(this, message);
                }

            } catch (JSONException e) {
                Log.d(Constants.TAG, "payWithMoMo: JSONException " + e.getMessage());
                e.printStackTrace();
            }

        }, error -> {

            runOnUiThread(() -> {
                Dialogs.alertWithSweet(this, error.getMessage());
                LoadingGif.hide();
            });

        });

        requestObject.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestObject.send(this);
    }

    private void emitTripToServer(Trip trip) {

        Log.d(Constants.TAG, "call: about to call server - " + trip.requestId);

        JSONObject obj = new JSONObject();
        try {
            obj.put("requestId", trip.requestId);

//            if (mSocket == null) {
//                mSocket = SockIOConnection.getSocketInstance(this);
//            }

            Log.d(Constants.TAG, "emitTripToServer: mSocket => " + mSocket);

            mSocket.emit("request::created", obj, (Ack) args -> {
                JSONObject objRet = (JSONObject)args[0];
                Log.d(Constants.TAG, "call: server received it - " + objRet);
            });

            runOnUiThread(() -> {

                //OnGoingTripIntentService.enqueueWork(this,OnGoingTripIntentService.class, RSS_JOB_ID_ONGOING, serviceIntent);

                Intent intent = new Intent(CheckOutActivity.this, TrackTripActivity.class);
                intent.putExtra(Constants.TRIP, trip.requestId);
                intent.putExtra(Constants.IS_FROM_CHECKOUT, true);

                LoadingGif.hide();
                startActivity(intent);
                Bungee.slideUp(CheckOutActivity.this);

            });

            mSocket.on("request::not-exists", args2 -> {

                LoadingGif.hide();
                Toast.makeText(this, "Sorry please try again", Toast.LENGTH_SHORT).show();

                JSONObject obj2 = (JSONObject) args2[0];
                Log.d(Constants.TAG, "request::not-exists - " + obj2);

                // resend request reference
                mSocket.emit("request::created", obj);

                //runOnUiThread(() -> Toast.makeText(TestActivity.this, "" + obj, Toast.LENGTH_SHORT).show());
            });



        } catch (JSONException e) {
            Log.d(Constants.TAG, "emitTripToServer: JSONException " + e.getMessage());
            e.printStackTrace();
        }
    }


    class ForServer {
        public String place;
        public double latitude;
        public double longitude;

        ForServer(String place, double latitude, double longitude){
            this.place = place;
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }


//    coupon
void saveApplyCoupon(Coupon c){

    db.collection("COUPONS").whereEqualTo("status", "ACTIVE").whereEqualTo("code", c.code).get().addOnCompleteListener(task -> {

        if (!task.isSuccessful()){
            return;
        }

        QuerySnapshot queryDocumentSnapshots = task.getResult();
        if (queryDocumentSnapshots == null) {
            return;
        }

        if (queryDocumentSnapshots.isEmpty()){
            return;
        }

        List<Coupon> coupons = task.getResult().toObjects(Coupon.class);

        if (coupons.isEmpty()){
            return;
        }

        Coupon coupon =  coupons.get(0);

        if (coupon.totalUsed >= coupon.limit){
            return;
        }

        // check if user has used the coupon before

        boolean userHasUsedCouponBefore = false;

        int i = -1;
        for (Coupon.Entry entry: coupon.entriesList){
            i++;
            if (firebaseUser.getUid().equals(entry.userId)){
                // user has used this code before

                // check if user has exceeded his limit

                if (coupon.individualUserLimit >= entry.usage){
                    break;
                }

                userHasUsedCouponBefore = true;

                entry.usage  = entry.usage + 1;
                coupon.entriesList.set(i, entry);

                db.collection("COUPONS").document(applicaitonUser.id).update("entries", FieldValue.arrayRemove(entry))
                        .addOnSuccessListener( aVoid -> {
                            applyCoupon(coupon.docId, entry, coupon);
                        });
                break;
            }

        }

        if (!userHasUsedCouponBefore){
            // user's first time of using the code
            Coupon.Entry entry = new Coupon.Entry();
            entry.usage = 1;
            entry.userId = firebaseUser.getUid();
            coupon.entriesList.add(entry);
            applyCoupon(coupon.docId, entry, coupon);

        }

    });

}

    private void applyCoupon(String docId,Coupon.Entry entry, Coupon coupon){

        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("totalUsed", FieldValue.increment(1));
        objectMap.put("entries", FieldValue.arrayUnion(entry));
        db.collection(com.delivery.enroute.webservices.enums.Collections.COUPONS.toString()).document(docId).update(objectMap).addOnSuccessListener(aVoid -> {
        });

    }


}
