package com.delivery.enroute.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.delivery.enroute.MainActivity;
import com.delivery.enroute.R;
import com.delivery.enroute.utils.Constants;

public class ErrorActivity extends AppCompatActivity {

    String error;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);

        String mError = getIntent().getStringExtra(Constants.ERROR);
        TextView textView = findViewById(R.id.message);
        if (mError != null){
            error = mError;
           // textView.setText(error);
        }else{
            //error = String.valueOf(textView.getText());
            error = "";
            Log.d(Constants.TAG, "onCreate: message "  + error);
        }

    }


    public void report(View view){
        Constants.openEmail(this, "APP CRUSHED", error);
    }

    public void tryAgain(View view){
        Toast.makeText(this, "Please wait ...", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
