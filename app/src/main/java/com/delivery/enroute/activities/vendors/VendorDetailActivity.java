package com.delivery.enroute.activities.vendors;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.delivery.enroute.MainActivity;
import com.delivery.enroute.R;
import com.delivery.enroute.adapters.ProductsAdapter;
import com.delivery.enroute.models.Pickup;
import com.delivery.enroute.models.Pos;
import com.delivery.enroute.models.Product;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.models.Vendor;
import com.delivery.enroute.utils.Constants;
import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import spencerstudios.com.bungeelib.Bungee;

public class VendorDetailActivity extends AppCompatActivity {

    ImageButton closeBtn;
    ImageView banner;
    TextView description, vendorTtitle;
    RecyclerView recyclerView;
    ArrayList<Product> productArrayList = new ArrayList<>();
    ProductsAdapter productsAdapter = new ProductsAdapter(productArrayList);
    AlertDialog.Builder alertDialog;
    EditText input;

    Vendor vendor;
    Product selectedProduct;

    private ImageLoader imageLoader = ImageLoader.getInstance();

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_detail);

        closeBtn = findViewById(R.id.closeBtn);
        closeBtn.setOnClickListener(view -> {
            backPressedTapped();
        });
        banner = findViewById(R.id.vendor_banner);
        description = findViewById(R.id.vendor_description);
        vendorTtitle = findViewById(R.id.vendor_title);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(productsAdapter);

        NestedScrollView scrollView = findViewById(R.id.nested_scroll);
        scrollView.getParent().requestChildFocus(scrollView, scrollView);

        ArrayList<Product> products = (ArrayList<Product>) getIntent().getSerializableExtra(Constants.PRODUCTS);
        productArrayList.addAll(products);

        vendor = (Vendor) getIntent().getSerializableExtra(Constants.VENDOR);
        if(vendor.vendorBanner != null && !vendor.vendorBanner.equals(""))
            imageLoader.displayImage(vendor.vendorLogo, banner);

        description.setText(vendor.vendorDescription);
        vendorTtitle.setText(vendor.vendorName);

        productsAdapter.notifyDataSetChanged();

        alertDialog = new AlertDialog.Builder(VendorDetailActivity.this);
        alertDialog.setTitle("Quantity");
        alertDialog.setMessage("How many do you want ?");

        alertDialog.setNegativeButton("Cancel", null);

        alertDialog.setPositiveButton("Done", (dialogInterface, i) -> {
            String quantity = String.valueOf(input.getText());

            try
            {
                int qty = Integer.parseInt(quantity);
                if (qty < 1){
                    Toast.makeText(this, "Invalid quantity entered", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(selectedProduct == null){
                    Toast.makeText(this, "Please select item again", Toast.LENGTH_SHORT).show();
                    return;
                }

                Pickup pickup = new Pickup();
                pickup.instruction = "Please buy me " + qty + " of " + selectedProduct.productName + " worth GHS " + (qty * selectedProduct.productPrice);
                pickup.isArrived = false;
                pickup.isArrivedNotified = false;
                pickup.isPickedup = false;
                pickup.isPickedupNotified = false;
                pickup.itemCategory = "OTHER";
                pickup.pickUpName = vendor.vendorName;
                pickup.pickUpPersonPhone = vendor.vendorPhoneNumber;
                pickup.place = vendor.vendorLocation.address;

                Pos ppPos = new Pos();
                ppPos.geopoint = vendor.vendorLocation;
                ppPos.geohash = Constants.getGeoHash(new LatLng(vendor.vendorLocation.latitude, vendor.vendorLocation.longitude));

                pickup.pos = ppPos;

                Trip request = new Trip();
                request.isPickFromVendor = true;
                request.payForMe = qty * selectedProduct.productPrice;


                // set pick up and go to main activity
                Intent intent = new Intent(this, MainActivity.class);

                intent.putExtra(Constants.TRIP, request);
                intent.putExtra(Constants.PICK_UP, pickup);

                startActivity(intent);
                Bungee.split(this);
            }
            catch(NumberFormatException e)
            {
                Toast.makeText(this, "Invalid quantity entered", Toast.LENGTH_SHORT).show();
            }


            //
        });


        productsAdapter.setRowTappedLister(row -> {
            input = new EditText(VendorDetailActivity.this);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            lp.setMargins(20,0,20,0);
            input.setLayoutParams(lp);
            input.setText("1");

            selectedProduct = row;

            alertDialog.setView(input); // uncomment this line
            alertDialog.show();
        });

    }

    @Override
    public void onBackPressed() {
        backPressedTapped();
    }

    void backPressedTapped(){
        super.onBackPressed();
        Bungee.slideRight(this);
    }
}
