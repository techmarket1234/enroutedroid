package com.delivery.enroute.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.delivery.enroute.R;
import com.delivery.enroute.adapters.ReceiptDestinationAdapter;
import com.delivery.enroute.adapters.RecieptPickupAdapter;
import com.delivery.enroute.adapters.SelectedItemCategoriesAdapter;
import com.delivery.enroute.models.Destination;
import com.delivery.enroute.models.Pickup;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.utils.Constants;

import java.util.ArrayList;

import spencerstudios.com.bungeelib.Bungee;

public class ReceiptActivity extends AppCompatActivity {

    TextView thankyouMessage, date, time, orderNo, amount, distanceCovered, timeTravel, paid_via;
    RecyclerView pick_up_recycler_view, destination_recycler_view, selected_item_categories;

    TextView riderName;
    ImageView riderImage;
    Button done, sendReceipt;
    Trip trip;
    ArrayList<String> pickupArrayList = new ArrayList<>();
    RecieptPickupAdapter recieptPickupAdapter = new RecieptPickupAdapter(pickupArrayList);

    ArrayList<String> destinationArrayList = new ArrayList<>();
    ReceiptDestinationAdapter receiptDestinationAdapter = new ReceiptDestinationAdapter(destinationArrayList);

    ArrayList<String> itemCategories = new ArrayList<>();
    SelectedItemCategoriesAdapter selectedItemCategoriesAdapter = new SelectedItemCategoriesAdapter(itemCategories);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);

        trip =  Constants.TEMPORAL_TRIP_HOLDER;

        thankyouMessage = findViewById(R.id.thank_you_message);
        thankyouMessage.setText(getString(R.string.thank_you_message, trip.senderName));

        amount = findViewById(R.id.amount);
        amount.setText(getString(R.string.total_amount_value, String.valueOf(trip.actualCost)));

        orderNo = findViewById(R.id.order_number);
        orderNo.setText(getString(R.string.order_number, trip.orderNumber));

        date = findViewById(R.id.date);
        date.setText(Constants.formatToFriendlyDate(trip.requestDatetime));

        time = findViewById(R.id.time);
        time.setText(Constants.formatToFriendlyTime(trip.requestDatetime));

        distanceCovered = findViewById(R.id.distance_covered);
        distanceCovered.setText(getString(R.string.distance_covered, String.valueOf(trip.estimatedDistanceInMeters/100)));

        timeTravel = findViewById(R.id.travel_time);
        timeTravel.setText(getString(R.string.travel_time, String.valueOf(trip.estimatedTimeInSeconds)));

        pick_up_recycler_view = findViewById(R.id.pick_up_recycler_view);
        pick_up_recycler_view.setLayoutManager(new LinearLayoutManager(this));
        pick_up_recycler_view.setHasFixedSize(true);
        pick_up_recycler_view.setAdapter(recieptPickupAdapter);

        destination_recycler_view =  findViewById(R.id.destination_recycler_view);
        destination_recycler_view.setLayoutManager(new LinearLayoutManager(this));
        destination_recycler_view.setHasFixedSize(true);
        destination_recycler_view.setAdapter(receiptDestinationAdapter);

        selected_item_categories = findViewById(R.id.selected_item_categories);
        selected_item_categories.setLayoutManager(new LinearLayoutManager(this));
        selected_item_categories.setHasFixedSize(true);
        selected_item_categories.setAdapter(selectedItemCategoriesAdapter);


        riderName = findViewById(R.id.rider_name);
        riderName.setText(trip.riderName);


        riderImage = findViewById(R.id.rider_image);

        done = findViewById(R.id.done);
        done.setOnClickListener(view -> {
            super.onBackPressed();
            Bungee.slideRight(this);
        });

        paid_via = findViewById(R.id.paid_via);
        paid_via.setText(getString(R.string.paid_via_format, trip.paymentMethod));


        sendReceipt = findViewById(R.id.send_receipt);
        sendReceipt.setOnClickListener(view -> {
            share();
        });

        pickupArrayList.addAll(trip.pickupNames);
        destinationArrayList.addAll(trip.destinationNames);
        itemCategories.addAll(trip.selectedItemCategories);

        selectedItemCategoriesAdapter.notifyDataSetChanged();
        recieptPickupAdapter.notifyDataSetChanged();
        receiptDestinationAdapter.notifyDataSetChanged();

    }

    private void share(){
        StringBuilder sb = new StringBuilder();
        sb.append("Enrout Delivery Services\n");
        sb.append("Order No - ").append(trip.orderNumber).append("\n");
        sb.append("Date - ").append(Constants.formatToFriendlyDateTime(trip.requestDatetime)).append("\n");
        sb.append("Rider - ").append(trip.riderName).append("\n");
        sb.append("Plate - ").append(trip.riderPlateNumber).append("\n");
        sb.append("PICK UP (S)\n");
        sb.append("---------------- \n");
        for (String pickupString : trip.pickupNames){
            sb.append(pickupString);
        }
        sb.append("\n\n");
        sb.append("DESTINATIONS (S)\n");
        sb.append("----------------\n");
        for (String destinationString : trip.destinationNames){
            sb.append(destinationString);
            sb.append("\n");
        }

        sb.append("Thank your for using Enrout\n");
        sb.append("Contact us on : 020 111 2225\n");

        Intent i=new Intent(android.content.Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(android.content.Intent.EXTRA_SUBJECT,"TRIP RECEIPT");
        i.putExtra(android.content.Intent.EXTRA_TEXT, sb.toString());
        startActivity(Intent.createChooser(i,"Send via"));
    }
}
