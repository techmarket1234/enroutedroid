package com.delivery.enroute.activities.wallet;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.delivery.enroute.R;
import com.delivery.enroute.adapters.PaymodeAdapter;
import com.delivery.enroute.enums.Collections;
import com.delivery.enroute.models.Payment;
import com.delivery.enroute.singletons.ApplicaitonUser;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.Dialogs;
import com.delivery.enroute.utils.LoadingGif;
import com.delivery.enroute.utils.MSlideUp;
import com.delivery.enroute.utils.volley.RequestObject;
import com.github.florent37.viewanimator.ViewAnimator;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mancj.slideup.SlideUp;

import org.json.JSONException;

import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;
import spencerstudios.com.bungeelib.Bungee;

public class PayWithMomoActivity extends AppCompatActivity {

    ListView listView;
    PaymodeAdapter adapter;
    SlideUp chooseNetworkPicker, paymentSuccessLayout;
    EditText chooseNetworkEt, phoneNumber, amount, vodafoneAuthToken;
    private int mSelectedNetworkPosition;
    ImageView iconSuccess;
    TextView responseMessage;
    Button paymentDone;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    double amountToPay;
    TextView description;
    double temporalAmount;
    String paymentRefId;
    Button button;
    SweetAlertDialog sweetAlertDialog;

    double fixedAmount = 0.0;
    boolean shouldDisableAmountField = false;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_with_momo);


        ApplicaitonUser applicaitonUser = ApplicaitonUser.getInstance(this);
//        Initialize views
        findViewById(R.id.closeBtn).setOnClickListener(view -> {
            super.onBackPressed();
            Bungee.slideRight(this);
        });

        //screenHeight = getScreenHeight();

       fixedAmount = getIntent().getDoubleExtra(Constants.PAYMENT_FIXED_AMOUNT, 0.0);
        shouldDisableAmountField = getIntent().getBooleanExtra(Constants.SHOULD_DISABLE_AMOUNT_FIELD, false);

        RelativeLayout pickerLayout = findViewById(R.id.choose_network_layout);
//        int preferredHeight = screenHeight / 3;
//        pickerLayout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, preferredHeight));
        chooseNetworkPicker = MSlideUp.controlView(pickerLayout);



        chooseNetworkEt = findViewById(R.id.etChooseNetwork);
        chooseNetworkEt.setOnClickListener(view -> chooseNetworkPicker.show());

        phoneNumber = findViewById(R.id.phone_number);
        vodafoneAuthToken = findViewById(R.id.vodafone_auth_token);
        vodafoneAuthToken.setVisibility(View.GONE);
        phoneNumber.setText(applicaitonUser.phoneNumber);
        amount =  findViewById(R.id.cash_amount);
        description = findViewById(R.id.description);
        description.setText("Kindly enter your mobile money details. You'd receive a prompt");

        if(fixedAmount != 0.0){
            // comment the code below
           // fixedAmount = 1.1 ;
            amount.setText(String.valueOf(fixedAmount + 1));
            if (shouldDisableAmountField)
                amount.setEnabled(false);
        }else{
            amount.setEnabled(true);
        }

        findViewById(R.id.up).setOnClickListener(scrollUp());
        findViewById(R.id.down).setOnClickListener(scrollDown());

        listView = findViewById(R.id.listView_select_pay_mode);

        adapter = new  PaymodeAdapter();
        listView.setAdapter(adapter);


        listView.setOnItemClickListener((adapterView, view, i, l) -> {

            mSelectedNetworkPosition = i;
            adapter.setmSelectedItem(i);
            adapter.notifyDataSetChanged();

            applySelection();


        });

        findViewById(R.id.doneBtn).setOnClickListener(view -> {

        });

//        payment success layout ###############

        iconSuccess = findViewById(R.id.icon);
        responseMessage = findViewById(R.id.payment_success_label);
        //// pay now button is tapped
        paymentSuccessLayout = MSlideUp.controlView(findViewById(R.id.payment_success_layout));


        button = findViewById(R.id.pay_now);
        button.setOnClickListener(view -> {

            if (TextUtils.isEmpty(amount.getText())){
                amount.setError("Amount is required");
                return;
            }

            amountToPay = Double.parseDouble(String.valueOf(amount.getText()));
            
            if (TextUtils.isEmpty(phoneNumber.getText())){
                phoneNumber.setError("Required");
                return;
            }
            
            if (TextUtils.isEmpty(chooseNetworkEt.getText())){
                chooseNetworkEt.setError("Required");
                return;
            }

            if (amountToPay < 2.0){
                String msg = "Amount cannot be less than GHS 2";
                amount.setError(msg);
                Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                return;
            }

            if ("VODAFONE".equals(String.valueOf(chooseNetworkEt.getText()))){
                if (TextUtils.isEmpty(vodafoneAuthToken.getText())){
                    vodafoneAuthToken.setError("Token required");
                    return;
                }
            }
            
            makePayment();
        });

//        payment done tapped
        paymentDone = findViewById(R.id.payment_done);
        paymentDone.setOnClickListener(view -> {
            paymentDone.setVisibility(View.GONE);
            ViewAnimator.animate(iconSuccess).bounceOut().duration(2000).onStop(() -> {
            PayWithMomoActivity.super.onBackPressed();
            Bungee.slideRight(PayWithMomoActivity.this);
            //paymentSuccessLayout.hide();
            }).start();
        });


    }

    @SuppressLint("SetTextI18n")
    private void applySelection(){
        String selectedNetwork = adapter.getItem(mSelectedNetworkPosition);
        if (mSelectedNetworkPosition == 0){ // mtn
            description.setText("Please make sure you have enough balance in your momo wallet");
        }else{
            description.setText("Kindly enter your mobile money details. You'd receive a prompt");
        }

        if (mSelectedNetworkPosition == 2){
            // vodafone
            vodafoneAuthToken.setVisibility(View.VISIBLE);
        }else{
            vodafoneAuthToken.setVisibility(View.GONE);
        }


        chooseNetworkEt.setText(selectedNetwork);
        chooseNetworkPicker.hide();
    }

    private void makePayment() {

        button.setEnabled(false);


        paymentRefId = Constants.getUniqueString();
        // write into the payments collection
        Payment payment = new Payment();
        payment.amount = amountToPay;
        payment.mobileAuthToken = String.valueOf(vodafoneAuthToken.getText());
        payment.momoNumber = String.valueOf(phoneNumber.getText());
        payment.network = String.valueOf(chooseNetworkEt.getText());
        payment.orderId = paymentRefId;
        payment.senderId = firebaseUser.getUid();
        payment.senderName = firebaseUser.getDisplayName();

        LoadingGif.show(this);
        db.collection(Collections.PAYMENTS.toString()).document(paymentRefId).set(payment).addOnSuccessListener(aVoid -> {

            //"MTN","TIGO / AIRTEL", "VODAFONE"
            String apiNetwork;
            switch (String.valueOf(chooseNetworkEt.getText())){
                case "MTN":
                    apiNetwork = "MTN_MM";
                    break;
                case "TIGO":
                    apiNetwork = "TIGO_CASH";
                    break;
                case "AIRTEL":
                    apiNetwork = "AIRTEL_MM";
                    break;
                case "VODAFONE":
                    apiNetwork = "VODAFONE_CASH";
                    break;
                default:
                    apiNetwork = "VODAFONE_CASH";
                    break;
            }


            // access express pay

            String url = Constants.PAYMENT_BASE_URL + "/deposits/customer-deposit";
            HashMap<String, String> params = new HashMap<>();
            params.put("amount",String.valueOf(amountToPay));
            params.put("orderId",paymentRefId);
            params.put("momoNumber",String.valueOf(phoneNumber.getText()));
            params.put("network",apiNetwork);
            params.put("mobileAuthToken",String.valueOf(vodafoneAuthToken.getText()));
            params.put("senderName",firebaseUser.getDisplayName());
            params.put("transType","CREDIT");
            params.put("paymentType","DEPOSIT");
            params.put("currency","GHS");
            params.put("senderId",firebaseUser.getUid());

            Log.d(Constants.TAG, "makePayment: url " + url);
            Log.d(Constants.TAG, "makePayment: params = "  + params);

            RequestObject requestObject = new RequestObject(Request.Method.POST, url, params, response -> {

                LoadingGif.hide();

                try {
                    boolean status = response.getBoolean("status");
                    String message = response.getString("message");

                    if (status){

                        db.collection(Collections.PAYMENTS.toString()).document(paymentRefId).addSnapshotListener((documentSnapshot, e) -> {

                            button.setEnabled(true);

                            if (documentSnapshot == null) return;

                            Payment getPayment = documentSnapshot.toObject(Payment.class);
                            if (getPayment == null) return;

                            if ("APPROVED".equals(getPayment.status)){
                                if (sweetAlertDialog != null)
                                    sweetAlertDialog.dismiss();
                                //iconSuccess.setImageDrawable(getResources().getDrawable(R.drawable.));
                                paymentSuccessLayout.show();
                                ViewAnimator.animate(iconSuccess).bounceIn().duration(2000).start();
                                button.setEnabled(true);
                            }

                            if ("FAILED".equals(getPayment.status)){
                                if (sweetAlertDialog != null)
                                    sweetAlertDialog.dismiss();

                                String responseMsg = "FAILED";
                                responseMessage.setText(responseMsg);
                            }

                        });
                    }

                    sweetAlertDialog = Dialogs.alertWithSweet(this, message);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }, error -> {
                Log.d(Constants.TAG, "makePayment: " + error);
                Dialogs.alert(PayWithMomoActivity.this,error.getMessage());
                LoadingGif.hide();
                button.setEnabled(true);
            });

            requestObject.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestObject.send(this);

        }).addOnFailureListener(e -> {
            LoadingGif.hide();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            button.setEnabled(true);
        });

         


        
    }


    @Override
    public void onBackPressed() {

        super.onBackPressed();
        Bungee.slideRight(this);

    }

    public View.OnClickListener scrollUp(){
       return view -> {

           int newPosition = mSelectedNetworkPosition - 1;
           if (newPosition < 0) return;


           mSelectedNetworkPosition = newPosition;
           adapter.setmSelectedItem(newPosition);
           adapter.notifyDataSetChanged();

           listView.smoothScrollToPosition(mSelectedNetworkPosition);
       };
    }

    public View.OnClickListener scrollDown(){
       return view -> {


           int newPosition = mSelectedNetworkPosition + 1;
           if (newPosition == listView.getAdapter().getCount()) return;

           mSelectedNetworkPosition = newPosition;

           adapter.setmSelectedItem(newPosition);
           adapter.notifyDataSetChanged();
           listView.smoothScrollToPosition(mSelectedNetworkPosition);

       };
    }

    private int getScreenHeight(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
        //int width = displayMetrics.widthPixels;

    }


}
