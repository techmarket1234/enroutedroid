package com.delivery.enroute.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.delivery.enroute.MainActivity;
import com.delivery.enroute.R;
import com.delivery.enroute.enums.TripStatus;
import com.delivery.enroute.models.Driver;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.models.UserDriverComment;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.LoadingGif;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.HashMap;
import java.util.Locale;

import spencerstudios.com.bungeelib.Bungee;

public class RateRiderActivity extends AppCompatActivity {

    Trip trip;
    TextView rider_name;
    ImageView riderImage;
    Button done;
    RatingBar ratingBar;
    EditText comment;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    TextView removeRider, notTheBest, belowAverage, couldBeBetter, awesomeDelivery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_rider);
        ImageLoader imageLoader = ImageLoader.getInstance();

        trip = (Trip) getIntent().getSerializableExtra(Constants.TRIP);
        // private Trip trip;
        rider_name = findViewById(R.id.rider_name);
        riderImage = findViewById(R.id.rider_image);
        done = findViewById(R.id.done);
        done.setOnClickListener(rateOnClickListener);
        ratingBar = findViewById(R.id.ratingBar);
        comment = findViewById(R.id.comment);

        ratingBar.setOnRatingBarChangeListener((ratingBar1, v, b) -> {
            selectComment(ratingBar1);
        });

        // message templates
        removeRider = findViewById(R.id.remove_this_rider);
        notTheBest = findViewById(R.id.not_the_best);
        belowAverage = findViewById(R.id.below_average);
        couldBeBetter = findViewById(R.id.could_be_better);
        awesomeDelivery = findViewById(R.id.awesome_delivery);

        removeRider.setOnClickListener(messageTemplateTapped);
        notTheBest.setOnClickListener(messageTemplateTapped);
        belowAverage.setOnClickListener(messageTemplateTapped);
        couldBeBetter.setOnClickListener(messageTemplateTapped);
        awesomeDelivery.setOnClickListener(messageTemplateTapped);

        if (trip != null){
            String rateText = "Rate " + trip.riderName;
            rider_name.setText(rateText);
            byte[] byteArray = getIntent().getByteArrayExtra(Constants.RIDER_IMAGE);
            if (byteArray != null){
                Log.d(Constants.TAG, "onCreateDialog: byteArray NOT null ");
                Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                riderImage.setImageBitmap(bmp);
            }else{
                Log.d(Constants.TAG, "onCreateDialog: byteArray is null ");
                if (trip.riderImage != null){
                    imageLoader.loadImage(trip.riderImage, new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            // Do whatever you want with Bitmap
                            if (loadedImage != null){
                                riderImage.setImageBitmap(loadedImage);
                            }
                        }
                    });
                }
            }


        }
    }

    private void selectComment(RatingBar ratingBar1) {
        int mark = (int) ratingBar1.getRating();
        unselectAll();
        switch (mark){
            case 1 :
                selectView(removeRider);
                break;
            case 2 :
                selectView(notTheBest);
                break;
            case 3 :
                selectView(belowAverage);
                break;
            case 4 :
                selectView(couldBeBetter);
                break;
            case 5 :
                selectView(awesomeDelivery);
                break;
            default:
                  break;
        }
    }

    private void unselectAll(){
        removeRider.setBackground(getResources().getDrawable(R.drawable.rr_not_selected));
        removeRider.setTextColor(Color.BLACK);

        notTheBest.setBackground(getResources().getDrawable(R.drawable.rr_not_selected));
        notTheBest.setTextColor(Color.BLACK);

        belowAverage.setBackground(getResources().getDrawable(R.drawable.rr_not_selected));
        belowAverage.setTextColor(Color.BLACK);

        couldBeBetter.setBackground(getResources().getDrawable(R.drawable.rr_not_selected));
        couldBeBetter.setTextColor(Color.BLACK);

        awesomeDelivery.setBackground(getResources().getDrawable(R.drawable.rr_not_selected));
        awesomeDelivery.setTextColor(Color.BLACK);
    }

    private void selectView(TextView textView){
        textView.setBackground(getResources().getDrawable(R.drawable.rr_selected));
        textView.setTextColor(Color.WHITE);
        comment.setText(textView.getText().toString());
    }

    private View.OnClickListener messageTemplateTapped = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            unselectAll();
            selectView((TextView) view);
            switch (view.getId()){
                case R.id.remove_this_rider:
                    ratingBar.setRating(1);
                    break;
                case R.id.not_the_best:
                    ratingBar.setRating(2);
                    break;
                case R.id.below_average:
                    ratingBar.setRating(3);
                    break;
                case R.id.could_be_better:
                    ratingBar.setRating(4);
                    break;
                case R.id.awesome_delivery:
                    ratingBar.setRating(5);
                    break;
            }
            comment.setText(((TextView) view).getText());
        }
    };



    private View.OnClickListener rateOnClickListener = view -> {
        double mark = ratingBar.getRating();

        new Thread(() -> {
            runOnUiThread(() -> {
                LoadingGif.show(RateRiderActivity.this);
            });
            db.collection("DRIVERS").document(trip.riderId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    runOnUiThread(LoadingGif::hide);

                    Driver driver = documentSnapshot.toObject(Driver.class);


                    if (driver == null) return;


                    if (mark != 0.0){
                        double newSumOfVotes = driver.sumOfVotes + mark;
                        double newTotalVotes = driver.totalNoOfVotes + 1;

                        double newAverateRating =  newSumOfVotes / newTotalVotes;


                        HashMap<String, Object> updatedValues = new HashMap<>();
                        updatedValues.put("sumOfVotes", newSumOfVotes);
                        updatedValues.put("totalNoOfVotes", newTotalVotes);
                        updatedValues.put("averageRating", newAverateRating);

                        /// insert into back into the rider rating
                        db.collection("DRIVERS").document(trip.riderId).update(updatedValues);

                        HashMap<String, Object> updateRequestObj =  new HashMap<>();
                        updateRequestObj.put("rated", true);
                        updateRequestObj.put("rating", mark);
                        db.collection("REQUEST").document(trip.requestId).update(updateRequestObj);

                        String message = comment.getText().toString();
                        if (!"".equals(message)){
                            UserDriverComment userDriverComment = new UserDriverComment();
                            userDriverComment.userId = firebaseUser.getUid();
                            userDriverComment.driverId = trip.riderId;
                            userDriverComment.comment = message;

                            /// write into comments
                            db.collection("DRIVER_COMMENTS").add(userDriverComment);
                        }

                        runOnUiThread(() -> {
//                            average_rating.setText(String.format(Locale.getDefault(), "%.2f", newAverateRating));
                            Log.d(Constants.TAG, "onRated: completed trip status - " + trip.status);
                            Toast.makeText(RateRiderActivity.this, "Thanks for rating", Toast.LENGTH_SHORT).show();
                            if (!trip.status.equals(TripStatus.ONGOING.toString())){
                                compeleteTrip();
                            }
                        });
                    }


                }
            }).addOnFailureListener(e -> {
                runOnUiThread(() -> {
                    LoadingGif.hide();
                    Toast.makeText(RateRiderActivity.this, "Thanks for rating", Toast.LENGTH_SHORT).show();
                    if (!trip.status.equals(TripStatus.ONGOING.toString())){
                        compeleteTrip();
                    }
                });
                Log.d(Constants.TAG, "initRatingDialog: addOnFailureListener " + e.getMessage());

            });
        }).start();
    };


    @Override
    public void onBackPressed() {
        Toast.makeText(this, "Please rate your rider", Toast.LENGTH_SHORT).show();
    }

    private void compeleteTrip(){
        startActivity(new Intent(this, MainActivity.class));
        Bungee.slideRight(this);
    }
}
