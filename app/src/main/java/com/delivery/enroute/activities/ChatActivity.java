package com.delivery.enroute.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.EditText;
import android.widget.Toast;

import com.delivery.enroute.R;
import com.delivery.enroute.broadcastreceivers.ChatReceiver;
import com.delivery.enroute.models.Chat;
import com.delivery.enroute.models.ChatUser;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.Dialogs;
import com.github.bassaer.chatmessageview.model.Message;
import com.github.bassaer.chatmessageview.view.MessageView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.gson.Gson;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import spencerstudios.com.bungeelib.Bungee;

public class ChatActivity extends AppCompatActivity {

    private MessageView mChatView;
    EditText chatEt;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef;
    String riderId, riderPhoneNumber, riderName;

    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        riderId = getIntent().getStringExtra(Constants.RIDER_ID);
        riderName = getIntent().getStringExtra(Constants.RIDER_NAME);
        riderPhoneNumber = getIntent().getStringExtra(Constants.RIDER_PHONE);

        mChatView = findViewById(R.id.chat_view);
        //mChatView.setRightBubbleColor(R.color.message_color);
        mChatView.setMessageFontSize(35F);


        if (firebaseUser == null) return;
        myRef = database.getReference("messages/"+firebaseUser.getUid() + riderId);


        /*
        * Init Toolbar
        * */

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
//            replace the home (back) button with the menu icon
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
//            set the Home / Back Button
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            setTitle(riderName);

        }

        chatEt = findViewById(R.id.chatEt);
        chatEt.setOnTouchListener((view, motionEvent) -> {
            //            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
//            final int DRAWABLE_BOTTOM = 3;

            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                if (motionEvent.getRawX() >= (chatEt.getRight() - chatEt.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    // your action here

                    // send to the server
                    if (TextUtils.isEmpty(chatEt.getText())) return false;

                    String chatMessage = String.valueOf(chatEt.getText());

                    DatabaseReference newRef = myRef.push();

                    Chat mChat = new Chat(
                            chatMessage, riderId, firebaseUser.getUid()
                    );
                    mChat.riderName = riderName;
                    mChat.riderPhone = riderPhoneNumber;
                    newRef.setValue(mChat);

                    chatEt.getText().clear();

                    return true;
                }
            }
            return false;
        });



        // The filter's action is BROADCAST_ACTION
        IntentFilter statusIntentFilter = new IntentFilter(
                Constants.BROADCAST_ACTION_CHAT);

        // Instantiates a new DownloadStateReceiver
        ChatReceiver chatReceiver =
                new ChatReceiver();
        // Registers the DownloadStateReceiver and its intent filters
        LocalBroadcastManager.getInstance(this).registerReceiver(
                chatReceiver,
                statusIntentFilter);
        chatReceiver.setOnChatRecievedListener(chat -> {
            Log.d(Constants.TAG, "onCreate: message received in chat activity " );
        });

    }

    @Override
    protected void onResume() {

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                updateUI(dataSnapshot);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(Constants.TAG, "onCancelled: " + databaseError);
            }
        });

        super.onResume();
    }

    private void updateUI(DataSnapshot dataSnapshots) {

        mChatView.removeAll();
        for(DataSnapshot chatSnapShot : dataSnapshots.getChildren()){

            Chat chat = chatSnapShot.getValue(Chat.class);
            if (chat == null) continue;

//            if (chat.getCreatedTimestampLong() > lastChat.getCreatedTimestampLong()){
//                lastChat = chat;
//            }
            // Read from the database
            Calendar cal =  GregorianCalendar.getInstance();
            cal.setTimeInMillis(chat.getCreatedTimestampLong());
            Message message = new Message.Builder()
                    .setUser(new ChatUser(firebaseUser.getUid(), firebaseUser.getDisplayName()))
                    .setRight("customer".equals(chat.sentBy))
                    .setText(chat.messageText)
                    .setSendTime(cal)
                    .hideIcon(true)
                    .build();

            //Set to chat view
            mChatView.setMessage(message);
        }

        mChatView.scrollToEnd();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       switch (item.getItemId()){
           case android.R.id.home:
               super.onBackPressed();
               Bungee.slideRight(this);
               break;
           case R.id.call_rider:

               proceedWithCall(true);
               break;
       }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chat_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case Constants.MY_PERMISSIONS_REQUEST_CALL:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    proceedWithCall(true);

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    proceedWithCall(false);
                }
                break;
        }
    }

    private void proceedWithCall(boolean shouldProceedWithCall){

        try {

            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:"+ riderPhoneNumber));//change the number
            startActivity(intent);
        }catch (Exception e){

        }
    }
}
