package com.delivery.enroute.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.delivery.enroute.R;

public class MaintenanceModeActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintenance_mode);

        String message = getIntent().getStringExtra("message");
        TextView textView = findViewById(R.id.message);
        if (message != null && !"".equals(message)){
            textView.setText(message);
        }

    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
