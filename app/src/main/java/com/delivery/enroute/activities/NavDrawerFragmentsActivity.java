package com.delivery.enroute.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.delivery.enroute.R;
import com.delivery.enroute.adapters.NavMenuAdapter;
import com.delivery.enroute.enums.MenuItems;
import com.delivery.enroute.fragments.FaqFragment;
import com.delivery.enroute.fragments.NotificationsFragment;
import com.delivery.enroute.fragments.TripsFragment;
import com.delivery.enroute.fragments.ProfileFragment;
import com.delivery.enroute.fragments.PromosFragment;
import com.delivery.enroute.fragments.WalletFragment;
import com.delivery.enroute.singletons.ApplicaitonUser;
import com.delivery.enroute.utils.Constants;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.infideap.drawerbehavior.Advance3DDrawerLayout;

import spencerstudios.com.bungeelib.Bungee;


public class NavDrawerFragmentsActivity extends AppCompatActivity {

    //Fragment [] fragments;
    Fragment currentFragment;
    private Advance3DDrawerLayout drawer;
    ProfileFragment profileFragment;
    PromosFragment promosFragment;
    WalletFragment walletFragment;
    FaqFragment faqFragment;
    NotificationsFragment notificationsFragment;
    TextView username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_drawer_fragments);

        /*
        *   Initialize toolbar
        * */

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
//            replace the home (back) button with the menu icon
            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_menu_black_24dp, null);
            if (drawable == null)return;

            drawable = DrawableCompat.wrap(drawable);
            DrawableCompat.setTint(drawable, Color.WHITE);

            getSupportActionBar().setHomeAsUpIndicator(drawable);
//            set the Home / Back Button
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        /*
        *   End of toolbar
        * */


         profileFragment = new ProfileFragment();
         promosFragment = new PromosFragment();

         walletFragment = new WalletFragment();


         faqFragment = new FaqFragment();
         notificationsFragment = new NotificationsFragment();


        /// the code below determines which fragment to show first when this activity is created ////////////
        MenuItems navDrawerFragmentsEnums =  (MenuItems) getIntent().getSerializableExtra(Constants.NAV_DRAWER_ENUM);
//
        if(navDrawerFragmentsEnums != null){
            switch (navDrawerFragmentsEnums){
                case PROMO:
                    currentFragment = promosFragment;
                    break;
                case WALLET:
                    currentFragment = walletFragment;
                    break;
                case PERSONAL_DETAILS:
                    currentFragment = profileFragment;
                    break;
                case CONTACT_US:
                    Constants.openEmail(getApplicationContext());
                    return;
                case VENDORS:
                    Constants.openVendors(this);
                    return;
                case FAQ:
                    Constants.openFAQ(this);
                    return;
                case NOTIFICATIONS:
                    currentFragment = notificationsFragment;
                    break;
                case ORDER:
                    currentFragment = new TripsFragment();
                    break;
                default:
                    currentFragment = profileFragment;
            }
        }


        if (currentFragment != null){
            try{
                getSupportFragmentManager().beginTransaction().replace(R.id.container, currentFragment).commit();
            }catch (Exception e){
                Log.d(Constants.TAG, "beginTransaction Exception: " + e.getMessage());
            }
        }


        /*
         *   Side drawer initializations ############
         * */

        drawer = findViewById(R.id.drawer_layout);


        NavigationView navigationView = findViewById(R.id.nav_view);
        ViewGroup headerView = (ViewGroup) navigationView.getHeaderView(0);
        ViewGroup relativeLaoutViewGroup = (ViewGroup) headerView.getChildAt(0);
        ViewGroup LinearLaoutViewGroup = (ViewGroup) relativeLaoutViewGroup.getChildAt(1);
        username = (TextView) LinearLaoutViewGroup.getChildAt(0);


        RecyclerView listView = (RecyclerView) headerView.getChildAt(1);
        NavMenuAdapter navMenuAdapter = new NavMenuAdapter();
        navMenuAdapter.setItemRowTappedLister(row -> {
            Intent intent = new Intent(this, NavDrawerFragmentsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // when a menu item is tapped
            switch (row.menuItem){
                case HOME:
                    super.onBackPressed();
                    Bungee.slideLeft(this);

                    return;
                case PERSONAL_DETAILS:
                    currentFragment = profileFragment;
                    break;
                case SHARE_AND_EARN:
                    currentFragment = profileFragment;
                    break;
                case COURIER_TYPE:
                    currentFragment = profileFragment;
                    break;
                case ORDER:
                    currentFragment = new TripsFragment();
                    break;
                case PROMO:
                    currentFragment = promosFragment;
                    break;
                case WALLET:
                    currentFragment = walletFragment;
                    break;
                case VENDORS:
                    Constants.openVendors(this);
                    break;
                case FAQ:
                    Constants.openFAQ(this);
                    break;
                case CONTACT_US:
                    Constants.openEmail(this);
                    return;
                case NOTIFICATIONS:
                    currentFragment = notificationsFragment;
                    break;
                case LOGOUT:
                    Constants.logout(this);
                    return;
            }

            drawer.closeDrawer(GravityCompat.START);
            try{
                getSupportFragmentManager().beginTransaction().replace(R.id.container, currentFragment).commit();
            }catch (Exception e){
                Log.d(Constants.TAG, "beginTransaction second Exception: " + e.getMessage());
            }


        });
        listView.setLayoutManager(new LinearLayoutManager(this));
        listView.setHasFixedSize(true);
        listView.setAdapter(navMenuAdapter);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        drawer.setViewScale(Gravity.START, 0.9f);
        drawer.setViewElevation(Gravity.START, 20);
        drawer.setRadius(Gravity.START, 25);//set end container's corner radius (dimension)
//        drawer.setViewRotation(Gravity.START, 15); // MAKES IT 3D

        drawer.useCustomBehavior(Gravity.START); //assign custom behavior for "Left" drawer
//
//        /*
//         *   End of side drawer initializations =====
//         * */

//        search

        // Get the intent, verify the action and get the query
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            doMySearch(query);
        }


    }

    private void doMySearch(String query) {
        Toast.makeText(this, query, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        username.setText(getString(R.string.hello_username, ApplicaitonUser.getInstance(this).fullName));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
