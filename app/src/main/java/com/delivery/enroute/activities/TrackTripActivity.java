package com.delivery.enroute.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.annotation.TargetApi;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Property;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.delivery.enroute.MainActivity;
import com.delivery.enroute.R;
import com.delivery.enroute.adapters.CancelOrderReasonsAdapter;
import com.delivery.enroute.adapters.OnGoingDestinationAdapter;
import com.delivery.enroute.adapters.OnGoingPickUpAdapter;
import com.delivery.enroute.enums.Collections;
import com.delivery.enroute.enums.TripStatus;
import com.delivery.enroute.layoutdialogs.ImageZoomFragment;
import com.delivery.enroute.layoutdialogs.RatingDialogFragment;
import com.delivery.enroute.models.CancelOrderReason;
import com.delivery.enroute.models.Destination;
import com.delivery.enroute.models.Driver;
import com.delivery.enroute.models.Pickup;
import com.delivery.enroute.models.Pos;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.models.UserDriverComment;
import com.delivery.enroute.singletons.ApplicaitonUser;
import com.delivery.enroute.singletons.SockIOConnection;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.CustomMarkerInfoWindowView;
import com.delivery.enroute.utils.Dialogs;
import com.delivery.enroute.utils.LoadingGif;
import com.delivery.enroute.utils.MSlideUp;
import com.delivery.enroute.utils.NotificationHelper;
import com.delivery.enroute.utils.RiderLocationManager;
import com.delivery.enroute.utils.maps.MapAnimator;
import com.delivery.enroute.webservices.services.ChatService;
import com.delivery.enroute.webservices.services.OnGoingTripIntentService;
import com.delivery.enroute.webservices.services.TripListenerIntentService;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.mancj.slideup.SlideUp;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import io.socket.client.Ack;
import io.socket.client.Socket;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import spencerstudios.com.bungeelib.Bungee;

import static com.delivery.enroute.utils.Constants.DEFAULT_ZOOM;

public class TrackTripActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String SHOW_RATING = "SHOW_RATING";
    private static final int RSS_JOB_ID = 1000;
    private static final String ZOOM_IMAGE = "ZOOM_IMAGE";
    private static String[] MY_PERMISSIONS = {
            "android.permission.ACCESS_FINE_LOCATION",
            "android.permission.ACCESS_COARSE_LOCATION",
            "android.permission.INTERNET"
    };

    RatingDialogFragment ratingDialogFragment;


    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    ApplicaitonUser applicaitonUser;

    Animation mBounceAnimation;
    private Handler mHandler;
    CustomMarkerInfoWindowView customInfoWindow;

    private GoogleMap map;
    Marker riderMarker;
//    SlidingUpPanelLayout slidingUpPanelLayout;
    SlideUp searchRiderLayout,
        riderFoundLayout,
        cancelOrderLayout,
        fetchTripStatusLayout,
        //poorConnection
        ridersAreCurrentlyBusyLayout, tripCancelledByRider, tripCompleted
                ;

    Marker destMarker;

    LatLng pinLocation;

    float zoomValue = DEFAULT_ZOOM;

    SlidingUpPanelLayout slidingUpPanelLayout;
    ImageView search_rider;
    TextView rateTv;
    TextView completingNearByTrip;

    Trip trip;

    RecyclerView pickupRecyclerView, destinationRecyclerView;

    CancelOrderReasonsAdapter cancelOrderReasonsAdapter = new CancelOrderReasonsAdapter(CancelOrderReason.getReasons());
    View findRiderDim;
    SpinKitView progressBarFindingRider;

    private int screenHeight;

    private Socket mSocket;

    private double mTargetLong;
    private double mTargetLat;

    ArrayList<Pickup> pickupArrayList = new ArrayList<>();
    OnGoingPickUpAdapter onGoingPickUpAdapter = new OnGoingPickUpAdapter(pickupArrayList);

    ArrayList<Destination> destinationArrayList = new ArrayList<>();
    OnGoingDestinationAdapter onGoingDestinationAdapter = new OnGoingDestinationAdapter(destinationArrayList);


    private static final int REQUEST_PERM_CODE = 2;

    //int tripPosition;
    String requestId;
    private TextView riderNameTv, riderPlateTv, orderNoTv;
    ImageView riderImage;
    private boolean isFromCheckOut;
    private TextView average_rating;
    ImageLoader imageLoader;


    private Bitmap loadedRiderImage;

    MaterialProgressBar general_progress_bar;
    private View arrival_time_layout;
    private TextView arrival_time;

    ListenerRegistration requestListenerReg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_trip);

        imageLoader = ImageLoader.getInstance();

        isFromCheckOut = getIntent().getBooleanExtra(Constants.IS_FROM_CHECKOUT,false);

        // this activity should receive a request object

       requestId = getIntent().getStringExtra(Constants.TRIP);

       applicaitonUser = ApplicaitonUser.getInstance(this);

        /*
        *   Initializations ======
        * */
            initObjects();

        /*
        *   End of Initializations
        * */

        /*
        *   Init layouts
        * */

        initLayouts();
        /*
        *   End of init layouts
        * */


        new Thread(() -> {
           try{
               mSocket = SockIOConnection.getSocketInstance();

               JSONObject obj = new JSONObject();
               obj.put("requestId",requestId);
               mSocket.emit("request::join::live-locations", obj , (Ack) args2 -> {
                   JSONObject objRet = (JSONObject)args2[0];
                   Log.d(Constants.TAG, "request::join::live-locations" + objRet);
               });

           }catch (Exception e){
               Log.d(Constants.TAG, "onResume: Exception " + e.getMessage());
               Constants.callErrorPage(this, "onResume: Exception " + e.getMessage());
           }
        }).start();

        listenToChangesOnTrip();

        mBounceAnimation = AnimationUtils.loadAnimation(this,R.anim.bounce_animation);



    }

    @Override
    protected void onPause() {
        super.onPause();


        try {
            JSONObject obj = new JSONObject();
            obj.put("requestId",requestId);
            mSocket.emit("request::leave::live-locations", obj, (Ack) args2 -> {
                JSONObject objRet = (JSONObject)args2[0];
                Log.d(Constants.TAG, "request::join::live-locations" + objRet);
            });
        } catch (JSONException e) {
            Log.d(Constants.TAG, "onPause: JSONException => " + e.getMessage());
            e.printStackTrace();
        }

    }

    /// update UI with the status of the trip ===============

    private void listenToChangesOnTrip(){
        if (requestId == null) return;
       requestListenerReg = db.collection(Collections.REQUEST.toString()).document(requestId).addSnapshotListener((documentSnapshot, e) -> {

            if (e != null){
                Dialogs.alertWithSweet(this, e.getMessage());
                return;
            }

            if (documentSnapshot == null) return;

            Trip trip = documentSnapshot.toObject(Trip.class);

            if (trip == null) return;



            // hide all layouts
            if(fetchTripStatusLayout.isVisible()){
                fetchTripStatusLayout.hide();
            }

            if(searchRiderLayout.isVisible()){
                searchRiderLayout.hide();
            }

            if(riderFoundLayout.isVisible()){
                if (!trip.status.equals(TripStatus.COMPLETED.toString())){
                    riderFoundLayout.hide();
                }
            }

            if(cancelOrderLayout.isVisible()){
                cancelOrderLayout.hide();
            }

            if(ridersAreCurrentlyBusyLayout.isVisible()){
                ridersAreCurrentlyBusyLayout.hide();
            }

//            if(poorConnection.isVisible()){
//                poorConnection.hide();
//            }

            if (tripCancelledByRider.isVisible()){
                tripCancelledByRider.hide();
            }

            if (tripCompleted.isVisible()){
                tripCompleted.hide();
            }

            Log.d(Constants.TAG, "listenToChangesOnTrip: STATUS - " + trip.status);

            this.trip = trip;

            arrival_time_layout.setVisibility(View.GONE);

//            start a background service to monitor the status change,
//           and also fire notifications based on change
           Intent serviceIntent1 = new Intent(TrackTripActivity.this, OnGoingTripIntentService.class);
           serviceIntent1.putExtra(Constants.TRIP, trip);

           try{
               startService(serviceIntent1);
           }catch (Exception e1){
               Log.d(Constants.TAG, "listenToChangesOnTrip: " + e1.getMessage());
           }

            switch (trip.status){

                case "PROCESSING":
                    RiderLocationManager.getInstance().saveTripStatus(this, trip.riderId, TripStatus.PROCESSING);
                    showFindingRiderProgress();
                    searchRiderLayout.show();
                    setSearchPoint(trip);
                    break;
                case "PENDING":
                    RiderLocationManager.getInstance().saveTripStatus(this, trip.riderId, TripStatus.PENDING);
                    showFindingRiderProgress();
                    searchRiderLayout.show();
                    setSearchPoint(trip);
                    break;
                case "ONGOING":
                    showFoundRider();

                    hideFindingRiderProgress();
                    RiderLocationManager.getInstance().saveTripStatus(this, trip.riderId, TripStatus.ONGOING);
                    Intent intent = new Intent(TrackTripActivity.this, ChatService.class);
                    intent.putExtra(Constants.RIDER_ID, trip.riderId);
                    intent.putExtra(Constants.RIDER_NAME, trip.riderName);
                    intent.putExtra(Constants.RIDER_PHONE, trip.riderPhoneNumber);
                    try{
                        startService(intent);
                    }catch (Exception e2){
                        Log.d(Constants.TAG, "listenToChangesOnTrip: " + e2.getMessage());
                    }

                    initRatingDialog(trip);

                    listenToRiderUpdates();

                    break;
                case "NOT_FOUND":
                    hideFindingRiderProgress();
                    RiderLocationManager.getInstance().saveTripStatus(this, trip.riderId, TripStatus.NOT_FOUND);
                    ridersAreCurrentlyBusyLayout.show();
                    RiderLocationManager.getInstance().removeRiderLocation(this,trip.riderId);
                    break;
                case "DELAYED":
                    hideFindingRiderProgress();
                    RiderLocationManager.getInstance().saveTripStatus(this, trip.riderId, TripStatus.NOT_FOUND);
                    RiderLocationManager.getInstance().removeRiderLocation(this,trip.riderId);
                    ridersAreCurrentlyBusyLayout.show();
                    break;
                case "CANCELLED":
                    hideFindingRiderProgress();
                    RiderLocationManager.getInstance().saveTripStatus(this, trip.riderId, TripStatus.CANCELLED);
                    RiderLocationManager.getInstance().removeRiderLocation(this,trip.riderId);
                    tripCancelledByRider.show();
                    break;
                case "CANCELLED_R":
                    hideFindingRiderProgress();
                    RiderLocationManager.getInstance().saveTripStatus(this, trip.riderId, TripStatus.CANCELLED_R);
                    RiderLocationManager.getInstance().removeRiderLocation(this,trip.riderId);
                    tripCancelledByRider.show();
                    break;
                case "COMPLETED":
                    hideFindingRiderProgress();
                    RiderLocationManager.getInstance().saveTripStatus(this, trip.riderId, TripStatus.COMPLETED);
                    RiderLocationManager.getInstance().removeRiderLocation(this,trip.riderId);
                    rateTv.setText(getString(R.string.rate_text, trip.riderName));


                    // show rate rider dialog
                    try{
                        fetchTripStatusLayout.hide();
                        //tripCompleted.show();

                        Intent intent1 = new Intent(this, RateRiderActivity.class);
                        intent1.putExtra(Constants.TRIP, trip);

                        if (loadedRiderImage != null){
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            loadedRiderImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
                            byte[] byteArray = stream.toByteArray();
                            intent1.putExtra(Constants.RIDER_IMAGE,byteArray);
                        }

                        requestListenerReg.remove();
                        if (!trip.rated){
                            try{
                                startActivity(intent1);
                                Bungee.slideUp(this);
                            }catch (Exception e3) {
                                Log.d(Constants.TAG, "listenToChangesOnTrip: " + e3.getMessage());
                            }
                        }else {
                            compeleteTrip();
                        }

//                        initRatingDialog(trip);
//                        if (ratingDialogFragment != null) {
//                            ratingDialogFragment.show(getSupportFragmentManager(), SHOW_RATING);
//                        }

                    }catch (Exception ex){}
                    break;
            }

            if (isFromCheckOut){
                Intent serviceIntent = new Intent();
                serviceIntent.putExtra(Constants.TRIP, trip.requestId);
                TripListenerIntentService.enqueueWork(this,TripListenerIntentService.class, RSS_JOB_ID, serviceIntent);
            }


        });
    }

    private void setSearchPoint(Trip trip) {

        if (trip.pickupsRef.size() > 0){
           db.collection(Collections.PICKUPPOINTS.toString()).document(trip.pickupsRef.get(0)).get().addOnSuccessListener(documentSnapshot -> {
                Pickup pickup = documentSnapshot.toObject(Pickup.class);

                if (pickup == null) return;

                if (map != null){
                    pinLocation = new LatLng(pickup.pos.geopoint.latitude, pickup.pos.geopoint.longitude);
                    CameraPosition cameraPosition = new CameraPosition.Builder().
                            target(pinLocation).
                            zoom(DEFAULT_ZOOM).
                            bearing(0).
                            build();
                    map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }

            });
        }


    }


    private void initRatingDialog(Trip trip) {
        ratingDialogFragment = RatingDialogFragment.newInstance(this.loadedRiderImage,trip);
        ratingDialogFragment.setOnRatedListener(new RatingDialogFragment.OnRatedListener() {
            @Override
            public void onRated(double mark, String message) {

                new Thread(() -> {
                    runOnUiThread(() -> {
                        LoadingGif.show(TrackTripActivity.this);
                    });
                    db.collection("DRIVERS").document(trip.riderId).get().addOnSuccessListener(documentSnapshot -> {
                        runOnUiThread(LoadingGif::hide);

                        Driver driver = documentSnapshot.toObject(Driver.class);


                        if (driver == null) return;


                        if (mark != 0.0){
                            double newSumOfVotes = driver.sumOfVotes + mark;
                            double newTotalVotes = driver.totalNoOfVotes + 1;

                            double newAverateRating =  newSumOfVotes / newTotalVotes;


                            HashMap<String, Object> updatedValues = new HashMap<>();
                            updatedValues.put("sumOfVotes", newSumOfVotes);
                            updatedValues.put("totalNoOfVotes", newTotalVotes);
                            updatedValues.put("averageRating", newAverateRating);

                            /// insert into back into the rider rating
                            db.collection("DRIVERS").document(trip.riderId).update(updatedValues);

                            if (!"".equals(message)){
                                UserDriverComment userDriverComment = new UserDriverComment();
                                userDriverComment.userId = firebaseUser.getUid();
                                userDriverComment.driverId = trip.riderId;
                                userDriverComment.comment = message;

                                /// write into comments
                                db.collection("DRIVER_COMMENTS").add(userDriverComment);
                            }

                            runOnUiThread(() -> {
                                average_rating.setText(String.format(Locale.getDefault(), "%.2f", newAverateRating));
                                Log.d(Constants.TAG, "onRated: completed trip status - " + trip.status);
                                Toast.makeText(TrackTripActivity.this, "Thanks for rating", Toast.LENGTH_SHORT).show();
                                if (!trip.status.equals(TripStatus.ONGOING.toString())){
                                    compeleteTrip();
                                }
                            });
                        }



                    }).addOnFailureListener(e -> {
                        runOnUiThread(() -> {
                            LoadingGif.hide();
                            Toast.makeText(TrackTripActivity.this, "Thanks for rating", Toast.LENGTH_SHORT).show();
                            if (!trip.status.equals(TripStatus.ONGOING.toString())){
                                compeleteTrip();
                            }
                        });
                        Log.d(Constants.TAG, "initRatingDialog: addOnFailureListener " + e.getMessage());

                    });
                }).start();
            }

            @Override
            public void onCancelled() {
                runOnUiThread(() -> {
                    if (!trip.status.equals(TripStatus.ONGOING.toString())){
                        compeleteTrip();
                    }
                });
            }
        });

    }


    private String removeLastCharacter(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == 'x') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home){
            backPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void backPressed() {

        if (isFromCheckOut){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            Bungee.slideRight(this);
            return;
        }

        if (trip.status.equals(TripStatus.COMPLETED.toString())){
            compeleteTrip();
            return;
        }

        super.onBackPressed();
        Bungee.slideRight(this);
    }

    private void compeleteTrip(){
        try {
            startActivity(new Intent(this, MainActivity.class));
            Bungee.slideRight(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initLayouts() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
//            replace the home (back) button with the menu icon
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
//            set the Home / Back Button
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

        setTitle("");

        /*
        *   Fetch trip status ================
        * */
        fetchTripStatusLayout = MSlideUp.controlView(findViewById(R.id.fetch_trip_status));
        ShimmerFrameLayout mShimmerViewContainerFetchTripStatus = findViewById(R.id.shimmer_view_container_fetch_status);
        mShimmerViewContainerFetchTripStatus.startShimmerAnimation();
        fetchTripStatusLayout.show();
        /*
        *   End of fetch trip status ==============
        * */

        /*
         *  Looking for rider  (Search rider) =================================
         *
         * */

        searchRiderLayout = MSlideUp.controlView(findViewById(R.id.looking_for_rider));

        search_rider = findViewById(R.id.search_rider);

//        mHandler = new Handler();
//        startRepeatingTask();
        //searchRiderLayout.show();

        findViewById(R.id.cancel_before_rider_found).setOnClickListener(view -> {
           // hideFindingRiderProgress();
//            slidingUpPanelLayout.setPanelHeight(screenHeight/2);

            cancelOrderLayout.show();
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

        });

        /*
         *   End of search rider ############################################
         * */

        /*
         *  Rider found form =============================
         * */
        riderFoundLayout = MSlideUp.controlView(findViewById(R.id.rider_found_layout));
        riderNameTv = findViewById(R.id.rider_name);
        riderImage = findViewById(R.id.rider_avatar);
        riderNameTv.setAllCaps(true);
        arrival_time_layout = findViewById(R.id.arrival_time);
        arrival_time = findViewById(R.id.time);
        arrival_time_layout.setVisibility(View.GONE);
        completingNearByTrip = findViewById(R.id.completing_trip);
        completingNearByTrip.setVisibility(View.GONE);

        general_progress_bar = findViewById(R.id.general_progress_bar);
        general_progress_bar.setVisibility(View.GONE);
        if (trip != null && trip.status.equals(TripStatus.ONGOING.toString())){
            general_progress_bar.setVisibility(View.VISIBLE);
        }

        average_rating = findViewById(R.id.average_rating);
        average_rating.setVisibility(View.GONE);

        riderImage.setOnClickListener(view -> {
            if (loadedRiderImage != null) {
                ImageZoomFragment.newInstance(loadedRiderImage).show(getSupportFragmentManager(), ZOOM_IMAGE);

            }

        });

        riderNameTv.setOnClickListener(view -> {
            if (trip != null){
                initRatingDialog(trip);
                if (ratingDialogFragment != null){
                    ratingDialogFragment.show(getSupportFragmentManager(),SHOW_RATING);
                }
            }

        });

        riderPlateTv = findViewById(R.id.rider_plate);
        orderNoTv = findViewById(R.id.order_number_label);

        pickupRecyclerView = findViewById(R.id.pick_up_recycler_view);
        pickupRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        pickupRecyclerView.setHasFixedSize(true);

        pickupRecyclerView.setAdapter(onGoingPickUpAdapter);

        destinationRecyclerView = findViewById(R.id.destination_recycler_view);
        destinationRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        destinationRecyclerView.setHasFixedSize(true);

        destinationRecyclerView.setAdapter(onGoingDestinationAdapter);

//        call the rider
        findViewById(R.id.call_rider).setOnClickListener(view -> {

            proceedWithCall(true);

//            if (!isPermissionGranted()){
//
//                // tell user you'll be reading sms,
//
//                Dialogs.confirmWithSweet(this)
//                        .setTitleText("Grant Permission")
//                        .setContentText("The app would want to place a call on your behalf")
//                        .setCancelText("No")
//                        .setCancelClickListener(sDialog -> {
//                            sDialog.dismiss();
//
//                           // Toast.makeText(this, "Phone verification will not be automated", Toast.LENGTH_SHORT).show();
//                            proceedWithCall(false);
//
//                        }).setConfirmClickListener(sweetAlertDialog -> {
//                    sweetAlertDialog.dismiss();
//                    askPermission();
//                }).show();
//
//
//            }else{
//                proceedWithCall(true);
//            }

        });

//        message the rider

        findViewById(R.id.message_rider).setOnClickListener(view -> {
           Intent intent = new Intent(this, ChatActivity.class);
           intent.putExtra(Constants.RIDER_ID, trip.riderId) ;
           intent.putExtra(Constants.RIDER_NAME, trip.riderName) ;
           intent.putExtra(Constants.RIDER_PHONE, trip.riderPhoneNumber) ;
           intent.putExtra(Constants.Rider_PLATE, trip.riderPlateNumber) ;
            startActivity(intent);
            Bungee.slideLeft(this);
        });

        findViewById(R.id.cancel_after_rider_found).setOnClickListener(view -> {

            int newHeight = screenHeight / 2;
//            slidingUpPanelLayout.setPanelHeight(newHeight);
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            cancelOrderLayout.show();
        });
        /*
         * End of rider found form ===================================
         * */

        /*
         *   Cancel Form initialization ####################
         * */

        cancelOrderLayout = MSlideUp.controlView(findViewById(R.id.cancel_order_layout));
        RecyclerView recyclerViewReasonsList = findViewById(R.id.recyclerView_reasons_list);
        recyclerViewReasonsList.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewReasonsList.setHasFixedSize(true);
        recyclerViewReasonsList.setAdapter(cancelOrderReasonsAdapter);

        findViewById(R.id.closeBtn).setOnClickListener(view -> {
            cancelOrderLayout.hide();
        });

        findViewById(R.id.do_not_cancel_order).setOnClickListener(view -> {
            cancelOrderLayout.hide();
        });

        findViewById(R.id.yes_cancel_order).setOnClickListener(view -> {


           initiateTripCancellation();

//            refreshState();
            // todo hide all show forms and move to pick up edit text
        });
        findViewById(R.id.complete_trip_after_cancelled).setOnClickListener(view -> {
            super.onBackPressed();
            Bungee.slideRight(this);
        });
        /*
         *   End of cancel order  ==========================
         * */

        /*
         *   Riders are currently busy ==================
         * */
        ridersAreCurrentlyBusyLayout = MSlideUp.controlView(findViewById(R.id.riders_currently_busy));
        findViewById(R.id.cancelTrip).setOnClickListener(view -> {
           initiateTripCancellation();
        });
        findViewById(R.id.requestAgain).setOnClickListener(view -> {
            NotificationHelper.newInstance(TrackTripActivity.this).createNotification("Request Booked", "A rider will contact you shortly", null);
            backPressed();
        });
        /*
         *   End of riders are currently busy ---------
         * */

        /*
         *   Poor Connection =========
         * */
        //poorConnection = MSlideUp.controlView(findViewById(R.id.poor_connection));
        /*
         *   End of Poor Connection
         * */

        /*
        *   trip cancelled by rider
        * */

        tripCancelledByRider = MSlideUp.controlView(findViewById(R.id.trip_cancelled_by_rider));

        /*
        *   End of trip cancelled by rider
        * */


        /*
         *   trip completed
         * */

        tripCompleted = MSlideUp.controlView(findViewById(R.id.trip_completed));
        findViewById(R.id.trip_completed_btn).setOnClickListener(view -> {
           startActivity(new Intent(this, MainActivity.class));
           Bungee.slideRight(this);
        });
        findViewById(R.id.rate_rider_btn).setOnClickListener(view -> {
            if (ratingDialogFragment != null)
                 ratingDialogFragment.show(getSupportFragmentManager(), SHOW_RATING);
        });

       rateTv = findViewById(R.id.rate_text);

        /*
         *   End of trip completed
         * */

        progressBarFindingRider = findViewById(R.id.spin_kit);
        findRiderDim = findViewById(R.id.finding_rider_dim);


       // (new Handler()).postDelayed(this::showFoundRider, 5000);
    }

    private void initiateTripCancellation() {

        JSONObject obj = new JSONObject();
        try {

            StringBuilder sb = new StringBuilder();
            List<String> stringList = cancelOrderReasonsAdapter.getSelectedCancellationReasons();
            for (String reason : stringList){
                sb.append(reason);
                sb.append("|");
            }

            String stringifiedReasons = sb.toString();


            // List<String> stringList = cancelOrderReasonsAdapter.getSelectedCancellationReasons();
            obj.put("requestId", requestId);
            obj.put("customerId", firebaseUser.getUid());
            obj.put("reasons", stringList.size() > 0 ? removeLastCharacter(stringifiedReasons) : "");
            obj.put("customerName", applicaitonUser.fullName);
            obj.put("customerPhoneNumber", applicaitonUser.phoneNumber);
            obj.put("isAccepted", trip != null && "ONGOING".equals(trip.status));

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    LoadingGif.show(TrackTripActivity.this);
                }
            });

            if (mSocket == null) return;

            mSocket.emit("request::cancelled", obj, (Ack) args -> {
                JSONObject objRet = (JSONObject)args[0];
                Log.d(Constants.TAG, "call: server received it - " + objRet);
            });

            mSocket.on("request::cancelled::done", args -> {
                // JSONObject obj2 = (JSONObject) args[0];

                LoadingGif.hide();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(TrackTripActivity.this, "Your request has been cancelled", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(TrackTripActivity.this, MainActivity.class));
                        Bungee.slideRight(TrackTripActivity.this);
                    }
                });
                // resend request reference

                //runOnUiThread(() -> Toast.makeText(TestActivity.this, "" + obj, Toast.LENGTH_SHORT).show());
            });




        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initObjects(){

        setScreenHeight();
        slidingUpPanelLayout = findViewById(R.id.sliding_layout);
        int defaultPanelHeight = (screenHeight)/2; // increase denominator for panel to move down
        Log.d(Constants.TAG, "defaultPanelHeight = " + defaultPanelHeight);
//        slidingUpPanelLayout.setPanelHeight(defaultPanelHeight);
        // tripPosition = (int) getIntent().getSerializableExtra("tripId");
        // if (tripPosition < 0) finish();
        requestMapPermission();
    }

    private void initMap() {
        MapFragment mMapFragment = MapFragment.newInstance();
        FragmentTransaction fragmentTransaction =
                getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.container, mMapFragment);
        fragmentTransaction.commit();

        mMapFragment.getMapAsync(this);


    }

    @Override
    public void onBackPressed() {
       backPressed();
    }

    private void showFindingRiderProgress(){

        if (pickupArrayList.size() > 0){
            Pickup pickup = pickupArrayList.get(0);
            CameraPosition cameraPosition = new CameraPosition.Builder().
                    target(new LatLng(pickup.pos.geopoint.latitude, pickup.pos.geopoint.longitude)).
                    zoom(DEFAULT_ZOOM).
                    bearing(0).
                    build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }

        findRiderDim.setVisibility(View.VISIBLE);
        progressBarFindingRider.setVisibility(View.VISIBLE);
        search_rider.setVisibility(View.VISIBLE);

        int delay = 2000; // delay for 2 sec.
        int period = 4000; // repeat every 4 sec.
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask()
        {
            public void run()
            {
                if (trip.status.equals(TripStatus.PENDING.toString()) || trip.status.equals(TripStatus.PROCESSING.toString())){
                    //Log.d(Constants.TAG, "onRepeat:  called" );
                    zoomValue = zoomValue - ((float) 0.1);
                    if (zoomValue <= 10){
                        zoomValue = DEFAULT_ZOOM;
                        return;
                    }

                    runOnUiThread(() -> {
                       // Log.d(Constants.TAG, "zoomValue: =>  " + zoomValue);
                        CameraPosition cameraPosition = new CameraPosition.Builder().
                                target(pinLocation).
                                zoom(zoomValue).
                                bearing(0).
                                build();
                        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    });
                }else{
                    zoomValue = DEFAULT_ZOOM;
                }

            }
        }, delay, period);

        YoYo.with(Techniques.BounceIn)
                .duration(2000)
                .repeat(10)
                .playOn(search_rider);



    }


    private void hideFindingRiderProgress(){

        findRiderDim.setVisibility(View.GONE);
        progressBarFindingRider.setVisibility(View.GONE);
        search_rider.setVisibility(View.GONE);

    }

    private void setScreenHeight(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenHeight = displayMetrics.heightPixels;
    }

    private void refreshState() {
//        LoadingGif.show(this);
        finish();
        startActivity(getIntent());
//        LoadingGif.hide();
    }

    private void showFoundRider() {
//        Toast.makeText(this, "Rider found", Toast.LENGTH_SHORT).show();
//        searchRiderLayout.hide();
//        int newHeight = screenHeight / 2;
//        slidingUpPanelLayout.setPanelHeight(newHeight);
//        slidingUpPanelLayout.setOverlayed(true);
//        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
       // hideFindingRiderProgress();

        riderNameTv.setText(trip.riderName);
        riderPlateTv.setText(trip.riderPlateNumber);

        orderNoTv.setText(getString(R.string.order_number, trip.orderNumber));

        if (trip != null && trip.status.equals(TripStatus.ONGOING.toString())){
            general_progress_bar.setVisibility(View.VISIBLE);
        }

        // listen to the pick ups and destinations
        db.collection(Collections.PICKUPPOINTS.toString()).whereEqualTo("requestId", trip.requestId)
            .get().addOnSuccessListener(queryDocumentSnapshots -> {

                pickupArrayList.clear();

                List<Pickup> pickupList =  queryDocumentSnapshots.toObjects(Pickup.class);
                pickupArrayList.addAll(pickupList);
                onGoingPickUpAdapter.notifyDataSetChanged();


        });

        // real time listening
        db.collection(Collections.PICKUPPOINTS.toString()).whereEqualTo("requestId", trip.requestId)
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null) return;

                    if (queryDocumentSnapshots == null) return;

                    pickupArrayList.clear();

                    List<Pickup> pickupList =  queryDocumentSnapshots.toObjects(Pickup.class);
                    pickupArrayList.addAll(pickupList);
                    onGoingPickUpAdapter.notifyDataSetChanged();

         });


        db.collection(Collections.DESTINATIONS.toString()).whereEqualTo("requestId", trip.requestId)
                .get().addOnSuccessListener(queryDocumentSnapshots -> {

            List<Destination> destinationsList =  queryDocumentSnapshots.toObjects(Destination.class);

            destinationArrayList.clear();

            destinationArrayList.addAll(destinationsList);
            onGoingDestinationAdapter.notifyDataSetChanged();

        });

        // real time listening to the server
        db.collection(Collections.DESTINATIONS.toString()).whereEqualTo("requestId", trip.requestId)
               .addSnapshotListener((queryDocumentSnapshots, e) -> {
                   if (e != null) return;

                   if (queryDocumentSnapshots == null) return;

                   List<Destination> pickupList =  queryDocumentSnapshots.toObjects(Destination.class);

                   destinationArrayList.clear();

                   destinationArrayList.addAll(pickupList);
                   onGoingDestinationAdapter.notifyDataSetChanged();
               });

        // get rider pick and ratings

        db.collection("DRIVERS").document(trip.riderId).get().addOnSuccessListener(documentSnapshot -> {
            Driver driver = documentSnapshot.toObject(Driver.class);

            if (driver == null) return;

            average_rating.setVisibility(View.VISIBLE);
            average_rating.setText(String.format(Locale.getDefault(), "%.2f", driver.averageRating));
        });



        int newHeight = screenHeight / 2;
        slidingUpPanelLayout.setPanelHeight(newHeight);
        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);


        riderFoundLayout.show();

        // Load image, decode it to Bitmap and return Bitmap to callback

        if (trip.riderImage != null){

            imageLoader.loadImage(trip.riderImage, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    // Do whatever you want with Bitmap
                    if (loadedImage != null){
                       TrackTripActivity. this.loadedRiderImage = loadedImage;
                        riderImage.setImageBitmap(loadedImage);
                    }
                }
            });
        }


    }
    

    @Override
    public void onMapReady(GoogleMap googleMap) {

        Log.d(Constants.TAG, "onMapReady: TrackTrip ");

        map = googleMap;
        googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        this, R.raw.style_json));


        pinLocation = new LatLng(5.5912045, -0.2497703);
        CameraPosition cameraPosition = new CameraPosition.Builder().
                target(pinLocation).
                zoom(DEFAULT_ZOOM).
                bearing(0).
                build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


        if (RiderLocationManager.getInstance().getTripStatus(this, trip.riderId) == TripStatus.ONGOING){
            Pos.PosLatLng posLatLng = RiderLocationManager.getInstance().getRiderLastLocation(this, trip.riderId);
            if (posLatLng == null)return;

            LatLng newPosition = new LatLng(posLatLng.latitude, posLatLng.longitude);

            if (riderMarker == null){
                riderMarker = map.addMarker(new MarkerOptions()
                        .draggable(true)
                        .position(newPosition)
                        .icon(BitmapDescriptorFactory.fromBitmap(Constants.resizeDrawable(TrackTripActivity.this, R.drawable.rider_icon)))
                        .title(trip.riderName)
                        .snippet(trip.riderPlateNumber)
                );

            }


            CameraPosition cameraPosition1 = new CameraPosition.Builder().
                    target(newPosition).
                    zoom(DEFAULT_ZOOM).
                    bearing(0).
                    build();
            // track rider
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition1));

            //animateMarker(riderMarker, newPosition, false, cameraUpdate);
            listenToRiderUpdates();
        }


    }

    private void listenToRiderUpdates(){
        if (map != null){
            //todo change the event name, when it goes live
            mSocket.on("request::trip-ongoing::customer", args -> {
                JSONObject obj2 = (JSONObject) args[0];

                Log.d(Constants.TAG, " rider location " + obj2);


                runOnUiThread(() -> {
                    try {

                        general_progress_bar.setVisibility(View.GONE);

                        if(!requestId.equals(obj2.getString("requestId"))){
                            return;
                        }


                        LatLng newPosition = new LatLng(obj2.getDouble("latitude"), obj2.getDouble("longitude"));
                        // Log.d(Constants.TAG, "onMapReady: emitting - lat : " + newPosition.latitude + " long: " + newPosition.longitude);

                        RiderLocationManager.getInstance().addRiderLocation(this, trip.riderId, new Pos.PosLatLng(newPosition.latitude, newPosition.longitude));

                        if (RiderLocationManager.getInstance().getTripStatus(this, trip.riderId) != TripStatus.ONGOING){
                            return;
                        }

                        Log.d(Constants.TAG, "onMapReady: rider emmited - " + obj2);

                        double targetLong = obj2.getDouble("targetLongitude");
                        double targetLat = obj2.getDouble("targetLatitude");
                        boolean isHeadingToPickup = obj2.getBoolean("tripType");
                        String tripTimeUpdate = obj2.getString("triptimeUpdate");
                        boolean isTapped = obj2.getBoolean("isTapped");
                        boolean isFull = false;
                        String senderId = null;
                        if(obj2.has("isFull") && obj2.has("customerId")){
                           try {
                               isFull = obj2.getBoolean("isFull");
                               senderId = obj2.getString("customerId");
                           }catch (Exception e){

                           }
                        }

                        if (!isTapped){
                            if (riderMarker == null){

                                // CustomMarkerInfoWindowView markerWindowView = new CustomMarkerInfoWindowView(getLayoutInflater());

                                Log.d(Constants.TAG, "onMapReady: tripTimeUpdate " + tripTimeUpdate );

                                riderMarker = map.addMarker(new MarkerOptions()
                                        .draggable(true)
                                        .position(newPosition)
                                        .icon(BitmapDescriptorFactory.fromBitmap(Constants.resizeDrawable(TrackTripActivity.this, R.drawable.rider_icon)))
                                        .title(trip.riderName)
                                        .snippet(tripTimeUpdate != null && !"calculating...".equals(tripTimeUpdate) ? "Arrives in " + tripTimeUpdate : trip.riderPlateNumber)
                                );

                            }

                            if(isFull && senderId != null && senderId.equals(FirebaseAuth.getInstance().getUid())){
                                completingNearByTrip.setVisibility(View.VISIBLE);
                            }else{
                                completingNearByTrip.setVisibility(View.GONE);
                            }

                            riderMarker.setSnippet(tripTimeUpdate != null && !"calculating...".equals(tripTimeUpdate) ? "Arrives in " + tripTimeUpdate : trip.riderPlateNumber);

                            general_progress_bar.setVisibility(View.VISIBLE);


                            CameraPosition cameraPosition1 = new CameraPosition.Builder().
                                    target(newPosition).
                                    zoom(DEFAULT_ZOOM).
                                    bearing(0).
                                    build();
                            // track rider
                            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition1));

                            findViewById(R.id.cancel_after_rider_found).setVisibility(View.VISIBLE);

                        }else{

                            findViewById(R.id.cancel_after_rider_found).setVisibility(View.GONE);

                            String place = obj2.getString("place");
                            if ("the location".equals(place)){
                                place = "";
                            }

                            if (targetLat != mTargetLat || targetLong != mTargetLong){
                                // draw route

                                mTargetLat = targetLat;
                                mTargetLong = targetLong;

                                drawRoute(newPosition, new LatLng(mTargetLat, mTargetLong));


                                if (destMarker != null)
                                    destMarker.remove();

                                destMarker = map.addMarker(new MarkerOptions()
                                        .draggable(true)
                                        .position(new LatLng(mTargetLat, mTargetLong))
                                        .icon(BitmapDescriptorFactory.fromBitmap(Constants.resizeDrawable(TrackTripActivity.this, isHeadingToPickup ? R.drawable.pick_up_single : R.drawable.delivery_raw )))
                                        .title("Destination")
                                        .snippet(place)
                                );

                            }

                            CameraUpdate cameraUpdate = getBounds(newPosition, new LatLng(mTargetLat, mTargetLong));

//                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
//                                newPosition, DEFAULT_ZOOM);

                            map.animateCamera(cameraUpdate);



                            if (RiderLocationManager.getInstance().getTripStatus(this, trip.riderId) == TripStatus.ONGOING){
                                if (riderMarker == null){

                                    // CustomMarkerInfoWindowView markerWindowView = new CustomMarkerInfoWindowView(getLayoutInflater());

                                    Log.d(Constants.TAG, "onMapReady: tripTimeUpdate " + tripTimeUpdate );

                                    riderMarker = map.addMarker(new MarkerOptions()
                                            .draggable(true)
                                            .position(newPosition)
                                            .icon(BitmapDescriptorFactory.fromBitmap(Constants.resizeDrawable(TrackTripActivity.this, R.drawable.rider_icon)))
                                            .title(trip.riderName)
                                            .snippet(tripTimeUpdate != null && !"calculating...".equals(tripTimeUpdate) ? "Arrives in " + tripTimeUpdate : trip.riderPlateNumber)
                                    );

                                }



//                            if (customInfoWindow == null){
//                                customInfoWindow = new CustomMarkerInfoWindowView(getLayoutInflater());
//                                map.setInfoWindowAdapter(customInfoWindow);
//                            }

//                            Marker m = map.get;
//                            if (m != null && !m.isCluster()) {
//                                m.showInfoWindow();
//                            }
//                            InfoWindowData info = new InfoWindowData();
//                            info.arrivalText = tripTimeUpdate != null && !"calculating...".equals(tripTimeUpdate) ? "Arrives in " + tripTimeUpdate : trip.riderPlateNumber;

                                Log.d(Constants.TAG, "onMapReady: tripTimeUpdate - " + tripTimeUpdate);
                                riderMarker.setSnippet(tripTimeUpdate != null && !"calculating...".equals(tripTimeUpdate) ? "Arrives in " + tripTimeUpdate : trip.riderPlateNumber);
                                //riderMarker.showInfoWindow();
                                if (tripTimeUpdate != null && !"calculating...".equals(tripTimeUpdate)){
                                    arrival_time.setText(tripTimeUpdate);
                                    arrival_time_layout.setVisibility(View.VISIBLE);
                                    YoYo.with(Techniques.Pulse)
                                            .duration(2000)
                                            .repeat(10)
                                            .playOn(arrival_time_layout);
                                }

                                /// show trip update on snippet


                                // track rider

                                if(isFull && senderId != null && senderId.equals(FirebaseAuth.getInstance().getUid())){
                                    completingNearByTrip.setVisibility(View.VISIBLE);
                                }else{
                                    completingNearByTrip.setVisibility(View.GONE);
                                }


                                animateMarker(riderMarker, newPosition, false, cameraUpdate);

                                //                        track the rider
                                // trackRider();

                            }

                        }




//                        riderMarker.setPosition(newPosition);
//                        map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition1));

                    } catch (JSONException e) {
                        Log.d(Constants.TAG, "onMapReady:  JSONException " + e.getMessage());
                        e.printStackTrace();
                    }


                });


                // resend request reference
                //mSocket.emit("request::created", obj);

                //runOnUiThread(() -> Toast.makeText(TestActivity.this, "" + obj, Toast.LENGTH_SHORT).show());
            });
        }
    }

    private CameraUpdate getBounds(LatLng src, LatLng dest){

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(src);
        builder.include(dest);
        LatLngBounds bounds = builder.build();
        Point displaySize = new Point();
        getWindowManager().getDefaultDisplay().getSize(displaySize);

        return CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, screenHeight / 2, 200);

    }



    /*
     *   Trip permissions from user //////////////////////////////////////////////
     * */
    private void requestMapPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int checkCallPhonePermission = ContextCompat.checkSelfPermission(TrackTripActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);

            if (checkCallPhonePermission != PackageManager.PERMISSION_GRANTED) {
                //Without the permission to Write, to apply for the permission to Read and Write, the system will pop up the permission itemCategoriesDialog
                ActivityCompat.requestPermissions(this, MY_PERMISSIONS, REQUEST_PERM_CODE);
            } else {
                initMap();
            }
        } else {
            initMap();
        }
    }


    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hideMarker, CameraUpdate cameraUpdate) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = map.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));


//                LatLng coordinate = new LatLng(lat, lng); //Store these lat lng values somewhere. These should be constant.
//                CameraUpdateFactory.newLatLngZoom(
//                        coordinate, 18.0f);

//                CameraPosition cameraPosition = new CameraPosition.Builder().
//                        target(toPosition).
//                        zoom(DEFAULT_ZOOM).
//                        bearing(0).
//                        build();

               // map.animateCamera(cameraPosition);
                map.animateCamera(cameraUpdate);

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {

                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }


    private void drawRoute(LatLng origin, LatLng destination){

        Log.d(Constants.TAG, "drawRoute: origin - " + origin);
        Log.d(Constants.TAG, "drawRoute: destination - " + destination);

        GoogleDirection.withServerKey(getResources().getString(R.string.the_google_server_key))
                .from(origin)
                .to(destination)
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        if(direction.isOK()) {
                            // Do something
                            Log.d(Constants.TAG, "onDirectionSuccess: OK " + rawBody);

                            Route route = direction.getRouteList().get(0);
                            Leg leg = route.getLegList().get(0);

                            ArrayList<LatLng> directionPositionList = leg.getDirectionPoint();
//                            PolylineOptions polylineOptions = DirectionConverter.createPolyline(MainActivity.this, directionPositionList, 5, Color.BLACK);
////                            map.addPolyline(polylineOptions);
                            startAnim(directionPositionList);

                        } else {
                            // Do something
                            Log.d(Constants.TAG, "onDirectionFailure: NOt OK" + rawBody);
                        }
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {
                        // Do something
                        Log.d(Constants.TAG, "onDirectionFailure: " + t);
                    }
                });

    }

    private void startAnim(List<LatLng> bangaloreRoute){
        if(map != null) {
            MapAnimator.getInstance().animateRoute(map, bangaloreRoute);
        } else {
            Toast.makeText(getApplicationContext(), "Map not ready", Toast.LENGTH_LONG).show();
        }
    }



    private boolean isPermissionGranted(){
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED);
    }

    //    ask permission
//    private void askPermission(){
//
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case Constants.MY_PERMISSIONS_REQUEST_CALL:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    proceedWithCall(true);

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    proceedWithCall(false);
                }
                break;
        }
    }

    private void proceedWithCall(boolean shouldProceedWithCall){

        if (shouldProceedWithCall){

            try {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+ trip.riderPhoneNumber));
                startActivity(intent);

            }catch (Exception e){

            }
        }
    }




    /*
    *   Interpolate marker
    * */


    public static void animateMarkerTo(final Marker marker, final LatLng finalPosition) {
        // Use the appropriate implementation per API Level
        animateMarkerToICS(marker, finalPosition);
    }




    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private static void animateMarkerToICS(Marker marker, LatLng finalPosition) {
        final LatLngInterpolator latLngInterpolator = new LatLngInterpolator.Linear();
        TypeEvaluator<LatLng> typeEvaluator = new TypeEvaluator<LatLng>() {
            @Override
            public LatLng evaluate(float fraction, LatLng startValue, LatLng endValue) {
                return latLngInterpolator.interpolate(fraction, startValue, endValue);
            }
        };
        Property<Marker, LatLng> property = Property.of(Marker.class, LatLng.class, "position");
        ObjectAnimator animator = ObjectAnimator
                .ofObject(marker, property, typeEvaluator, finalPosition);
        animator.setDuration(3000);
        animator.start();
    }

    /**
     * For other LatLngInterpolator interpolators, see https://gist.github.com/broady/6314689
     */
    interface LatLngInterpolator {

        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class Linear implements LatLngInterpolator {

            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;

                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }



}
