package com.delivery.enroute.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.delivery.enroute.R;
import com.delivery.enroute.activities.wallet.PayWithMomoActivity;
import com.delivery.enroute.adapters.PaymentOptionsAdapter;
import com.delivery.enroute.adapters.PaymodeAdapter;
import com.delivery.enroute.enums.Collections;
import com.delivery.enroute.enums.TransactionType;
import com.delivery.enroute.models.Payment;
import com.delivery.enroute.models.Settings;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.models.User;
import com.delivery.enroute.singletons.ApplicaitonUser;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.Dialogs;
import com.delivery.enroute.utils.LoadingGif;
import com.delivery.enroute.utils.MSlideUp;
import com.delivery.enroute.utils.volley.RequestObject;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.florent37.viewanimator.ViewAnimator;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.mancj.slideup.SlideUp;

import org.json.JSONException;

import java.util.HashMap;
import java.util.List;

import spencerstudios.com.bungeelib.Bungee;

public class ReceivePaymentActivity extends AppCompatActivity {

    ListView listView;
    PaymodeAdapter adapter;
    ApplicaitonUser applicaitonUser;
    TextView description, amountToPay;
    EditText chooseNetworkEt, phoneNumber, amount, vodafoneAuthToken;
    SlideUp chooseNetworkPicker,choosePaymentOption, paymentSuccessLayout;
    private int mSelectedNetworkPosition;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    RecyclerView payment_option_list;
    Button paymentDone;
    ImageView iconSuccess;
    Trip trip;
    PaymentOptionsAdapter paymentOptionsAdapter;
    private FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    ShimmerFrameLayout shimmerFrameLayout;
    View paymentOverview;
    TextView costOfTrip, transactionFee;
    private String totalAmountToPay = "0.0";

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_payment);

        applicaitonUser = ApplicaitonUser.getInstance(this);

        trip = Constants.TEMPORAL_TRIP_HOLDER;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
//            replace the home (back) button with the menu icon
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
//            set the Home / Back Button
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }


        paymentOverview  = findViewById(R.id.payment_overview);
        paymentOverview.setVisibility(View.GONE);

        TextView tripLoc = findViewById(R.id.trip_loc);
        tripLoc.setText(getAbbrFromTrip(trip));

        shimmerFrameLayout = findViewById(R.id.shimmer_view_container);
        shimmerFrameLayout.setVisibility(View.VISIBLE);
        shimmerFrameLayout.startShimmerAnimation();

        costOfTrip = findViewById(R.id.cost_of_trip);
        transactionFee = findViewById(R.id.transaction_fee);

        fetchFromServer();

        phoneNumber = findViewById(R.id.phone_number);
        phoneNumber.setText(applicaitonUser.phoneNumber);
        vodafoneAuthToken = findViewById(R.id.vodafone_auth_token);
        vodafoneAuthToken.setVisibility(View.GONE);

        amount =  findViewById(R.id.cash_amount);
        //amount.setText(String.valueOf(trip.actualCost));
        amount.setEnabled(false);

        description = findViewById(R.id.description);
        description.setText(Constants.fullPickupsDestinations(this, trip));

        RelativeLayout pickerLayout = findViewById(R.id.choose_network_layout);
//        int preferredHeight = screenHeight / 3;
//        pickerLayout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, preferredHeight));
        chooseNetworkPicker = MSlideUp.controlView(pickerLayout);

        chooseNetworkEt = findViewById(R.id.etChooseNetwork);
        chooseNetworkEt.setOnClickListener(view -> chooseNetworkPicker.show());

        findViewById(R.id.up).setOnClickListener(scrollUp());
        findViewById(R.id.down).setOnClickListener(scrollDown());

        listView = findViewById(R.id.listView_select_pay_mode);

        paymentSuccessLayout = MSlideUp.controlView(findViewById(R.id.payment_success_layout));
        choosePaymentOption = MSlideUp.controlView(findViewById(R.id.choose_payment_option));

        amountToPay = findViewById(R.id.amount_to_pay);
//        amountToPay.setText((getString(R.string.amount, String.valueOf(trip.actualCost))));
        choosePaymentOption.show();
        iconSuccess = findViewById(R.id.icon);
        paymentDone = findViewById(R.id.payment_done);
        paymentDone.setOnClickListener(view -> {
            paymentDone.setVisibility(View.GONE);
            ViewAnimator.animate(iconSuccess).bounceOut().duration(2000).onStop(() -> {
                ReceivePaymentActivity.super.onBackPressed();
                Bungee.slideRight(ReceivePaymentActivity.this);
                //paymentSuccessLayout.hide();
            }).start();
        });

        adapter = new PaymodeAdapter();
        listView.setAdapter(adapter);

        payment_option_list = findViewById(R.id.payment_option_list);
        payment_option_list.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        payment_option_list.setHasFixedSize(true);

        paymentOptionsAdapter = new PaymentOptionsAdapter(row -> {
            switch (row.paymentTypes){
                case MOMO:
                    choosePaymentOption.hide();
                    paymentSuccessLayout.hide();
                    break;
                case WALLET:
                    Dialogs.confirmWithSweet(this).setContentText("You're about to pay with wallet")
                            .setTitleText("Confirm")
                            .setConfirmText("continue")
                            .setCancelText("cancel")
                            .setConfirmClickListener(sweetAlertDialog -> {
                                sweetAlertDialog.dismiss();
                                payWithWallet();

                            }).show();

                    break;
                default:
                    Toast.makeText(this, "Invalid selection", Toast.LENGTH_SHORT).show();
                    break;
            }
        });
        //paymentOptionsAdapter.setShowLabel(false);
        payment_option_list.setAdapter(paymentOptionsAdapter); // put payment options adapter here


        listView.setOnItemClickListener((adapterView, view, i, l) -> {

            mSelectedNetworkPosition = i;
            adapter.setmSelectedItem(i);
            adapter.notifyDataSetChanged();

            applySelection();


        });

        findViewById(R.id.closeBtn).setOnClickListener(view -> {
            super.onBackPressed();
            Bungee.slideRight(this);
        });

        findViewById(R.id.pay_now).setOnClickListener(view -> {

            if (TextUtils.isEmpty(phoneNumber.getText())){
                phoneNumber.setError("Required");
                return;
            }

            if (TextUtils.isEmpty(chooseNetworkEt.getText())){
                chooseNetworkEt.setError("Required");
                return;
            }

            if ("VODAFONE".equals(String.valueOf(chooseNetworkEt.getText()))){
                if (TextUtils.isEmpty(vodafoneAuthToken.getText())){
                    vodafoneAuthToken.setError("Token required");
                    return;
                }
            }

            if (TextUtils.isEmpty(amount.getText())){
                amount.setError("Amount is required");
                return;
            }

            payWithMoMo();

        });


    }

    private String getAbbrFromTrip(Trip trip){
        StringBuilder ppbuilder = new StringBuilder();

        for(String pickUpString : trip.pickupNames){

            String abbrvPickUps = pickUpString;
            if (pickUpString.length() > 4){
                abbrvPickUps = pickUpString.substring(0, Math.min(pickUpString.length(), 4));
            }

            ppbuilder.append(abbrvPickUps);
            ppbuilder.append("/");
        }
        String pickupStringResult = ppbuilder.toString().substring(0, ppbuilder.toString().length() - 1);

        StringBuilder destStringBuilder = new StringBuilder();
        for(String destString : trip.destinationNames){
            String abbrvPickUps = destString;
            if (destString.length() > 4){
                abbrvPickUps = destString.substring(0, Math.min(destString.length(), 4));
            }
            destStringBuilder.append(abbrvPickUps);
            destStringBuilder.append("/");
        }

        String destStringResult = destStringBuilder.toString().substring(0, destStringBuilder.toString().length() - 1);

        return getResources().getString(R.string.pick_dest, pickupStringResult, destStringResult);
    }

    private void fetchFromServer() {
        db.collection("SETTINGS").get().addOnCompleteListener(task -> {
            if (!task.isSuccessful()){
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                Dialogs.alertWithSweet(this, "Check your connection");
                return;
            }

             QuerySnapshot queryDocumentSnapshots =  task.getResult();
            if (queryDocumentSnapshots == null){
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                Dialogs.alertWithSweet(this, "Check your connection");
                return;
            }

            if (queryDocumentSnapshots.size() < 1){
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmerAnimation();
                Dialogs.alertWithSweet(this, "Please try again later");
                return;
            }

            shimmerFrameLayout.stopShimmerAnimation();
            shimmerFrameLayout.setVisibility(View.GONE);


            List<Settings> settingsList = queryDocumentSnapshots.toObjects(Settings.class);

            Settings settings =  settingsList.get(0);

            costOfTrip.setText(getString(R.string.cost_of_trip, String.valueOf(trip.actualCost)));
            transactionFee.setText(getString(R.string.transction_charge, String.valueOf(trip.actualCost * settings.commissionOnTripPayment)));

            totalAmountToPay = String.valueOf(Constants.toWholeNumber(trip.actualCost));

            amount.setText(totalAmountToPay);
            amountToPay.setText((getString(R.string.amount, totalAmountToPay)));

            paymentOverview.setVisibility(View.VISIBLE);


        });
    }

    private void payWithWallet() {


        LoadingGif.show(this);

        db.collection(Collections.USERS.toString()).document(firebaseUser.getUid()).get().addOnSuccessListener(documentSnapshot -> {

            if (documentSnapshot == null) return;
            User user = documentSnapshot.toObject(User.class);
            if (user == null) return;

            applicaitonUser.currentBalance = user.currentBalance;

            if (applicaitonUser.currentBalance < trip.actualCost){
                LoadingGif.hide();
                Dialogs.confirmWithSweet(this).setTitleText("INSUFFICIENT FUNDS")
                        .setContentText("Your wallet balance is below cost. Would you like to top up?")
                        .setConfirmText("Yes")
                        .setCancelText("No")
                        .setConfirmClickListener(sweetAlertDialog -> {
                            sweetAlertDialog.dismiss();
                            Constants.TEMPORAL_TRIP_HOLDER = trip;
                            startActivity(new Intent(this, PayWithMomoActivity.class));
                        }).show();
            }else{

                String url = Constants.PAYMENT_BASE_URL + "/transactions/pay/pay-for-trip-wallet";
                HashMap<String, String> params = new HashMap<>();
                params.put("amount",totalAmountToPay);
                params.put("coupon",String.valueOf(trip.actualCost * trip.couponValueInPercentage/100));
                params.put("requestId",String.valueOf(trip.requestId));
                params.put("payForMe",String.valueOf(trip.payForMe));
                params.put("riderId",String.valueOf(trip.riderId));
                params.put("senderId",String.valueOf(trip.senderId));
                params.put("fleetId",String.valueOf(trip.fleetId));
                params.put("type","TRIP_PAYMENT");
                params.put("orderId",trip.orderNumber);
                params.put("senderPhoneNumber",trip.senderPhoneNumber.trim());
                params.put("riderPlateNumber",trip.riderPlateNumber);
                params.put("riderPhoneNumber",trip.riderPhoneNumber);

                Log.d(Constants.TAG, "makePayment: url " + url);
                Log.d(Constants.TAG, "makePayment: params = "  + params);

                // push to the endpoint

                RequestObject requestObject = new RequestObject(Request.Method.POST, url, params, response -> {
                    LoadingGif.hide();
                    Log.d(Constants.TAG, "payWithMoMo: response " + response);
                    // check if status is true and show success view
                    try {
                        boolean status = response.getBoolean("status");
                        String message = response.getString("message");
                        if(status){
                            choosePaymentOption.hide();
                            paymentSuccessLayout.show();
                        }else{
                            Dialogs.alertWithSweet(this, "".equals(message) ? "System maintenance, please try later." : message);
                        }
                    } catch (JSONException e) {
                        Log.d(Constants.TAG, "payWithMoMo: JSONException " + e.getMessage());
                        e.printStackTrace();
                    }


                }, error -> {
                    Log.d(Constants.TAG, "payWithWallet: " + error);
                    Dialogs.alertWithSweet(this, error.getMessage());
                    LoadingGif.hide();
                });
                requestObject.setRetryPolicy(new DefaultRetryPolicy(
                        0,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                requestObject.send(this);

            }

        }).addOnFailureListener(e -> {
            LoadingGif.hide();
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            super.onBackPressed();
            Bungee.slideRight(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void payWithMoMo() {

        // write into the payments collection
        Payment payment = new Payment();
        payment.amount = Double.parseDouble(totalAmountToPay);
        payment.mobileAuthToken = String.valueOf(vodafoneAuthToken.getText());
        payment.momoNumber = String.valueOf(phoneNumber.getText());
        payment.network = String.valueOf(chooseNetworkEt.getText());
        payment.orderId = trip.orderNumber;
        payment.senderId = firebaseUser.getUid();
        payment.senderName = firebaseUser.getDisplayName();

        LoadingGif.show(this);
        db.collection(Collections.PAYMENTS.toString()).document(trip.orderNumber).set(payment).addOnSuccessListener(aVoid -> {

            //"MTN","TIGO / AIRTEL", "VODAFONE"
            String apiNetwork;
            switch (String.valueOf(chooseNetworkEt.getText())){
                case "MTN":
                    apiNetwork = "MTN_MM";
                    break;
                case "TIGO":
                    apiNetwork = "TIGO_CASH";
                    break;
                case "AIRTEL":
                    apiNetwork = "AIRTEL_MM";
                    break;
                case "VODAFONE":
                    apiNetwork = "VODAFONE_CASH";
                    break;
                default:
                    apiNetwork = "VODAFONE_CASH";
                    break;
            }


            String url = Constants.BaseUrl + "/transactions/pay/pay-for-trip-momo";
            HashMap<String, String> params = new HashMap<>();
            params.put("amount",totalAmountToPay);
            params.put("orderId", trip.orderNumber);
            params.put("momoNumber", String.valueOf(phoneNumber.getText()));
            params.put("network", apiNetwork);
            params.put("mobileAuthToken", String.valueOf(vodafoneAuthToken.getText()));
            params.put("paymentType", TransactionType.TRIP_PAYMENT.toString());
            params.put("senderName", trip.senderName);
            params.put("transType","DEBIT");
            params.put("senderId", trip.senderId);
            params.put("currency","GHS");
            params.put("coupon",String.valueOf(trip.actualCost * trip.couponValueInPercentage/100));
            params.put("requestId", trip.requestId);
            params.put("payForMe", String.valueOf(trip.payForMe));
            params.put("riderId", trip.riderId);
            params.put("fleetId", trip.fleetId);
            params.put("type",TransactionType.TRIP_PAYMENT.toString());
            params.put("senderPhoneNumber", trip.senderPhoneNumber);
            params.put("riderPlateNumber", trip.riderPlateNumber);
            params.put("riderPhoneNumber", trip.riderPhoneNumber);

            RequestObject requestObject = new RequestObject(Request.Method.POST, url, params, response -> {
                LoadingGif.hide();
                Log.d(Constants.TAG, "payWithMoMo: " + response);
                // check if status is true and show success view
                try {
                    boolean status = response.getBoolean("status");
                    String message = response.getString("message");
                    if(status){

                        Dialogs.alertWithSweet(this, "You'll receive the prompt soon");
                        db.collection(Collections.PAYMENTS.toString()).document(trip.orderNumber).addSnapshotListener((documentSnapshot, e) -> {
                            if (e != null){
                                Dialogs.alertWithSweet(this, e.getMessage());
                                return;
                            }

                            if(documentSnapshot == null) return;
                            Payment paymentReceived = documentSnapshot.toObject(Payment.class);
                            if (paymentReceived == null) return;

                            if ("APPROVED".equals(paymentReceived.status)){
                                paymentSuccessLayout.show();
                            }

                        });


                    }else{
                        Dialogs.alertWithSweet(this, "".equals(message) ? "System maintenance, please try later." : message);
                        Log.d(Constants.TAG, "payWithMoMo: not status " + message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(Constants.TAG, "payWithMoMo: JSONException " + e.getMessage());
                }
            }, error -> {
                Log.d(Constants.TAG, "payWithWallet: " + error);
                Dialogs.alertWithSweet(this, error.getMessage());
                LoadingGif.hide();
            });
            requestObject.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestObject.send(this);

        }).addOnFailureListener(e -> {
            Toast.makeText(this, "Please try again", Toast.LENGTH_SHORT).show();
            LoadingGif.hide();
        });

    }

    @SuppressLint("SetTextI18n")
    private void applySelection(){
        String selectedNetwork = adapter.getItem(mSelectedNetworkPosition);
        if (mSelectedNetworkPosition == 0){
            description.setText("Please make sure you have enough balance in your momo wallet");
        }else{
            description.setText("Kindly enter your mobile money details. You'd receive a prompt");
        }
        chooseNetworkEt.setText(selectedNetwork);
        chooseNetworkPicker.hide();

        if (mSelectedNetworkPosition == 2){
            // vodafone
            vodafoneAuthToken.setVisibility(View.VISIBLE);
        }else{
            vodafoneAuthToken.setVisibility(View.GONE);
        }
    }



    public View.OnClickListener scrollUp(){
        return view -> {

            int newPosition = mSelectedNetworkPosition - 1;
            if (newPosition < 0) return;


            mSelectedNetworkPosition = newPosition;
            adapter.setmSelectedItem(newPosition);
            adapter.notifyDataSetChanged();

            listView.smoothScrollToPosition(mSelectedNetworkPosition);
        };
    }

    public View.OnClickListener scrollDown(){
        return view -> {


            int newPosition = mSelectedNetworkPosition + 1;
            if (newPosition == listView.getAdapter().getCount()) return;

            mSelectedNetworkPosition = newPosition;

            adapter.setmSelectedItem(newPosition);
            adapter.notifyDataSetChanged();
            listView.smoothScrollToPosition(mSelectedNetworkPosition);

        };
    }
}
