package com.delivery.enroute.activities.vendors;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.delivery.enroute.R;
import com.delivery.enroute.activities.PoorConnectionActivity;
import com.delivery.enroute.adapters.VendorListAdapter;
import com.delivery.enroute.listeners.RowTappedLister;
import com.delivery.enroute.models.Pos;
import com.delivery.enroute.models.Product;
import com.delivery.enroute.models.Vendor;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.volley.RequestObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import spencerstudios.com.bungeelib.Bungee;

public class VendorListActivity extends AppCompatActivity {

    Toolbar toolbar;
    ArrayList<Vendor> vendorArrayList = new ArrayList<>();
    VendorListAdapter vendorListAdapter = new VendorListAdapter(vendorArrayList);
    ShimmerRecyclerView vendorsRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_list);

       vendorsRecyclerView = findViewById(R.id.shimmer_recycler_view);
       vendorsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
       vendorsRecyclerView.setHasFixedSize(true);

       vendorsRecyclerView.setAdapter(vendorListAdapter);
       vendorsRecyclerView.showShimmerAdapter();

       vendorListAdapter.setVendorRowTappedLister(row -> {
            Intent intent = new Intent(this, VendorDetailActivity.class);
            intent.putExtra(Constants.PRODUCTS, row.products);
            intent.putExtra(Constants.VENDOR, row);
            startActivity(intent);
            Bungee.slideLeft(this);
       });


        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
//            replace the home (back) button with the menu icon
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
//            set the Home / Back Button
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        fetchVendors();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home){
            super.onBackPressed();
            Bungee.shrink(this);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.shrink(this);
    }

    void fetchVendors(){
        String url = Constants.BaseUrl + "/vendors/ads/get-all";
        Log.d(Constants.TAG, "fetchVendors: url => " + url);
        new RequestObject(url, new HashMap<>(), response -> {

            Log.d(Constants. TAG, "fetchVendors: response received " + response);
            try {
                boolean status = response.getBoolean("status");
                JSONArray advertsArry = response.getJSONArray("adverts");

                if(!status) {
                    showPoorConnection();
                    return;
                }

                // for each
                ArrayList<Vendor> vendors = new ArrayList<>();
                for (int i = 0; i < advertsArry.length(); i++) {

                    JSONObject jsonObject = advertsArry.getJSONObject(i);
                    Vendor vendor = new Vendor();
                    vendor.vendorId = jsonObject.getString("vendorId");
                    vendor.vendorBanner = jsonObject.getString("vendorBanner");
                    vendor.vendorBranchId = jsonObject.getString("vendorBranchId");
                    vendor.vendorName = jsonObject.getString("vendorName");
                    vendor.vendorLogo = jsonObject.getString("vendorLogo");
                    vendor.vendorDescription = jsonObject.getString("vendorDescription");
                    vendor.vendorPhoneNumber = jsonObject.getString("vendorPhoneNumber");

                    JSONObject locationObj = jsonObject.getJSONObject("vendorLocation");
                    double lat = locationObj.getDouble("latitude");
                    double lng = locationObj.getDouble("longitude");
                    String address = locationObj.getString("place");

                    Pos.PosLatLng posLatLng = new Pos.PosLatLng(lat, lng);
                    posLatLng.address = address;

                    vendor.vendorLocation = posLatLng;

                    JSONArray prodArray = jsonObject.getJSONArray("products");
                    ArrayList<Product> products = new ArrayList<>();


                    for (int j = 0; j < prodArray.length(); j++) {

                        JSONObject prodObj = prodArray.getJSONObject(j);
                        Product product = new Product();
                        product.productDescription = prodObj.getString("productDescription");
                        product.productId = prodObj.getString("productId");
                        product.productImage = prodObj.getString("productImage");
                        product.productName = prodObj.getString("productTitle");
                        product.productPrice = prodObj.getDouble("productPrice");
                        product.productTitle = prodObj.getString("productName");

                        products.add(product);
                    }

                    vendor.products = products;
                    vendors.add(vendor);
                }

                vendorArrayList.clear();
                vendorArrayList.addAll(vendors);

                vendorListAdapter.notifyDataSetChanged();
                vendorsRecyclerView.hideShimmerAdapter();

            } catch (JSONException e) {
                showPoorConnection();
                e.printStackTrace();
            }


        }, error -> {
            parseVolleyError(error);
            showPoorConnection();
        }).useBasicAuth().send(this);
    }

    public void parseVolleyError(VolleyError error) {
        if(error.networkResponse != null && error.networkResponse.data != null){
            //new VolleyError();6
            Log.d(Constants.TAG, "coreConnectionToService: Error " + new Date() + " - " + new String(error.networkResponse.data));
        }else{
            Log.d(Constants.TAG, "coreConnectionToService: Error " + new Date() + " - " + error);
        }



        if (error instanceof NoConnectionError) {
            Log.d(Constants.TAG, "coreConnectionToService: Error " + new Date() + " - " + error);
        }
        else if (error instanceof ServerError)
            Log.d(Constants.TAG, "coreConnectionToService: Error " + new Date() + " - " + error);


    }

    void showPoorConnection(){
        startActivity(new Intent(this, PoorConnectionActivity.class));
        Bungee.slideUp(this);
    }
}
