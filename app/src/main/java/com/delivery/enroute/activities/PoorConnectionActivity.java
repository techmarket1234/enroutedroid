package com.delivery.enroute.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.delivery.enroute.MainActivity;
import com.delivery.enroute.R;

import spencerstudios.com.bungeelib.Bungee;

public class PoorConnectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poor_connection);

        findViewById(R.id.back_icon).setOnClickListener(view -> {
            onBackTapped();
        });
    }

    void onBackTapped(){
        startActivity(new Intent(this, MainActivity.class));
        Bungee.slideDown(this);
    }

    @Override
    public void onBackPressed() {
        onBackTapped();
    }
}
