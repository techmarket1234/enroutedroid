package com.delivery.enroute;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.delivery.enroute.activities.CheckOutActivity;
import com.delivery.enroute.activities.NavDrawerFragmentsActivity;
import com.delivery.enroute.activities.RateRiderActivity;
import com.delivery.enroute.activities.vendors.VendorListActivity;
import com.delivery.enroute.adapters.ChoosePlacePointsAdapter;
import com.delivery.enroute.adapters.NavMenuAdapter;
import com.delivery.enroute.adapters.PlacesCompleteAdapter;
import com.delivery.enroute.enums.Collections;
import com.delivery.enroute.enums.ContactTypes;
import com.delivery.enroute.enums.MenuItems;
import com.delivery.enroute.enums.PlaceCompleteTypes;
import com.delivery.enroute.enums.ViaMapType;
import com.delivery.enroute.layoutdialogs.AreaNotIncludedDialog;
import com.delivery.enroute.layoutdialogs.ConnectionErrorFragment;
import com.delivery.enroute.layoutdialogs.LocationSettingsDialog;
import com.delivery.enroute.layoutdialogs.ReviewFragment;
import com.delivery.enroute.layoutdialogs.UpdateAppDialog;
import com.delivery.enroute.listeners.OnGetServerUserCompleted;
import com.delivery.enroute.listeners.PlaceFoundListner;
import com.delivery.enroute.listeners.UnPaidTripListener;
import com.delivery.enroute.listeners.GetTripListener;
import com.delivery.enroute.models.CheckOut;
import com.delivery.enroute.models.Destination;
import com.delivery.enroute.models.MyCurrentPlace;
import com.delivery.enroute.models.Pickup;
import com.delivery.enroute.models.PlaceComplete;
import com.delivery.enroute.models.Pos;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.singletons.ApplicaitonUser;
import com.delivery.enroute.singletons.SockIOConnection;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.layoutdialogs.ItemCategoriesDialog;
import com.delivery.enroute.utils.Dialogs;
import com.delivery.enroute.utils.LoadingGif;
import com.delivery.enroute.utils.MFile;
import com.delivery.enroute.utils.MSlideUp;
import com.delivery.enroute.utils.TutorialManager;
import com.delivery.enroute.utils.maps.MapAnimator;
import com.delivery.enroute.utils.volley.RequestObject;
import com.delivery.enroute.webservices.services.KeepAliveService;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.common.api.ApiException;
//import com.google.android.gms.location.places.AutocompletePrediction;
//import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.PlacesClient;


import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.gson.Gson;
import com.infideap.drawerbehavior.AdvanceDrawerLayout;
import com.mancj.slideup.SlideUp;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.socket.client.Socket;
import me.toptas.fancyshowcase.FancyShowCaseView;
import me.toptas.fancyshowcase.listener.AnimationListener;
import spencerstudios.com.bungeelib.Bungee;

import static com.delivery.enroute.utils.Constants.DEFAULT_ZOOM;
import static com.delivery.enroute.utils.Constants.isNetworkAvailable;


public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    /*
     *   Trip object
     * */

    Trip request;
    ArrayList<Pickup> pickupArrayList = new ArrayList<>();
    ArrayList<Destination> destinationArrayLists = new ArrayList<>();
    boolean doubleBackToExitPressedOnce;

    MapFragment mMapFragment;

    ViaMapType viaMapType = ViaMapType.PICK_UP;

    /*
     *   End of request object
     * */

    /*
     * Internal static fields initialization starts here
     * */
    private static final int REQUEST_PERM_CODE = 2;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private static final int PICK_CONTACT = 3;
    private static final int REQUEST_LOC_PERM = 5;
    int screenHeight;
    int defaultPanelHeight;
    private static String[] MY_PERMISSIONS = {
            "android.permission.ACCESS_FINE_LOCATION",
            "android.permission.ACCESS_COARSE_LOCATION",
            "android.permission.INTERNET"
    };

    private boolean gps_enabled = false;
    private boolean network_enabled = false;
    /*
     *  Internal static fields initialization ends here
     *
     * */

    /*
     *   UI VIEWS START HERE
     * */

    EditText pickUpPointEt, sendToPointEt, senderName, senderPhone, itemCategory, itemInstructions, recipientPhone, recipientName;
    Button useMyCurrentLocation,
            savePickUpdetails, saveSenderDetails, selectViaMap, selectDestViaMap,
            useCurrentLocationAsDest;
    TextView pickContact;
    ImageView deliveryGif,centered_pin;
    TextView  pickupLocViaMap;
    RecyclerView pickUpRecyclerView, destinationRecyclerView;
    SlidingUpPanelLayout slidingUpPanelLayout;
    SlideUp pickUpDetailsLayout,
            senderDetailsLayout,
            destinationPointLayout,
            destinationDetailsLayout,
            recipientDetailsLayout,

             formMultiItemDetail, selectPickUpViaMapLayout;

    View pickUpDetailForm;
    ImageButton addPickUpPoint;

    private AdvanceDrawerLayout drawer;
    private FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

    /*
     *   UI VIEW ENDS HERE
     * */

    //    set trip type (single / multiple)
    private boolean isMultiplePickUps; // default is false
    private boolean isMultipleDestinations; // default is false

    /*
     *   Global initializations starts here
     * */
    private GoogleMap map;
    FusedLocationProviderClient fusedLocationClient;
    ConnectionErrorFragment connectionErrorFragment;

    private ShimmerFrameLayout mShimmerViewContainer, mShimmerViewContainerDestination, via_map_shimmer_view_container;
    private PlacesClient placesClient;
    private AutocompleteSessionToken token;
    private RectangularBounds bounds;

    List<ItemCategoriesDialog> multipleItemCategoriesDialogs = new ArrayList<>();

    private PlaceCompleteTypes placeCompleteTypes = PlaceCompleteTypes.PICK_UP;
    private ContactTypes contactTypes = ContactTypes.SENDER_CONTACT;
    Geocoder geocoder;
    Timer markerAnimateTimer = new Timer();
    TimerTask updateProfile;
    private boolean selectViaMapTapped;
    private AreaNotIncludedDialog areaNotIncludedDialog;

    boolean canShowToolbar;

    String initialAlertText;
    int initialAlertBackgroundColor;
    ImageView alertLove;

    //    private Socket mSocket;
//    {
//        try {
//            mSocket = IO.socket("http://159.89.17.30:3003/");
//        } catch (URISyntaxException e) {
//            Log.d(Constants.TAG, "instance initializer: " + e.getMessage());
//        }
//    }


    //  ############ start of array lists ###########

    //    place picker for pick up


    //      arrayList used when typing the places
    ArrayList<PlaceComplete> typingPlacesPickUpArrayList = new ArrayList<>();
    ArrayList<PlaceComplete> typingPlacesDestinationArrayList = new ArrayList<>();

    //    array list used when place is selected
    ArrayList<PlaceComplete> selectedPickupPlaces = new ArrayList<>();
    ArrayList<PlaceComplete> selectedDestinationPlaces = new ArrayList<>();

    // checkout array list
    ArrayList<CheckOut> pickPtsCheckOutArrayList = new ArrayList<>();
    ArrayList<CheckOut> destinationsCheckOutArrayList = new ArrayList<>();


    //   //  ############ End of array lists ###########


    //  ############ Adapters ###########

    //  this adapter contain the points selected
    ChoosePlacePointsAdapter selectedDestinationPointsAdapter = new ChoosePlacePointsAdapter(selectedDestinationPlaces);
    ChoosePlacePointsAdapter selectedPickUpPointsAdapter = new ChoosePlacePointsAdapter(selectedPickupPlaces);

    // adapters here is used when the user is typing the place
    PlacesCompleteAdapter typingPlacePickUpCompleteAdapter = new PlacesCompleteAdapter(typingPlacesPickUpArrayList);
    PlacesCompleteAdapter typingPlaceDestinationCompleteAdapter = new PlacesCompleteAdapter(typingPlacesDestinationArrayList);

    // adapters to render list on checkout page
//    CheckOutListAdapter pickPtCheckOutListAdapter = new CheckOutListAdapter(pickPtsCheckOutArrayList);
//    CheckOutListAdapter destCheckOutListAdapter = new CheckOutListAdapter(destinationsCheckOutArrayList);

    // cancel order adapter



    private boolean cameraIsMoving;
    private int setDetailsForPickPtIndex;
    private EditText multi_item_instructions;
    private EditText multi_item_category;
    private TextView multi_item_detail_title;
    private TextView multi_pick_up_person;
    private TextView multi_pick_up_person_phone;
    //private Marker draggablePickPtMarker;

    //   //  ############ End of Adapters ###########
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    private ImageButton addDestinationPointBtn;
    private View singleRecipientDetailLayout;
    private int currentDestinationSenderDetailsIndex = -1;
    private TextView recipientDetailsTitle;
    private EditText recipientNameEtMultple;
    private EditText recipientPhoneEtMultple;
    private TextView estimatedTimeTv, suggestedWalletFee, estimatedCostTv;
    private double walletFee = 0.00d;

    private Button addPickupFromCheckout, addDestinationFromCheckout;
    private boolean isAddPickupFromCheckout, isAddDestinationFromCheckout;
    private View hr_2;

    View alertCardView;
    TextView alertText;

    //private boolean isUpdatingPickUp;


    /*
     *  Global initializations ends here
     * */

    /*
     *   Entry point to Main activity (onCreate) starts here
     * */

    Socket mSocket;
    private MyCurrentPlace selectedAddress;

    UpdateAppDialog updateAppDialog;
    ReviewFragment reviewFragment;
    private String debugUrl;
    private SweetAlertDialog noConnectionDialog;
    private TextView senderPageTitle;

    private int getScreenHeight(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
        //int width = displayMetrics.widthPixels;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {

            setContentView(R.layout.activity_main);

            ApplicaitonUser applicaitonUser1 = ApplicaitonUser.getInstance(this);
            request = new Trip();

            // request permission from user
            requestMapPermission();

            initViews();


            // Retrieve a PlacesClient (previously initialized - see MainActivity)
            placesClient = Places.createClient(this);
            // Create a new token for the autocomplete session. Pass this to FindAutocompletePredictionsRequest,
            // and once again when the user makes a selection (for example when calling fetchPlace()).
            token = AutocompleteSessionToken.newInstance();

            // Create a RectangularBounds object.
            bounds = RectangularBounds.newInstance(
                    new LatLng(-33.880490, 151.184363),
                    new LatLng(-33.858754, 151.229596));

            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            geocoder = new Geocoder(this, Locale.getDefault());
            connectionErrorFragment = new ConnectionErrorFragment();
            /// for last known location

            // initialize chat service

            // get an update of the activated pick up locations

//        pickUpPointEt.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override public void onGlobalLayout() {
//                pickUpPointEt.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                // TEXTVIEW
//                if(pickUpPointEt.getParent() != null) {
//                    ((ViewGroup)pickUpPointEt.getParent()).removeView(pickUpPointEt); // <- fix
//                }
//                Tutorial.getInstance().show(MainActivity.this, pickUpPointEt);
//            }
//        });

            reviewFragment = new ReviewFragment();
            reviewFragment.setOnReviewListener(() -> {
                ApplicaitonUser applicaitonUser = ApplicaitonUser.getInstance(this);
                LoadingGif.show(this);
                applicaitonUser.appReviewed = true;
                Constants.updateServerUser(this, applicaitonUser, isSuccessful -> {
                    LoadingGif.hide();
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("market://details?id="+ getPackageName()));
                    startActivity(intent);
                });

            });

            updateAppDialog = new UpdateAppDialog();
            updateAppDialog.setOnupdateappListener(new UpdateAppDialog.OnupdateappListener() {
                @Override
                public void onUpdate(String debugUrl) {
                    try {

                       // Log.d(Constants.TAG, "onCreate: setOnupdateappListener tapped " + this.debugUrl);

                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+ getPackageName()));
                        startActivity(intent);

//                    if (this.debugUrl != null){
//                        //                                                        debug mode --------
//                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(this.debugUrl));
//                        startActivity(browserIntent);
//                    }else{
//                        //                          production mode --------------
//                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+ getPackageName()));
//                        startActivity(intent);
//                    }


                    } catch (Exception e) {
                        Constants.callErrorPage(MainActivity.this, e.getMessage());
                    }
                }

                @Override
                public void onEverythinOk() {

                }
            });



               new Handler().postDelayed(() -> {
                   //TutorialManager.getInstance().clearAllTutorials(MainActivity.this);
                   boolean shouldShowTutorial = TutorialManager.getInstance().showTutorial(MainActivity.this, pickUpPointEt.getId());
                   if (shouldShowTutorial){
                       canShowToolbar = false;
                       runOnUiThread(() -> new FancyShowCaseView.Builder(this)
                               .focusOn(pickUpPointEt)
                               .title("select pick up point")
                               .closeOnTouch(true)
                               .titleSize(20, 1)
                               .build()
                               .show());
                   }else{
                       canShowToolbar = true;
                   }
               },1000);


               startService(new Intent(MainActivity.this, KeepAliveService.class));


        }catch (Exception e){
            Log.d(Constants.TAG, "onCreate: Exception " + e.getMessage());
            Constants.callErrorPage(this, "onCreate: Exception " + e.getMessage());
        }

        new Thread(() -> {
            if (firebaseUser != null){
                HashMap<String, Object> params = new HashMap<>();
                params.put("isOnline", true);
                params.put("device", "ANDROID");
                db.collection(Collections.USERS.toString()).document(firebaseUser.getUid()).update(params);
            }

        }).start();
        //RestrictLocation.getInstance().refreshActivePickupUps(this);
       // LoadingGif.show(this);
    }



    Handler handler;
    Thread thread;
    @Override
    protected void onResume() {
        super.onResume();

       try {

           // todo show previous pickups
           showPreviousPickups();

            new Thread(() -> {
               try{
                   Map<String, Object> params = new HashMap<>();
                   params.put("isOnline", true);
                   db.collection("USERS").document(firebaseUser.getUid()).update(params);
                   mSocket = SockIOConnection.getSocketInstance();


               }catch(Exception e){
                   Log.d(Constants.TAG, "onResume: Exception " + e.getMessage());
                   Constants.callErrorPage(this, "onResume: Exception " + e.getMessage());
               }

           });


            Constants.checkIfTheresACompletedUnratedTrip(new GetTripListener() {
                @Override
                public void found(Trip trip) {
                    // start
                    Intent intent1 = new Intent(MainActivity.this, RateRiderActivity.class);
                    intent1.putExtra(Constants.TRIP, trip);

                    startActivity(intent1);
                    Bungee.slideUp(MainActivity.this);

                }

                @Override
                public void notFound() {
                    Constants.checkOutstandingPayment(MainActivity.this, new UnPaidTripListener() {
                        @Override
                        public void unpaidTrip(Trip trip, boolean allowUserToRequestAnotherTrip) {
                            // already implemented
                        }

                        @Override
                        public void notFound() {

                            MFile.diagnoseApp(MainActivity.this, new UpdateAppDialog.OnupdateappListener() {
                                @Override
                                public void onUpdate(String debugUrl) {
                                    MainActivity.this.debugUrl = debugUrl;
                                    updateAppDialog.show(getSupportFragmentManager(), "app_update");
                                }

                                @Override
                                public void onEverythinOk() {
                                    // check if user has rated us before
                                    Constants.getServerUser(MainActivity.this, new OnGetServerUserCompleted() {
                                        @Override
                                        public void userFound(ApplicaitonUser applicaitonUser) {

                                            Log.d(Constants.TAG, "userFound: user id =>  " + applicaitonUser.id);

                                            // further checks to see if user has completed more than 2 trips
                                            // and if user has completed trip today
                                            Constants.getCheckIfTheresACompletedTrip(new GetTripListener() {
                                                @Override
                                                public void found(Trip trip) {

                                                    if (!applicaitonUser.appReviewed){
                                                        if(!reviewFragment.isAdded() && !reviewFragment.isVisible()){
                                                            reviewFragment.show(getSupportFragmentManager(), "review_app");
                                                        }

                                                    }

                                                }

                                                @Override
                                                public void notFound() {

                                                }
                                            });


                                        }

                                        @Override
                                        public void userNotFound() {

                                        }
                                    });

                                }
                            });

                        }
                    });
                }
            });





           if(handler == null){
               handler = new Handler();
           }
          handler.postDelayed(() -> {
              if (!TutorialManager.getInstance().hasShownTutorial(MainActivity.this, toolbar.getId()) && canShowToolbar){
                  // check if he has requested for at least on trip
                  db.collection(Collections.REQUEST.toString()).whereEqualTo("senderId", firebaseUser.getUid()).get().addOnSuccessListener(queryDocumentSnapshots -> {
                      if (queryDocumentSnapshots != null){
                          List<Trip> trips = queryDocumentSnapshots.toObjects(Trip.class);
                          if (trips.isEmpty()) return;
                          if(!drawer.isDrawerOpen(GravityCompat.START) && TutorialManager.getInstance().showTutorial(MainActivity.this, toolbar.getId()) && slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED){
                              new FancyShowCaseView.Builder(MainActivity.this)
                                      .focusOn(toolbar)
                                      .title("click here for more options")
                                      .titleSize(20, 1)
                                      .closeOnTouch(true)
                                      .enableAutoTextPosition()
                                      .focusCircleAtPosition(100,100,80)
                                      .titleGravity(GravityCompat.END)
                                      .build()
                                      .show();
                          }

                      }
                  });
              }
          },1000);

       }catch (Exception e){
           Log.d(Constants.TAG, "onResume: Exception " + e.getMessage());
           Constants.callErrorPage(this, "onResume: Exception " + e.getMessage());
       }
       
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       try {

           new Thread(() -> {
               new Thread(() -> {
                   if (firebaseUser != null){
                       HashMap<String, Object> params = new HashMap<>();
                       params.put("isOnline", false);
                       params.put("device", "ANDROID");
                       db.collection(Collections.USERS.toString()).document(firebaseUser.getUid()).update(params);
                   }
               }).start();
           }).start();

       }catch (Exception e){
           Log.d(Constants.TAG, "onDestroy: Exception " + e.getMessage());
           Constants.callErrorPage(this, "onDestroy: Exception " + e.getMessage());
       }
    }

    private void registerCustomerEmailInOneSignal(){

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) return;

        String email = user.getEmail();
        if (email == null) return;

        String emailAuthHash = "..."; // Email auth hash generated from your server
//        OneSignal.setEmail(email, emailAuthHash, new OneSignal.EmailUpdateHandler() {
//            @Override
//            public void onSuccess() {
//                // Email successfully synced with OneSignal
//            }
//
//            @Override
//            public void onFailure(OneSignal.EmailUpdateError error) {
//                // Error syncing email, check error.getType() and error.getMessage() for details
//            }
//        });
    }

    Toolbar toolbar;
    NavMenuAdapter navMenuAdapter;
    LinearLayoutManager menuLinearLayoutManager;

    //    initialize views on map
    @SuppressLint("ClickableViewAccessibility")
    private void initViews() {

        /*
         *   Helping items initializations here =========================================
         * */

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
//            replace the home (back) button with the menu icon
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.open_drawer_icon);
//            set the Home / Back Button
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            setTitle("");

        }

        alertLove = findViewById(R.id.alert_love);

        YoYo.with(Techniques.Pulse)
                .duration(1000)
                .repeat(1000)
                .playOn(alertLove);

        ImageButton alertClose = findViewById(R.id.alert_close);
        alertCardView = findViewById(R.id.alert_card_view);
        alertText = findViewById(R.id.alert_text);
        initialAlertText = String.valueOf(alertText.getText());

        alertCardView.setOnClickListener(view -> {
            startActivity(new Intent(this, VendorListActivity.class));
            Bungee.split(this);
        });

        alertClose.setOnClickListener(view -> alertCardView.setVisibility(View.GONE));



//        findViewById(R.id.left_toolbar_icon).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (drawer.isDrawerOpen(GravityCompat.START)){
//                    drawer.closeDrawer(GravityCompat.START);
//                }else{
//                    drawer.openDrawer(GravityCompat.START);
//                }
//
//            }
//        });


        drawer = findViewById(R.id.drawer_layout);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                if (TutorialManager.getInstance().hasShownTutorial(MainActivity.this, toolbar.getId()) && menuLinearLayoutManager != null){
                    if (TutorialManager.getInstance().showTutorial(MainActivity.this, drawerView.getId()) && menuLinearLayoutManager.findViewByPosition(2) != null){
                        new FancyShowCaseView.Builder(MainActivity.this)
                                .focusOn(Objects.requireNonNull(menuLinearLayoutManager.findViewByPosition(1)))
                                .title("track your ongoing deliveries")
                                .closeOnTouch(true)
                                .enableAutoTextPosition()
                                .titleSize(16,1)
                                .focusCircleRadiusFactor(0.5)
                                .titleGravity(GravityCompat.END)
                                .build()
                                .show();
                    }else if (TutorialManager.getInstance().showTutorial(MainActivity.this, Constants.HOME_ID) && menuLinearLayoutManager.findViewByPosition(0) != null){
                        new FancyShowCaseView.Builder(MainActivity.this)
                                .focusOn(drawerView)
                                .closeOnTouch(true)
                                .titleSize(16,1)
                                .enableAutoTextPosition()
                                .customView(R.layout.reset_app_tut_layout, null)
                                .focusCircleRadiusFactor(0.5)
                                .focusOn(Objects.requireNonNull(menuLinearLayoutManager.findViewByPosition(0)))
                                .titleGravity(GravityCompat.END)
                                .build()
                                .show();
                    }else if (TutorialManager.getInstance().showTutorial(MainActivity.this, Constants.WALLET_ID) && menuLinearLayoutManager.findViewByPosition(2) != null){
                        new FancyShowCaseView.Builder(MainActivity.this)
                                .focusOn(drawerView)
                                .title("Keep your wallet topped up to request a rider even when you're out of cash")
                                .closeOnTouch(true)
                                .titleSize(16,1)
                                .enableAutoTextPosition()
                                .focusCircleRadiusFactor(0.5)
                                .focusOn(Objects.requireNonNull(menuLinearLayoutManager.findViewByPosition(2)))
                                .titleGravity(GravityCompat.END)
                                .build()
                                .show();
                    }
                }
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        toggle.syncState();

        centered_pin = findViewById(R.id.centered_pin);

        // position the pin on the map

        NavigationView navigationView = findViewById(R.id.nav_view);
        ViewGroup headerView = (ViewGroup) navigationView.getHeaderView(0);
        ViewGroup relativeLaoutViewGroup = (ViewGroup) headerView.getChildAt(0);
        ViewGroup LinearLaoutViewGroup = (ViewGroup) relativeLaoutViewGroup.getChildAt(1);
        TextView username = (TextView) LinearLaoutViewGroup.getChildAt(0);

        if (FirebaseAuth.getInstance().getCurrentUser() != null)
            username.setText(getString(R.string.hello_username, FirebaseAuth.getInstance().getCurrentUser().getDisplayName()));

        RecyclerView menuListView = (RecyclerView) headerView.getChildAt(1);
        navMenuAdapter = new NavMenuAdapter();
        navMenuAdapter.setItemRowTappedLister(row -> {
            Intent intent = new Intent(this, NavDrawerFragmentsActivity.class);

            // when a menu item is tapped
            switch (row.menuItem){

                case HOME:
                    refreshState();
                    return;
                case PERSONAL_DETAILS:
                   // Toast.makeText(this, "Personal Details Tapped", Toast.LENGTH_SHORT).show();
                    intent.putExtra(Constants.NAV_DRAWER_ENUM, MenuItems.PERSONAL_DETAILS);
                    break;
                case SHARE_AND_EARN:
                    intent.putExtra(Constants.NAV_DRAWER_ENUM, MenuItems.SHARE_AND_EARN);
                    break;
                case COURIER_TYPE:
                    intent.putExtra(Constants.NAV_DRAWER_ENUM, MenuItems.COURIER_TYPE);
                    break;
                case ORDER:
                    intent.putExtra(Constants.NAV_DRAWER_ENUM, MenuItems.ORDER);
                    break;
                case PROMO:
                    intent.putExtra(Constants.NAV_DRAWER_ENUM, MenuItems.PROMO);
                    break;
                case VENDORS:
                    Constants.openVendors(this);
                    return;
                case WALLET:
                    intent.putExtra(Constants.NAV_DRAWER_ENUM, MenuItems.WALLET);
                    break;
                case FAQ:
                    Constants.openFAQ(MainActivity.this);
                    return;
                case CONTACT_US:
                    Constants.openEmail(this);
                    return;
                case NOTIFICATIONS:
                    intent.putExtra(Constants.NAV_DRAWER_ENUM, MenuItems.NOTIFICATIONS);
                    break;
                case LOGOUT:
                    Constants.logout(this);
                    return;
                    default:
                        drawer.closeDrawer(GravityCompat.START);
                        return;
            }

            startActivity(intent);
            Bungee.slideLeft(this);

            drawer.closeDrawer(GravityCompat.START);

        });

        menuLinearLayoutManager = new LinearLayoutManager(this);
        menuListView.setLayoutManager(menuLinearLayoutManager);
        menuListView.setHasFixedSize(true);
        menuListView.setAdapter(navMenuAdapter);


        drawer.setViewScale(Gravity.START, 0.9f);
        drawer.setViewElevation(Gravity.START, 20);
        drawer.setRadius(Gravity.START, 25);//set end container's corner radius (dimension)

        drawer.useCustomBehavior(Gravity.START); //assign custom behavior for "Left" drawer



        deliveryGif = findViewById(R.id.delivery_gif);
        View openSlide = findViewById(R.id.open_drawer_icon_pp);
//        view.setOnClickListener(view -> {
//            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
//        });
        openSlide.setOnClickListener(view -> {
            if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED){
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }else{
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
        });




        slidingUpPanelLayout = findViewById(R.id.sliding_layout);

         screenHeight =  getScreenHeight();
         //defaultPanelHeight =  (4 * screenHeight)/10; // increase denominator for panel to move down
        defaultPanelHeight = screenHeight / 2;

        Log.d(Constants.TAG, "defaultPanelHeight = " + defaultPanelHeight);
        slidingUpPanelLayout.setPanelHeight(defaultPanelHeight);

        slidingUpPanelLayout.addPanelSlideListener(slideUpPanelListener);
//        slidingUpPanelLayout.setDragView(findViewById(R.id.panelLayout));

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.setVisibility(View.GONE);

        mShimmerViewContainerDestination = findViewById(R.id.shimmer_view_container_destination);
        mShimmerViewContainerDestination.setVisibility(View.GONE);

        via_map_shimmer_view_container = findViewById(R.id.via_map_shimmer_view_container);



        /*
         *   End of helping items initialization ###############################
         * */

        /*
         *   Pick up point FORM initializations begin here ====================================
         * */
        pickUpPointEt = findViewById(R.id.pick_from_et);
        pickUpPointEt.setOnFocusChangeListener((view, hasFocus) -> {

            if (hasFocus){
                selectViaMapTapped = false;
                centered_pin.setVisibility(View.GONE);
                placeCompleteTypes = PlaceCompleteTypes.PICK_UP;
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

            }

            Log.d(Constants.TAG, "initViews: map is "+ map);
        });



        pickUpPointEt.setOnClickListener(view -> {
            placeCompleteTypes = PlaceCompleteTypes.PICK_UP;
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            Log.d(Constants.TAG, "initViews: map is "+ map);
        });


        pickUpPointEt.addTextChangedListener(choosePointChange);
        pickUpPointEt.setOnTouchListener((view, event) -> {
//            final int DRAWABLE_LEFT = 0;
//            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
//            final int DRAWABLE_BOTTOM = 3;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (pickUpPointEt.getRight() - pickUpPointEt.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    // your action here
                    pickUpPointEt.setText("");
                    typingPlacesPickUpArrayList.clear();
                    typingPlacePickUpCompleteAdapter.notifyDataSetChanged();

                    return false;
                }
            }
            return false;
        });

        //        pick up point recyclerView //////////////////
        pickUpRecyclerView = findViewById(R.id.pick_up_recycler_view);
        pickUpRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        pickUpRecyclerView.setHasFixedSize(true);
        pickUpRecyclerView.setAdapter(typingPlacePickUpCompleteAdapter);


        setupUI(pickUpRecyclerView);
        typingPlacePickUpCompleteAdapter.setRowTappedLister(placeComplete -> {


            LoadingGif.show(this);

            // add marker to map
            getLatLngFromPlaceId(placeComplete.placeId, place -> {

                LoadingGif.hide();

               // Toast.makeText(this, "pick up selected via search", Toast.LENGTH_SHORT).show();
//                pickUpPointEt.setText(placeComplete.name);
                pickUpPointEt.setText("");
                //            add place selected to pick up points adapter
                if (place == null || place.getLatLng() == null) return;
                placeComplete.latitude = place.getLatLng().latitude;
                placeComplete.longitude = place.getLatLng().longitude;
                showPickUpDetailsForm(placeComplete);
            });
        });

        useMyCurrentLocation = findViewById(R.id.use_my_current_location);
//        useMyCurrentLocation.setOnTouchListener(slideLayoutUp);
        useMyCurrentLocation.setOnClickListener(view -> {
            //Toast.makeText(this, "Use my current location tapped", Toast.LENGTH_SHORT).show();
            // this method contains getUserCurrentLocation
            centered_pin.setVisibility(View.GONE);
            selectViaMapTapped = false;
            requestLocationPermission();

        });

        useMyCurrentLocation.setVisibility(View.GONE);
        setupUI(useMyCurrentLocation);
        /// select via map
        selectViaMap = findViewById(R.id.select_via_map);
        selectViaMap.setVisibility(View.GONE);
        selectViaMap.setOnClickListener(view -> {

           // Toast.makeText(this, "Relax, its almost done :D", Toast.LENGTH_SHORT).show();
           configureSelectViaMap();
           viaMapType = ViaMapType.PICK_UP;

        });

        setupUI(selectViaMap);

        /*
         *  End of Pick up point FORM initializations #######################################
         * */

        /*
        *   Select Via map layout here ##########################
        * */
        selectPickUpViaMapLayout =  MSlideUp.controlView(findViewById(R.id.selectViaMapLayout));
        pickupLocViaMap = findViewById(R.id.pick_up_loc_via_map);
        pickupLocViaMap.setOnClickListener(view -> {
            LatLng midLatLng = map.getCameraPosition().target;

            centered_pin.setVisibility(View.GONE);

            if (selectedAddress == null) return;

           if (selectViaMapTapped){

               PlaceComplete placeComplete = new PlaceComplete();
               placeComplete.name = selectedAddress.name;
               placeComplete.description = selectedAddress.description;
               placeComplete.latitude = selectedAddress.latitude;
               placeComplete.longitude = selectedAddress.longitude;


               selectPickUpViaMapLayout.hide();
               if (viaMapType == ViaMapType.PICK_UP)
                   showPickUpDetailsForm(placeComplete);
               else
                   showDestinationDetailsLayout(placeComplete);

               selectViaMapTapped = false;

           }

        });

        /*
         *  End of Select Via map layout here ##########################
         * */

        /*
        *   Multi Item detail layout #################################
        * */
        formMultiItemDetail = MSlideUp.controlView(findViewById(R.id.form_multi_item_detail));
        findViewById(R.id.multi_close_item_detail_layout).setOnClickListener(view -> {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            formMultiItemDetail.hide();
        });


        multi_item_detail_title =  findViewById(R.id.multi_item_detail_title);
        multi_pick_up_person = findViewById(R.id.multi_pick_up_person);
        //multi_pick_up_person.setOnTouchListener(slideLayoutUp);

        multi_pick_up_person_phone = findViewById(R.id.multi_pick_up_person_phone);
       // multi_pick_up_person_phone.setOnTouchListener(slideLayoutUp);

        TextView multi_pick_up_pick_contact = findViewById(R.id.multi_pick_up_pick_contact);
        multi_pick_up_pick_contact.setOnClickListener(view -> {
            contactTypes = ContactTypes.PICK_UP_CONTACTS;
            readContact();
        });



       multi_item_category =  findViewById(R.id.multi_item_category);
//        multi_item_category.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                ItemCategoriesDialog itemCategoriesDialog  =   multipleItemCategoriesDialogs.get(setDetailsForPickPtIndex);
//                itemCategoriesDialog.show(getSupportFragmentManager(),"");
//
//                return false;
//            }
//        });

        multi_item_instructions = findViewById(R.id.multi_item_instructions);
//        multi_item_instructions.setOnTouchListener(
//                (view, motionEvent) -> {
//            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
//            return false;
//
//        });

        multi_item_category.setOnClickListener(view -> {

              ItemCategoriesDialog itemCategoriesDialog  =   multipleItemCategoriesDialogs.get(setDetailsForPickPtIndex);

              itemCategoriesDialog.show(getSupportFragmentManager(),"");

        });

        
        findViewById(R.id.multi_savePickUpDetails).setOnClickListener(
                view -> {

                   // Toast.makeText(this, "save details", Toast.LENGTH_SHORT).show();
                    Log.d(Constants.TAG, "save multiple details: ");
                    addPickUpDetails(setDetailsForPickPtIndex,
                            String.valueOf( multi_item_category.getText()),
                            String.valueOf(multi_item_instructions.getText()),
                            String.valueOf(multi_pick_up_person.getText()),
                            String.valueOf(multi_pick_up_person_phone.getText())
                    );

                    //selectedPickupPlaces.get(setDetailsForPickPtIndex)

                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                    formMultiItemDetail.hide();
                });

        /*
        *   End of Multi Item detail layout #################################
        * */

        /*
         *  Pick up details FORM initializations begin here ===============================
         * */

        //       selected pick up points recycler view
        RecyclerView selectedPickUpRecyclerView = findViewById(R.id.selected_pick_up_places_recyclerView);
        selectedPickUpRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        selectedPickUpRecyclerView.setHasFixedSize(true);

        selectedPickUpPointsAdapter.setRowMoreOptionTapped(
                row -> {
                    setDetailsForPickPtIndex = row.index;
                    multi_item_detail_title.setText(row.name);
                    multi_item_category.setText("");
                    multi_item_instructions.setText("");
                    multi_pick_up_person.setText("");
                    multi_pick_up_person_phone.setText("");
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                    formMultiItemDetail.show();
                }
        );


        selectedPickUpPointsAdapter.setRowTapped(row -> {
            updateExistingPickUpPoint(row.index, pickUpDetailsLayout);
        });

        selectedPickUpPointsAdapter.setRowRemoveItemTapped(row -> {

           // Toast.makeText(this, "remove btn tapped", Toast.LENGTH_SHORT).show();
//            if (selectedPickupPlaces.isEmpty()){
//                pickUpDetailForm.setVisibility(View.GONE);
//            }

            Log.d(Constants.TAG, "initViews: remove from pick point " + row.index);

            if (selectedPickupPlaces.size() == 1){
                refreshState();
                return;
            }

            // remove corresponding marker on map

//            selectedPickupPlaces.get(row.index).marker.remove();

            Log.d(Constants.TAG, "initViews:selectedPickupPlaces before removal " + selectedPickupPlaces.size());
            selectedPickupPlaces.remove(row.index);
            Log.d(Constants.TAG, "initViews:selectedPickupPlaces AFTER removal " + selectedPickupPlaces.size());

            Log.d(Constants.TAG, "initViews:pickupArrayList before removal " + new Gson().toJson(pickupArrayList));
            pickupArrayList.remove(row.index);
            Log.d(Constants.TAG, "initViews:pickupArrayList AFTER removal " + new Gson().toJson(pickupArrayList));

            if (selectedPickupPlaces.size() == 1){
                isMultiplePickUps = false;
                addDestinationPointBtn.setVisibility(View.VISIBLE);
            }

            reRenderPickList();

            reRenderMapItems();


//            placeCompletes.remove(getAdapterPosition());
//            notifyDataSetChanged();

        });

        selectedPickUpRecyclerView.setAdapter(selectedPickUpPointsAdapter);

        pickUpDetailForm = findViewById(R.id.single_trip_form);

        pickUpDetailsLayout = MSlideUp.controlView(findViewById(R.id.pick_up_details));

        itemCategory = findViewById(R.id.item_category);

//        itemCategory.setOnTouchListener((view, motionEvent) -> {
//            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
//            return false;
//        });

        ItemCategoriesDialog itemCategoriesDialog = new ItemCategoriesDialog();
        multipleItemCategoriesDialogs.add(itemCategoriesDialog);
        itemCategoriesDialog.setItemRowTappedLister(row -> {
            Log.d(Constants.TAG, "initViews: row tapped ");
            multi_item_category.setText(row.itemName);
            itemCategory.setText(row.itemName);
        });

        itemCategory.setOnClickListener(view -> {
            /// show itemCategoriesDialog with item categories arranged for user to pic
           try{
               if (itemCategoriesDialog.isAdded()) return;
               itemCategoriesDialog.show(getSupportFragmentManager(), "Item Categories");
           }catch (Exception e){

           }
        });

        itemInstructions = findViewById(R.id.item_instructions);
        itemInstructions.setOnTouchListener((view, motionEvent) -> {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            return false;
        });

        addPickUpPoint = findViewById(R.id.add_pick_up_point);
        addPickUpPoint.setOnClickListener(view -> {
            isAddPickupFromCheckout = false;
            addAnotherPickUpPoint();
        });

        savePickUpdetails = findViewById(R.id.savePickUpDetails);
        savePickUpdetails.setOnClickListener(savePickUpDetailsAndContinue);

        /*
         *   End of pick up FORM details initializations  ###############################
         * */


        /*
         *   Sender details FORM initializations here ==============================
         * */

        senderDetailsLayout = MSlideUp.controlView(findViewById(R.id.layout_sender_details));
        senderName = findViewById(R.id.sender_name);
        senderPhone = findViewById(R.id.sender_phone);
        senderPageTitle = findViewById(R.id.sender_detail_title);
        

        senderName.setText(firebaseUser.getDisplayName());
        senderPhone.setText(firebaseUser.getPhoneNumber());

        senderName.setOnFocusChangeListener((view, b) -> {
            if (b) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
        });
        senderPhone.setOnFocusChangeListener((view, b) -> {
            if (b) {
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
        });

        senderName.setOnClickListener(view -> slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED));
        senderPhone.setOnClickListener(view -> slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED));

        saveSenderDetails = findViewById(R.id.saveSenderDetails);
        saveSenderDetails.setOnClickListener(saveSenderDetailsAndContinue);
        setupUI(saveSenderDetails);

        pickContact = findViewById(R.id.pick_contact);
        pickContact.setOnClickListener(view -> {
            contactTypes = ContactTypes.SENDER_CONTACT;
            readContact();
        });
        /*
         *   End of sender details From initializations  ################################
         * */


        /*
         *   Destination point FORM begins here ==================================
         *
         * */
        destinationPointLayout = MSlideUp.controlView(findViewById(R.id.layout_destination_point));
        sendToPointEt = findViewById(R.id.send_to_et);

        sendToPointEt.setOnTouchListener((view, event) -> {
//            final int DRAWABLE_LEFT = 0;
//            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
//            final int DRAWABLE_BOTTOM = 3;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (sendToPointEt.getRight() - sendToPointEt.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    // your action here
                    sendToPointEt.setText("");
                    return true;
                }
            }
            return false;
        });

        sendToPointEt.setOnFocusChangeListener(destinationPointFocusChanged);

        sendToPointEt.setOnClickListener(view -> {
        //    sendToPointEt.requestFocus();
            placeCompleteTypes = PlaceCompleteTypes.DESTINATION;
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        });
        sendToPointEt.addTextChangedListener(choosePointChange);

        destinationRecyclerView = findViewById(R.id.destination_recycler_view);
        destinationRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        destinationRecyclerView.setHasFixedSize(true);

        destinationRecyclerView.setAdapter(typingPlaceDestinationCompleteAdapter);

        setupUI(destinationRecyclerView);

        selectDestViaMap = findViewById(R.id.select_dest_via_map);
        selectDestViaMap.setVisibility(View.GONE);
        selectDestViaMap.setOnClickListener(view -> {

            configureSelectViaMap();
            viaMapType = ViaMapType.DESTINATION;

        });
        setupUI(selectDestViaMap);
        useCurrentLocationAsDest = findViewById(R.id.use_my_current_location_as_dest);
        useCurrentLocationAsDest.setVisibility(View.GONE);
        useCurrentLocationAsDest.setOnClickListener(view -> requestLocationPermission());

        setupUI(useCurrentLocationAsDest);

        // when more action is tapped on destination point

        selectedDestinationPointsAdapter.setRowMoreOptionTapped(row -> {

            recipientDetailsTitle.setText(row.name);
            currentDestinationSenderDetailsIndex = row.index;
            recipientPhoneEtMultple.setText("");
            recipientNameEtMultple.setText("");
            recipientDetailsLayout.show();

            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

        });


        typingPlaceDestinationCompleteAdapter.setRowTappedLister(placeComplete -> {
//            sendToPointEt.setText(placeComplete.name);
            sendToPointEt.setText("");

            //            add place selected to pick up points adapter
            //Toast.makeText(this, "destination point chosen", Toast.LENGTH_SHORT).show();
            Log.d(Constants.TAG, "destination point chosen: " + new Gson().toJson(placeComplete));

            LoadingGif.show(this);

            //Log.d(Constants.TAG, "destination place id : " + placeComplete.placeId);

            getLatLngFromPlaceId(placeComplete.placeId, place -> {

                if (place == null || place.getLatLng() == null) return;

                placeComplete.latitude = place.getLatLng().latitude;
                placeComplete.longitude = place.getLatLng().longitude;

                LoadingGif.hide();


                showDestinationDetailsLayout(placeComplete);

            });



        });

        /*
         *   End of destination point FORM ######################################
         * */

        /*
         *   Destination details FORM begins here =====================================
         * */

        //        destination recycler view //////////////

        destinationDetailsLayout = MSlideUp.controlView(findViewById(R.id.destination_details));
        singleRecipientDetailLayout = findViewById(R.id.singleRecipientDetailLayout);
        addDestinationPointBtn = findViewById(R.id.add_destination_point);
        addDestinationPointBtn.setOnClickListener(view -> {

                isMultipleDestinations = true;

                addPickUpPoint.setVisibility(View.GONE);

                typingPlacesDestinationArrayList.clear();
                typingPlaceDestinationCompleteAdapter.notifyDataSetChanged();


                destinationDetailsLayout.hide();
                destinationPointLayout.show();

               placeCompleteTypes = PlaceCompleteTypes.DESTINATION;
               slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);


            }
        );
        recipientPhone = findViewById(R.id.recipient_phone);
        recipientName = findViewById(R.id.recipient_name);



        findViewById(R.id.saveSaveDestinationDetails).setOnClickListener(view -> {

            Log.d(Constants.TAG, "initViews: saveSaveDestinationDetails destinationArrayLists " + destinationArrayLists);

            if (!isMultipleDestinations){
                // if its single destination

                Destination destination = destinationArrayLists.get(0);
                destination.recipientName = String.valueOf(recipientName.getText());
                destination.recipientNumber = String.valueOf(recipientPhone.getText());

                destinationArrayLists.set(0, destination);

            }

            // check if user wants to leave recipient details empty

            boolean recipientDetailsNotSet;
            if (isMultipleDestinations){
                recipientDetailsNotSet = TextUtils.isEmpty(destinationArrayLists.get(0).recipientName) || TextUtils.isEmpty(destinationArrayLists.get(0).recipientNumber);
            }else{
                recipientDetailsNotSet = TextUtils.isEmpty(recipientName.getText()) || TextUtils.isEmpty(recipientPhone.getText());
            }

            if (recipientDetailsNotSet){
                //                            addRecipientDetails();
//
//
//                            renderAndShowCheckout();
                Dialogs.confirmWithSweet(MainActivity.this)
                        .setTitleText("Fields Required")
                        .setContentText("All recipient details are required")
                        .setConfirmClickListener(Dialog::dismiss)
                        .setConfirmText("Okay")
                        .showCancelButton(false)
                        .setCancelText("Done")
                        .show();
                return;
            }



            if(!isMultipleDestinations)addRecipientDetails();

            renderAndShowCheckout();

        });

        RecyclerView destinationPlacesRecyclerView = findViewById(R.id.selected_destination_places_recyclerView);
        destinationPlacesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        destinationPlacesRecyclerView.setHasFixedSize(true);

//        selectedPickUpPointsAdapter.setRowRemoveItemTapped

        selectedDestinationPointsAdapter.setRowRemoveItemTapped(row -> {
            // remove corresponding marker on map

            Log.d(Constants.TAG, "initViews: remove from destination " + row.index);

            destinationArrayLists.remove(row.index);
            if (selectedDestinationPlaces.get(row.index) != null &&
                    selectedDestinationPlaces.get(row.index).marker != null
            ){
                selectedDestinationPlaces.get(row.index).marker.remove();
            }


            selectedDestinationPlaces.remove(row.index);

            if (selectedDestinationPlaces.size() == 0){
                sendToPointEt.setText("");
                destinationDetailsLayout.hide();
            }

            if (selectedDestinationPlaces.size() == 1){
                isMultipleDestinations = false;
                addPickUpPoint.setVisibility(View.VISIBLE);
            }

            reRenderDestination();
            reRenderMapItems();


        });
        destinationPlacesRecyclerView.setAdapter(selectedDestinationPointsAdapter);

        /*
         *   End of destination details form
         * */

        /*
         *   Recipient Details ===============================================
         *
         * */

        recipientDetailsLayout = MSlideUp.controlView(findViewById(R.id.layout_recipient_details));

        recipientNameEtMultple = findViewById(R.id.recipient_name_multiple);
        recipientPhoneEtMultple = findViewById(R.id.recipient_phone_multiple);
        
        
        recipientName.setOnTouchListener(slideLayoutUp);
        recipientPhone.setOnTouchListener(slideLayoutUp);
        //recipientNameEtMultple.setOnTouchListener(slideLayoutUp);
        //recipientPhoneEtMultple.setOnTouchListener(slideLayoutUp);

        ///

        findViewById(R.id.pick_recipient_contact_multiple).setOnClickListener(view -> {
            contactTypes = ContactTypes.RECIPIENT_CONTACT;
            readContact();
        });

        recipientDetailsTitle = findViewById(R.id.recipient_details_title);
        findViewById(R.id.saveRecipientDetails).setOnClickListener(view -> {

//            when recipient details sent, update the destination with the recipient details
            Destination destination = destinationArrayLists.get(currentDestinationSenderDetailsIndex);

            if (!TextUtils.isEmpty(recipientPhoneEtMultple.getText()))
                    destination.recipientNumber = String.valueOf(recipientPhoneEtMultple.getText());

            if (!TextUtils.isEmpty(recipientNameEtMultple.getText()))
                destination.recipientName = String.valueOf(recipientNameEtMultple.getText());

            destinationArrayLists.set(currentDestinationSenderDetailsIndex, destination);

            Log.d(Constants.TAG, "initViews: desinatinArrayList index = " + currentDestinationSenderDetailsIndex + " destination = " + destination.place + " recipient = " + destination.recipientNumber);

            PlaceComplete placeComplete = selectedDestinationPlaces.get(currentDestinationSenderDetailsIndex);

            placeComplete.description = recipientNameEtMultple.getText() + " " + recipientPhoneEtMultple.getText();
            selectedDestinationPlaces.set(currentDestinationSenderDetailsIndex, placeComplete);

            selectedDestinationPointsAdapter.changeDesciptionColor(true, currentDestinationSenderDetailsIndex);

            selectedDestinationPointsAdapter.notifyDataSetChanged();


            recipientDetailsLayout.hide();
            //selectedDestinationPlaces.get(currentDestinationSenderDetailsIndex).
        });

        findViewById(R.id.closeMultiRecipientDetails).setOnClickListener(view -> {
            recipientDetailsLayout.hide();
        });

        recipientName.setOnTouchListener(((view, motionEvent) -> {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
          return false;
        }));

        recipientPhone.setOnTouchListener(((view, motionEvent) -> {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            return false;
        }));

        findViewById(R.id.pick_recipient_contact).setOnClickListener(view -> {
            contactTypes = ContactTypes.RECIPIENT_CONTACT;
            readContact();
        });

        /*
         *   End of recipient details #########
         * */


//        selected destination points recycler view;
//        RecyclerView sel = findViewById(R.id.selected_pick_up_places_recyclerView);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        recyclerView.setHasFixedSize(true);
//
//        recyclerView.setAdapter(selectedPickUpPointsAdapter);
//
        // itemcategory.setAdapter(new CustomSpinnerAdapter(this, Item.getItems()));

        //slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);


    }

    private void showPreviousPickups() {
        new Thread(() -> {
            db.collection(Collections.PICKUPPOINTS.toString())
                    .whereEqualTo("userId", firebaseUser.getUid()).orderBy("dateCreated", Query.Direction.DESCENDING).limit(3).get()
                    .addOnSuccessListener(queryDocumentSnapshots -> {

                        if (queryDocumentSnapshots == null) return;

                        List<Pickup> pickups = queryDocumentSnapshots.toObjects(Pickup.class);

                        Log.d(Constants.TAG, "showPreviousPickups: " + new Gson().toJson(pickups));
                        if (pickups.isEmpty()){
                            return;
                        }

                        for (Pickup pickup : pickups){

                            PlaceComplete placeComplete = new PlaceComplete();
                            placeComplete.latitude = pickup.pos.geopoint.latitude;
                            placeComplete.longitude = pickup.pos.geopoint.longitude;
                            placeComplete.name = pickup.place;
                            placeComplete.description = "";

                            typingPlacesDestinationArrayList.add(placeComplete);
                        }

                        deliveryGif.setVisibility(View.GONE);
                        typingPlacePickUpCompleteAdapter.notifyDataSetChanged();



            });
        }).start();
    }

    private void configureSelectViaMap() {

        selectViaMapTapped = true;
//
        via_map_shimmer_view_container.startShimmerAnimation();

        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

        selectPickUpViaMapLayout.show();


        centered_pin.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.BounceIn)
                .duration(2000)
                .repeat(1)
                .playOn(centered_pin);

    }


    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    Constants.hideSoftKeyboard(MainActivity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }


    /// this is adding another pick up point -----------------
    private void addAnotherPickUpPoint() {
        pickUpDetailsLayout.hide();

        pickUpPointEt.setText("");
        typingPlacesPickUpArrayList.clear();
        typingPlacePickUpCompleteAdapter.notifyDataSetChanged();

        placeCompleteTypes = PlaceCompleteTypes.PICK_UP;
        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
    }

    ///// update existing pick up point -----------------
    private void updateExistingPickUpPoint(int index, SlideUp currentScreen){

//        currentScreen.hide();
//
//        pickUpPointEt.setText("");
//        typingPlacesPickUpArrayList.clear();
//        typingPlacePickUpCompleteAdapter.notifyDataSetChanged();
//
//
//        pickUpPointEt.setText(selectedPickupPlaces.get(index).name);
//
//        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

    }

    private View.OnTouchListener slideLayoutUp = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            return false;
        }
    };

    private void renderAndShowCheckout() {

        // check if there's pay for me
       // slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        if(request.payForMe > 0){

            request.isPayForMe = true;
        }

        pickPtsCheckOutArrayList.clear();
        destinationsCheckOutArrayList.clear();

//        For pick ups .........

        int j = 0;
        for (PlaceComplete placeComplete : selectedPickupPlaces) {
            CheckOut checkOut = new CheckOut();
            checkOut.index = placeComplete.index;
            checkOut.description = placeComplete.description;
            checkOut.latitude = placeComplete.latitude;
            checkOut.longitude = placeComplete.longitude;
            checkOut.leftIcon = placeComplete.leftIcon;
            checkOut.placeId = placeComplete.placeId;
            checkOut.type = Constants.PICK_UP;
            checkOut.name =  placeComplete.name;


            if (!pickupArrayList.isEmpty()){
                checkOut.phone = pickupArrayList.get(j).pickUpPersonPhone != null ? pickupArrayList.get(j).pickUpPersonPhone : "";
                checkOut.personName = pickupArrayList.get(j).pickUpName != null ? pickupArrayList.get(j).pickUpName : "";
            }

            j++;

            pickPtsCheckOutArrayList.add(checkOut);
        }

//        For destinations ..............

        int i = 0;
        for (PlaceComplete placeComplete : selectedDestinationPlaces){
            CheckOut checkOut = new CheckOut();
            checkOut.index = placeComplete.index;
            checkOut.description = placeComplete.description;
            checkOut.latitude = placeComplete.latitude;
            checkOut.longitude = placeComplete.longitude;
            checkOut.leftIcon = placeComplete.leftIcon;
            checkOut.placeId = placeComplete.placeId;
            checkOut.type = Constants.DELIVERY;
            checkOut.name = placeComplete.name;

            if (!destinationArrayLists.isEmpty()){
                checkOut.phone =  destinationArrayLists.get(i).recipientNumber != null ?  destinationArrayLists.get(i).recipientNumber : "";
                checkOut.personName =  destinationArrayLists.get(i).recipientName != null ? destinationArrayLists.get(i).recipientName : "";
            }

            i++;

            destinationsCheckOutArrayList.add(checkOut);
        }


        Intent intent = new Intent(MainActivity.this, CheckOutActivity.class);

        intent.putExtra(Constants.TRIP, request);
        intent.putExtra(Constants.CHECKOUT_PICK_UPS, pickPtsCheckOutArrayList);
        intent.putExtra(Constants.CHECKOUT_DESTINATIONS, destinationsCheckOutArrayList);


        intent.putExtra(Constants.IS_MULTIPLE_PICK_UPS, isMultiplePickUps);
        intent.putExtra(Constants.IS_MULTIPLE_DESTINATIONS, isMultipleDestinations);

        intent.putExtra(Constants.PICK_UPS_ARRAY_LIST, pickupArrayList);
        intent.putExtra(Constants.DESTINATIONS_ARRAY_LIST, destinationArrayLists);

        startActivity(intent);
        Bungee.slideLeft(this);


    }

    private void refreshState() {
//        LoadingGif.show(this);
        request = new Trip();
        request.isPickFromVendor = false;
        getIntent().putExtra(Constants.TRIP, request);
        selectedPickupPlaces.clear();
        map.clear();
        alertText.setText(initialAlertText);
        alertCardView.setBackgroundColor(getResources().getColor(R.color.pink));
        alertLove.setVisibility(View.VISIBLE);
        finish();
        startActivity(getIntent());

//        LoadingGif.hide();
    }


    //    save pick up details and continue
    private View.OnClickListener savePickUpDetailsAndContinue = view -> {
        // get teh item category,
        // item instruction

//        if (!TextUtils.isEmpty(itemCategory.getText())){
//            Toast.makeText(this, "Please select add pick up item", Toast.LENGTH_SHORT).show();
//            return;
//        }


        if(!(isMultiplePickUps))
        addPickUpDetails(0, String.valueOf(itemCategory.getText()), String.valueOf(itemInstructions.getText()), String.valueOf(senderName.getText()), String.valueOf(senderPhone.getText()));

        //Toast.makeText(this, "item details added", Toast.LENGTH_SHORT).show();
        
        //pickUpDetailsLayout.hide();
        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        senderDetailsLayout.show();

    };


    /*
     *   save sender details and continue
     */
    private View.OnClickListener saveSenderDetailsAndContinue = view -> {

        if (TextUtils.isEmpty(senderName.getText())){
            senderName.setError("Required");
            return;
        }

        if (TextUtils.isEmpty(senderPhone.getText())){
            senderPhone.setError("Required");
            return;
        }

        addSenderDetails();

        // get teh item category,
        // item instruction

        //senderDetailsLayout.hide();
        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        if (destinationArrayLists.size() > 0){
            destinationDetailsLayout.show();
        }else{
            destinationPointLayout.show();
        }

    };


    //    this event is called when pick up point changes focus
//    private View.OnFocusChangeListener pickUpPointFocusChanged = new View.OnFocusChangeListener() {
//        @Override
//        public void onFocusChange(View view, boolean b) {
//            if (b) {
//                placeCompleteTypes = PlaceCompleteTypes.PICK_UP;
//                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
////                selectViaMap.setVisibility(View.VISIBLE);
//            }
//        }
//    };

    //    this event is called when destination point point changes focus
    private View.OnFocusChangeListener destinationPointFocusChanged = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean b) {
            if (b) {
                selectDestViaMap.setVisibility(View.VISIBLE);
                useCurrentLocationAsDest.setVisibility(View.VISIBLE);

                placeCompleteTypes = PlaceCompleteTypes.DESTINATION;
                slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

            }
        }
    };


    //    listen to pick up point sliding panel
    private SlidingUpPanelLayout.PanelSlideListener slideUpPanelListener = new SlidingUpPanelLayout.PanelSlideListener() {
        @Override
        public void onPanelSlide(View panel, float slideOffset) {
//            final int panelHeight = findViewById(R.id.sliding_layout).getHeight();
//            final int visiblePanelHeight = slidingUpPanelLayout.getPanelHeight();
//            mMapFragment.getMapAsync(new OnMapReadyCallback() {
//                @Override
//                public void onMapReady(GoogleMap googleMap) {
//                    CameraPosition cameraPosition = googleMap.getCameraPosition();
//                    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
//                    googleMap.setPadding(0,0,0, (int) (visiblePanelHeight + (panelHeight - visiblePanelHeight) * slideOffset));
//                    googleMap.moveCamera(cameraUpdate);
//                }
//            });
        }

        @Override
        public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {

            if (newState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                // panel expanded

                Log.d(Constants.TAG, "onPanelStateChanged: expanded ");

                if (placeCompleteTypes == PlaceCompleteTypes.PICK_UP){
                    useMyCurrentLocation.setVisibility(View.VISIBLE);
                    selectViaMap.setVisibility(View.VISIBLE);
                    Log.d(Constants.TAG, "onPanelStateChanged: expanded");
                    pickUpPointEt.requestFocus();
                }

                else if(placeCompleteTypes == PlaceCompleteTypes.DESTINATION){
                    useCurrentLocationAsDest.setVisibility(View.VISIBLE);
                    selectDestViaMap.setVisibility(View.VISIBLE);
                    sendToPointEt.requestFocus();
                    deliveryGif.setVisibility(View.VISIBLE);
                }


            } else {
                Log.d(Constants.TAG, "onPanelStateChanged: collapsed ");
                if (placeCompleteTypes == PlaceCompleteTypes.PICK_UP){
                    Log.d(Constants.TAG, "onPanelStateChanged: collapsed");
                    pickUpPointEt.clearFocus();
                    useMyCurrentLocation.setVisibility(View.GONE);
                    selectViaMap.setVisibility(View.GONE);
                }
                else if(placeCompleteTypes == PlaceCompleteTypes.DESTINATION){
                    useCurrentLocationAsDest.setVisibility(View.GONE);
                    selectDestViaMap.setVisibility(View.GONE);
                    sendToPointEt.clearFocus();
                }

            }
        }
    };

    // Initialize map #############
        private void initMap() {
//        MapFragment mapFragment = (MapFragment) getFragmentManager()
//                .findFragmentById(R.id.main_map);

        mMapFragment = MapFragment.newInstance();
        FragmentTransaction fragmentTransaction =
                getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.container, mMapFragment);
        fragmentTransaction.commit();

        mMapFragment.getMapAsync(this);

//        init dummy map
//        MapFragment dummyMapFragment =  MapFragment.newInstance();
//        getFragmentManager().beginTransaction().add(R.id.dummy_map, dummyMapFragment).commit();
//        dummyMapFragment.getMapAsync(googleMap -> {
//            CameraPosition cameraPosition = new CameraPosition.Builder().
//                    target(new LatLng(5.5912045, -0.2497703)).
//                    zoom(DEFAULT_ZOOM).
//                    bearing(0).
//                    build();
//            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//            googleMap.getUiSettings().setZoomControlsEnabled(true);
//            googleMap.getUiSettings().setScrollGesturesEnabled(false);
//            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
//            googleMap.getUiSettings().setRotateGesturesEnabled(false);
//        });
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(Constants.TAG, "onMapReady: called " + googleMap);

        map = googleMap;

        if(getIntent() != null){
           Pickup pickup = (Pickup) getIntent().getSerializableExtra(Constants.PICK_UP);
            request = (Trip) getIntent().getSerializableExtra(Constants.TRIP);

           if (pickup != null && request.isPickFromVendor){
               PlaceComplete placeComplete = new PlaceComplete();
               placeComplete.index = 0;
               placeComplete.name = pickup.place;
               placeComplete.description = pickup.instruction;
               placeComplete.longitude = pickup.pos.geopoint.longitude;
               placeComplete.latitude = pickup.pos.geopoint.latitude;
               selectedPickupPlaces.add(placeComplete);
               pickupArrayList.add(pickup);

               request.senderName = firebaseUser.getDisplayName();
               request.senderPhoneNumber = firebaseUser.getPhoneNumber();


               reRenderMapItems();

               alertCardView.setBackgroundColor(getResources().getColor(R.color.black));
               alertText.setText("Enter delivery details in the panel below");
               alertText.setTextColor(getResources().getColor(R.color.white));
               alertLove.setVisibility(View.GONE);

               new Handler().postDelayed(() -> {
                   alertCardView.setVisibility(View.GONE);
               }, 5000);


               destinationPointLayout.show();

           }
           // showDestination pick pt panel
        }


        googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        this, R.raw.style_json));

        // map.setOnMarkerDragListener(this);
         map.setOnCameraIdleListener(() -> {


             //get latlng at the center by calling
             LatLng midLatLng = map.getCameraPosition().target;

              Constants.getAddress(this, geocoder, midLatLng, address1 -> {
                 runOnUiThread(new Runnable() {
                     @Override
                     public void run() {
                         if (address1 == null)return;

                         selectedAddress = address1;

                         pickupLocViaMap.setText(address1.name);

                         via_map_shimmer_view_container.setVisibility(View.GONE);
                         via_map_shimmer_view_container.stopShimmerAnimation();

                         pickupLocViaMap.setVisibility(View.VISIBLE);

                         runOnUiThread(() -> {
                             // check if camera has settled for 2 seconds
                             (new Handler()).postDelayed(() -> {

                                 if (!cameraIsMoving){

                                     Log.d(Constants.TAG, "take pick up point called ");

                                     if (selectViaMapTapped){

                                         centered_pin.setVisibility(View.GONE);

                                         PlaceComplete placeComplete = new PlaceComplete();
                                         placeComplete.name = address1.name;
                                         placeComplete.description = address1.description;
                                         placeComplete.latitude = address1.latitude;
                                         placeComplete.longitude = address1.longitude;

                                         selectPickUpViaMapLayout.hide();

                                         if (viaMapType == ViaMapType.PICK_UP)
                                            showPickUpDetailsForm(placeComplete);
                                         else
                                            showDestinationDetailsLayout(placeComplete);

                                         selectViaMapTapped = false;
                                     }

                                 }

                                 cameraIsMoving = false;

                             } , 5000);
                         });
                     }
                 });
              });



//             int screenHeight = getScreenHeight();
//             slidingUpPanelLayout.setPanelHeight(screenHeight / 3);


         });

         map.setOnCameraMoveListener(() -> {

             cameraIsMoving = true;

//             int screenHeight = getScreenHeight();
//             slidingUpPanelLayout.setPanelHeight(screenHeight / 4);

             pickupLocViaMap.setVisibility(View.GONE);

             via_map_shimmer_view_container.setVisibility(View.VISIBLE);
             via_map_shimmer_view_container.startShimmerAnimation();


         });

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
////
////        set the map to a default location so that it doesn't show the world
        CameraPosition cameraPosition = new CameraPosition.Builder().
                target(new LatLng(5.5912045, -0.2497703)).
                zoom(DEFAULT_ZOOM).
                bearing(0).
                build();

        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
        }

        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.getUiSettings().setMapToolbarEnabled(true);

//        GoogleMapOptions googleMapOptions = new GoogleMapOptions().liteMode(true);
//        googleMap.setMapType(googleMapOptions.getMapType());

        if (checkIfLocationIsOn() && fusedLocationClient != null){
            fusedLocationClient.getLastLocation().addOnCompleteListener(task -> {
                if (!task.isSuccessful()){
                    return;
                }

                Location location = task.getResult();
                if (location == null) return;

                if (request != null && request.isPickFromVendor){
                    reRenderMapItems();
                    return;
                }

                CameraPosition myLocationPosition = new CameraPosition.Builder().
                        target(new LatLng(location.getLatitude(), location.getLongitude())).
                        zoom(DEFAULT_ZOOM).
                        bearing(0).
                        build();

                map.animateCamera(CameraUpdateFactory.newCameraPosition(myLocationPosition));

            });

        }else{
            return;
        }

        if (!isNetworkAvailable(MainActivity.this)){
            connectionErrorFragment.show(getSupportFragmentManager(), "CONNECTION_ERROR");
        }

    }


    @Override
    public void onBackPressed() {
        if (slidingUpPanelLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            return;
        }

        if (selectPickUpViaMapLayout.isVisible()){
            selectViaMapTapped = false;
            centered_pin.setVisibility(View.GONE);
            selectPickUpViaMapLayout.hide();
            slidingUpPanelLayout.setPanelHeight(defaultPanelHeight);
            return;
        }

        if (recipientDetailsLayout.isVisible()) {
            recipientDetailsLayout.hide();
            return;
        }


        if (destinationDetailsLayout.isVisible()) {

//            if (isMultiplePickUps){
////                selectedDestinationPlaces.clear();
////                selectedDestinationPointsAdapter.notifyDataSetChanged();
//            }

            typingPlacesDestinationArrayList.clear();
            typingPlaceDestinationCompleteAdapter.notifyDataSetChanged();

            destinationDetailsLayout.hide();
            return;
        }

        if (destinationPointLayout.isVisible()) {

            if(request.isPickFromVendor){
                refreshState();
                return;
            }

            if (isMultipleDestinations){
               // destinationDetailsLayout.show();
            }
            destinationPointLayout.hide();
            return;
        }

        if (senderDetailsLayout.isVisible()) {
            senderDetailsLayout.hide();
            return;
        }

        if (pickUpDetailsLayout.isVisible()) {
          //  pickUpDetailsLayout.hide();

//            if (isMultipleDestinations){
//                selectedPickupPlaces.clear();
//                selectedPickUpPointsAdapter.notifyDataSetChanged();
//            }

           // isAddPickupFromCheckout = false;
            pickUpPointEt.setText("");
            typingPlacesPickUpArrayList.clear();
            typingPlacePickUpCompleteAdapter.notifyDataSetChanged();

//            addAnotherPickUpPoint();
            if(isMultipleDestinations){
                Dialogs.confirm(this).setTitle("Do you want to exit app?").setPositiveButton("Yes", view -> {
                    refreshState();
                    moveTaskToBack(true);
                    Bungee.slideRight(this);
                }).show();
                return;
            }

            pickUpDetailsLayout.hide();
            return;
        }

       if (!selectedPickupPlaces.isEmpty()){
           pickUpDetailsLayout.show();
           return;
       }


       if (drawer.isDrawerOpen(GravityCompat.START)){
           drawer.closeDrawer(GravityCompat.START);
           return;
       }


        if (doubleBackToExitPressedOnce) {
            moveTaskToBack(true);
            Bungee.slideRight(this);
            return;
        }

        doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    /*
     *   Trip permissions from user //////////////////////////////////////////////
     * */
    private void requestMapPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int checkCallPhonePermission = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);

            if (checkCallPhonePermission != PackageManager.PERMISSION_GRANTED) {
                //Without the permission to Write, to apply for the permission to Read and Write, the system will pop up the permission itemCategoriesDialog
                ActivityCompat.requestPermissions(this, MY_PERMISSIONS, REQUEST_PERM_CODE);
            } else {
                initMap();
            }
        } else {
            initMap();
        }
    }

    /*
     *   Trip location permission
     * */

    private void requestLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int checkCallPhonePermission = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);
            if (checkCallPhonePermission != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, MY_PERMISSIONS, REQUEST_LOC_PERM);
            } else {
                getUserLastKnowLocation();
            }
        } else {
            getUserLastKnowLocation();
        }
    }

    private void getUserLastKnowLocation() {

        // check if user's location is turned on, and turn it on

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }

        LoadingGif.show(this);

        fusedLocationClient.getLastLocation().addOnSuccessListener(this, location -> {

           //
            // Got last known location. In some rare situations this can be null.

           // Log.d(Constants.TAG, "getUserLastKnowLocation: " + location);

            if (location != null) {
                // Logic to handle location object

               Constants.getAddress(this,geocoder, new LatLng(location.getLatitude(), location.getLongitude()), foundAddress -> runOnUiThread(() -> {
                   if (foundAddress == null) {
                       Toast.makeText(MainActivity.this, "Unable to find your location", Toast.LENGTH_SHORT).show();
                        LoadingGif.hide();
                       return;
                   }

                   PlaceComplete placeComplete = new PlaceComplete();
                   placeComplete.name = foundAddress.name;
                   placeComplete.description = foundAddress.description;
                   placeComplete.latitude = location.getLatitude();
                   placeComplete.longitude = location.getLongitude();

                    LoadingGif.hide();

                   if (placeCompleteTypes == PlaceCompleteTypes.PICK_UP){
                       //pickUpPointEt.setText(foundAddress);

                       showPickUpDetailsForm(placeComplete);
                   }

                   if (placeCompleteTypes == PlaceCompleteTypes.DESTINATION){
                       //sendToPointEt.setText(foundAddress);

                       showDestinationDetailsLayout(placeComplete);

                   }
               }));

                return;

            }

            LoadingGif.hide();

            checkIfLocationIsOn();
            //Toast.makeText(this, "Unable to get your location. We recommend you to type in the search box above", Toast.LENGTH_SHORT).show();


        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERM_CODE) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this,"Kindly grant permission for maps to function",Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(this, MY_PERMISSIONS, REQUEST_PERM_CODE);
            }else
            {
                initMap();
            }
        }

        if (requestCode == REQUEST_LOC_PERM){
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this,"Kindly grant permission to get your current location",Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(this, MY_PERMISSIONS, REQUEST_PERM_CODE);
            }else
            {
                getUserLastKnowLocation();
            }
        }
    }


//    content loader ///////////////
    private void showContentLoader(){


        if (placeCompleteTypes == PlaceCompleteTypes.PICK_UP){
            pickUpRecyclerView.setVisibility(View.GONE);
            mShimmerViewContainer.startShimmerAnimation();
            mShimmerViewContainer.setVisibility(View.VISIBLE);
            deliveryGif.setVisibility(View.GONE);
        }

        if (placeCompleteTypes == PlaceCompleteTypes.DESTINATION){
            destinationRecyclerView.setVisibility(View.GONE);
            mShimmerViewContainerDestination.startShimmerAnimation();
            mShimmerViewContainerDestination.setVisibility(View.VISIBLE);
        }



    }

    private void hideContentLoader(){

        if (placeCompleteTypes == PlaceCompleteTypes.PICK_UP){
            pickUpRecyclerView.setVisibility(View.VISIBLE);
            mShimmerViewContainer.stopShimmerAnimation();
            mShimmerViewContainer.setVisibility(View.GONE);
            deliveryGif.setVisibility(View.VISIBLE);
        }

        if (placeCompleteTypes == PlaceCompleteTypes.DESTINATION){
            destinationRecyclerView.setVisibility(View.VISIBLE);
            mShimmerViewContainerDestination.stopShimmerAnimation();
            mShimmerViewContainerDestination.setVisibility(View.GONE);
        }

    }




/////////////////    auto complete ////////////////////////////////

    //    this event is called when the user is typing in the itemName
    private TextWatcher choosePointChange = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

            //Log.d(Constants.TAG, "onTextChanged: " + charSequence);


            if (!String.valueOf(charSequence).isEmpty()){
                showContentLoader();
            }else{
                hideContentLoader();

                if (placeCompleteTypes == PlaceCompleteTypes.PICK_UP){
                    typingPlacesPickUpArrayList.clear();
                    typingPlacePickUpCompleteAdapter.notifyDataSetChanged();
                }

                if (placeCompleteTypes == PlaceCompleteTypes.DESTINATION){
                    typingPlacesDestinationArrayList.clear();
                    typingPlaceDestinationCompleteAdapter.notifyDataSetChanged();
                }

              //  Toast.makeText(MainActivity.this, "Toast", Toast.LENGTH_SHORT).show();
            }

            if (selectViaMapTapped){
                return;
            }

            // Use the builder to create a FindAutocompletePredictionsRequest.
            FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                    // Call either setLocationBias() OR setLocationRestriction().
                    .setLocationBias(bounds)
                    .setCountry("GH")
                    .setSessionToken(token)
                    .setQuery(charSequence.toString())
                    .build();

            placesClient.findAutocompletePredictions(request).addOnSuccessListener((response) -> {


                if (response.getAutocompletePredictions().isEmpty()){
                    hideContentLoader();
                }

                if (placeCompleteTypes == PlaceCompleteTypes.PICK_UP)
                    typingPlacesPickUpArrayList.clear();

                if (placeCompleteTypes == PlaceCompleteTypes.DESTINATION)
                    typingPlacesDestinationArrayList.clear();

                for (AutocompletePrediction prediction : response.getAutocompletePredictions()) {
                    String foundPlace = prediction.getPrimaryText(null).toString();
                    Log.i(Constants.TAG, prediction.getPlaceId());
                    Log.i(Constants.TAG, foundPlace);

                    PlaceComplete placeComplete = new PlaceComplete();
                    placeComplete.name = foundPlace;
                    placeComplete.description = prediction.getSecondaryText(null).toString();
                    placeComplete.placeId = prediction.getPlaceId();
                    placeComplete.showRemoveBtn = false;
//                    placeComplete.latLng = new LatLng(prediction.)

                    if (placeCompleteTypes == PlaceCompleteTypes.PICK_UP){
                        placeComplete.leftIcon = R.drawable.pick_up_icon;
                        typingPlacesPickUpArrayList.add(placeComplete);
                    }


                    if (placeCompleteTypes == PlaceCompleteTypes.DESTINATION){
                        placeComplete.leftIcon = R.mipmap.destination_icon;
                        typingPlacesDestinationArrayList.add(placeComplete);
                    }

                    hideContentLoader();

                    if (placeCompleteTypes == PlaceCompleteTypes.PICK_UP){
                        deliveryGif.setVisibility(View.GONE);
                        typingPlacePickUpCompleteAdapter.notifyDataSetChanged();
                    }


                    if (placeCompleteTypes == PlaceCompleteTypes.DESTINATION){
                        typingPlaceDestinationCompleteAdapter.notifyDataSetChanged();
                    }


                }
            }).addOnFailureListener((exception) -> {

                if (exception instanceof NoConnectionError){
                    hideContentLoader();
                    return;
                }

                if (exception instanceof ApiException) {
                    ApiException apiException = (ApiException) exception;

                    hideContentLoader();
                    Log.e(Constants.TAG, "PlaceComplete not found: " + apiException.getMessage());
                    Log.e(Constants.TAG, "PlaceComplete not found: " + apiException.getLocalizedMessage());

                }

                hideContentLoader();

//                if (noConnectionDialog == null){
//                    noConnectionDialog = Dialogs.alertWithSweet(MainActivity.this, "Please check your connection");
//                }else if(!noConnectionDialog.isShowing()){
//                    noConnectionDialog.show();
//                }

            });


        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

/////////////////   End of auto complete ////////////////////////////////

    //    pick user contact

    public void readContact(){
        try {
            Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            startActivityForResult(i, PICK_CONTACT);

        } catch (Exception e) {
            e.printStackTrace();
            Constants.callErrorPage(this, "Read contact error : " + e.getMessage());
        }
    }

//    onActivity result

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(Constants.TAG, "onActivityResult: request code - " + requestCode);


        try {
            if (requestCode == PICK_CONTACT) {
                if (resultCode == Activity.RESULT_OK) {

                    if (data == null) return;

                    Uri contactData = data.getData();
                    if (contactData == null) return;
                    Cursor cursor = getContentResolver().query(contactData, null, null, null, null);

                    if (cursor == null) return;
                    cursor.moveToFirst();

//                        int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
//                        (new normalizePhoneNumberTask()).execute(cursor.getString(column));
//                        Log.d("phone number", cursor.getString(column));

                    String number = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    String name = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

                    cursor.close();
                    //contactName.setText(name);
                    if (contactTypes == ContactTypes.SENDER_CONTACT) {
                        senderName.setError(null);
                        senderPhone.setError(null);
                        senderName.setText(name);
                        senderPhone.setText(number);
                    }


                    if (contactTypes == ContactTypes.RECIPIENT_CONTACT) {

                        if (isMultipleDestinations) {
                            recipientNameEtMultple.setText(name);
                            recipientPhoneEtMultple.setText(number);
                        }else{
                            recipientName.setText(name);
                            recipientPhone.setText(number);
                        }

                    }

                    if (contactTypes == ContactTypes.PICK_UP_CONTACTS) {
                        multi_pick_up_person.setText(name);
                        multi_pick_up_person_phone.setText(number);
                    }

                }
            }
        }catch (Exception e){
            Constants.callErrorPage(this, "after pick contact: Exception " + e.getMessage());
        }
    }

//    @Override
//    public void onMarkerDragStart(Marker marker) {
//
//    }
//
//    @Override
//    public void onMarkerDrag(Marker marker) {
//        markerAnimateTimer.cancel();
//    }
//
//    @Override
//    public void onMarkerDragEnd(Marker marker) {
//        updateProfile = new CustomTimerTask(MainActivity.this, draggablePickPtMarker);
//        markerAnimateTimer = new Timer();
//        markerAnimateTimer.scheduleAtFixedRate(updateProfile, 10,5000);
//
//        if (placeCompleteTypes == PlaceCompleteTypes.PICK_UP){
//
//            LatLng newPosition = marker.getPosition();
//            String address =  Constants.getAddress(geocoder, newPosition);
//
//            if (address == null){
//                Toast.makeText(this, "unable to get name of location", Toast.LENGTH_SHORT).show();
//                // show the name of the place in a dialog
//              return;
//            }
//
//            Dialogs.confirm(MainActivity.this, "You dragged to " + address).setPositiveButton("Accept", view -> {
//                 PlaceComplete placeComplete =  new PlaceComplete();
//                                placeComplete.name = address;
//                                showPickUpDetailsForm(placeComplete);
//            }).show();
//
//        }
//
//    }

    //// ========================== Forms Transitions ======================

    private void showPickUpDetailsForm(PlaceComplete placeComplete){

//         check if the selected location is with active locations

        if(request == null){
            request = new Trip();
        }
        if(!request.isPickFromVendor){
            alertCardView.setVisibility(View.GONE);
        }

        String url = Constants.BaseUrl + "/universal/calculate-distances/check-location";
        HashMap<String, String> param = new HashMap<>();
        param.put("place", placeComplete.name);
        param.put("latitude", String.valueOf(placeComplete.latitude));
        param.put("longitude", String.valueOf(placeComplete.longitude));
        param.put("pointType", "PICKUP");


        runOnUiThread(() -> {
            LoadingGif.show(MainActivity.this);
        });

             new RequestObject(Request.Method.POST, url, param, response -> {

                try {

                    boolean status = response.getBoolean("status");
                    String message = response.getString("message");

                    if(status){

                        runOnUiThread(() -> {

                            pickUpPointEt.setText(placeComplete.name);

                            Log.d(Constants.TAG, "showPickUpDetailsForm: index of uuu " + placeComplete.index);

                            pickUpPointEt.setText("");

                            //            show panel

                            //int screenHeight = getScreenHeight();
//        int panelHeight = (screenHeight) / 2;
//        slidingUpPanelLayout.setPanelHeight(panelHeight);
//        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

                            if (selectedPickupPlaces.size() > 0){
                                isMultiplePickUps = true;
                                addDestinationPointBtn.setVisibility(View.GONE);
                            }else{
                                isMultiplePickUps = false;
                            }

                            String senderTitle;
                            if (isMultiplePickUps){
                                senderTitle = "Sender contact";
                            }else{
                                senderTitle = "Pick up contact";
                            }
                            senderPageTitle.setText(senderTitle);


                            placeComplete.index = selectedPickupPlaces.size();
                            selectedPickupPlaces.add(placeComplete);

                            reRenderPickList();


//        add pick up to pickup arrayList
                            addOrUpdatePickPoint(placeComplete);


                            reRenderMapItems();

                            selectViaMapTapped = false;

                            Pos pickPos1 = new Pos();
                            LatLng pickLatLng1 = new LatLng(placeComplete.latitude, placeComplete.longitude);
                            pickPos1.geohash = Constants.getGeoHash(pickLatLng1);
                            pickPos1.geopoint = new Pos.PosLatLng(pickLatLng1.latitude, pickLatLng1.longitude);
                            pickPos1.geohash = Constants.getGeoHash(pickLatLng1);

                            Pickup pickup1 = pickupArrayList.get(placeComplete.index);
                            pickup1.isPickedup = false;
                            pickup1.pos = pickPos1;

                            pickup1.userId = firebaseUser.getUid();
                            pickup1.documentReference = db.collection(Collections.PICKUPPOINTS.toString()).document(Constants.getUniqueString());

                            pickupArrayList.set(placeComplete.index, pickup1);

                            //        if (isAddPickupFromCheckout){
                            //            pickUpDetailsLayout.show();
                            //            senderDetailsLayout.show();
                            //            destinationPointLayout.show();
                            //            destinationDetailsLayout.show();
                            //            recipientDetailsLayout.show();
                            //
                            //
                            //            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                            //
                            //            return;
                            //        }


                            slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                            pickUpDetailsLayout.show();
                            LoadingGif.hide();

                        });

                    }else{

                           runOnUiThread(() -> {

                               areaNotIncludedDialog = AreaNotIncludedDialog.newInstance(message);
                               areaNotIncludedDialog.show(getSupportFragmentManager(), "area not included");
                               LoadingGif.hide();
                            });
                    }

                } catch (JSONException e) {
                    Log.d(Constants.TAG, "payWithMoMo: JSONException " + e.getMessage());
                    e.printStackTrace();
                }


            }, error -> {
                Log.d(Constants.TAG, "showPickUpDetailsForm: " + error);
                runOnUiThread(() -> {
                    LoadingGif.hide();
                    Dialogs.alert(MainActivity.this, "Please try again later");
                });

            }).send(MainActivity.this);





//        // TODO: 2019-09-07 uncomment this
//        ActiveLocationResult activeLocationResult = RestrictLocation.getInstance().validatePoint(placeComplete, MainActivity.this, ActiveLocation.PICK_UP);
////
//        if(!activeLocationResult.isDistanceWithinActiveLocation){
//
//            StringBuilder message = new StringBuilder("We currently operate within ");
//            for (ActivatedLocations activatedLocations : activeLocationResult.activeLocationList){
//                message.append(activatedLocations.name).append(",");
//            }
//
//            String fieldName = message.toString();
//            if(fieldName.endsWith(","))
//            {
//                fieldName = fieldName.substring(0,fieldName.length() - 1) + ".";
//            }
//            areaNotIncludedDialog = AreaNotIncludedDialog.newInstance(fieldName);
//            areaNotIncludedDialog.show(getSupportFragmentManager(), "area not included");
//            return;
//        }



        new Thread(() -> {
            try{
                mSocket = SockIOConnection.getSocketInstance();
            }catch (Exception e){

            }
        }).start();

//        draggablePickPtMarker.setVisible(false);

    }


    private void reRenderPickList(){

        if (isMultiplePickUps){

            pickUpDetailForm.setVisibility(View.GONE);

            int drawable;
            int i = 0;
            for (PlaceComplete pc : selectedPickupPlaces) {
                pc.showMoreBtn = true;

                pc.index = i;
                Log.d(Constants.TAG, "showPickUpDetailsForm: index " + pc.index);

                switch (i){
                    case 0:
                        drawable = R.drawable.pickup_raw_1;
                        break;
                    case 1:
                        drawable = R.drawable.pickup_raw_2;
                        break;
                    case 2:
                        drawable = R.drawable.pickup_raw_3;
                        break;
                    case 3:
                        drawable = R.drawable.pickup_raw_4;
                        break;
                    case 4:
                        drawable = R.drawable.pickup_raw_5;
                        break;
                    case 5:
                        drawable = R.drawable.pickup_raw_6;
                        break;
                    default:
                        drawable = R.drawable.pickup_raw_1;
                        break;

                }

                pc.leftIcon = drawable;

                i++;

            }

            // add another item category dialog to correspond
            ItemCategoriesDialog itemCategoriesDialog = new ItemCategoriesDialog();
            itemCategoriesDialog.setItemRowTappedLister(row -> multi_item_category.setText(row.itemName));
            multipleItemCategoriesDialogs.add(itemCategoriesDialog);


            selectedPickUpPointsAdapter.notifyDataSetChanged();

            Log.d(Constants.TAG, "multipleItemCategoriesDialogs added : size " + multipleItemCategoriesDialogs.size());

            //            add place selected to pick up points adapter

        }else{

            selectedPickupPlaces.get(0).index = 0;
            selectedPickupPlaces.get(0).leftIcon = R.drawable.pick_up_icon;
            selectedPickUpPointsAdapter.notifyDataSetChanged();

        }


    }

    private void reRenderDestination(){

        if (isMultipleDestinations){

            Log.d(Constants.TAG, "showDestinationDetailsLayout:selectedDestinationPlaces.size() " + selectedDestinationPlaces.size());
            int i = 0;
            int drawable;
            for (PlaceComplete pc : selectedDestinationPlaces) {
                pc.showMoreBtn = true;

                pc.index = i;
                switch (i){
                    case 0:
                        // pc.marker.remove();
                        drawable = R.drawable.deliver_raw_1;
                        break;
                    case 1:
                        drawable = R.drawable.deliver_raw_2;
                        break;
                    case 2:
                        drawable = R.drawable.deliver_raw_3;
                        break;
                    case 3:
                        drawable = R.drawable.deliver_raw_4;
                        break;
                    case 4:
                        drawable = R.drawable.deliver_raw_5;
                        break;
                    case 5:
                        drawable = R.drawable.deliver_raw_6;
                        break;
                    default:
                        drawable = R.drawable.deliver_raw_1;
                        break;
                }

                pc.leftIcon = drawable;
                selectedDestinationPlaces.set(i, pc);
                i++;
            }

            selectedDestinationPointsAdapter.notifyDataSetChanged();


        }else {
            //todo was here
            if (selectedDestinationPlaces.size() ==  1){
                PlaceComplete placeComplete = selectedDestinationPlaces.get(0);
                placeComplete.leftIcon = R.drawable.delivery_raw;
                placeComplete.index = 0;

                selectedDestinationPointsAdapter.notifyDataSetChanged();
            }


        }
    }

    @SuppressLint("SetTextI18n")
    private void showDestinationDetailsLayout(PlaceComplete placeComplete) {

//        sendToPointEt.setText("");

//        ActiveLocationResult activeLocationResult = RestrictLocation.getInstance().validatePoint(placeComplete, MainActivity.this, ActiveLocation.DESTINATION);
//        if(!activeLocationResult.isDistanceWithinActiveLocation){
//            StringBuilder message = new StringBuilder("We currently operate within ");
//            for (ActivatedLocations activatedLocations : activeLocationResult.activeLocationList){
//                message.append(activatedLocations.name).append(",");
//            }
//            String fieldName = message.toString();
//            if(fieldName.endsWith(","))
//            {
//                fieldName = fieldName.substring(0,fieldName.length() - 1) + ".";
//            }
//            areaNotIncludedDialog = AreaNotIncludedDialog.newInstance(fieldName);
//            areaNotIncludedDialog.show(getSupportFragmentManager(), "area not included");
//            return;
//        }

        // hide the recipient details form
        /// show the more buttons on the destination



            String url = Constants.BaseUrl + "/universal/calculate-distances/check-location";
            HashMap<String, String> param = new HashMap<>();
            param.put("place", placeComplete.name);
            param.put("latitude", String.valueOf(placeComplete.latitude));
            param.put("longitude", String.valueOf(placeComplete.longitude));
            param.put("pointType", "DELIVERY");

        runOnUiThread(() -> {
            LoadingGif.show(MainActivity.this);
        });

           // LoadingGif.show(MainActivity.this);
            RequestObject requestObject = new RequestObject(Request.Method.POST, url, param, response -> {

                try {
                    boolean status = response.getBoolean("status");
                    String message = response.getString("message");

                    if(status){

                       runOnUiThread(() -> {
                           placeComplete.index = selectedDestinationPlaces.size();
                           selectedDestinationPlaces.add(placeComplete);

                           reRenderDestination();

                           if (isMultipleDestinations){
                               singleRecipientDetailLayout.setVisibility(View.GONE);
                           }else{
                               singleRecipientDetailLayout.setVisibility(View.VISIBLE);
                           }

                           reRenderMapItems();
                           addDestinationPoint(placeComplete);

                           Destination destination = destinationArrayLists.get(placeComplete.index);

                           Pos destPos1 = new Pos();

                           destPos1.geohash = Constants.getGeoHash  (new LatLng(placeComplete.latitude, placeComplete.longitude));
                           destPos1.geopoint = new Pos.PosLatLng(placeComplete.latitude, placeComplete.longitude);
                           destination.pos = destPos1;
                           destination.userId = firebaseUser.getUid();
                           destination.documentReference = db.collection(Collections.DESTINATIONS.toString()).document(Constants.getUniqueString());

                           destinationArrayLists.set(placeComplete.index, destination);

                           slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                           destinationPointLayout.hide();
                           destinationDetailsLayout.show();

                           LoadingGif.hide();

                       });

                    }else{
                       runOnUiThread(() -> {

                           areaNotIncludedDialog = AreaNotIncludedDialog.newInstance(message);
                           areaNotIncludedDialog.show(getSupportFragmentManager(), "area not included");

                           LoadingGif.hide();
                       });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }, error -> {
                runOnUiThread(() -> {
                    LoadingGif.hide();
                    Dialogs.alert(MainActivity.this, "Please try again later");
                });
            });

            requestObject.send(MainActivity.this);




        new Thread(() -> {
            try{
                mSocket = SockIOConnection.getSocketInstance();
            }catch (Exception e){

            }
        }).start();

    }


    private void drawRoute(ArrayList<LatLng> points){

        Log.d(Constants.TAG, "drawRoute: called " + points);

        if (points.size() <= 1) return;

        if (points.size() == 2) {
            // single trip
            LatLng firstPt = points.get(0);
            LatLng lastPt = points.get(points.size() - 1);

            GoogleDirection.withServerKey(getResources().getString(R.string.the_google_server_key))
                    .from(firstPt)
                    .to(lastPt)
                    .transportMode(TransportMode.DRIVING)
                    .execute(new DirectionCallback() {
                        @Override
                        public void onDirectionSuccess(Direction direction, String rawBody) {
                            if (direction.isOK()) {
                                animateRouteAndCalculateTimeDistance(direction);
                            } else {
                                Log.d(Constants.TAG, "onDirectionFailure: NOt OK" + rawBody);
                            }

                        }
                        @Override
                        public void onDirectionFailure(Throwable t) {

                        }
                    });

        }else{

            LatLng previous;
            LatLng next;

            for (int i = 0; i < points.size() - 1; i++) {
                previous = points.get(i);
                next = points.get( i + 1);

                GoogleDirection.withServerKey(getResources().getString(R.string.the_google_server_key))
                        .from(previous)
                        .to(next).execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        if (direction.isOK()) {
                            animateRouteAndCalculateTimeDistance(direction);
                        } else {
                            Log.d(Constants.TAG, "onDirectionFailure: NOt OK" + rawBody);
                        }
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {

                    }
                });

            }

        }

    }



    private void animateRouteAndCalculateTimeDistance(Direction direction){
        // Do something
        Log.d(Constants.TAG, "direction.getRouteList(): " + direction.getRouteList().size());
        Route route = direction.getRouteList().get(0);
        Leg leg = route.getLegList().get(0);

//        request.estimatedDistanceInMeters += Double.parseDouble(leg.getDistance().getValue());
//        request.estimatedTimeInSeconds += Integer.parseInt(leg.getDuration().getValue());

        Log.d(Constants.TAG, "onDirectionSuccess: total distance - " +leg.getDistance().getValue());
        Log.d(Constants.TAG, "onDirectionSuccess: total time - " + leg.getDuration().getValue());

        ArrayList<LatLng> directionPositionList = leg.getDirectionPoint();
        startAnim(directionPositionList);
    }


    private boolean checkIfLocationIsOn() {
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e) {
            Log.d(Constants.TAG, "location check error : " + e.getMessage());
        }
        boolean isLocationOn = false;
        if(!gps_enabled && !network_enabled){
            isLocationOn = false;
            LocationSettingsDialog fragment = new LocationSettingsDialog();
            fragment.addActivity(this);
            fragment.show(getFragmentManager(),Constants.LOCATION_SETTINGS);
        }else{
            Log.d(Constants.TAG, "location is already on : ");
            isLocationOn = true;
        }

        return isLocationOn;
    }

    private void addPickPointPointMarkerToMap(ArrayList<PlaceComplete> selectedPickupPlaces, String place){

//        LatLngBounds.Builder builder = new LatLngBounds.Builder();
//        // 1 pick up multiple destinations
//
//        int i = 0;
//        int drawable;
//
//        for(PlaceComplete pickupSelected : selectedPickupPlaces){
//            builder.include(new LatLng(pickupSelected.latitude, pickupSelected.longitude));
//
//            if (isMultiplePickUps){
//
//                switch (i){
//                    case 0:
//                        if (pickupSelected.marker == null) return;
//                        pickupSelected.marker.remove();
//                        drawable = R.drawable.pick_up_1;
//                        break;
//                    case 1:
//                        drawable = R.drawable.pick_up_2;
//                        break;
//                    case 2:
//                        drawable = R.drawable.pick_up_3;
//                        break;
//                    case 3:
//                        drawable = R.drawable.pick_up_4;
//                        break;
//                    case 4:
//                        drawable = R.drawable.pick_up_5;
//                        break;
//                    case 5:
//                        drawable = R.drawable.pick_up_6;
//                        break;
//                    default:
//                        drawable = R.drawable.pick_up_1;
//                        break;
//                }
//            }else{
//
//                drawable = R.drawable.pick_up_single;
//            }
//
//            Bitmap bm = Constants.resizeDrawable(this, drawable);
//
//            Marker marker = map.addMarker(
//                    new MarkerOptions()
//                            .position(new LatLng(pickupSelected.latitude, pickupSelected.longitude))
//                            .title(place)
//                            .icon(BitmapDescriptorFactory.fromBitmap(bm))
//            );
//
//            marker.showInfoWindow();
//            pickupSelected.marker = marker;
//
//            i++;
//        }
//
////        if (isMultiplePickUps){
////             map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
////        }else{
////
////        }
//
//        if (selectedPickupPlaces.size() == 1){
//            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(selectedPickupPlaces.get(0).latitude, selectedPickupPlaces.get(0).longitude), DEFAULT_ZOOM));
//        }else{
//            LatLngBounds bounds = builder.build();
//            Point displaySize = new Point();
//            getWindowManager().getDefaultDisplay().getSize(displaySize);
//            map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 250, 30));
//        }
    }

    private void reRenderMapItems(){
        Log.d(Constants.TAG, "reRenderMapItems: pickupArrayList size - " + pickupArrayList.size());
        Log.d(Constants.TAG, "reRenderMapItems: destinationArrayLists size - " + destinationArrayLists.size());

        Log.d(Constants.TAG, "reRenderMapItems: selectedPickupPlaces size - " + selectedPickupPlaces.size());
        Log.d(Constants.TAG, "reRenderMapItems: selectedDestinationPlaces size - " + selectedDestinationPlaces.size());

        map.clear();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        ArrayList<LatLng> routeBuilder = new ArrayList<>();
        // 1 pick up multiple destinations

        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

        int i = 0;
        int drawable;

        for(PlaceComplete pickupSelected : selectedPickupPlaces){
            builder.include(new LatLng(pickupSelected.latitude, pickupSelected.longitude));
            LatLng origin = new LatLng(pickupSelected.latitude, pickupSelected.longitude);
            routeBuilder.add(origin);

            String place = pickupSelected.name;
            if (isMultiplePickUps){


                switch (i){
                    case 0:
                        drawable = R.drawable.pick_up_1;
                        break;
                    case 1:
                        drawable = R.drawable.pick_up_2;
                        break;
                    case 2:
                        drawable = R.drawable.pick_up_3;
                        break;
                    case 3:
                        drawable = R.drawable.pick_up_4;
                        break;
                    case 4:
                        drawable = R.drawable.pick_up_5;
                        break;
                    case 5:
                        drawable = R.drawable.pick_up_6;
                        break;
                    default:
                        drawable = R.drawable.pick_up_1;
                        break;
                }
            }else{

                drawable = R.drawable.pick_up_single;
            }

            Bitmap bm = Constants.resizeDrawable(this, drawable);

            Marker marker = map.addMarker(
                    new MarkerOptions()
                            .position(new LatLng(pickupSelected.latitude, pickupSelected.longitude))
                            .title(place)
                            .icon(BitmapDescriptorFactory.fromBitmap(bm))
            );

            marker.showInfoWindow();
            pickupSelected.marker = marker;


            i++;
        }




        if (isMultipleDestinations){

            int destDrawer;
            int j = 0;
            // find the area bound of all the pick ups and destination
            for (PlaceComplete pc : selectedDestinationPlaces) {
                pc.index = j;
                switch (j){
                    case 0:
                        destDrawer = R.drawable.delivery_1;
                        break;
                    case 1:
                        destDrawer = R.drawable.delivery_2;
                        break;
                    case 2:
                        destDrawer = R.drawable.delivery_3;
                        break;
                    case 3:
                        destDrawer = R.drawable.delivery_4;
                        break;
                    case 4:
                        destDrawer = R.drawable.delivery_5;
                        break;
                    case 5:
                        destDrawer = R.drawable.delivery_6;
                        break;
                    default:
                        destDrawer = R.drawable.delivery_1;
                        break;
                }

                Marker marker = map.addMarker(
                        new MarkerOptions()
                                .position(new LatLng(pc.latitude, pc.longitude))
                                .title(pc.name)
                                .icon(BitmapDescriptorFactory.fromBitmap(Constants.resizeDrawable(this, destDrawer)))
                );
                pc.marker = marker;

                //selectedDestinationPlaces.set(pc.index, pc);

                marker.showInfoWindow();

                builder.include(new LatLng(pc.latitude, pc.longitude));
                LatLng destination = new LatLng(pc.latitude, pc.longitude);
                routeBuilder.add(destination);

                j++;
            }

//            for (PlaceComplete pc : selectedPickupPlaces) {
//                builder.include(new LatLng(pc.latitude, pc.longitude));
//            }


        }else if(selectedDestinationPlaces.size() == 1){
            PlaceComplete placeComplete = selectedDestinationPlaces.get(0);
            LatLng mLatLng = new LatLng(placeComplete.latitude, placeComplete.longitude);
            builder.include(mLatLng);
            routeBuilder.add(mLatLng);
            Marker marker = map.addMarker(
                    new MarkerOptions()
                            .position(new LatLng(placeComplete.latitude, placeComplete.longitude))
                            .title(placeComplete.name)
                            .icon(BitmapDescriptorFactory.fromBitmap(Constants.resizeDrawable(this, R.drawable.delivery_icon)))
            );
            marker.showInfoWindow();

            PlaceComplete complete = selectedDestinationPlaces.get(0);
            complete.marker = marker;
            complete.markerIcon = placeComplete.leftIcon;
            selectedDestinationPlaces.set(0,complete);
        }


        if (selectedPickupPlaces.size() == 1 && selectedDestinationPlaces.size() == 0){
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(selectedPickupPlaces.get(0).latitude, selectedPickupPlaces.get(0).longitude), DEFAULT_ZOOM));
        }else{
            LatLngBounds bounds = builder.build();
            Point displaySize = new Point();
            getWindowManager().getDefaultDisplay().getSize(displaySize);
            map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, screenHeight / 2, 150));

            drawRoute(routeBuilder);
        }


        new Handler().postDelayed(() -> {

            boolean hasShownTutorial = TutorialManager.getInstance().hasShownTutorial(MainActivity.this, pickUpPointEt.getId());
            if (hasShownTutorial && TutorialManager.getInstance().showTutorial(MainActivity.this, addPickUpPoint.getId())){

               FancyShowCaseView swipeToDelete = new FancyShowCaseView.Builder(MainActivity.this)
                        .customView(R.layout.swipe_to_delete_layout, null)
                        .closeOnTouch(true)
                        .titleSize(20, 1)
                        .build();

                new FancyShowCaseView.Builder(MainActivity.this)
                        .customView(R.layout.add_place_layout, null)
                        .closeOnTouch(true)
                        .titleSize(20, 1)
                        .animationListener(new AnimationListener() {
                            @Override
                            public void onEnterAnimationEnd() {

                            }

                            @Override
                            public void onExitAnimationEnd() {

                                swipeToDelete.show();
                            }
                        })
                        .build()
                        .show();
            }

        }, 1000);


    }

    private void addDestinationPointMarkerToMap(PlaceComplete placeComplete){

//        LatLngBounds.Builder builder = new LatLngBounds.Builder();
//
//        Log.d(Constants.TAG, "addDestinationPointMarkerToMap: destination index " + placeComplete.index);
//        if (isMultipleDestinations){
//
//
//            // find the area bound of all the pick ups and destination
//            for (PlaceComplete pc : selectedDestinationPlaces) {
//                if (pc.marker != null)
//                    pc.marker.remove();
//
//                switch (pc.index){
//                    case 0:
//                        pc.markerIcon = R.drawable.delivery_1;
//                        break;
//                    case 1:
//                        pc.markerIcon = R.drawable.delivery_2;
//                        break;
//                    case 2:
//                        pc.markerIcon = R.drawable.delivery_3;
//                        break;
//                    case 3:
//                        pc.markerIcon = R.drawable.delivery_4;
//                        break;
//                    case 4:
//                        pc.markerIcon = R.drawable.delivery_5;
//                        break;
//                    case 5:
//                        pc.markerIcon = R.drawable.delivery_6;
//                        break;
//                    default:
//                        pc.markerIcon = R.drawable.delivery_1;
//                        break;
//                }
//
//                Marker marker = map.addMarker(
//                        new MarkerOptions()
//                                .position(new LatLng(pc.latitude, pc.longitude))
//                                .title(pc.name)
//                                .icon(BitmapDescriptorFactory.fromBitmap(Constants.resizeDrawable(this, pc.markerIcon)))
//                );
//                pc.marker = marker;
//
//                //selectedDestinationPlaces.set(pc.index, pc);
//
//                marker.showInfoWindow();
//
//                builder.include(new LatLng(pc.latitude, pc.longitude));
//            }
//
//            for (PlaceComplete pc : selectedPickupPlaces) {
//                builder.include(new LatLng(pc.latitude, pc.longitude));
//            }
//
//
//        }else{
//            builder.include(new LatLng(placeComplete.latitude, placeComplete.longitude));
//            Marker marker = map.addMarker(
//                    new MarkerOptions()
//                            .position(new LatLng(placeComplete.latitude, placeComplete.longitude))
//                            .title(placeComplete.name)
//                            .icon(BitmapDescriptorFactory.fromBitmap(Constants.resizeDrawable(this, R.drawable.delivery_icon)))
//            );
//            marker.showInfoWindow();
//
//            PlaceComplete complete = selectedDestinationPlaces.get(0);
//            complete.marker = marker;
//            complete.markerIcon = placeComplete.leftIcon;
//            selectedDestinationPlaces.set(0,complete);
//        }
//
//        LatLngBounds bounds = builder.build();
//        Point displaySize = new Point();
//        getWindowManager().getDefaultDisplay().getSize(displaySize);
//        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 250, 30));

        //map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));

    }

    private void getLatLngFromPlaceId(String placeId, PlaceFoundListner placeFoundListner){

        Log.d(Constants.TAG, "getLatLngFromPlaceId: place id = " + placeId);
        // Specify the fields to return (in this example all fields are returned).
        List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);
        // Construct a request object, passing the place ID and fields array.
        FetchPlaceRequest request = FetchPlaceRequest.builder(placeId, placeFields).build();
        placesClient.fetchPlace(request).addOnSuccessListener((response) -> {
            Place place = response.getPlace();
           // place.getLatLng();
            Log.i(Constants.TAG, "Place found: " + place.getName());
            placeFoundListner.found(place);
        }).addOnFailureListener((exception) -> {
            if (exception instanceof ApiException) {
                ApiException apiException = (ApiException) exception;
                int statusCode = apiException.getStatusCode();
                runOnUiThread(() -> {
                    LoadingGif.hide();
                    Toast.makeText(this, "Place not found, please try again", Toast.LENGTH_SHORT).show();
                });
                // Handle error with given status code.
                Log.e(Constants.TAG, "Place not found: " + exception.getMessage());
            }
        });

//        Places.GeoDataApi.getPlaceById(placesClient, placeId)
//                .setResultCallback(new ResultCallback<PlaceBuffer>() {
//                    @Override
//                    public void onResult(PlaceBuffer places) {
//                        if (places.getStatus().isSuccess()) {
//                            final Place myPlace = places.get(0);
//                            LatLng queriedLocation = myPlace.getLatLng();
//                            Log.v("Latitude is", "" + queriedLocation.latitude);
//                            Log.v("Longitude is", "" + queriedLocation.longitude);
//                        }
//                        places.release();
//                    }
//       });
    }

    private void startAnim(List<LatLng> bangaloreRoute){
        if(map != null) {
            MapAnimator.newInstance().animateRoute(map, bangaloreRoute);
        } else {
            Toast.makeText(getApplicationContext(), "Map not ready", Toast.LENGTH_LONG).show();
        }
    }



//    Trip building here ----------------------------------------

    private void addOrUpdatePickPoint(PlaceComplete placeComplete){

        Pickup pickup = new Pickup();
        pickup.place = placeComplete.name;

        Pos pos = new Pos();
        pos.geohash = Constants.getGeoHash(new LatLng(placeComplete.latitude, placeComplete.longitude));
        pos.geopoint = new Pos.PosLatLng(placeComplete.latitude, placeComplete.longitude);

        pickup.pos = pos;

        pickupArrayList.add(pickup);

        Log.d(Constants.TAG, "addOrUpdatePickPoint: size of pickupArrayList = " + pickupArrayList.size());
    }

    private void addDestinationPoint(PlaceComplete placeComplete) {
        Destination destination = new Destination();
        destination.place = placeComplete.name;

        Pos pos = new Pos();
        pos.geohash = Constants.getGeoHash(new LatLng(placeComplete.latitude, placeComplete.longitude));
        pos.geopoint = new Pos.PosLatLng(placeComplete.latitude , placeComplete.longitude);

        destination.pos = pos;

        destinationArrayLists.add(destination);

        Log.d(Constants.TAG, "addDestinationPoint: size of destinationArrayLists = " + destinationArrayLists.size());

    }




    private void addPickUpDetails(int pickUpIndex, String itemCategory, String instruction, String pickPersonName, String pickPersonContact) {
        Pickup pickup =  pickupArrayList.get(pickUpIndex);
        pickup.pickUpName = pickPersonName;
        pickup.pickUpPersonPhone = pickPersonContact;
        pickup.itemCategory = itemCategory;
        pickup.instruction = instruction;

        selectedPickupPlaces.get(pickUpIndex).description = pickPersonName + " , " + pickPersonContact;
        selectedPickUpPointsAdapter.changeDesciptionColor(true, pickUpIndex);
        selectedPickUpPointsAdapter.notifyDataSetChanged();

        pickupArrayList.set(pickUpIndex, pickup);
    }

    private void addSenderDetails() {
       if(request != null){
           // pick up instead
           if(pickupArrayList.size() == 1){
               Pickup pickup = pickupArrayList.get(0);
               pickup.pickUpName = String.valueOf(senderName.getText());
               pickup.pickUpPersonPhone = String.valueOf(senderPhone.getText());
               pickupArrayList.set(0,pickup);
           }else if(pickupArrayList.size() > 1){
                request.senderName = String.valueOf(senderName.getText());
                request.senderPhoneNumber = String.valueOf(senderPhone.getText());
           }

       }
    }

    private void addRecipientDetails() {
        String phone =  String.valueOf(recipientPhone.getText());
        String name = String.valueOf(recipientName.getText());

        // For now, we're registering it under the first destination point
        Destination destination = destinationArrayLists.get(0);
        destination.recipientName = name;
        destination.recipientNumber = phone;


        destinationArrayLists.set(0, destination);

    }

//    End of request building --------------------------------------------


//    private void animateMarker(final Marker marker) {
//        final Handler handler = new Handler();
//
//        final long startTime = SystemClock.uptimeMillis();
//        final long duration = 300; // ms
//
//        Projection proj = map.getProjection();
//        final LatLng markerLatLng = marker.getPosition();
//        Point startPoint = proj.toScreenLocation(markerLatLng);
//        startPoint.offset(0, -10);
//        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
//
//        final Interpolator interpolator = new BounceInterpolator();
//
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                long elapsed = SystemClock.uptimeMillis() - startTime;
//                float t = interpolator.getInterpolation((float) elapsed / duration);
//                double lng = t * markerLatLng.longitude + (1 - t) * startLatLng.longitude;
//                double lat = t * markerLatLng.latitude + (1 - t) * startLatLng.latitude;
//                marker.setPosition(new LatLng(lat, lng));
//
//                if (t < 1.0) {
//                    // Post again 16ms later (60fps)
//                    handler.postDelayed(this, 16);
//                }
//            }
//        });
//    }
}
