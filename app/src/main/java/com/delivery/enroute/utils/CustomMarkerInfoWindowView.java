package com.delivery.enroute.utils;

import android.view.LayoutInflater;
import android.view.View;

import com.delivery.enroute.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class CustomMarkerInfoWindowView implements GoogleMap.InfoWindowAdapter {
    private final View markerItemView;

    public CustomMarkerInfoWindowView(LayoutInflater inflater) {
        markerItemView = inflater.inflate(R.layout.marker_info_window, null);  // 1
    }

    @Override
    public View getInfoWindow(Marker marker) { // 2
        //if (user == null) return clusterItemView;
//        TextView itemNameTextView = markerItemView.findViewById(R.id.itemNameTextView);
//        TextView itemAddressTextView = markerItemView.findViewById(R.id.itemAddressTextView);
//        itemNameTextView.setText(marker.getTitle());
//        itemAddressTextView.setText(user.getAddress());
        return markerItemView;  // 4
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}
