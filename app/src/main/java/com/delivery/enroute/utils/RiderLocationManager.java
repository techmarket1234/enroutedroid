package com.delivery.enroute.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.delivery.enroute.enums.TripStatus;
import com.delivery.enroute.models.Pos;
import com.google.gson.Gson;

public class RiderLocationManager {

    private static RiderLocationManager riderLocationManager;
    private SharedPreferences sp;
    private static Gson gson;
    private String tripStatusKey = "_status";

    private RiderLocationManager(){}

    public static RiderLocationManager getInstance() {
        if (riderLocationManager == null) riderLocationManager = new RiderLocationManager();
        if (gson == null) gson = new Gson();
        return riderLocationManager;
    }

    public void addRiderLocation(Context context, String riderId, Pos.PosLatLng posLatLng){
        sp = context.getSharedPreferences(Constants.APP, Context.MODE_PRIVATE);
        sp.edit().putString(riderId, gson.toJson(posLatLng)).apply();
    }

    public Pos.PosLatLng getRiderLastLocation(Context context, String riderId) throws NullPointerException{
        sp = context.getSharedPreferences(Constants.APP, Context.MODE_PRIVATE);
        String rider = sp.getString(riderId,null);
        if (rider == null)return null;

        try {
            Log.d(Constants.TAG, "getRiderLastLocation: " + rider);
            return gson.fromJson(rider, Pos.PosLatLng.class);
        }catch (Exception e){
            Log.d(Constants.TAG, "getRiderLastLocation: Exception " + e.getMessage());
            return null;
        }
    }

    public void removeRiderLocation(Context context, String riderId){
        sp = context.getSharedPreferences(Constants.APP, Context.MODE_PRIVATE);
        sp.edit().remove(riderId).apply();
    }

    public void saveTripStatus(Context context, String riderId, TripStatus tripStatus){
        sp = context.getSharedPreferences(Constants.APP, Context.MODE_PRIVATE);
        if (tripStatus == TripStatus.COMPLETED || tripStatus == TripStatus.CANCELLED || tripStatus == TripStatus.CANCELLED_R){
            removeTripStatus(context, riderId);
            return;
        }
        sp.edit().putString(riderId + tripStatusKey, tripStatus.toString()).apply();
    }

    public TripStatus getTripStatus(Context context, String riderId){
        sp = context.getSharedPreferences(Constants.APP, Context.MODE_PRIVATE);
        String tripStatusString = sp.getString(riderId + tripStatusKey, null);
        if (tripStatusString == null) return TripStatus.PENDING;

        return TripStatus.valueOf(tripStatusString);
    }

    private void removeTripStatus(Context context, String riderId){
        sp = context.getSharedPreferences(Constants.APP, Context.MODE_PRIVATE);
        sp.edit().remove(riderId + tripStatusKey).apply();
    }

}
