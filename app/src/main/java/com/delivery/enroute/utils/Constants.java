package com.delivery.enroute.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Debug;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.delivery.enroute.BuildConfig;
import com.delivery.enroute.R;
import com.delivery.enroute.activities.ErrorActivity;
import com.delivery.enroute.activities.ReceivePaymentActivity;
import com.delivery.enroute.activities.vendors.VendorListActivity;
import com.delivery.enroute.enums.Collections;
import com.delivery.enroute.getstarted.WelcomeActivity;

import com.delivery.enroute.layoutdialogs.OutstandingBalance;
import com.delivery.enroute.listeners.OnAddressRetrieved;
import com.delivery.enroute.listeners.OnGetServerUserCompleted;
import com.delivery.enroute.listeners.OnUpdateServerUserCompleted;
import com.delivery.enroute.listeners.OnWalletBalanceFetched;
import com.delivery.enroute.listeners.UnPaidTripListener;
import com.delivery.enroute.listeners.GetTripListener;
import com.delivery.enroute.models.Coupon;
import com.delivery.enroute.models.MyCurrentPlace;
import com.delivery.enroute.models.Settings;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.models.User;
import com.delivery.enroute.singletons.ApplicaitonUser;
import com.delivery.enroute.utils.volley.RequestObject;
import com.fonfon.geohash.GeoHash;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import me.toptas.fancyshowcase.FancyShowCaseView;
import spencerstudios.com.bungeelib.Bungee;

//    Constants in the web services developed by Danny Kay ###################################
public class Constants {

    public static final String APP = "com.delivery.enroute";
    public static final String TAG = "ContentValues";

    public static final String PHONE_AUT_TYPE = "PHONE_AUT_TYPE";
    public static final String VERIFICATION_ID = "VERIFICATION_ID";
    public static final String USER_DISPLAY_NAME = "USER_DISPLAY_NAME";
    public static final String PHONE_AUTH_PHONE_NUMBER = "PHONE_AUTH_PHONE_NUMBER";
    public static final String USER = "USER";
    public static final int MY_PERMISSIONS_REQUEST_READ_SMS = 4;
    public static final String READ_SMS_PERMISSION_GRANTED = "READ_SMS_PERMISSION_GRANTED";
    public static final String TRIP = "TRIP";
    public static final int MY_PERMISSIONS_REQUEST_CALL = 5;
    public static final String RIDER_ID = "RIDER_ID";
    public static final String RIDER_NAME = "RIDER_NAME";
    public static final String RIDER_PHONE = "RIDER_PHONE";
    public static final String BROADCAST_ACTION_CHAT = "com.delivery.enroute.utils.CHAT";
    public static final String CHAT = "CHAT";
    public static final String CHECKOUT_PICK_UPS = "CHECKOUT_PICK_UPS";
    public static final String CHECKOUT_DESTINATIONS = "CHECKOUT_DESTINATIONS";
    public static final String IS_MULTIPLE_PICK_UPS = "IS_MULTIPLE_PICK_UPS";
    public static final String IS_MULTIPLE_DESTINATIONS = "IS_MULTIPLE_DESTINATIONS";
    public static final String PICK_UPS_ARRAY_LIST = "PICK_UPS_ARRAY_LIST";
    public static final String DESTINATIONS_ARRAY_LIST = "DESTINATIONS_ARRAY_LIST";
    public static final String IS_FROM_CHECKOUT = "IS_FROM_CHECKOUT";
    public static final String ACTIVE_PICK_UPS = "ACTIVE_PICK_UPS";
    public static final String BASIC_USERNAME = "enrout_bDNnHGDmihi@8uyddG0x97nQAxKzXUpl8yvQh";
    public static final String BASIC_PASSWORD = "rockD.H418Cc9MuaOromZpIUD5nfl";
    public static final String BEARER_TOKEN = "";

    public static final String USER_FIRST_TIME = "USER_FIRST_TIME";
    public static final String RIDER_IMAGE = "RIDER_IMAGE";
    public static final String ERROR = "ERROR";
    public static final String IS_PHONE_LOGIN = "IS_PHONE_LOGIN";
    public static final String PHONE_AUTH_USERNAME = "PHONE_AUTH_USERNAME";
    public static final String TUTORIALS = "TUTORIALS";
    public static final int HOME_ID = 11;
    public static final int WALLET_ID = 12;
    public static final String ACTIVE_DESTINATIONS = "ACTIVE_DESTINATIONS";
    public static final String PRODUCTS = "PRODUCTS";
    public static final String VENDOR = "VENDOR";
    public static final String PRODUCT_IMAGE = "PRODUCT_IMAGE";
    public static final String PRODUCT_TITLE = "PRODUCT_TITLE";
    public static final String PRODUCT_DESC = "PRODUCT_DESC";
    public static final String PRODUCT_PRICE = "PRODUCT_DESC";
    public static final String PAYMENT_FIXED_AMOUNT = "PAYMENT_FIXED_AMOUNT";
    public static final String SHOULD_DISABLE_AMOUNT_FIELD = "SHOULD_DISABLE_AMOUNT_FIELD";
    public static Trip TEMPORAL_TRIP_HOLDER = new Trip();
    public static String Rider_PLATE = "Rider_PLATE";

    public static Coupon TEMP_COUPON = null;

    public static User AppUser;


    public static final int REQUEST_PERMISSIONS = 4;
    public static final int REQUEST_LOCATION = 5;
    public static final String REQUEST_BUILDER = "REQUEST_BUILDER";


//    private boolean checkRadius(double current_latitude, double current_longitude, double destination_latitude, double destination_longitude, double definedRadius){
//
//        float[] distance = new float[2];
//
//
//        Circle mapcircle = mMap.addCircle(new CircleOptions()
//                .strokeColor(Color.TRANSPARENT)
//                .center(new LatLng(destination_latitude,destination_longitude))
//
//                .radius(definedRadius)
//        );
//
//        Location.distanceBetween(current_latitude,
//                current_longitude, mapcircle.getCenter().latitude,
//                mapcircle.getCenter().longitude, distance);
//
//        if (distance[0] <= mapcircle.getRadius()) {
//
//            return true;
//
//        }
//
//        else{
//
//            return false;
//
//        }
//
//    }


    public static final int INITIAL_REQUEST_RADIUS = 10;
    public static final int STEP_REQUEST_RADIUS = 10;
    public static final long REQUEST_WAITING_TIME = 5000L;
    public static final String NAV_DRAWER_ENUM = "NAV_DRAWER_ENUM";
    public static final int ENTER_PHONE_NUBER = 1;
    public static final int LOCATION_SETTINGS_REQUEST = 4;
    public static final String LOCATION_SETTINGS = "LOCATION_SETTINGS";
    public static final String PICK_UP = "PICK_UP";
    public static final String DELIVERY = "DELIVERY";


    //    public static String default_web_client_id = "1055065293584-ufqp03ljq2r0g0g7d9vkbe4fskq67dhl.apps.googleusercontent.com";
//    public static String default_web_client_id = "1084242519010-t0tpei24alec7b0mmi0ou5iqqs7ad477.apps.googleusercontent.com";
    public static String default_web_client_id = "1084242519010-nmltg0iba00b07brubil7aqd2aa8ca3n.apps.googleusercontent.com";
//    public static String default_web_client_id = "1084242519010-qk8gm8d0bfagp7l3ukkcivo29pan710f.apps.googleusercontent.com";
    public static final int RC_SIGN_IN = 7;
    public static final String GOOGLE_AUTH_TYPE = "GOOGLE_AUTH_TYPE";
    public static final String FACE_BOOK_AUTH_TYPE = "FACE_BOOK_AUTH_TYPE";


    public static final float DEFAULT_ZOOM = 15.0f;
    public static String tripInTemporaryHolder = "tripInTemporaryHolder";
//    public static String BaseUrl = "https://ws.realtime.enroutdelivery.com";
    public static String BaseUrl = BuildConfig.DEBUG ? "https://ws.realtime.enroutdelivery.com" :  "https://ws.realtime.enroutdelivery.com";
    public static final String PAYMENT_BASE_URL = BaseUrl;


    public static FirebaseUser getCurrentUser(Context context){
       FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
       if (firebaseUser == null){
           logout(context);
       }

       return firebaseUser;
    }

    public static void getServerUser(Context context, OnGetServerUserCompleted onGetApplicationUserCompleted){
       FirebaseFirestore db = FirebaseFirestore.getInstance();
       db.collection("USERS").document(getCurrentUser(context).getUid()).get().addOnSuccessListener(documentSnapshot -> {

           User user = documentSnapshot.toObject(User.class);

           ApplicaitonUser applicaitonUser = ApplicaitonUser.getInstance(context);
           applicaitonUser.setAppUser(context,user);

           onGetApplicationUserCompleted.userFound(applicaitonUser);

       }).addOnFailureListener(e -> {
            onGetApplicationUserCompleted.userNotFound();
       });
    }

    public static void updateServerUser(Context context, User user, OnUpdateServerUserCompleted onUpdateServerUserCompleted){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        HashMap<String, Object> params = new HashMap<>();

        params.put("fullName",user.fullName);
        params.put("email",user.email);
        params.put("phoneNumber",user.phoneNumber);
        params.put("authType",user.authType);
        params.put("userType",user.userType);
        params.put("address",user.address);
        params.put("dateAdded",user.dateAdded);
        params.put("lastLogin",user.lastLogin);
        params.put("gender",user.gender);
        params.put("photoUrl",user.photoUrl);
        params.put("currentBalance",user.currentBalance);
        params.put("outStandingBalance",user.outStandingBalance);
        params.put("id",user.id);
        params.put("device","ANDROID");
        params.put("appReviewed", user.appReviewed);

        ApplicaitonUser applicaitonUser = ApplicaitonUser.getInstance(context);
        applicaitonUser.setAppUser(context,user);

        db.collection("USERS").document(getCurrentUser(context).getUid()).update(params).addOnCompleteListener(task -> onUpdateServerUserCompleted.onComplete(task.isSuccessful()));
    }

    public static void logout(Context context) {
        Dialogs.confirm(context).setTitle("Do you wish to logout?").setPositiveButton("Yes", view -> {
            FirebaseAuth.getInstance().signOut();
            TutorialManager.getInstance().clearAllTutorials(context);
            Intent intent = new Intent(context, WelcomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }).show();

    }

    public static void openEmail(Context context){
        openEmail(context, null, null);
        
//        try {
//            String recepientEmail = "info@enroutdelivery.com"; // either set to destination email or leave empty
//            Intent intent = new Intent(Intent.ACTION_SENDTO);
//            intent.setData(Uri.parse("mailto:" + recepientEmail));
//            context.startActivity(intent);
//        }catch (Exception e){
//            Toast.makeText(context, "No application found to handle email", Toast.LENGTH_SHORT).show();
//        }

//        mail.putExtra(Intent.EXTRA_STREAM, Uri.parse("content://path/to/email/attachment"));
//        startActivity(intent);

    }

    public static void openEmail(Context context, String subject, String message){

        try {
            String recepientEmail = "info@enroutdelivery.com"; // either set to destination email or leave empty
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            if (subject != null){
                intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            }
            if (message != null){
                intent.putExtra(Intent.EXTRA_TEXT, message);
            }
            intent.setData(Uri.parse("mailto:" + recepientEmail));
            context.startActivity(intent);
        }catch (Exception e){
            Toast.makeText(context, "No application found to handle email", Toast.LENGTH_SHORT).show();
        }

//        mail.putExtra(Intent.EXTRA_STREAM, Uri.parse("content://path/to/email/attachment"));
//        startActivity(intent);

    }





    public static Bitmap resizeDrawable(Context context, int drawable){
        int height = 100;
        int width = 100;
        BitmapDrawable bitmapdraw=(BitmapDrawable) context.getResources().getDrawable(drawable);
        Bitmap b=bitmapdraw.getBitmap();
        return Bitmap.createScaledBitmap(b, width, height, false);
    }


    public static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        if (activity.getCurrentFocus() != null){
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static void getAddress(Context context, Geocoder geocoder, LatLng latLng, OnAddressRetrieved onAddressRetrieved){

       new Thread(()->{

//           String apikey = "AIzaSyDZG2zY0Qh1VtLEN1sN7GkA2bC8o9dHP_w";
//           String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latLng.latitude+","+latLng.longitude+"&key="+apikey;
//           HashMap<String, String> params = new HashMap<>();
//           params.put("latitude", String.valueOf(latLng.latitude));
//           params.put("longitude",String.valueOf(latLng.longitude));
//
//           Log.d(Constants.TAG, "url " + url);
//           Log.d(Constants.TAG, "params " + params);
//
//           new RequestObject(Request.Method.POST, url, params, response -> {
//               Log.d(TAG, "getAddress: response: " + response);
//           }, error -> {
//               Log.d(TAG, "getAddress: Error: " +error );
//           }).send(context);

           List<Address> addresses = null;
           try {
               addresses = geocoder.getFromLocation(
                       latLng.latitude,
                       latLng.longitude,
                       // In this sample, get just a single address.
                       1);
           } catch (IOException e) {
               e.printStackTrace();
           }

           // Handle case where no address was found.
           if (addresses == null || addresses.size()  == 0){
               //Toast.makeText(this, getString(R.string.no_address_found), Toast.LENGTH_SHORT).show();
                onAddressRetrieved.addressFound(null);
           }


           Address address;
           if (addresses != null && addresses.size() > 0) {
               address = addresses.get(0);
               // Fetch the address lines using getAddressLine,
               // join them, and send them to the thread.
               MyCurrentPlace myCurrentPlace = new MyCurrentPlace();
               myCurrentPlace.name = address.getAddressLine(0);
               myCurrentPlace.description = address.getAdminArea();
               myCurrentPlace.latitude = address.getLatitude();
               myCurrentPlace.longitude = address.getLongitude();

               Log.d(TAG, "getFeatureName:- " + address.getFeatureName());
               Log.d(TAG, "getAddressLine:- " + address.getAddressLine(0));
               Log.d(TAG, " getLocality:- " + address.getLocality());
               Log.d(TAG, "getAdminArea:- " + address.getAdminArea());
               Log.d(TAG, "getPremises:-" + address.getPremises());
               Log.d(TAG, "getSubAdminArea:- " + address.getSubAdminArea());
               Log.d(TAG, "getSubAdminArea:- " + address.getSubAdminArea());
               Log.d(TAG, "getLocale:- " + address.getLocale());
               Log.d(TAG, "getSubLocality:- " + address.getSubLocality());
               Log.d(TAG, "getCountryName:- " + address.getCountryName());


//        Log.d(TAG, "getAddress: name " + address.getAddressLine(0));
//        Log.d(TAG, "getAddress: description " + address.getAddressLine(0));

               onAddressRetrieved.addressFound(myCurrentPlace);
           }

       }).start();

        //return myCurrentPlace;
//        for(int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
//            addressFragments.add(address.getAddressLine(i));
//        }
//        Log.i(TAG, getString(R.string.address_found));


//        return TextUtils.join(Objects.requireNonNull(System.getProperty("line.separator")),
//                addressFragments);

    }

    public static String getGeoHash(LatLng latLng){

        Location location = new Location("geohash");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);

        GeoHash hash = GeoHash.fromLocation(location, 9);
        return hash.toString();
    }

    public static String getUniqueString(){
       return FirebaseAuth.getInstance().getUid() + android.text.format.DateFormat.format("yyyyMMddHHmmss", new java.util.Date()) + getAlphaNumericString(5);
    }

    static String getAlphaNumericString(int n)
    {
        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int)(AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }



    public static String formatToFriendlyDateTime(Date date) {
        return new SimpleDateFormat("EEE, d MMM yyyy HH:mm", Locale.getDefault()).format(date);
    }

    public static String formatToFriendlyDate(Date date){
        return new SimpleDateFormat("EEE, d MMM yyyy HH:mm", Locale.getDefault()).format(date);
    }

    public static String formatToFriendlyTime(Date date){
        return new SimpleDateFormat("HH:mm a", Locale.getDefault()).format(date);
    }


    public static String shortenPickupsDestinations(Context context, Trip trip) {
        StringBuilder ppbuilder = new StringBuilder();

        for(String pickUpString : trip.pickupNames){

            String abbrvPickUps = pickUpString;
            if (pickUpString.length() > 4){
                abbrvPickUps = pickUpString.substring(0, Math.min(pickUpString.length(), 4));
            }

            ppbuilder.append(abbrvPickUps);
            ppbuilder.append("/");
        }
        String pickupStringResult = ppbuilder.toString().substring(0, ppbuilder.toString().length() - 1);

        StringBuilder destStringBuilder = new StringBuilder();
        for(String destString : trip.destinationNames){
            String abbrvPickUps = destString;
            if (destString.length() > 4){
                abbrvPickUps = destString.substring(0, Math.min(destString.length(), 4));
            }
            destStringBuilder.append(abbrvPickUps);
            destStringBuilder.append("/");
        }

        String destStringResult = destStringBuilder.toString().substring(0, destStringBuilder.toString().length() - 1);

       return context.getResources().getString(R.string.pick_dest, pickupStringResult, destStringResult);
    }

    public static String fullPickupsDestinations(Context context, Trip trip){
        StringBuilder ppbuilder = new StringBuilder();

        for(String pickUpString : trip.pickupNames){

            ppbuilder.append(pickUpString);
             ppbuilder.append("/");
        }
        String pickupStringResult = ppbuilder.toString().substring(0, ppbuilder.toString().length() - 1);

        StringBuilder destStringBuilder = new StringBuilder();
        for(String destString : trip.destinationNames){

            destStringBuilder.append(destString);
            destStringBuilder.append("/");
        }

        String destStringResult = destStringBuilder.toString().substring(0, destStringBuilder.toString().length() - 1);

        return context.getResources().getString(R.string.pick_dest, pickupStringResult, destStringResult);
    }

    public static double convertTo2dp(double number){
        number = Math.round(number * 100);
        return number/100;
    }

    public static int toWholeNumber(double x){
        return (int) Math.rint(x);
    }

    public static void getWalletBalance(Context context, OnWalletBalanceFetched balanceFetched){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        String uId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        db.collection(Collections.USERS.toString()).document(uId).get().addOnSuccessListener(documentSnapshot -> {
            if (documentSnapshot == null) return;
            User user = documentSnapshot.toObject(User.class);
            if (user == null) return;

           ApplicaitonUser applicaitonUser = ApplicaitonUser.getInstance(context);

            if (applicaitonUser != null){
                applicaitonUser.currentBalance = Constants.toWholeNumber(user.currentBalance);
            }

            balanceFetched.onFetch(user);

        });
    }

    public static void checkIfTheresACompletedUnratedTrip(GetTripListener unratedTripListener){

        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            unratedTripListener.notFound();
            return;
        }
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(Collections.REQUEST.toString())
                .whereEqualTo("senderId",FirebaseAuth.getInstance().getCurrentUser().getUid())
                .whereEqualTo("rated", false)
                .whereEqualTo("status", "COMPLETED").get().addOnSuccessListener(queryDocumentSnapshots -> {

                if (queryDocumentSnapshots == null) {
                    unratedTripListener.notFound();
                    return;
                }

                if (queryDocumentSnapshots.isEmpty()) {
                    unratedTripListener.notFound();
                    return;
                }

                List<Trip> tripList = queryDocumentSnapshots.toObjects(Trip.class);
                if (tripList.isEmpty()){
                    unratedTripListener.notFound();
                    return;
                }

                Trip firstTripFound = tripList.get(0);
                unratedTripListener.found(firstTripFound);

        });
    }

    public  static void getCheckIfTheresACompletedTrip(GetTripListener listener){
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            listener.notFound();
            return;
        }

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection(Collections.REQUEST.toString())
                .whereEqualTo("senderId",FirebaseAuth.getInstance().getCurrentUser().getUid())
                .whereEqualTo("status", "COMPLETED").get().addOnSuccessListener(queryDocumentSnapshots -> {

            if (queryDocumentSnapshots == null) {
                listener.notFound();
                return;
            }

            if (queryDocumentSnapshots.isEmpty()) {
                listener.notFound();
                return;
            }

            List<Trip> tripList = queryDocumentSnapshots.toObjects(Trip.class);
            if (tripList.isEmpty()){
                listener.notFound();
                return;
            }

            boolean hasRequestedToday = false;

            for(Trip trip: tripList){

                if(DateUtils.isToday(trip.requestDatetime.getTime())){
                    hasRequestedToday = true;
                    break;
                }
            }

            // check if the user has completed more than 2 trips
            if(hasRequestedToday && tripList.size() > 2){
                Trip firstTripFound = tripList.get(0);
                listener.found(firstTripFound);
            }else {
                listener.notFound();
            }


        });
    }

    public static void checkOutstandingPayment(Activity context, UnPaidTripListener listener) {
        new Thread(() -> UnPaidTripsManager.getInstance().getUnFirstUnpaidTrip(new UnPaidTripListener() {
            @Override
            public void unpaidTrip(Trip trip, boolean allowUserToRequestAnotherTrip) {
                if (!allowUserToRequestAnotherTrip){
                    Constants.TEMPORAL_TRIP_HOLDER = trip;
                    Toast.makeText(context, "You have a trip pending payment ...", Toast.LENGTH_SHORT).show();
                    context.startActivity(new Intent(context, ReceivePaymentActivity.class));
                    //listener.
                }else{
                    context.runOnUiThread(() -> {

                        OutstandingBalance outstandingBalance = new OutstandingBalance();
                        outstandingBalance.setOnFragmentInteractedListener(view -> {
                            outstandingBalance.dismiss();
                            Constants.TEMPORAL_TRIP_HOLDER = trip;
                            context.startActivity(new Intent(context, ReceivePaymentActivity.class));
                            Bungee.slideLeft(context);
                        });

                        try{
                            outstandingBalance.show(( (AppCompatActivity) context ).getSupportFragmentManager(), "pay for trip");
                        }catch (Exception e){}

//                        Dialogs.confirmWithSweet(context).setTitleText("PAYMENTS PENDING")
//                                .setContentText("Your trip " + Constants.shortenPickupsDestinations(context, trip) + "is pending payment")
//                                .setConfirmText("Pay now")
//                                .setCancelText("Pay later")
//                                .setConfirmClickListener(sweetAlertDialog -> {
//                                    sweetAlertDialog.dismiss();
//
//                                }).show();

                    });
                }

                listener.unpaidTrip(trip, allowUserToRequestAnotherTrip);
            }

            @Override
            public void notFound() {
                listener.notFound();
            }
        })).start();
    }

    public static void showTutorial(Activity context, View view, String title, String destination){
//        SimpleTarget simpleTarget = new SimpleTarget.Builder(context)
//                .setPoint(view.getX(), view.getY())
//                .setShape(new Circle(200f)) // or RoundedRectangle()
//                .setTitle(title != null ? title : "Surge")
//                .setDescription()
//                .setOverlayPoint(view.getX(), view.getY())
//                .build();
//
//        Spotlight.with(context)
//                .setOverlayColor(R.color.background)
//                .setDuration(1000L)
//                .setAnimation(new DecelerateInterpolator(2f))
//                .setTargets(simpleTarget)
//                .setOverlayColor(R.color.black)
//                .setClosedOnTouchedOutside(true)
//                .start();

        new FancyShowCaseView.Builder(context)
                .focusOn(view)
                .title(destination != null ? destination : "Prices are slightly higher after 6 PM")
                .closeOnTouch(true)
                .titleSize(16, 1)
                .roundRectRadius(80)
                .build()
                .show();
    }



    public static void callErrorPage(Activity mainActivity, String errorMessage) {
        Intent intent = new Intent(mainActivity, ErrorActivity.class);
        intent.putExtra(ERROR, errorMessage);
        mainActivity.startActivity(intent);
        Bungee.slideLeft(mainActivity);
    }

    public static void openTerms(Activity context) {


        LoadingGif.show(context);
        FirebaseFirestore.getInstance().collection("SETTINGS").get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    LoadingGif.hide();

                    List<Settings> settings = queryDocumentSnapshots.toObjects(Settings.class);

                    if (settings.isEmpty())
                        return;

                    Settings setting = settings.get(0);

                    // replace web page url with terms and condition page
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(setting.terms_and_conditions));
                    context.startActivity(browserIntent);

                }).addOnFailureListener(e -> {
                    LoadingGif.hide();
                    Dialogs.alertWithSweet(context, e.getMessage());
                });

    }

    public static void openFAQ(Activity context) {


            LoadingGif.show(context);
            FirebaseFirestore.getInstance().collection("SETTINGS").get()
                    .addOnSuccessListener(queryDocumentSnapshots -> {
                        LoadingGif.hide();

                        List<Settings> settings = queryDocumentSnapshots.toObjects(Settings.class);

                        if (settings.isEmpty())
                            return;

                        Settings setting = settings.get(0);
                        try {
                            // replace web page url with terms and condition page
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(setting.faq));
                            context.startActivity(browserIntent);

                        }catch (Exception e){
                            Toast.makeText(context, "No application found to handle email", Toast.LENGTH_SHORT).show();
                        }


                    }).addOnFailureListener(e -> {
                        LoadingGif.hide();
                        Dialogs.alertWithSweet(context, e.getMessage());
                    });


    }


    public static void openVendors(Context context) {
        context.startActivity(new Intent(context, VendorListActivity.class));
        Bungee.slideLeft(context);
    }
}
