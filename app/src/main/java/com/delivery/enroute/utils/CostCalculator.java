package com.delivery.enroute.utils;

import com.delivery.enroute.models.CheckOut;
import com.delivery.enroute.models.Trip;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CostCalculator {

    private static CostCalculator costCalculator;
    private double baseFare = 4d;
    private double costpermin = 0.90d;
    private double costperkm = 0.40d;
//    total = (duration * costpermin) + (distance * 0.621371 * costperkm) + basefare;

    private CostCalculator(
            double baseFare, double costpermin, double costperkm
    ){
        this.baseFare = baseFare;
        this.costpermin = costpermin;
        this.costperkm = costperkm;
    }

    public static CostCalculator getInstance(double baseFare, double costpermin, double costperkm){
        if (costCalculator == null)
            costCalculator = new CostCalculator(baseFare, costpermin, costperkm);

        return costCalculator;
    }

    public double calculate(Trip trip, ArrayList<CheckOut> mPickPtsCheckOutArrayList, ArrayList<CheckOut> mDestinationsCheckOutArrayList){

        double estimatedDistanceInKm = trip.estimatedDistanceInMeters / 1000;
        double estimatedTimeInMins = trip.estimatedTimeInSeconds / 60;

        // check if they pick up and destination are within east legon

        double value =  (estimatedTimeInMins * costCalculator.costpermin) + (estimatedDistanceInKm * 0.621371 * costCalculator.costperkm) + costCalculator.baseFare;
        // check if its after 6
        int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY); //Current hour
        if (currentHour >= 18){
            // its after 6
            value = value + 5;
        }



        return Math.rint(value);

    }
}
