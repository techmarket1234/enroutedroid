package com.delivery.enroute.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.util.Log;

import com.delivery.enroute.enums.ActiveLocation;
import com.delivery.enroute.models.ActivatedLocations;
import com.delivery.enroute.models.ActiveLocationResult;
import com.delivery.enroute.models.PlaceComplete;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RestrictLocation {

    private static RestrictLocation restrictPickUpPoint;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private Type activatedLocationType = new TypeToken<ArrayList<ActivatedLocations>>() {}.getType();
    private Gson gson = new Gson();

    private RestrictLocation(){}


    public static RestrictLocation getInstance() {
        if (restrictPickUpPoint == null)
            restrictPickUpPoint = new RestrictLocation();

        return restrictPickUpPoint;
    }

    public void refreshActivePickupUps(Context context){

        db.collection("ACTIVE_PICKUP_POINTS")
                .get().addOnSuccessListener(queryDocumentSnapshots -> {

               if (queryDocumentSnapshots == null) return;

               List<ActivatedLocations> activatedPickupPts = queryDocumentSnapshots.toObjects(ActivatedLocations.class);

               SharedPreferences sp = context.getSharedPreferences(Constants.APP,Context.MODE_PRIVATE);
               String activatedPickupPtsString = gson.toJson(activatedPickupPts, activatedLocationType);
               sp.edit().putString(Constants.ACTIVE_PICK_UPS, activatedPickupPtsString).apply();

        });

        db.collection("ACTIVE_DESTINATION_POINTS")
                .get().addOnSuccessListener(queryDocumentSnapshots -> {

                if (queryDocumentSnapshots == null) return;

                List<ActivatedLocations> activatedDestPts = queryDocumentSnapshots.toObjects(ActivatedLocations.class);

                SharedPreferences sp = context.getSharedPreferences(Constants.APP,Context.MODE_PRIVATE);
                String activatedDestPtsString = gson.toJson(activatedDestPts, activatedLocationType);
                sp.edit().putString(Constants.ACTIVE_DESTINATIONS, activatedDestPtsString).apply();

        });

    }

    public ActiveLocationResult validatePoint(PlaceComplete selectedLocation, Context context, ActiveLocation activeLocation){

        ActiveLocationResult activeLocationResult = new ActiveLocationResult();
        String activeLocationString;
        SharedPreferences sp = context.getSharedPreferences(Constants.APP,Context.MODE_PRIVATE);

        if (activeLocation == ActiveLocation.PICK_UP){
            activeLocationString = sp.getString(Constants.ACTIVE_PICK_UPS, null);
        }else if(activeLocation == ActiveLocation.DESTINATION){
            activeLocationString = sp.getString(Constants.ACTIVE_DESTINATIONS, null);
        }else{
            activeLocationResult.isDistanceWithinActiveLocation = false;
            return activeLocationResult;
        }

        if (activeLocationString == null) {
            activeLocationResult.isDistanceWithinActiveLocation = false;
            return activeLocationResult;
        }

        List<ActivatedLocations> activatedLocations =  gson.fromJson(activeLocationString, activatedLocationType);
        activeLocationResult.activeLocationList = activatedLocations;

        boolean isDistanceWithinActiveLocation = false;

        for(ActivatedLocations activePt : activatedLocations){
            // distance is stored in result array at index 0

            Location loc1 = new Location("");
            loc1.setLatitude(activePt.latitude);
            loc1.setLongitude(activePt.longitude);

            Location loc2 = new Location("");
            loc2.setLatitude(selectedLocation.latitude);
            loc2.setLongitude(selectedLocation.longitude);

            float distanceInMeters = loc1.distanceTo(loc2);

            if (distanceInMeters <= activePt.radiusInMeters) {
                // distance between first and second location is less than 5km
                isDistanceWithinActiveLocation = true;
                break;
            }
        }

        activeLocationResult.isDistanceWithinActiveLocation = isDistanceWithinActiveLocation;
        return activeLocationResult;

    }

}
