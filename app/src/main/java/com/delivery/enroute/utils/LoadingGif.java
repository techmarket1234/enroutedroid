package com.delivery.enroute.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.delivery.enroute.R;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.roger.gifloadinglibrary.GifLoadingView;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LoadingGif {

    private static KProgressHUD mGifLoadingView;

    private static void instantiate(Context context) {

      //  mGifLoadingView = pDialog;
    }

    public static void show(Activity context){
//        SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
//        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
//        pDialog.setTitleText("");
//        pDialog.setCancelable(true);
//        mGifLoadingView = pDialog;
//        pDialog.show();

        LayoutInflater inflater = LayoutInflater.from(context);
        View contentView = inflater.inflate(R.layout.loading_gif, null,false);

       mGifLoadingView = KProgressHUD.create(context)
               .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
               .setCancellable(true)
               .setAnimationSpeed(2)
               .setCustomView(contentView)
               .setBackgroundColor(Color.TRANSPARENT)
               .setDimAmount(0.2f);


       mGifLoadingView.show();


    }

    public static void hide(){
        mGifLoadingView.dismiss();
    }


}
