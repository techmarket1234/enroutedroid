package com.delivery.enroute.utils.volley;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.delivery.enroute.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;


public class RequestObject extends Request<JSONObject> {

    private Listener<JSONObject> listener;
    private Map<String, String> params;
    private boolean _useBearerToken, _useBasicAuth;
    private String username, password;


    public RequestObject(String url, Map<String, String> params,
                         Listener<JSONObject> reponseListener, ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.listener = reponseListener;
        this.params = params;
    }

    public RequestObject(int method, String url, Map<String, String> params,
                         Listener<JSONObject> reponseListener, ErrorListener errorListener) {
        super(method, url, errorListener);
        this.listener = reponseListener;
        this.params = params;
    }

    protected Map<String, String> getParams()
            throws AuthFailureError {
        return params;
    }

    public RequestObject send(final Context activity){

        useBasicAuth();

        String tag = null;
        if (params != null && params.size() > 0){
            Map.Entry<String,String> entry = params.entrySet().iterator().next();
            tag = entry.getKey();
        }
        MySingleton.getInstance(activity).addToRequestQueue(activity,this, tag);
        return this;
    }

    public RequestObject useBearerToken(){
        _useBearerToken = true;
        return this;
    }

    public RequestObject useBasicAuth(){
        _useBasicAuth = true;
        return this;
    }

    public RequestObject useBasicAuth(String username, String password){
        _useBasicAuth = true;
        this.username = username;
        this.password = password;
        return this;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        if(_useBearerToken){
            Map<String,String> headers = new HashMap<>();
            headers.put("Authorization","Bearer " + Constants.BEARER_TOKEN);
            return headers;
        }

        if(_useBasicAuth){
            Map<String,String> headers = new HashMap<>();
            String text;

            if(username != null && password != null){
                text = username + ":" + password;
            }else{
                text = Constants.BASIC_USERNAME + ":" + Constants.BASIC_PASSWORD;
            }

            byte[] data = new byte[0];
            try {
                data = text.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                Log.d(TAG, "getHeaders: " + e.getMessage());
            }
            String encoding = Base64.encodeToString(data, Base64.NO_WRAP);
            headers.put("Authorization","Basic " + encoding);
            return headers;
        }
        return super.getHeaders();
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(new JSONObject(jsonString),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException je) {
            return Response.error(new ParseError(je));
        }
    }

    @Override
    protected void deliverResponse(JSONObject response) {
        listener.onResponse(response);
    }

//    @Override
//    public Request<?> setRetryPolicy(RetryPolicy retryPolicy) {
//
//        return super.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {
//                return 20000;
//            }
//
//            @Override
//            public int getCurrentRetryCount() {
//                return 20000;
//            }
//
//            @Override
//            public void retry(VolleyError error) throws VolleyError {
//
//            }
//        });
//    }
}