package com.delivery.enroute.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;

import com.delivery.enroute.R;
import com.takusemba.spotlight.OnSpotlightStateChangedListener;
import com.takusemba.spotlight.OnTargetStateChangedListener;
import com.takusemba.spotlight.Spotlight;
import com.takusemba.spotlight.shape.Circle;
import com.takusemba.spotlight.target.CustomTarget;
import com.takusemba.spotlight.target.SimpleTarget;
import com.takusemba.spotlight.target.Target;

public class Tutorial {

    private static  Tutorial tutorial;

    public static Tutorial getInstance() {
        if (tutorial == null)
            tutorial = new Tutorial();

        return tutorial;
    }

    public void show(Activity context, View view){
        Spotlight.with(context)
                .setOverlayColor(R.color.background)
                .setDuration(1000L)
                .setAnimation(new DecelerateInterpolator(2f))
                .setTargets(createTarget(context, view))
        .setClosedOnTouchedOutside(false)
                .setOnSpotlightStateListener(new OnSpotlightStateChangedListener() {
                    @Override
                    public void onStarted() {
                        Toast.makeText(context, "spotlight is started", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onEnded() {
                        Toast.makeText(context, "spotlight is ended", Toast.LENGTH_SHORT).show();
                    }
                })
                .start();
    }

    private Target createTarget(Activity context, View view){
       return  new CustomTarget.Builder(context)
                .setPoint(100f, 340f)
                .setShape(new Circle(200f)) // or RoundedRectangle()
                .setOverlay(view)
                .setOnSpotlightStartedListener(new OnTargetStateChangedListener<CustomTarget>() {
                    @Override
                    public void onStarted(CustomTarget target) {
                        // do something
                    }
                    @Override
                    public void onEnded(CustomTarget target) {
                        // do something
                    }
                })
                .build();
    }
}
