package com.delivery.enroute.utils;

import android.view.Gravity;
import android.view.View;

import com.mancj.slideup.SlideUp;
import com.mancj.slideup.SlideUpBuilder;

public class MSlideUp {

    public static SlideUp controlView(View view){
        return new SlideUpBuilder(view)
                .withStartState(SlideUp.State.HIDDEN)
                .withStartGravity(Gravity.BOTTOM)
                .withGesturesEnabled(false)
                .withHideSoftInputWhenDisplayed(true)
                .withListeners()
                .build();
    }

    public static SlideUp controlView(View view, SlideUp.Listener.Events events){
        return new SlideUpBuilder(view)
                .withStartState(SlideUp.State.HIDDEN)
                .withStartGravity(Gravity.BOTTOM)
                .withGesturesEnabled(true)
                .withHideSoftInputWhenDisplayed(true)
                .withListeners(events)
                .build();
    }
}
