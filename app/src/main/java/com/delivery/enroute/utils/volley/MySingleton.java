package com.delivery.enroute.utils.volley;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.delivery.enroute.BuildConfig;
import com.delivery.enroute.utils.Constants;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;




/**
 * Created on 01-Feb-18.
 */

public class MySingleton {
    private static MySingleton mInstance;
    private RequestQueue mRequestQueue;

    private MySingleton(Context context) {

        mRequestQueue = getRequestQueue(context);
    }

    public static synchronized MySingleton getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MySingleton(context);
        }
        return mInstance;
    }

    private RequestQueue getRequestQueue(final Context mCtx) {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            HurlStack hurlStack = new HurlStack() {
                @Override
                protected HttpURLConnection createConnection(URL url) throws IOException {
                    HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
                    try {
                        httpsURLConnection.setSSLSocketFactory(HttpsTrustManager.getSSLSocketFactory());
                        httpsURLConnection.setHostnameVerifier(HttpsTrustManager.getHostnameVerifier());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return httpsURLConnection;
                }
            };

            // for http calls
//            mRequestQueue =  (BuildConfig.DEBUG) ? Volley.newRequestQueue(mCtx.getApplicationContext()) : Volley.newRequestQueue(mCtx.getApplicationContext(),hurlStack);

//            Log.d(Constants.TAG, "getRequestQueue: Debug mode " + BuildConfig.DEBUG);
            // for httpS calls
            mRequestQueue = Volley.newRequestQueue(mCtx,hurlStack);

        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Context context, Request<T> req, String tag) {

        Log.d(Constants.TAG, "addToRequestQueue: before tag : " + req.getTag());

        if (tag != null && tag.equals(req.getTag()))
            return;

        Log.d(Constants.TAG, "addToRequestQueue: after tag " + req.getTag());

        if (tag != null)
            req.setTag(tag);

        getRequestQueue(context).add(req);

    }

}