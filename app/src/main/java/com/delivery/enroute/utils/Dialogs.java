package com.delivery.enroute.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.widget.Toast;

import com.delivery.enroute.R;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class Dialogs {

    public static LovelyStandardDialog confirm(Context context){

        return  new LovelyStandardDialog(context, LovelyStandardDialog.ButtonLayout.HORIZONTAL)
                .setTopColorRes(R.color.colorPrimaryDark)
                .setButtonsColorRes(R.color.colorAccent)
                .setIcon(R.drawable.help)
                .setTitle("Are you sure ?")
                .setMessage("Tap yes to continue")
                .setIconTintColor(R.color.white)
                .setNegativeButtonText("No");

    }

    public static LovelyStandardDialog confirm(Context context, String message){

        return  new LovelyStandardDialog(context, LovelyStandardDialog.ButtonLayout.HORIZONTAL)
                .setTopColorRes(R.color.colorPrimaryDark)
                .setButtonsColorRes(R.color.colorAccent)
                .setIconTintColor(R.color.white)
                .setIcon(R.drawable.help)
                .setTitle("Are you sure ?")
                .setMessage(message)
                .setNegativeButtonText("No");

    }


    public static Dialog alert(Context context, String message){

        try {
            return  new LovelyStandardDialog(context, LovelyStandardDialog.ButtonLayout.HORIZONTAL)
                    .setTopColorRes(R.color.colorPrimaryDark)
                    .setButtonsColorRes(R.color.colorAccent)
                    .setIconTintColor(R.color.white)
                    .setIcon(R.drawable.notice)
                    .setTitle("Notice!")
                    .setMessage(message)
                    .setPositiveButtonText("Okay")
                    .show();
        }catch (Exception e){
            return null;
        }

    }

    public static SweetAlertDialog confirmWithSweet(Context context){
        return new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are sure")
                .setContentText("Tap yes to continue")
                .setCancelText("No")
                .setConfirmText("Yes")
                .showCancelButton(true);

    }

    public static SweetAlertDialog alertWithSweet(Context context, String message){
        SweetAlertDialog sweetAlertDialog =  new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Notice!")
                .setContentText(message)
                .setConfirmText("Okay")
                .showCancelButton(true);
        sweetAlertDialog.show();
        return sweetAlertDialog;

    }

}
