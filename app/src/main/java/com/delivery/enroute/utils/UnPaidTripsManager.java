package com.delivery.enroute.utils;

import com.delivery.enroute.enums.Collections;
import com.delivery.enroute.listeners.UnPaidTripListener;
import com.delivery.enroute.models.Trip;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class UnPaidTripsManager {

    private static UnPaidTripsManager unPaidTripsManager;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

    public static UnPaidTripsManager getInstance() {
        if (unPaidTripsManager == null)
            unPaidTripsManager = new UnPaidTripsManager();

        return unPaidTripsManager;
    }

   // public void getUnPa
    public void getUnFirstUnpaidTrip(UnPaidTripListener unPaidTripListener){
        if (firebaseUser == null) return;

//
        db.collection(Collections.REQUEST.toString())
                .whereEqualTo("senderId",firebaseUser.getUid())
                .whereEqualTo("hasPaid", false)
                .whereEqualTo("status", "COMPLETED")
                .addSnapshotListener((queryDocumentSnapshots, e) -> {
                    if (e != null)
                        return;

                    if (queryDocumentSnapshots == null) {
                        unPaidTripListener.notFound();
                        return;
                    }

                    if (queryDocumentSnapshots.isEmpty()) {
                        unPaidTripListener.notFound();
                        return;
                    }

                    List<Trip> tripList = queryDocumentSnapshots.toObjects(Trip.class);

                    Trip nonCashPayFound = null;
                    int countNonCashTrip = 0;
                    for (Trip trip : tripList){
                        if (!"CASH".equals(trip.paymentMethod) && !"Cash".equals(trip.paymentMethod)){
                            nonCashPayFound = trip;
                            countNonCashTrip++;
                        }
                    }
                    // prompt for only first trip
                    if (unPaidTripListener != null){
                        if (nonCashPayFound != null){
                            unPaidTripListener.unpaidTrip(nonCashPayFound, countNonCashTrip < 2);
                        }else{
                            unPaidTripListener.notFound();
                        }
                    }


                    // user can only pend 2 trips

                });

    }
}
