package com.delivery.enroute.utils;

import android.content.Context;
import android.view.View;

import com.delivery.enroute.R;

import java.util.ArrayList;


public class DecimalKeypad implements View.OnClickListener {

    int pinSize = 6;
    private StringBuilder builder = new StringBuilder();
    private OnNumberTappedListner onNumberTappedListner;


    public DecimalKeypad(View view) {
        View number0 = view.findViewById(R.id._0);
        View number1 = view.findViewById(R.id._1);
        View number2 = view.findViewById(R.id._2);
        View number3 = view.findViewById(R.id._3);
        View number4 = view.findViewById(R.id._4);
        View number5 = view.findViewById(R.id._5);
        View number6 = view.findViewById(R.id._6);
        View number7 = view.findViewById(R.id._7);
        View number8 = view.findViewById(R.id._8);
        View number9 = view.findViewById(R.id._9);
        View number_del = view.findViewById(R.id.del);
        View number_dot = view.findViewById(R.id.point);

       number0.setOnClickListener(this);
       number1.setOnClickListener(this);
       number2.setOnClickListener(this);
       number3.setOnClickListener(this);
       number4.setOnClickListener(this);
       number5.setOnClickListener(this);
       number6.setOnClickListener(this);
       number7.setOnClickListener(this);
       number8.setOnClickListener(this);
       number9.setOnClickListener(this);
       number_del.setOnClickListener(this);
       number_dot.setOnClickListener(this);
    }

    public void setOnNumberTappedListner(OnNumberTappedListner onNumberTappedListner) {
        this.onNumberTappedListner = onNumberTappedListner;
    }

    private void addCharacter(Character c){
        //Log.d(Constants.TAG, "addCharacter: pin size " + pincode.size());

        if (onNumberTappedListner != null && builder.toString().length() <= pinSize){
            builder.append(c);
            onNumberTappedListner.onNumberTapped(builder.toString());
        }
      //  amount.setItemName(builder.toString());
//            editTexts[pincode.size() - 1].setItemName(c);


        // check if pin is complete


    }

    private void removeCharacter(){
        if (builder.length() >= 1){

            builder.deleteCharAt(builder.length() - 1);
           // amount.setItemName(builder.toString());
            //Log.d(Constants.TAG, "removeCharacter: pin size " + pincode.size());

            if (onNumberTappedListner != null){
                onNumberTappedListner.onNumberTapped(builder.toString());
            }
        }
    }



    public void keypadClicked(View view){
        switch (view.getId()){
            case R.id._0:
                addCharacter('0');
                break;
            case R.id._1:
                addCharacter('1');
                break;
            case R.id._2:
                addCharacter('2');
                break;
            case R.id._3:
                addCharacter('3');
                break;
            case R.id._4:
                addCharacter('4');
                break;
            case R.id._5:
                addCharacter('5');
                break;
            case R.id._6:
                addCharacter('6');
                break;
            case R.id._7:
                addCharacter('7');
                break;
            case R.id._8:
                addCharacter('8');
                break;
            case R.id._9:
                addCharacter('9');
                break;
            case R.id.del:
                removeCharacter();
                break;
            case R.id.point:
                addCharacter('.');
                break;
        }
    }


    @Override
    public void onClick(View v) {
        keypadClicked(v);
    }

    public interface OnNumberTappedListner {
        void onNumberTapped(String number);
    }
}
