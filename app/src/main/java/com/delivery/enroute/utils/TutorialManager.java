package com.delivery.enroute.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class TutorialManager {

    private  static TutorialManager tutorialManager;
    private ArrayList<Integer> showViews = new ArrayList<>();
    private Gson gson;

    private TutorialManager(){}

    public static TutorialManager getInstance() {
        if (tutorialManager == null)
            tutorialManager = new TutorialManager();

        tutorialManager.gson = new Gson();

        return tutorialManager;
    }

    public boolean showTutorial(Context context, int id){
        showViews.clear();
        // get the set of shown views
        SharedPreferences sp = context.getSharedPreferences(Constants.APP, Context.MODE_PRIVATE);
        String tutorialViewString = sp.getString(Constants.TUTORIALS, null);
        if (tutorialViewString == null){
            showViews.add(id);
            String toSaveString  = gson.toJson(showViews);
            sp.edit().putString(Constants.TUTORIALS, toSaveString).apply();
            return true;
        }

        Type collectionType = new TypeToken<ArrayList<Integer>>(){}.getType();
        ArrayList<Integer> savedViews =  gson.fromJson(tutorialViewString, collectionType);
        showViews.addAll(savedViews);
        boolean viewFound = false;
        for (int savedView : showViews){
            if (savedView == id){
                viewFound = true;
                break;
            }
        }

        if (viewFound){
            return false;
        }else{
            showViews.add(id);
            String toSaveString  = gson.toJson(showViews);
            sp.edit().putString(Constants.TUTORIALS, toSaveString).apply();
            return true;
        }
    }

    public boolean hasShownTutorial(Context context, int id){
        SharedPreferences sp = context.getSharedPreferences(Constants.APP, Context.MODE_PRIVATE);
        String tutorialViewString = sp.getString(Constants.TUTORIALS, null);

        if (tutorialViewString == null){
            return false;
        }

        Type collectionType = new TypeToken<ArrayList<Integer>>(){}.getType();
        ArrayList<Integer> savedViews =  gson.fromJson(tutorialViewString, collectionType);
        boolean viewFound = false;
        for (int savedView : savedViews){
            if (savedView == id){
                viewFound = true;
                break;
            }
        }

        return viewFound;
    }

    public void clearAllTutorials(Context context){
        SharedPreferences sp = context.getSharedPreferences(Constants.APP, Context.MODE_PRIVATE);
        sp.edit().remove(Constants.TUTORIALS).apply();
    }
}
