package com.delivery.enroute.utils;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.delivery.enroute.BuildConfig;
import com.delivery.enroute.MainActivity;
import com.delivery.enroute.activities.MaintenanceModeActivity;
import com.delivery.enroute.layoutdialogs.UpdateAppDialog;
import com.delivery.enroute.utils.volley.RequestObject;

import org.json.JSONException;

import spencerstudios.com.bungeelib.Bungee;

import static com.delivery.enroute.utils.Constants.TAG;

public class MFile {

    public static void diagnoseApp(MainActivity mainActivity, UpdateAppDialog.OnupdateappListener onupdateappListener) {
        new Thread(() -> {
            String url = "https://apps-99427.firebaseio.com/enrout.json";
            new RequestObject(url, null, response -> {
                Log.d(TAG, "diagnoseApp: " + response);

                try {
                    String debugUrl = response.getString("debugUrl");
                    String maintenanceMessage = response.getString("maintenanceMessage");
                    boolean maintenanceMode = response.getBoolean("maintenanceMode");
                    int versionCode = response.getInt("versionCode");
                    if (maintenanceMode){

                        Intent intent = new Intent(mainActivity, MaintenanceModeActivity.class);
                        intent.putExtra("message", maintenanceMessage);
                        mainActivity.startActivity(intent);
                        Bungee.slideLeft(mainActivity);

                    }else if (versionCode > BuildConfig.VERSION_CODE){
                        if (BuildConfig.DEBUG){

                            try {
                                mainActivity.runOnUiThread(() -> {
                                    onupdateappListener.onUpdate(debugUrl);
                                });

                            } catch (Exception e) {

                            }

                        }else{
                            try {

                                mainActivity.runOnUiThread(() -> {
                                   onupdateappListener.onUpdate(null);
                                });

                            } catch (Exception e) {

                            }
                        }
                    }else{
                        // not in maintenance mode
                        // app is up to date
                        mainActivity.runOnUiThread(onupdateappListener::onEverythinOk);
                    }

                } catch (JSONException e) {
                    Log.d(TAG, "diagnoseApp: JSONException " + e.getMessage());
                    e.printStackTrace();
                }

            }, error -> {
                Log.d(TAG, "diagnoseApp: error " + error);
            }).send(mainActivity);
        }).start();
    }
}
