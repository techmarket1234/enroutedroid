package com.delivery.enroute.getstarted.appintro;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.viewpager.widget.ViewPager;

import com.delivery.enroute.R;
import com.delivery.enroute.getstarted.WelcomeActivity;
import com.delivery.enroute.utils.Constants;
import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.app.OnNavigationBlockedListener;
import com.heinrichreimersoftware.materialintro.slide.FragmentSlide;
import com.heinrichreimersoftware.materialintro.slide.Slide;

import spencerstudios.com.bungeelib.Bungee;

public class AppIntroActivity extends IntroActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        FragmentSlide intro1Slide = new FragmentSlide.Builder()
                .background(R.color.black)
                .backgroundDark(R.color.white)
                .fragment(new Intro1Fragment())
                .build();

        addSlide(intro1Slide);

        FragmentSlide intro2Slide = new FragmentSlide.Builder()
                .background(R.color.black)
                .backgroundDark(R.color.white)
                .fragment(new Intro2Fragment())
                .canGoForward(false)
                .build();

        addSlide(intro2Slide);

        addOnNavigationBlockedListener((position, direction) -> {
            Log.d(Constants.TAG, "onNavigationBlocked:  " + position);
            startActivity(new Intent(AppIntroActivity.this, WelcomeActivity.class));
            Bungee.split(AppIntroActivity.this);
        });
    }


}
