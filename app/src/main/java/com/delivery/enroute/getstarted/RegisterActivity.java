package com.delivery.enroute.getstarted;

import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.delivery.enroute.R;
import com.delivery.enroute.utils.Constants;
import com.hbb20.CountryCodePicker;

import spencerstudios.com.bungeelib.Bungee;

public class RegisterActivity extends AppCompatActivity{

    EditText fullname, phoneNumber;
    Button captureName, goToLogin;
    CountryCodePicker ccp;
    String number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_registration);


        fullname = findViewById(R.id.fullName);
        phoneNumber = findViewById(R.id.etphoneNumber);
        findViewById(R.id.btn_terms).setOnClickListener(view -> {
            Constants.openTerms(this);
        });

//        initialize countries
        //Country Code
        ccp = findViewById(R.id.countryCode);
        ccp.registerCarrierNumberEditText(phoneNumber);

//        number = ccp.getFullNumberWithPlus();

        captureName = findViewById(R.id.continue_btn);
        captureName.setOnClickListener(captureNameTapped);

        goToLogin = findViewById(R.id.go_to_login);
        goToLogin.setOnClickListener(goToLoginTapped);
//        signin.setOnClickListener(this);

    }

    private View.OnClickListener goToLoginTapped = view -> {
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    };


    private View.OnClickListener captureNameTapped = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //        validate the username field,
            if (TextUtils.isEmpty(fullname.getText())){
                fullname.setError("Required");
                return;
            }

            if (TextUtils.isEmpty(phoneNumber.getText())){
                phoneNumber.setError("Required");
                return;
            }

            Log.d(Constants.TAG, "onClick: phone number is " + phoneNumber.getText());
            Log.d(Constants.TAG, "onClick: phone number length =  " + phoneNumber.getText().toString().length());
            if (String.valueOf(phoneNumber.getText()).trim().length() > 11 || String.valueOf(phoneNumber.getText()).trim().length() < 10){
                phoneNumber.setError("Invalid phone number");
                return;
            }

            String mPhone = String.valueOf(phoneNumber.getText()).trim();
            if (mPhone.startsWith("0")){
                phoneNumber.setText(removeFirstChar(mPhone));
            }

            number = ccp.getFullNumberWithPlus();

            Log.d(Constants.TAG, "onClick: ccp.getFullNumberWithPlus() " + ccp.getFullNumberWithPlus());

//        pass the username to the registration option page to create account
            Intent intent = new Intent(RegisterActivity.this, RegistrationOptionActivity.class);
            intent.putExtra("username", fullname.getText().toString());
            intent.putExtra("phone", number);
            startActivity(intent);
            Bungee.slideLeft(RegisterActivity.this);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);
    }

    private String removeFirstChar(String s){
        return s.substring(1);
    }
}
