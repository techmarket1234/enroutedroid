package com.delivery.enroute.getstarted;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.delivery.enroute.MainActivity;
import com.delivery.enroute.R;
import com.delivery.enroute.broadcastreceivers.SmsBroadcastReceiver;
import com.delivery.enroute.layoutdialogs.VerficationCodeSentFragment;
import com.delivery.enroute.models.User;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.DecimalKeypad;
import com.delivery.enroute.utils.Dialogs;
import com.delivery.enroute.utils.LoadingGif;
import com.delivery.enroute.webservices.Authenticate;
import com.delivery.enroute.webservices.callbacks.WebServiceCallbacks;
import com.delivery.enroute.webservices.errors.WebServiceErrors;
import com.google.firebase.auth.FirebaseAuth;


import spencerstudios.com.bungeelib.Bungee;

public class VerifyPhoneActivity extends AppCompatActivity{

    private static final String VERIFICATION_CODE_SENT = "VERIFICATION_CODE_SENT";
    int pincodeSize = 6;
    EditText[] editTexts;
    View codeGroup;
    Authenticate authenticate;
    Button resendCode;
    boolean smsPermissionGranted, isPhoneLogin;
    String phoneNumber, persoName;
    VerficationCodeSentFragment verficationCodeSentFragment;

    SmsBroadcastReceiver br;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);

        codeGroup = findViewById(R.id.code_group);
        codeGroup.setOnClickListener(pinViewTapped);

        smsPermissionGranted = getIntent().getBooleanExtra(Constants.READ_SMS_PERMISSION_GRANTED, false);
        isPhoneLogin = getIntent().getBooleanExtra(Constants.IS_PHONE_LOGIN, false);

        editTexts = new EditText[]{findViewById(R.id.slot_1),findViewById(R.id.slot_2),findViewById(R.id.slot_3),findViewById(R.id.slot_4),findViewById(R.id.slot_5),findViewById(R.id.slot_6)};

        View keypad = findViewById(R.id.keypad);
        DecimalKeypad decimalKeypad = new DecimalKeypad(keypad);
        decimalKeypad.setOnNumberTappedListner(keyPadTapped);

        authenticate = Authenticate.newInstance(this);

        resendCode = findViewById(R.id.resend_code);
        resendCode.setOnClickListener(resendCodeTapped);

        verficationCodeSentFragment = new VerficationCodeSentFragment();


        phoneNumber = getIntent().getStringExtra(Constants.PHONE_AUTH_PHONE_NUMBER);
        persoName = getIntent().getStringExtra(Constants.PHONE_AUTH_USERNAME);
        if (phoneNumber != null){
            ((TextView) findViewById(R.id.pageSubTitle)).setText(getResources().getString(R.string.verification_code_sent,phoneNumber));
        }

        if (smsPermissionGranted){
            br = new SmsBroadcastReceiver();
            br.setSmsListener(code -> {
                Log.d(Constants.TAG, "recieved: code received " + code);
                setTextAndVerify(code);
            });
        }


        // send code
        if (isPhoneLogin){
            loginWithPhone();

        }else{
            registerWithPhone();
        }
        
    }

    private void loginWithPhone() {


         new Thread(() -> {
             runOnUiThread(() -> LoadingGif.show(VerifyPhoneActivity.this));
             authenticate.loginWithPhoneNumber(phoneNumber).setCallbacks(new WebServiceCallbacks() {
             @Override
             public <T> void OnSuccess(T result) {
                 Log.d(Constants.TAG, "OnSuccess: " + result);
                 runOnUiThread(LoadingGif::hide);


                 if (result instanceof Integer){ // if result is an instance of an inger the verification code sent,
                     if (((Integer) result) == 100){
                        runOnUiThread(() -> {
                            verficationCodeSentFragment.show(getSupportFragmentManager(),VERIFICATION_CODE_SENT);
                            Toast.makeText(VerifyPhoneActivity.this, "Verification code sent", Toast.LENGTH_SHORT).show();
                        });
                     }

                 }

                 if (result instanceof User){
                     User user = ((User) result);
                     Log.d(Constants.TAG, "OnSuccess: login user - " + user.fullName);


                     runOnUiThread(() -> {
                         Toast.makeText(VerifyPhoneActivity.this, user.fullName + " has logged in", Toast.LENGTH_SHORT).show();

                         startActivity(new Intent(VerifyPhoneActivity.this, MainActivity.class));
                         Bungee.slideLeft(VerifyPhoneActivity.this);
                     });
                 }

             }

             @Override
             public void OnFail(WebServiceErrors reason) {
                 if (FirebaseAuth.getInstance().getCurrentUser() != null)
                     FirebaseAuth.getInstance().signOut();

                 runOnUiThread(() -> {
                     LoadingGif.hide();
                     Log.d(Constants.TAG, "OnFail: " + reason.message);
                     Dialogs.alert(VerifyPhoneActivity.this, reason.message);
                 });
             }
         });
    }
         ).start();

    }

    private void  registerWithPhone() {

        new Thread(() -> {
            runOnUiThread(() -> LoadingGif.show(VerifyPhoneActivity.this));
            authenticate.registerWithPhoneNumber(persoName, phoneNumber).setCallbacks(new WebServiceCallbacks() {
                @Override
                public <T> void OnSuccess(T result) {
                    Log.d(Constants.TAG, "OnSuccess: " + result);

                    runOnUiThread(LoadingGif::hide);

                    if (result instanceof Integer){ // if result is an instance of an inger the verification code sent,
                        if (((Integer) result) == 100){
                            runOnUiThread(() -> {
                                verficationCodeSentFragment.show(getSupportFragmentManager(),VERIFICATION_CODE_SENT);
                                Toast.makeText(VerifyPhoneActivity.this, "Verification code sent", Toast.LENGTH_SHORT).show();
                            });
                        }

                    }

                    if (result instanceof  User){
                        User user = ((User) result);
                        Log.d(Constants.TAG, "OnSuccess: login user - " + user.fullName);
                        runOnUiThread(() -> {
                            Toast.makeText(VerifyPhoneActivity.this, user.fullName + " has logged in", Toast.LENGTH_SHORT).show();

                            startActivity(new Intent(VerifyPhoneActivity.this, MainActivity.class));
                            Bungee.slideLeft(VerifyPhoneActivity.this);
                        });
                    }

                }

                @Override
                public void OnFail(WebServiceErrors reason) {

                    Log.d(Constants.TAG, "OnFail: " + reason.message);
                   runOnUiThread(() -> {
                       LoadingGif.hide();
                       Dialogs.alert(VerifyPhoneActivity.this, reason.message);
                   });
                }
            });
        }).start();

    }

    @Override
    protected void onPause() {
        super.onPause();
        try{
            unregisterReceiver(br);
        }catch (Exception e){
            e.getMessage();
        }

    }

    private View.OnClickListener resendCodeTapped = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            sendVerificationCodeAndAuthenticate();
        }
    };

    private void sendVerificationCodeAndAuthenticate(){
        LoadingGif.show(VerifyPhoneActivity.this);

        new Thread(() -> {
            authenticate.resendPhoneVerificationCode().setCallbacks(new WebServiceCallbacks() {
                @Override
                public <T> void OnSuccess(T result) {
                    Log.d(Constants.TAG, "OnSuccess: " + result);
                   runOnUiThread(LoadingGif::hide);
                    if (result instanceof Integer){ // if result is an instance of an inger the verification code sent,
                        if (((Integer) result) == 100){
                            runOnUiThread(() -> {
                                Dialogs.alert(VerifyPhoneActivity.this, "Verification has been sent");
                            });
                            // Toast.makeText(VerifyPhoneActivity.this, "Verification has bee resent", Toast.LENGTH_SHORT).show();
                        }

                    }

                    if (result instanceof  User){
                        User user = ((User) result);
                        runOnUiThread(() -> {
                            Toast.makeText(VerifyPhoneActivity.this, user.fullName + " has logged in", Toast.LENGTH_SHORT).show();

                            startActivity(new Intent(VerifyPhoneActivity.this, MainActivity.class));
                            Bungee.slideLeft(VerifyPhoneActivity.this);
                        });
                    }

                }

                @Override
                public void OnFail(WebServiceErrors reason) {
                    Log.d(Constants.TAG, "OnFail: " + reason.message);
                    runOnUiThread(() -> {
                        LoadingGif.hide();
                        Dialogs.alert(VerifyPhoneActivity.this, reason.message);
                    });
                }
            });
        }).start();

    }

    private View.OnClickListener pinViewTapped = view -> Toast.makeText(VerifyPhoneActivity.this, "Kindly use the dial pad below", Toast.LENGTH_SHORT).show();
    
    private DecimalKeypad.OnNumberTappedListner keyPadTapped = number -> {
        Log.d(Constants.TAG, "onNumberTapped: " + number);

        setTextAndVerify(number);

    };

    private void setTextAndVerify(String number){
        if (number.length() > pincodeSize){
            return;
        }

        for (int i = 0; i < editTexts.length; i++) {
            editTexts[i].setText("");
        }

        for (int i = 0; i < number.length(); i++) {
            editTexts[i].setText(String.valueOf(number.charAt(i)));
        }

        if (number.length() == pincodeSize){

            for (int i = 0; i < editTexts.length; i++) {
                editTexts[i].setText("");
            }
            // pin code completed
            verifySmsCode(number);
            //Toast.makeText(VerifyPhoneActivity.this, "Pin complete", Toast.LENGTH_SHORT).show();
        }
    }

    private void verifySmsCode(String number) {
        LoadingGif.show(this);
        authenticate.verifySMSCode(number).setCallbacks(new WebServiceCallbacks() {
            @Override
            public <T> void OnSuccess(T result) {
                LoadingGif.hide();
                User user = (User) result;

                if (user == null){
                    FirebaseAuth.getInstance().signOut();
                    Dialogs.alert(VerifyPhoneActivity.this, "You have not registered with us. Kindly sign up instead");
                    return;
                }


                Toast.makeText(VerifyPhoneActivity.this, user.fullName + " has logged in", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(VerifyPhoneActivity.this, MainActivity.class));
                Bungee.card(VerifyPhoneActivity.this);
            }

            @Override
            public void OnFail(WebServiceErrors reason) {
                LoadingGif.hide();
                Log.d(Constants.TAG, "OnFail: " + reason.message);
                Dialogs.alert(VerifyPhoneActivity.this, reason.message);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideRight(this);
    }
}
