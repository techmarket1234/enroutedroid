package com.delivery.enroute.getstarted;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Toast;

import com.delivery.enroute.R;
import com.delivery.enroute.utils.Constants;
import com.github.florent37.viewanimator.ViewAnimator;

import spencerstudios.com.bungeelib.Bungee;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {

    boolean mLocationPermissionGranted = false;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_and_login_option_screen);

        findViewById(R.id.go_to_sign_up).setOnClickListener(this);
        findViewById(R.id.go_to_login).setOnClickListener(this);

        //ViewAnimator.animate(findViewById(R.id.decorator)).flash().interpolator(new AccelerateDecelerateInterpolator()).start();

        sp = getSharedPreferences(Constants.APP, MODE_PRIVATE);
        sp.edit().putBoolean(Constants.USER_FIRST_TIME, false).apply();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.go_to_sign_up:
                startActivity(new Intent(this, RegisterActivity.class));
                Bungee.card(this);

                break;
            case R.id.go_to_login:
                startActivity(new Intent(this, LoginActivity.class));
                Bungee.card(this);

                break;
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

}
