package com.delivery.enroute.getstarted;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.delivery.enroute.MainActivity;
import com.delivery.enroute.R;
import com.delivery.enroute.models.User;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.Dialogs;
import com.delivery.enroute.utils.LoadingGif;
import com.delivery.enroute.webservices.Authenticate;
import com.delivery.enroute.webservices.callbacks.WebServiceCallbacks;
import com.delivery.enroute.webservices.errors.WebServiceErrors;
import com.google.android.gms.dynamic.IFragmentWrapper;
import com.google.firebase.auth.FirebaseAuth;
import com.mancj.slideup.SlideUp;


import spencerstudios.com.bungeelib.Bungee;

public class LoginActivity extends AppCompatActivity {

    //SlideUp slideUp;
   // View enter_phone_layout;
    Button termsButton;
//    EditText phoneNumber;
//    CountryCodePicker ccp;
    String number;
    boolean smsPermissionGranted;

    Authenticate authenticate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_login);


        authenticate = Authenticate.newInstance(LoginActivity.this);


        findViewById(R.id.go_to_sign_up).setOnClickListener(goToRegisterTapped);

        findViewById(R.id.id_facebook).setOnClickListener(loginWithFacebookTapped);
        findViewById(R.id.google_plus).setOnClickListener(loginWithGoogleTapped);
        findViewById(R.id.phone_number).setOnClickListener(loginWithPhoneTapped);
        termsButton = findViewById(R.id.btn_terms);
        termsButton.setOnClickListener(view -> {
            Constants.openTerms(LoginActivity.this);
        });
//        verifyPhone = findViewById(R.id.btnVerifyPhone);
//        verifyPhone.setOnClickListener(verifyPhoneTapped);

//        enter_phone_layout = findViewById(R.id.enter_phone_layout);
//        enter_phone_layout.setVisibility(View.VISIBLE);

//        slideUp = new SlideUpBuilder(enter_phone_layout)
//                .withStartState(SlideUp.State.HIDDEN)
//                .withStartGravity(Gravity.BOTTOM)
//                .withGesturesEnabled(true)
//                .withListeners(slideUpListner)
//                .withHideSoftInputWhenDisplayed(true)
//                .build();

//        findViewById(R.id.closeSlide).setOnClickListener(closeSlideTapped);

        //phoneNumber = findViewById(R.id.etphoneNumber);

//        initialize countries
        //Country Code
//        ccp = findViewById(R.id.countryCode);
//        ccp.registerCarrierNumberEditText(phoneNumber);


    }

    private View.OnClickListener closeSlideTapped = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //slideUp.hide();
        }
    };

    private void verifyPhoneEntered(){
//        if (!Constants.isNetworkAvailable(LoginActivity.this)){
//            display("No internet connection");
//            return;
//        }

//            if (TextUtils.isEmpty(phoneNumber.getText())){
//                phoneNumber.setError("required");
//                return;
//            }

        //  number = ccp.getFullNumberWithPlus();
        Log.d(Constants.TAG, "onClick: ccp.getFullNumberWithPlus() " + number);

        Intent intent = new Intent(LoginActivity.this, VerifyPhoneActivity.class);
        intent.putExtra(Constants.PHONE_AUTH_PHONE_NUMBER, number);
        intent.putExtra(Constants.IS_PHONE_LOGIN,true);
        startActivity(intent);
        Bungee.slideLeft(LoginActivity.this);


    }

    // listen for slide ups and downs
    private SlideUp.Listener.Events slideUpListner = new SlideUp.Listener.Events() {
        @Override
        public void onSlide(float percent) {

        }

        @Override
        public void onVisibilityChanged(int visibility) {
            termsButton.setVisibility(visibility == View.VISIBLE ? View.GONE : View.VISIBLE);
        }
    };

    private View.OnClickListener goToRegisterTapped = view -> {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Bungee.fade(LoginActivity.this);
    };

    //    user wants to register with facebook
    private View.OnClickListener loginWithFacebookTapped = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (!Constants.isNetworkAvailable(LoginActivity.this)){
                display("No internet connection");
                return;
            }

            LoadingGif.show(LoginActivity.this);

            authenticate.loginWithFacebook().setCallbacks(new WebServiceCallbacks() {
                @Override
                public <T> void OnSuccess(T result) {

                    LoadingGif.hide();
                    Log.d(Constants.TAG, "OnSuccess: ");


                    User user = (User) result;

                    if (user != null)
                        Toast.makeText(LoginActivity.this, user.fullName + " login successful ", Toast.LENGTH_SHORT).show();


                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    Bungee.slideLeft(LoginActivity.this);

                }

                @Override
                public void OnFail(WebServiceErrors reason) {

                    if (FirebaseAuth.getInstance().getCurrentUser() != null)
                        FirebaseAuth.getInstance().signOut();

//                    if (reason.statusCode == 501){
//                        display(reason.message);
//                        //Toast.makeText(LoginActivity.this, "Authentication cancelled", Toast.LENGTH_SHORT).show();
//                        return;
//                    }
                    LoadingGif.hide();

                    display(reason.message);
                }
            });

//            Toast.makeText(LoginActivity.this, "Login with facebook", Toast.LENGTH_SHORT).show();
//            startActivity(new Intent(LoginActivity.this, MainActivity.class));
//            Bungee.slideLeft(LoginActivity.this);

        }
    };

    //    user wants to register with google
    private View.OnClickListener loginWithGoogleTapped = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (!Constants.isNetworkAvailable(LoginActivity.this)){
                display("No internet connection");
                return;
            }
           // Toast.makeText(LoginActivity.this, "Login with google", Toast.LENGTH_SHORT).show();

          LoadingGif.show(LoginActivity.this);
            authenticate.loginWithGoogle().setCallbacks(new WebServiceCallbacks() {
                @Override
                public <T> void OnSuccess(T result) {

                }

                @Override
                public void OnFail(WebServiceErrors reason) {
                    if (FirebaseAuth.getInstance().getCurrentUser() != null)
                        FirebaseAuth.getInstance().signOut();

                    LoadingGif.hide();
                   display(reason.message);

                }
            });

//            startActivity(new Intent(LoginActivity.this, MainActivity.class));
//            Bungee.slideLeft(LoginActivity.this);

        }
    };

    //    user wants to register with phone
    private View.OnClickListener loginWithPhoneTapped = view -> {

        //slideUp.show();
        startActivityForResult(new Intent(this, EnterPhoneActivity.class),1);
        Bungee.slideUp(this);
        //Toast.makeText(LoginActivity.this, "Kindly enter your phone number", Toast.LENGTH_SHORT).show();

    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.ENTER_PHONE_NUBER && resultCode == RESULT_OK){
            if (data != null){
                number = data.getStringExtra(Constants.PHONE_AUTH_PHONE_NUMBER);
                smsPermissionGranted = data.getBooleanExtra(Constants.READ_SMS_PERMISSION_GRANTED, false);
            }

            verifyPhoneEntered();
            return;
        }

        //        Google authentication ......
        if (requestCode == Constants.RC_SIGN_IN){

//            Geohash geohash = Geohash.withCharacterPrecision(53.244664, -6.140530, 12);
//            String geohashString = geohash.toBase32().substring(0, 3); //3 characters for around 100km of precision


            authenticate.finalizeAuthWithGoogle(requestCode,data).setCallbacks(new WebServiceCallbacks() {
                @Override
                public <T> void OnSuccess(T result) {
                    LoadingGif.hide();

                    User user = (User) result;

                    if (user == null) {
                        FirebaseAuth.getInstance().signOut();
                        display("You have not registered with us. Kindly sign up instead");
                        return;
                    }


                    Toast.makeText(LoginActivity.this, user.fullName + " login successful", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    Bungee.slideLeft(LoginActivity.this);
                }

                @Override
                public void OnFail(WebServiceErrors reason) {

                    if (FirebaseAuth.getInstance().getCurrentUser() != null)
                        FirebaseAuth.getInstance().signOut();

                    Log.d(Constants.TAG, "OnFail: " + reason.message);
                    LoadingGif.hide();
                    display(reason.message);

                }
            });

            return;

        }

        //        Facebook authentication ....

        authenticate.finalizeAuthWithFacebook(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        if (slideUp.isVisible()){
//            slideUp.hide();
//            return;
//        }
        Bungee.slideRight(this);
    }

    private void display(String message){
        Log.d(Constants.TAG, message);
        Dialogs.alertWithSweet(this, message);
    }
}
