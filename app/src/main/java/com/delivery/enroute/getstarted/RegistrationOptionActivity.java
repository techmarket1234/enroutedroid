package com.delivery.enroute.getstarted;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.delivery.enroute.MainActivity;
import com.delivery.enroute.R;
import com.delivery.enroute.models.User;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.Dialogs;
import com.delivery.enroute.utils.LoadingGif;
import com.delivery.enroute.webservices.Authenticate;
import com.delivery.enroute.webservices.callbacks.WebServiceCallbacks;
import com.delivery.enroute.webservices.errors.WebServiceErrors;
import com.hbb20.CountryCodePicker;
import com.mancj.slideup.SlideUp;
import com.mancj.slideup.SlideUpBuilder;


import spencerstudios.com.bungeelib.Bungee;

public class RegistrationOptionActivity extends AppCompatActivity {

    String username;
    String phone;

    Authenticate authenticate;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_registration_options);

//        get the username entered from the previous registration screen
        username = getIntent().getStringExtra("username");
        phone = getIntent().getStringExtra("phone");

        if (username == null) {
            Toast.makeText(this, "Kindly enter your name", Toast.LENGTH_SHORT).show();
            finish();
        }

        authenticate = Authenticate.newInstance(RegistrationOptionActivity.this);

//        initialize registration option
        findViewById(R.id.id_facebook).setOnClickListener(registerWithFacebookTapped);
        findViewById(R.id.google_plus).setOnClickListener(registerWithGoogleTapped);
        findViewById(R.id.phone_number).setOnClickListener(verifyPhoneTapped);

        findViewById(R.id.go_to_login).setOnClickListener(goToLoginTapped);


//        slideUp = new SlideUpBuilder(enter_phone_layout)
//                .withStartState(SlideUp.State.HIDDEN)
//                .withStartGravity(Gravity.BOTTOM)
//                .withGesturesEnabled(true)
//                .withHideSoftInputWhenDisplayed(true)
//                .withListeners()
//                .build();


//       phoneNumber = findViewById(R.id.etphoneNumber);

//        initialize countries
        //Country Code


    }

    private View.OnClickListener verifyPhoneTapped = new View.OnClickListener() {
        @Override
        public void onClick(View view) {


//            if (!Constants.isNetworkAvailable(RegistrationOptionActivity.this)){
//                Dialogs.alert(RegistrationOptionActivity.this, "No internet connection");
//                return;
//            }
        proceedWithValidation(false);


        }
    };

    private boolean isPermissionGranted(){
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED);
    }

    //    ask permission
    private void askPermission(){
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED)
                || (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED)

        )

        {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECEIVE_SMS)
                    || ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_SMS)

            ) {

                Toast.makeText(this, "You need to grant permission before setup proceeds", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(this,new String[]{
                        Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.READ_SMS
                }, Constants.MY_PERMISSIONS_REQUEST_READ_SMS);
                Log.d(Constants.TAG, "Permission Being Requested");
            }else{
                ActivityCompat.requestPermissions(this,new String[]{
                        Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.READ_SMS
                }, Constants.MY_PERMISSIONS_REQUEST_READ_SMS);
                Log.d(Constants.TAG, "Permission Being Requested");
            }
            Log.d(Constants.TAG, "Permission not granted");


        }else{
            proceedWithValidation(true);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case Constants.MY_PERMISSIONS_REQUEST_READ_SMS:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    proceedWithValidation(true);

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    proceedWithValidation(false);
                }
                break;
        }
    }

    private void proceedWithValidation(boolean readSmsPermissionGranted) {

        // do this after user gives permission

        //Toast.makeText(RegistrationOptionActivity.this, "Verification code sent", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(RegistrationOptionActivity.this, VerifyPhoneActivity.class);
        intent.putExtra(Constants.PHONE_AUTH_PHONE_NUMBER, phone);
        intent.putExtra(Constants.PHONE_AUTH_USERNAME, username);
        intent.putExtra(Constants.READ_SMS_PERMISSION_GRANTED, readSmsPermissionGranted);
        startActivity(intent);
        Bungee.slideLeft(RegistrationOptionActivity.this);

//        String number = ccp.getFullNumberWithPlus();
//        Intent intent = new Intent();

//        intent.putExtra(Constants.READ_SMS_PERMISSION_GRANTED, readSmsPermissionGranted);
//        setResult(Activity.RESULT_OK, intent);
//        finish();

    }



    private View.OnClickListener goToLoginTapped = view -> {
        Intent intent = new Intent(RegistrationOptionActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Bungee.fade(RegistrationOptionActivity.this);
    };

//    user wants to register with facebook
    private View.OnClickListener registerWithFacebookTapped = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (!Constants.isNetworkAvailable(RegistrationOptionActivity.this)){
                Dialogs.alert(RegistrationOptionActivity.this, "No internet connection");
                return;
            }


            LoadingGif.show(RegistrationOptionActivity.this);
            authenticate.registerWithFacebook(username, phone).setCallbacks(new WebServiceCallbacks() {
                @Override
                public <T> void OnSuccess(T result) {

                    LoadingGif.hide();

                    Log.d(Constants.TAG, "OnSuccess: ");



                    User user = (User) result;

                    if (user != null)
                    Toast.makeText(RegistrationOptionActivity.this, user.fullName + " registration successful ", Toast.LENGTH_SHORT).show();


                    startActivity(new Intent(RegistrationOptionActivity.this, MainActivity.class));
                    Bungee.slideLeft(RegistrationOptionActivity.this);
                }

                @Override
                public void OnFail(WebServiceErrors reason) {

                    LoadingGif.hide();

                    Log.d(Constants.TAG, "OnFail: " + reason.message);
                    Toast.makeText(RegistrationOptionActivity.this, reason.message, Toast.LENGTH_SHORT).show();
                }
            });

//            Toast.makeText(RegistrationOptionActivity.this, "Register with facebook", Toast.LENGTH_SHORT).show();
//            startActivity(new Intent(RegistrationOptionActivity.this, MainActivity.class));
//            Bungee.slideLeft(RegistrationOptionActivity.this);
        }
    };

    //    user wants to register with google
    private View.OnClickListener registerWithGoogleTapped = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (!Constants.isNetworkAvailable(RegistrationOptionActivity.this)){
                Dialogs.alert(RegistrationOptionActivity.this, "No internet connection");
                return;
            }

            LoadingGif.show(RegistrationOptionActivity.this);

            authenticate.registerWithGoogle(username, phone).setCallbacks(new WebServiceCallbacks() {
                @Override
                public <T> void OnSuccess(T result) {

                }

                @Override
                public void OnFail(WebServiceErrors reason) {
                    LoadingGif.hide();
                    Dialogs.alert(RegistrationOptionActivity.this, reason.message);
                }
            });

        }
    };



    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        Google authentication ......
        if (requestCode == Constants.RC_SIGN_IN){

//            LoadingGif.show(RegistrationOptionActivity.this);
            authenticate.finalizeAuthWithGoogle(requestCode,data).setCallbacks(new WebServiceCallbacks() {
                @Override
                public <T> void OnSuccess(T result) {
                    LoadingGif.hide();

                    User user = (User) result;

                    if (user == null) return;



                    Toast.makeText(RegistrationOptionActivity.this, user.fullName + " registration successful", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(RegistrationOptionActivity.this, MainActivity.class));
                    Bungee.slideLeft(RegistrationOptionActivity.this);
                }

                @Override
                public void OnFail(WebServiceErrors reason) {
                    LoadingGif.hide();
                    Log.d(Constants.TAG, "OnFail: " + reason.message);
                    Dialogs.alert(RegistrationOptionActivity.this, reason.message);
                }
            });

            return;

        }


//        Facebook authentication ....

        authenticate.finalizeAuthWithFacebook(requestCode, resultCode, data);




    }


}
