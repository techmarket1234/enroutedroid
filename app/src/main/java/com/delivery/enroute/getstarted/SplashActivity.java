package com.delivery.enroute.getstarted;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.delivery.enroute.MainActivity;
import com.delivery.enroute.R;
import com.delivery.enroute.activities.ErrorActivity;
import com.delivery.enroute.getstarted.appintro.AppIntroActivity;
import com.delivery.enroute.getstarted.appintro.Intro1Fragment;
import com.delivery.enroute.utils.Constants;
import com.github.florent37.viewanimator.ViewAnimator;
import com.google.firebase.auth.FirebaseAuth;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import spencerstudios.com.bungeelib.Bungee;

public class SplashActivity extends AppCompatActivity {
    ImageView appLogo;
    TextView appName;
    SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        appLogo = findViewById(R.id.app_logo);
        appName = findViewById(R.id.app_name);

        sp = getSharedPreferences(Constants.APP, MODE_PRIVATE);
//        appDesc = findViewById(R.id.description);


//        Log.d(Constants.TAG, "onCreate: it pick point icon - " + R.mipmap.pick_up_icon);
//        Log.d(Constants.TAG, "onCreate: it destination point icon - " + R.mipmap.destination_icon);
        //hash; //"v12n8trdj"


    }

    @Override
    protected void onResume() {
        super.onResume();


        try {
            @SuppressLint("PackageManagerGetSignatures") PackageInfo info = getPackageManager().getPackageInfo(
                    "com.delivery.enroute",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d(Constants.TAG, Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(Constants.TAG, "onCreate: NameNotFoundException " + e.getLocalizedMessage());
        } catch (NoSuchAlgorithmException e) {
            Log.d(Constants.TAG, "onCreate: NoSuchAlgorithmException " + e.getLocalizedMessage());
        }

        appName.setVisibility(View.GONE);
//        appDesc.setVisibility(View.GONE);

        appName.setVisibility(View.VISIBLE);
        ViewAnimator.animate(appName).bounceIn().onStop(() -> {

//
////            // check if its user's first time of using the app
            if (sp.getBoolean(Constants.USER_FIRST_TIME, true)){
                startActivity(new Intent(SplashActivity.this, AppIntroActivity.class));
                Bungee.slideLeft(SplashActivity.this);
                return;
            }

            if(FirebaseAuth.getInstance().getCurrentUser() == null){
                startActivity(new Intent(SplashActivity.this, WelcomeActivity.class));
                Bungee.zoom(SplashActivity.this);

                return;
            }

            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            Bungee.split(SplashActivity.this);

        }).start();
    }
}
