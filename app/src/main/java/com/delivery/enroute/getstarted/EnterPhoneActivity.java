package com.delivery.enroute.getstarted;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.delivery.enroute.R;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.Dialogs;
import com.hbb20.CountryCodePicker;

import cn.pedant.SweetAlert.SweetAlertDialog;
import spencerstudios.com.bungeelib.Bungee;

public class EnterPhoneActivity extends AppCompatActivity {

    CountryCodePicker ccp;
    EditText phoneNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set_phone_number);

        findViewById(R.id.closeBtn).setOnClickListener(view -> {
            finish();
            Bungee.slideDown(this);
        });

        phoneNumber = findViewById(R.id.etphoneNumber);

        //        initialize countries
        //Country Code
        ccp = findViewById(R.id.countryCode);
        ccp.registerCarrierNumberEditText(phoneNumber);

        findViewById(R.id.btnVerifyPhone).setOnClickListener(view -> {

            if (TextUtils.isEmpty(phoneNumber.getText())){
                phoneNumber.setError("Required");
                return;
            }

            if (String.valueOf(phoneNumber.getText()).trim().length() > 11 || String.valueOf(phoneNumber.getText()).trim().length() < 10){
                phoneNumber.setError("Invalid phone number");
                return;
            }

            String mPhone = String.valueOf(phoneNumber.getText()).trim();
            if (mPhone.startsWith("0")){
                phoneNumber.setText(removeFirstChar(mPhone));
            }

            proceedWithValidation(false);


//            ask user for permission //////////////
//
//            if (!isPermissionGranted()){
//
//                // tell user you'll be reading sms,
//
//                Dialogs.confirmWithSweet(this)
//                .setTitleText("Grant Permission")
//                        .setContentText("The app would want to detect incoming sms for automatic verification of phone number")
//                        .setCancelText("No")
//                        .setCancelClickListener(sDialog -> {
//                            sDialog.dismiss();
//
//                            Toast.makeText(this, "Phone verification will not be automated", Toast.LENGTH_SHORT).show();
//                            proceedWithValidation(false);
//
//                        }).setConfirmClickListener(sweetAlertDialog -> {
//                    sweetAlertDialog.dismiss();
//                    askPermission();
//                }).show();
//
//
//            }else{
//
//            }



        });

    }



    private String removeFirstChar(String s){
        return s.substring(1);
    }



    private void proceedWithValidation(boolean readSmsPermissionGranted) {

        // do this after user gives permission

        String number = ccp.getFullNumberWithPlus();
        Intent intent = new Intent();
        intent.putExtra(Constants.PHONE_AUTH_PHONE_NUMBER, number);
        intent.putExtra(Constants.READ_SMS_PERMISSION_GRANTED, readSmsPermissionGranted);
        setResult(Activity.RESULT_OK, intent);
        finish();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideDown(this);
    }


        private boolean isPermissionGranted(){
            return (ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED)
                    && (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED);
        }

        //    ask permission
        private void askPermission(){
            if ((ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED)

            )

            {
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.RECEIVE_SMS)
                        || ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_SMS)

                ) {

                    Toast.makeText(this, "You need to grant permission before setup proceeds", Toast.LENGTH_SHORT).show();
                    ActivityCompat.requestPermissions(this,new String[]{
                            Manifest.permission.RECEIVE_SMS,
                            Manifest.permission.READ_SMS
                    }, Constants.MY_PERMISSIONS_REQUEST_READ_SMS);
                    Log.d(Constants.TAG, "Permission Being Requested");
                }else{
                    ActivityCompat.requestPermissions(this,new String[]{
                            Manifest.permission.RECEIVE_SMS,
                            Manifest.permission.READ_SMS
                    }, Constants.MY_PERMISSIONS_REQUEST_READ_SMS);
                    Log.d(Constants.TAG, "Permission Being Requested");
                }
                Log.d(Constants.TAG, "Permission not granted");


            }else{
                proceedWithValidation(true);
            }

        }

        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);

            switch (requestCode) {
                case Constants.MY_PERMISSIONS_REQUEST_READ_SMS:
                    if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // permission was granted, yay! Do the
                        // contacts-related task you need to do.

                            proceedWithValidation(true);

                    } else {
                        // permission denied, boo! Disable the
                        // functionality that depends on this permission.
                        proceedWithValidation(false);
                    }
                    break;
            }
        }
}
