package com.delivery.enroute.enums;

import com.fonfon.geohash.BoundingBox;

public enum Collections {
    PICKUPPOINTS, DESTINATIONS, REQUEST, USERS, PAYMENTS;
}
