package com.delivery.enroute.enums;

public enum ViaMapType {
    PICK_UP, DESTINATION
}
