package com.delivery.enroute.enums;

public enum  ContactTypes {
    SENDER_CONTACT, RECIPIENT_CONTACT, PICK_UP_CONTACTS
}
