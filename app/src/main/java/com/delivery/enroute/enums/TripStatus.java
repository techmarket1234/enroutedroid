package com.delivery.enroute.enums;

public enum TripStatus {
    CANCELLED, // customer cancelled the trip
    CANCELLED_R, // rider cancelled the trip
    ONGOING,
    PENDING,
    COMPLETED,
    NOT_FOUND,
    PROCESSING,
    DELAYED
}
