package com.delivery.enroute.enums;

public enum TripTypeCode {
//    MP (multiple pickups) / MD (multiple destinations) / SPSD (single pick up single destination)
    MP, MD, SPSD
}
