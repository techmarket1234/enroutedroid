package com.delivery.enroute.enums;

public enum PlaceCompleteTypes {
    PICK_UP, DESTINATION
}
