package com.delivery.enroute.enums;

public enum PaymentTypes {
    CASH, CARD, MOMO, WALLET
}
