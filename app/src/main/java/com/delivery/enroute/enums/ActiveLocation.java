package com.delivery.enroute.enums;

public enum  ActiveLocation {
    PICK_UP, DESTINATION
}
