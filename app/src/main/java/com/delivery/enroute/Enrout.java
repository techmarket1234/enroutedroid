package com.delivery.enroute;

import android.app.Application;
import android.content.Context;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.delivery.enroute.utils.Constants;
import com.facebook.FacebookSdk;
import com.google.android.libraries.places.api.Places;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


public class Enrout extends MultiDexApplication {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();


        // Create global configuration and initialize ImageLoader with this config
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .build();
        ImageLoader.getInstance().init(config);

//        Initialize google places
        Places.initialize(getApplicationContext(), getResources().getString(R.string.the_google_api_key));


        // OneSignal Initialization
//        OneSignal.startInit(this)
//                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
//                .unsubscribeWhenNotificationsAreDisabled(true)
//                .init();

        //        Initialise sqlite db ........... for more info, check : https://developer.android.com/training/data-storage/room/
//        Constants.appDB = Room.databaseBuilder(getApplicationContext(),
//                        AppDatabase.class, "items").fallbackToDestructiveMigration().build();

    }
}
