package com.delivery.enroute.models;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.firestore.GeoPoint;

import java.io.Serializable;

public class Pos implements Serializable {

    public Pos(){}

    public PosLatLng geopoint;
    public String geohash;

    public static class PosLatLng implements Serializable{

        public PosLatLng(){}
        public PosLatLng(double latitude, double longitude){
            this.latitude = latitude;
            this.longitude = longitude;

        }

       public double longitude;
       public double latitude;
       public String address;
    }
}
