package com.delivery.enroute.models;

public class MyCurrentPlace {
    public String name;
    public String description;
    public double latitude;
    public double longitude;
}
