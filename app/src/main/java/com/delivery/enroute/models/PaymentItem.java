package com.delivery.enroute.models;

import com.delivery.enroute.R;
import com.delivery.enroute.enums.PaymentTypes;

import java.util.ArrayList;

public class PaymentItem {

    public String itemName;
    public int icon;
    public PaymentTypes paymentTypes;

    public PaymentItem(){}

    public PaymentItem(String text, int icon) {
        this.itemName = text;
        this.icon = icon;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getIcon() {
        return icon;
    }

    public PaymentItem setPaymentTypes(PaymentTypes paymentTypes) {
        this.paymentTypes = paymentTypes;
        return this;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public static ArrayList<PaymentItem> getItems() {
        ArrayList<PaymentItem> items = new ArrayList<>();

        items.add(new PaymentItem(PaymentTypes.CASH.toString(), R.drawable.cash));
        items.add(new PaymentItem(PaymentTypes.CARD.toString(),R.drawable.card));
        items.add(new PaymentItem(PaymentTypes.MOMO.toString(), R.drawable.momo));
        items.add(new PaymentItem(PaymentTypes.WALLET.toString(), R.drawable.wallet_blue));
//        items.add(new Item("Other", R.drawable.dropdown));
        return items;
    }

    public static ArrayList<PaymentItem> getNonCashItems(){
        ArrayList<PaymentItem> items = new ArrayList<>();

        items.add(new PaymentItem("MOMO", R.drawable.momo).setPaymentTypes(PaymentTypes.MOMO));
        items.add(new PaymentItem("WALLET", R.drawable.wallet_blue).setPaymentTypes(PaymentTypes.WALLET));
//        items.add(new Item("Other", R.drawable.dropdown));
        return items;
    }
}
