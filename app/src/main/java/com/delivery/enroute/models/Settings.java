package com.delivery.enroute.models;

public class Settings {

    public double commissionOnTripPayment;
    public String faq;
    public String terms_and_conditions;

    public Settings() { }
}
