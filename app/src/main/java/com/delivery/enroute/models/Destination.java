package com.delivery.enroute.models;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.ServerTimestamp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Destination implements Serializable {

    @ServerTimestamp
    public Date deliveredAt;

    public boolean isDelivered;
    public String place;
    public Pos pos;
    public String recipientName;
    public String recipientNumber;

    public String referenceId;
    public String requestId;
    public boolean isArrived;


    public boolean isArrivedNotified;
    public boolean isDeliveredNotified;

    @ServerTimestamp
    public Date dateCreated;

    public String userId;

//    public List<DocumentReference> pickups = new ArrayList<>();

    public transient DocumentReference documentReference;

}
