package com.delivery.enroute.models;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;

import com.github.bassaer.chatmessageview.model.IChatUser;

public class ChatUser implements IChatUser {

    private String name;
    private String id;
    private Bitmap icon;

    public ChatUser(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public Bitmap getIcon() {
        return icon;
    }

    @NonNull
    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setIcon(@NonNull Bitmap bitmap) {
        icon = bitmap;
    }
}
