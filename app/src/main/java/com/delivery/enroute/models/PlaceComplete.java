package com.delivery.enroute.models;

import com.delivery.enroute.R;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.io.Serializable;

public class PlaceComplete{
    public int index;
    public String name;
    public String description;
    public boolean showRemoveBtn;
    public boolean showMoreBtn;
    public int leftIcon = R.drawable.pick_up_icon;
    //public transient LatLng latLng;
    public double latitude;
    public double longitude;
    public String placeId;
    public Marker marker;
    public int markerIcon;
}
