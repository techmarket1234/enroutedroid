package com.delivery.enroute.models;

import android.app.Activity;
import android.content.Intent;

import com.delivery.enroute.R;
import com.delivery.enroute.activities.CheckOutActivity;
import com.delivery.enroute.enums.Collections;
import com.delivery.enroute.enums.TripStatus;
import com.delivery.enroute.enums.TripTypeCode;
import com.delivery.enroute.fragments.trips.CompletedFragment;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.LoadingGif;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ServerTimestamp;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import spencerstudios.com.bungeelib.Bungee;

public class Trip implements Serializable {

    public double actualCost;

    @ServerTimestamp
    public Date arrivedAt;
    public double estimatedCost;
    public boolean isAccepted;
    public boolean isArrived;
    public String paymentMethod = PaymentItem.getItems().get(0).itemName;

    @ServerTimestamp
    public Date requestDatetime;

    public String requestId;
    public String riderId;
    public String riderName;
    public String riderPhoneNumber;
    public String riderPlateNumber;
    public String senderId;
    public String senderName;
    public String senderPhoneNumber;
    public String status = "PENDING";

    public double couponPreviousPrice;

    public transient List<DocumentReference> pickups = new ArrayList<>();
    public transient List<DocumentReference> destinations = new ArrayList<>();

    public List<String> pickupsRef = new ArrayList<>();
    public List<String> destinationsRef = new ArrayList<>();

    public List<String> pickupNames = new ArrayList<>();
    public List<String> destinationNames = new ArrayList<>();

    public List<String> selectedItemCategories = new ArrayList<>();
    public List<String> selectedItemInstructions = new ArrayList<>();

    public double estimatedTimeInSeconds;
    public double estimatedDistanceInMeters;

    public String estimatedTimeInText;
    public String estimatedDistanceInText;

    public double payForMe = 0.0d;
    public boolean hasPaid;
    public String typeCode = TripTypeCode.SPSD.toString(); // MP (multiple pickups) / MD (multiple destinations) / SPSD (single pick up single destination)

    public boolean couponApplied;
    public String  couponName;
    public double couponValueInPercentage;
    public transient DocumentReference  coupon;

    public String riderImage;

    public String orderNumber;
    public double userCurrentLng;
    public double userCurrentLan;

    public String device;

    public Pos pos;

    public CurrentLocation currentLocation;

    public boolean isPayForMe;

    public String fleetId;


    public String scheduleId;

    public int taskCount;

    public int taskCompleted;

    public boolean rated;

    public double rating;

    public String customerType;

    public Trip(){} // default constructor required for serialization

    public TripStatusNotified tripStatusNotified;

    public boolean useBox;

    public boolean isPickFromVendor = false;


    public static void requestAgain(Activity context, Trip trip) {

        LoadingGif.show(context);

        ArrayList<CheckOut> pickPtsCheckOutArrayList = new ArrayList<>();
        ArrayList<CheckOut> destinationsCheckOutArrayList = new ArrayList<>();

        // Toast.makeText(getActivity(), "Request Trip again", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(context, CheckOutActivity.class);
        trip.status = TripStatus.PENDING.toString();
        Constants.TEMPORAL_TRIP_HOLDER = trip;

        boolean isMultiplePickUps = false;
        if (trip.pickups.size() > 1){
            isMultiplePickUps = true;
        }

        trip.isPayForMe = false;
        trip.payForMe = 0.0d;


        intent.putExtra(Constants.IS_MULTIPLE_PICK_UPS, isMultiplePickUps);


        boolean isMultipleDestinations = false;

        if (trip.destinations.size() > 1){
            isMultipleDestinations = true;
        }
        intent.putExtra(Constants.IS_MULTIPLE_DESTINATIONS, isMultipleDestinations);

        getPickUpPointDetails(trip, pickupArrayList1 -> {

            getDetstinationPointDetails(trip, destinationsArrayList1 -> {

                //For pick ups .........

                int k = 0;
                pickPtsCheckOutArrayList.clear();
                for (Pickup pickup : pickupArrayList1) {

                    CheckOut checkOut = new CheckOut();
                    checkOut.index = k;
                    checkOut.description = pickup.place;
                    checkOut.latitude = pickup.pos.geopoint.latitude;
                    checkOut.longitude = pickup.pos.geopoint.longitude;
                    checkOut.leftIcon = getPickUpIcon(k, pickupArrayList1.size() > 1);

                    checkOut.placeId = pickup.place;
                    checkOut.type = Constants.PICK_UP;
                    checkOut.name =  pickup.place;

                    checkOut.itemTitle = "PICK UP LOCATION";

                    checkOut.phone = pickup.pickUpPersonPhone;
                    checkOut.personName = pickup.pickUpName;


                    pickPtsCheckOutArrayList.add(checkOut);

                    k++;
                }

//                            For destinations ..............

                int i = 0;
                destinationsCheckOutArrayList.clear();
                for (Destination destination : destinationsArrayList1){
                    CheckOut checkOut = new CheckOut();
                    checkOut.index = i;
                    checkOut.description = destination.place;
                    checkOut.latitude = destination.pos.geopoint.latitude;
                    checkOut.longitude = destination.pos.geopoint.longitude;
                    checkOut.leftIcon = getDestinationIcon(i, destinationsArrayList1.size() > 1);
                    checkOut.placeId = "";
                    checkOut.type = Constants.DELIVERY;
                    checkOut.name = destination.place;
                    checkOut.itemTitle = "DESTINATION LOCATION";

                    checkOut.personName = destination.recipientName;
                    checkOut.phone = destination.recipientNumber;


                    i++;

                    destinationsCheckOutArrayList.add(checkOut);
                }

                intent.putExtra(Constants.CHECKOUT_PICK_UPS, pickPtsCheckOutArrayList);
                intent.putExtra(Constants.CHECKOUT_DESTINATIONS, destinationsCheckOutArrayList);


                intent.putExtra(Constants.PICK_UPS_ARRAY_LIST, pickupArrayList1);
                intent.putExtra(Constants.DESTINATIONS_ARRAY_LIST, destinationsArrayList1);

                intent.putExtra(Constants.tripInTemporaryHolder, true);

                LoadingGif.hide();

                context.startActivity(intent);
                Bungee.slideLeft(context);


            });
        });

    }

    private static void getPickUpPointDetails(Trip trip, CompletedFragment.OnPickUpPointDetailsReceivedListener onPickUpPointDetailsReceivedListener){
        List<String> pickUpPointReferences = trip.pickupsRef;
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        AtomicInteger p = new AtomicInteger();
        ArrayList<Pickup> pickupArrayList = new ArrayList<>();
        for (String pickupRef : pickUpPointReferences){
            db.collection(Collections.PICKUPPOINTS.toString()).document(pickupRef).get().addOnSuccessListener(documentSnapshot -> {
                if (documentSnapshot == null) return;

                Pickup pickup = documentSnapshot.toObject(Pickup.class);

                if (pickup == null) return;

                pickup.isArrived = false;
                pickup.isPickedup = false;
                pickup.isArrivedNotified = false;
                pickup.isPickedupNotified = false;

                pickupArrayList.add(pickup);

                p.getAndIncrement();

                if (p.get() == pickUpPointReferences.size()){
                    onPickUpPointDetailsReceivedListener.pickUpPointDetailsReceived(pickupArrayList);
                }
            });
        }
    }
    private static void getDetstinationPointDetails(Trip trip, CompletedFragment.OnDestinationPointDetailsReceivedListener onDestinationPointDetailsReceivedListener){
        List<String> destinationsPointReferences = trip.destinationsRef;
        FirebaseFirestore db = FirebaseFirestore.getInstance();

       AtomicInteger d = new AtomicInteger();
        ArrayList<Destination> destinationArrayList = new ArrayList<>();
        for (String destinationRef : destinationsPointReferences){
            db.collection(Collections.DESTINATIONS.toString()).document(destinationRef).get().addOnSuccessListener(documentSnapshot -> {
                if (documentSnapshot == null) return;

                Destination destination = documentSnapshot.toObject(Destination.class);
                if (destination == null) return;

                destination.isDelivered = false;
                destination.isArrived = false;
                destination.isArrivedNotified = false;
                destination.isDeliveredNotified = false;

                destinationArrayList.add(destination);

                d.getAndIncrement();

                if (d.get() == destinationsPointReferences.size()){
                    onDestinationPointDetailsReceivedListener.destinationPointDetailsReceived(destinationArrayList);
                }
            });
        }
    }

    private static int getPickUpIcon(int position, boolean isMultiple) {
        if (!isMultiple){
            return R.drawable.pick_up_icon;
        }

        switch (position){
            case 0:
                return R.drawable.pickup_raw_1;
            case 1:
                return R.drawable.pickup_raw_2;
            case 2:
                return R.drawable.pickup_raw_3;
            case 3:
                return R.drawable.pickup_raw_4;
            case 4:
                return R.drawable.pickup_raw_5;
            case 5:
                return R.drawable.pickup_raw_6;
            default:
                return R.drawable.pickup_raw_1;
        }
    }

    private static int getDestinationIcon(int position, boolean isMultiple) {
        if(!isMultiple){
            return R.drawable.delivery_icon;
        }

        int drawable;
        switch (position){
            case 0:
                // pc.marker.remove();
                drawable = R.drawable.deliver_raw_1;
                break;
            case 1:
                drawable = R.drawable.deliver_raw_2;
                break;
            case 2:
                drawable = R.drawable.deliver_raw_3;
                break;
            case 3:
                drawable = R.drawable.deliver_raw_4;
                break;
            case 4:
                drawable = R.drawable.deliver_raw_5;
                break;
            case 5:
                drawable = R.drawable.deliver_raw_6;
                break;
            default:
                drawable = R.drawable.deliver_raw_1;
                break;
        }

        return drawable;
    }
}
