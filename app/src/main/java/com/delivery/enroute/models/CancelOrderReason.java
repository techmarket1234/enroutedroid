package com.delivery.enroute.models;

import java.util.ArrayList;

public class CancelOrderReason {
    public String text;
    public boolean selected;

    public CancelOrderReason() {
    }

    public CancelOrderReason(String text) {
        this.text = text;
    }

    public static ArrayList<CancelOrderReason> getReasons(){
        ArrayList<CancelOrderReason> reasons = new ArrayList<>();
        reasons.add(new CancelOrderReason("I found a different transport option"));
        reasons.add(new CancelOrderReason("I could not reach my rider"));
        reasons.add(new CancelOrderReason("Rider is too far away"));
        reasons.add(new CancelOrderReason("Pricey fare"));
        reasons.add(new CancelOrderReason("Rider said their bike is broken"));
        reasons.add(new CancelOrderReason("I've waited for too long"));
        reasons.add(new CancelOrderReason("I want to change my pick up location"));
        reasons.add(new CancelOrderReason("Rider doesn't seem to be moving"));

        return reasons;
    }
}
