package com.delivery.enroute.models;



import com.google.firebase.database.Exclude;
import com.google.firebase.database.ServerValue;
import com.google.firebase.firestore.ServerTimestamp;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

public class Chat implements Serializable {

    public Chat() {
    }

    public Chat(String messageText, String riderId, String customerId) {
        this.messageText = messageText;
        this.riderId = riderId;
        this.customerId = customerId;

        createdTimestamp = ServerValue.TIMESTAMP;
    }

    public String messageText;
    public String riderId;
    public String riderName;
    public String riderPhone;
    public String customerId;
    public Object createdTimestamp;
    public String sentBy = "customer";
    public boolean seen; // prevent the notification, every time chat is opened

    @Exclude
    public long getCreatedTimestampLong(){
        return (long)createdTimestamp;
    }
}
