package com.delivery.enroute.models;

import com.google.firebase.firestore.ServerTimestamp;

import java.io.Serializable;
import java.util.Date;

public class Payment implements Serializable {

    public Double amount;
    public String approvedAt;
    @ServerTimestamp
    public Date failedAt;
    public String failedStep;
    public String mobileAuthToken;
    public String momoNumber;
    public String network;
    public String orderId;
    public String status = "PENDING";
    public String token;
    public String senderId;
    public String senderName;
}
