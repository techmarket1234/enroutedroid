package com.delivery.enroute.models;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.ServerTimestamp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Pickup implements Serializable {
    public boolean isPickedup;
    public boolean isArrived;

    @ServerTimestamp
    public Date pickedupAt;
    public String place;
    public String requestId;

    public String itemCategory;
    public String instruction;
    public String pickUpName;
    public String pickUpPersonPhone;

    public boolean isPickedupNotified;
    public boolean isArrivedNotified;

    public String referenceId;
    public String userId;

    @ServerTimestamp
    public Date dateCreated;

    public Pos pos;

//    public List<DocumentReference> destinations = new ArrayList<>();

    public transient DocumentReference documentReference;
}
