package com.delivery.enroute.models;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class UserDriverComment {
    public String userId;
    public String driverId;
    public String comment;

    @ServerTimestamp
    public Date dateCreated;
}
