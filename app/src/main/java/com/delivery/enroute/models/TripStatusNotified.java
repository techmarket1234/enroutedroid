package com.delivery.enroute.models;

import java.io.Serializable;

public class TripStatusNotified implements Serializable {
    public boolean PROCESSING, PENDING, ONGOING, NOT_FOUND, DELAYED, CANCELLED, CANCELLED_R, COMPLETED;
}
