package com.delivery.enroute.models;

import java.util.ArrayList;
import java.util.List;

public class Coupon {

    public String docId;
    public String code;
    public int individualUserLimit;
    public int limit;
    public String name;
    public int totalUsed;
    public int usersAllowed;
    public double value;
    public String status;

    public List<Entry> entriesList = new ArrayList<>();

    public Coupon() { }

    public static class Entry {

        public Entry() { }

        public int usage;
        public String userId;

    }
}
