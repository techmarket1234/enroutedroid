package com.delivery.enroute.models;

import java.util.Date;

public class User {

    public User(){ //required default constructor
         }

    public String id;
    public String fullName; // always available
    public String email;   // available for only users with email and password authentication
    public String phoneNumber; // only available for users with phone verification
    public String authType; // phoneAuth, EmailPassword, OnlyEmail, Facebook, Google

    // all other fields go here
    public String userType;
    public String address;
    public Object dateAdded;
    public Object lastLogin;
    public String gender;
    public String photoUrl;
    public double currentBalance = 0.0d;
    public double outStandingBalance = 0.0d;
    public boolean isOnline;
    public String device = "ANDROID";
    public boolean appReviewed = false;

}
