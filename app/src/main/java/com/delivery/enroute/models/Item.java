package com.delivery.enroute.models;


import com.delivery.enroute.R;
import com.delivery.enroute.enums.MenuItems;

import java.util.ArrayList;
import java.util.List;

public class Item {
    public String itemName;
    public int icon;
    public MenuItems menuItem;
    public int color;
    public boolean changeIconColor;


    public Item(){}

    public Item(String text, int icon) {
        this.itemName = text;
        this.icon = icon;
    }

    public void setColor(int color) {
        this.color = color;
    }

    private Item setMenuItem(MenuItems menuItem) {
        this.menuItem = menuItem;
        return this;
    }

    public String getItemName() {
        return itemName;
    }


    public int getIcon() {
        return icon;
    }

    public void setChangeIconColor(boolean changeIconColor) {
        this.changeIconColor = changeIconColor;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public static ArrayList<Item> getItems() {
        ArrayList<Item> items = new ArrayList<>();

//        Item item = new Item("Food", R.drawable.item_category_icon);
        items.add(new Item("Documents",R.drawable.order_history_icon));
        items.add(new Item("Cakes", R.drawable.cake));
        items.add(new Item("Appliance", R.drawable.appliance));
        items.add(new Item("Books", R.drawable.books));
        items.add(new Item("Groceries", R.drawable.groceries));
        items.add(new Item("Device", R.drawable.device));
        items.add(new Item("Food", R.drawable.food));
        items.add(new Item("Other", R.drawable.others));
//        items.add(new Item("Other", R.drawable.dropdown));
        return items;
    }

    public static ArrayList<Item> getMenuItems(){

        ArrayList<Item> items = new ArrayList<>();
        items.add(new Item("Home", R.drawable.ic_menu_black_24dp).setMenuItem(MenuItems.HOME));
       // items.add(new Item("Share and earn", R.drawable.gift_icon).setMenuItem(MenuItems.SHARE_AND_EARN));
       // items.add(new Item("Courier Type", R.drawable.courier_icon).setMenuItem(MenuItems.COURIER_TYPE));
//        items.add(new Item("Our Vendors", R.drawable.shopping_cart).setMenuItem(MenuItems.VENDORS));
        items.add(new Item("Deliveries", R.drawable.order).setMenuItem(MenuItems.ORDER));
        //items.add(new Item("Promo Codes", R.drawable.promo_icon).setMenuItem(MenuItems.PROMO));
        items.add(new Item("Wallet", R.drawable.wallet_sidebar).setMenuItem(MenuItems.WALLET));
        items.add(new Item("Personal details", R.drawable.pd).setMenuItem(MenuItems.PERSONAL_DETAILS));
        items.add(new Item("F.A.Q", R.drawable.faq).setMenuItem(MenuItems.FAQ));
        items.add(new Item("Contact Us", R.drawable.email).setMenuItem(MenuItems.CONTACT_US));
//        items.add(new Item("Notifications", R.drawable.notification_icon).setMenuItem(MenuItems.NOTIFICATIONS));

        Item logout = new Item("Logout", R.drawable.logout).setMenuItem(MenuItems.LOGOUT);
        logout.setColor(R.color.red);
        logout.setChangeIconColor(true);
        items.add(logout);



        return items;
    }
}