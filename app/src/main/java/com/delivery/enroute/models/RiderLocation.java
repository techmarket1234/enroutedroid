package com.delivery.enroute.models;

import com.google.android.gms.maps.model.LatLng;

public class RiderLocation {
    public LatLng geopoint;
    public String geohash;
    // we'll add any other necessary properties of the rider here later on
    //eg. public double bearing


}
