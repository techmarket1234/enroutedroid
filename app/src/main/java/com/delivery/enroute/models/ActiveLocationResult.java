package com.delivery.enroute.models;

import com.delivery.enroute.enums.ActiveLocation;

import java.util.List;

public class ActiveLocationResult {
    public boolean isDistanceWithinActiveLocation;
    public List<ActivatedLocations> activeLocationList;
}
