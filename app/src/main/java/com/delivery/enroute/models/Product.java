package com.delivery.enroute.models;

import java.io.Serializable;

public class Product implements Serializable {
    public String productId;
    public String productName;
    public String productTitle;
    public String productImage;
    public String productDescription;
    public double productPrice;
}
