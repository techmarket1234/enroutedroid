package com.delivery.enroute.models;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class WalletTransaction {
    public double amount;
    @ServerTimestamp
    public Date createdAt;
    public String narration;
    public transient String network;
    public String accountNumber;

    public String senderId;

    public WalletTransaction(){}
}
