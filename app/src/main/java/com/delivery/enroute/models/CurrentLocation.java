package com.delivery.enroute.models;

import java.io.Serializable;

public class CurrentLocation implements Serializable {
    public double longitude;
    public double latitude;

    public CurrentLocation(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public CurrentLocation(){}
}
