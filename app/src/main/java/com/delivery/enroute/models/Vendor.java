package com.delivery.enroute.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Vendor implements Serializable {
    public String vendorId;
    public String vendorBranchId;
    public String vendorName;
    public Pos.PosLatLng vendorLocation;
    public String vendorPhoneNumber;
    public ArrayList<Product> products;
    public String vendorDescription;
    public String vendorLogo;
    public String vendorBanner;
}
