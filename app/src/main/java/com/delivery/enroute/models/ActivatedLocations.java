package com.delivery.enroute.models;

import com.google.firebase.firestore.GeoPoint;

import java.io.Serializable;

public class ActivatedLocations implements Serializable {

    public String name;
    public double longitude;
    public double latitude;
    public int radiusInMeters;

    public ActivatedLocations(){}
}
