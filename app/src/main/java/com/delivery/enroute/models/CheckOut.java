package com.delivery.enroute.models;

import com.delivery.enroute.R;


import java.io.Serializable;

public class CheckOut implements Serializable {

    public int index;
    public String name;
    public String description;
    public int leftIcon = R.drawable.pick_up_icon;
    //public transient LatLng latLng;
    public double latitude;
    public double longitude;
    public String placeId;

    public String itemTitle;
    public String personName;
    public String phone;

//    public LatLng latLng;

    public String type;
}
