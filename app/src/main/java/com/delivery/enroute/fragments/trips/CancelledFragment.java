package com.delivery.enroute.fragments.trips;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.delivery.enroute.MainActivity;
import com.delivery.enroute.R;
import com.delivery.enroute.adapters.CancelledTipsAdapter;
import com.delivery.enroute.adapters.OnGoingTripsAdapter;
import com.delivery.enroute.enums.Collections;
import com.delivery.enroute.enums.TripStatus;
import com.delivery.enroute.models.Destination;
import com.delivery.enroute.models.Pickup;
import com.delivery.enroute.models.Trip;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.List;

import static com.delivery.enroute.utils.Constants.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class CancelledFragment extends Fragment {

    private ArrayList<Trip> trips = new ArrayList<>();
    private CancelledTipsAdapter adapter = new CancelledTipsAdapter(trips);
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private ArrayList<Pickup> pickupArrayList = new ArrayList<>();
    private ArrayList<Destination> destinationsArrayList = new ArrayList<>();
    private FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private ShimmerFrameLayout mShimmerViewContainer;
    private View no_trip_layout;
    private RecyclerView recyclerView;

    public CancelledFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cancelled, container, false);

        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.setVisibility(View.VISIBLE);

        no_trip_layout = view.findViewById(R.id.no_trip_layout);
        no_trip_layout.setVisibility(View.GONE);

        recyclerView = view.findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        view.findViewById(R.id.request_a_trip).setOnClickListener(view1 -> {
            if (getActivity() == null) return;

            startActivity(new Intent(getActivity(), MainActivity.class));
        });

        // fetch trips from local storage
        //fetchTripsFromStorage();
        new Thread(this::fetchMyCancelledTrips).start();


        return view;
    }


    private void fetchMyCancelledTrips(){

        if (getActivity() == null) return;

        trips.clear();

        recyclerView.post(() -> adapter.notifyDataSetChanged());


        getActivity().runOnUiThread(() -> {
            mShimmerViewContainer.startShimmerAnimation();
        });

        db.collection(Collections.REQUEST.toString()).whereEqualTo("senderId", firebaseUser.getUid())
                .orderBy("requestDatetime", Query.Direction.DESCENDING)
                .get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {

                if (task.getResult() == null) return;

                List<Trip> mTrips = task.getResult().toObjects(Trip.class);

                if (getActivity() == null) return;

                getActivity().runOnUiThread(()->{
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                });
                if (mTrips.isEmpty()) {
                    getActivity().runOnUiThread(() ->  no_trip_layout.setVisibility(View.VISIBLE));
                    return;
                }

                trips.clear();

                for (Trip aTrip: mTrips) {
                    if (aTrip.status.equals(TripStatus.CANCELLED_R.toString())
                            || aTrip.status.equals(TripStatus.CANCELLED.toString())
                    ) {

                        trips.add(aTrip);
                    }
                }

                mTrips.addAll(trips);

                getActivity().runOnUiThread(() -> adapter.notifyDataSetChanged());

            } else {
                Log.w(TAG, "Error getting documents.", task.getException());
            }
        });
    }

}
