package com.delivery.enroute.fragments.trips;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.delivery.enroute.MainActivity;
import com.delivery.enroute.R;
import com.delivery.enroute.adapters.OnGoingTripsAdapter;
import com.delivery.enroute.enums.Collections;
import com.delivery.enroute.enums.TripStatus;
import com.delivery.enroute.models.Destination;
import com.delivery.enroute.models.Pickup;
import com.delivery.enroute.models.Pos;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.utils.Constants;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.annotation.Nullable;

import static com.delivery.enroute.utils.Constants.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class OngoingFragment extends Fragment {


    private ArrayList<Trip> trips = new ArrayList<>();
    private OnGoingTripsAdapter adapter = new OnGoingTripsAdapter(trips);
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private ArrayList<Pickup> pickupArrayList = new ArrayList<>();
    private ArrayList<Destination> destinationsArrayList = new ArrayList<>();
    private FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private ShimmerFrameLayout mShimmerViewContainer;
    private View no_trip_layout;

    public OngoingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ongoing, container, false);


        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.setVisibility(View.VISIBLE);

        no_trip_layout = view.findViewById(R.id.no_trip_layout);
        no_trip_layout.setVisibility(View.GONE);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);


        view.findViewById(R.id.request_a_trip).setOnClickListener(view1 -> {
            if (getActivity() == null) return;

            startActivity(new Intent(getActivity(), MainActivity.class));
        });

        // fetch trips from local storage
        //fetchTripsFromStorage();

       new Thread(this::fetchMyOngoingTrips).run();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        //fetchMyOngoingTrips();

//        db.collection(Collections.REQUEST.toString()).whereEqualTo("senderId",firebaseUser.getUid())
//                .addSnapshotListener((queryDocumentSnapshots, e) -> {
//
//                    if (queryDocumentSnapshots == null) return;
//
//
//
//                    List<Trip> mTrips = queryDocumentSnapshots.toObjects(Trip.class);
//
//
//                    if (mTrips.isEmpty()) {
//                        no_trip_layout.setVisibility(View.VISIBLE);
//                        return;
//                    }
//
//                    trips.clear();
//
//                    for (Trip aTrip: mTrips) {
//                        if (aTrip.status.equals(TripStatus.PENDING.toString())
//                                || aTrip.status.equals(TripStatus.NOT_FOUND.toString())
//                                || aTrip.status.equals(TripStatus.ONGOING.toString())
//                                || aTrip.status.equals(TripStatus.PROCESSING.toString())
//                        ) {
//
//                            trips.add(aTrip);
//                        }
//                    }
//
//                    adapter.notifyDataSetChanged();
//
//                });
    }

    //    private void fetchTripsFromStorage() {
//
//        trips.add(getADummyTrip());
//
//
//    }

    private void fetchMyOngoingTrips(){

        if (getActivity() == null)return;

        trips.clear();
        adapter.notifyDataSetChanged();

       getActivity().runOnUiThread(() -> {
           mShimmerViewContainer.startShimmerAnimation();
       });

        ///

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        Date yesterday = cal.getTime();

        Date tomorrow = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(tomorrow);
        c.add(Calendar.DATE, 1);
        tomorrow = c.getTime();

        db.collection(Collections.REQUEST.toString()).whereEqualTo("senderId", firebaseUser.getUid())
                .orderBy("requestDatetime", Query.Direction.DESCENDING)
                .whereGreaterThan("requestDatetime", yesterday)
                .whereLessThan("requestDatetime", tomorrow)
                .get().addOnCompleteListener(task -> {

                    if (task.isSuccessful()) {

                        if (task.getResult() == null) return;

                        List<Trip> mTrips = task.getResult().toObjects(Trip.class);

                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);

                        if (mTrips.isEmpty()) {
                            no_trip_layout.setVisibility(View.VISIBLE);
                            return;
                        }

                        trips.clear();

                        for (Trip aTrip: mTrips) {

                            if (aTrip.status.equals(TripStatus.PENDING.toString())
                                    || aTrip.status.equals(TripStatus.NOT_FOUND.toString())
                                    || aTrip.status.equals(TripStatus.ONGOING.toString())
                                    || aTrip.status.equals(TripStatus.PROCESSING.toString())
                                    || aTrip.status.equals(TripStatus.DELAYED.toString())
                            ) {

                                trips.add(aTrip);
                            }
                        }

                        if (getActivity() == null) return;

                        getActivity().runOnUiThread(() -> {
                            adapter.notifyDataSetChanged();
                        });

                    } else {
                        Log.w(TAG, "Error getting documents.", task.getException());
                    }
                });


    }



}
