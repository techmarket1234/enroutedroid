package com.delivery.enroute.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.delivery.enroute.R;
import com.delivery.enroute.enums.Collections;
import com.delivery.enroute.enums.TripStatus;
import com.delivery.enroute.fragments.trips.CancelledFragment;
import com.delivery.enroute.fragments.trips.CompletedFragment;
import com.delivery.enroute.fragments.trips.OngoingFragment;
import com.delivery.enroute.listeners.UnPaidTripListener;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.utils.Constants;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

import static com.delivery.enroute.utils.Constants.TAG;

public class TripsFragment extends Fragment {

    private Fragment [] fragments;

    private FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

    public TripsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_trips, container, false);

        CompletedFragment completedFragment = new CompletedFragment();
        CancelledFragment cancelledFragment = new CancelledFragment();
        OngoingFragment ongoingFragment = new OngoingFragment();

        fragments = new Fragment[] {ongoingFragment, cancelledFragment, completedFragment};

        if (getActivity() != null)
            getActivity().setTitle("Deliveries");

        ViewPager pager = view.findViewById(R.id.payment_pager);
        pager.setAdapter(new MyPagerAdapter(getChildFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT));


        if (getActivity() != null)
            Constants.checkOutstandingPayment(getActivity(), new UnPaidTripListener() {
                @Override
                public void unpaidTrip(Trip trip, boolean allowUserToRequestAnotherTrip) {

                }

                @Override
                public void notFound() {

                }
            });

        return view;
    }


    public class MyPagerAdapter extends FragmentStatePagerAdapter {


        MyPagerAdapter(@NonNull FragmentManager fm, int behavior) {
            super(fm, behavior);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments[position];
        }

        @Override
        public int getCount() {
            return fragments.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0:
                    return "ON GOING";
                case 1:
                    return "CANCELLED";
                case 2:
                    return "COMPLETED";
                default:
                    return null;
            }
        }
    }

}
