package com.delivery.enroute.fragments.trips;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.delivery.enroute.MainActivity;
import com.delivery.enroute.R;
import com.delivery.enroute.activities.CheckOutActivity;
import com.delivery.enroute.adapters.CompletedTripsAdapter;
import com.delivery.enroute.adapters.OnGoingTripsAdapter;
import com.delivery.enroute.enums.Collections;
import com.delivery.enroute.enums.TripStatus;
import com.delivery.enroute.listeners.OnRedoRequestTrip;
import com.delivery.enroute.models.CheckOut;
import com.delivery.enroute.models.Destination;
import com.delivery.enroute.models.Pickup;
import com.delivery.enroute.models.PlaceComplete;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.LoadingGif;
import com.delivery.enroute.utils.TutorialManager;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import me.toptas.fancyshowcase.FancyShowCaseView;
import spencerstudios.com.bungeelib.Bungee;

import static com.delivery.enroute.utils.Constants.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class CompletedFragment extends Fragment{

    private ArrayList<Trip> trips = new ArrayList<>();
    private CompletedTripsAdapter adapter;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
//    private ArrayList<Pickup> pickupArrayList = new ArrayList<>();
//    private ArrayList<Destination> destinationsArrayList = new ArrayList<>();
    private FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private ShimmerFrameLayout mShimmerViewContainer;
    private View no_trip_layout;
    private int p = 0;
    private int d = 0;

    private ArrayList<CheckOut> pickPtsCheckOutArrayList = new ArrayList<>();
    private ArrayList<CheckOut> destinationsCheckOutArrayList = new ArrayList<>();

    private LinearLayoutManager mLinearLayoutManager;
    private RecyclerView recyclerView;


    public CompletedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_completed, container, false);

        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.setVisibility(View.VISIBLE);

        no_trip_layout = view.findViewById(R.id.no_trip_layout);
        no_trip_layout.setVisibility(View.GONE);

        adapter = new CompletedTripsAdapter(trips, trip -> {

            LoadingGif.show(getActivity());

            // Toast.makeText(getActivity(), "Request Trip again", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(getActivity(), CheckOutActivity.class);

            trip.status = TripStatus.PENDING.toString();
            Constants.TEMPORAL_TRIP_HOLDER = trip;

            boolean isMultiplePickUps = false;
            if (trip.pickups.size() > 1){
                isMultiplePickUps = true;
            }

            trip.isPayForMe = false;
            trip.payForMe = 0.0d;


            intent.putExtra(Constants.IS_MULTIPLE_PICK_UPS, isMultiplePickUps);

            boolean isMultipleDestinations = false;

            if (trip.destinations.size() > 1){
                isMultipleDestinations = true;
            }
            intent.putExtra(Constants.IS_MULTIPLE_DESTINATIONS, isMultipleDestinations);




            // get corresponding pick ups


            getPickUpPointDetails(trip, pickupArrayList1 -> {
                // get corresponding destinations
                getDetstinationPointDetails(trip, destinationsArrayList1 -> {


                    //For pick ups .........

                    int k = 0;
                    pickPtsCheckOutArrayList.clear();
                    for (Pickup pickup : pickupArrayList1) {

                        CheckOut checkOut = new CheckOut();
                        checkOut.index = k;
                        checkOut.description = pickup.place;
                        checkOut.latitude = pickup.pos.geopoint.latitude;
                        checkOut.longitude = pickup.pos.geopoint.longitude;
                        checkOut.leftIcon = getPickUpIcon(k, pickupArrayList1.size() > 1);

                        checkOut.placeId = pickup.place;
                        checkOut.type = Constants.PICK_UP;
                        checkOut.name =  pickup.place;

                        checkOut.itemTitle = "PICK UP LOCATION";

                        checkOut.phone = pickup.pickUpPersonPhone;
                        checkOut.personName = pickup.pickUpName;


                        pickPtsCheckOutArrayList.add(checkOut);

                        k++;
                    }

//                            For destinations ..............

                    int i = 0;
                    destinationsCheckOutArrayList.clear();
                    for (Destination destination : destinationsArrayList1){
                        CheckOut checkOut = new CheckOut();
                        checkOut.index = i;
                        checkOut.description = destination.place;
                        checkOut.latitude = destination.pos.geopoint.latitude;
                        checkOut.longitude = destination.pos.geopoint.longitude;
                        checkOut.leftIcon = getDestinationIcon(i, destinationsArrayList1.size() > 1);
                        checkOut.placeId = "";
                        checkOut.type = Constants.DELIVERY;
                        checkOut.name = destination.place;
                        checkOut.itemTitle = "DESTINATION LOCATION";

                        checkOut.personName = destination.recipientName;
                        checkOut.phone = destination.recipientNumber;


                        i++;

                        destinationsCheckOutArrayList.add(checkOut);
                    }

                    intent.putExtra(Constants.CHECKOUT_PICK_UPS, pickPtsCheckOutArrayList);
                    intent.putExtra(Constants.CHECKOUT_DESTINATIONS, destinationsCheckOutArrayList);


                    intent.putExtra(Constants.PICK_UPS_ARRAY_LIST, pickupArrayList1);
                    intent.putExtra(Constants.DESTINATIONS_ARRAY_LIST, destinationsArrayList1);

                    intent.putExtra(Constants.tripInTemporaryHolder, true);

                    LoadingGif.hide();

                    startActivity(intent);
                    Bungee.slideLeft(getActivity());

                });

            });

        });

        recyclerView = view.findViewById(R.id.recyclerView);

        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLinearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

//        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new CompletedTripsAdapter.SwipeToTakeActionCallback(0, ItemTouchHelper.LEFT, this);
//        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);
//

        view.findViewById(R.id.request_a_trip).setOnClickListener(view1 -> {
            if (getActivity() == null) return;

            startActivity(new Intent(getActivity(), MainActivity.class));
        });

        // fetch trips from local storage
        //fetchTripsFromStorage();

        new Thread(this::fetchMyCompletedTrips).start();


        return view;
    }

    private int getPickUpIcon(int position, boolean isMultiple) {
        if (!isMultiple){
            return R.drawable.pick_up_icon;
        }

        switch (position){
            case 0:
                return R.drawable.pickup_raw_1;
            case 1:
               return R.drawable.pickup_raw_2;
            case 2:
               return R.drawable.pickup_raw_3;
            case 3:
               return R.drawable.pickup_raw_4;
            case 4:
               return R.drawable.pickup_raw_5;
            case 5:
               return R.drawable.pickup_raw_6;
            default:
               return R.drawable.pickup_raw_1;
        }
    }

    private int getDestinationIcon(int position, boolean isMultiple) {
        if(!isMultiple){
            return R.drawable.delivery_icon;
        }

        int drawable;
        switch (position){
            case 0:
                // pc.marker.remove();
                drawable = R.drawable.deliver_raw_1;
                break;
            case 1:
                drawable = R.drawable.deliver_raw_2;
                break;
            case 2:
                drawable = R.drawable.deliver_raw_3;
                break;
            case 3:
                drawable = R.drawable.deliver_raw_4;
                break;
            case 4:
                drawable = R.drawable.deliver_raw_5;
                break;
            case 5:
                drawable = R.drawable.deliver_raw_6;
                break;
            default:
                drawable = R.drawable.deliver_raw_1;
                break;
        }

        return drawable;
    }

    private void getPickUpPointDetails(Trip trip, OnPickUpPointDetailsReceivedListener onPickUpPointDetailsReceivedListener){
        List<String> pickUpPointReferences = trip.pickupsRef;
        p = 0;
        ArrayList<Pickup> pickupArrayList = new ArrayList<>();
        for (String pickupRef : pickUpPointReferences){
            db.collection(Collections.PICKUPPOINTS.toString()).document(pickupRef).get().addOnSuccessListener(documentSnapshot -> {
               if (documentSnapshot == null) return;

                Pickup pickup = documentSnapshot.toObject(Pickup.class);

                if (pickup == null) return;

                pickup.isArrived = false;
                pickup.isPickedup = false;
                pickup.isArrivedNotified = false;
                pickup.isPickedupNotified = false;

                pickupArrayList.add(pickup);

                p++;

                if (p == pickUpPointReferences.size()){
                    onPickUpPointDetailsReceivedListener.pickUpPointDetailsReceived(pickupArrayList);
                }
            });
        }
    }
    private void getDetstinationPointDetails(Trip trip, OnDestinationPointDetailsReceivedListener onDestinationPointDetailsReceivedListener){
        List<String> destinationsPointReferences = trip.destinationsRef;

        d = 0;
        ArrayList<Destination> destinationArrayList = new ArrayList<>();
        for (String destinationRef : destinationsPointReferences){
            db.collection(Collections.DESTINATIONS.toString()).document(destinationRef).get().addOnSuccessListener(documentSnapshot -> {
                if (documentSnapshot == null) return;

                Destination destination = documentSnapshot.toObject(Destination.class);
                if (destination == null) return;

                destination.isDelivered = false;
                destination.isArrived = false;
                destination.isArrivedNotified = false;
                destination.isDeliveredNotified = false;

                destinationArrayList.add(destination);

                d++;

                if (d == destinationsPointReferences.size()){
                    onDestinationPointDetailsReceivedListener.destinationPointDetailsReceived(destinationArrayList);
                }
            });
        }
    }


    public interface OnPickUpPointDetailsReceivedListener{
        void pickUpPointDetailsReceived(ArrayList<Pickup> pickupArrayList);
    }

    public interface OnDestinationPointDetailsReceivedListener{
        void destinationPointDetailsReceived(ArrayList<Destination> destinationsArrayList);
    }

    private void fetchMyCompletedTrips(){

        if (getActivity() == null) return;

        trips.clear();
        getActivity().runOnUiThread(() -> {
            adapter.notifyDataSetChanged();
        });

        getActivity().runOnUiThread(() -> {
            mShimmerViewContainer.startShimmerAnimation();
        });

        db.collection(Collections.REQUEST.toString()).whereEqualTo("senderId", firebaseUser.getUid())
                .whereEqualTo("status", TripStatus.COMPLETED.toString())
                .orderBy("requestDatetime", Query.Direction.DESCENDING)
                .get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {

                if (task.getResult() == null) return;

                List<Trip> mTrips = task.getResult().toObjects(Trip.class);

                if(getActivity() == null) return;

               getActivity().runOnUiThread(()->{
                   mShimmerViewContainer.stopShimmerAnimation();
                   mShimmerViewContainer.setVisibility(View.GONE);
               });


                trips.addAll(mTrips);

                if (mTrips.isEmpty()) {
                   getActivity().runOnUiThread(() ->  no_trip_layout.setVisibility(View.VISIBLE));
                   return;
                }

                mTrips.addAll(trips);
                getActivity().runOnUiThread(() -> {
                    adapter.notifyDataSetChanged();
                    new Handler().postDelayed(() -> {

                        if (isVisible()){
                            if (recyclerView != null && getActivity() != null){
                                boolean shouldShowTutorial = TutorialManager.getInstance().showTutorial(getActivity(), recyclerView.getId());
                                if (shouldShowTutorial){
                                    new FancyShowCaseView.Builder(getActivity())
                                            .closeOnTouch(true)
                                            .titleGravity(Gravity.TOP)
                                            .customView(R.layout.swipe_to_request_layout, null)
                                            .enableAutoTextPosition()
                                            .build()
                                            .show();
                                }
                            }
                        }

                    }, 1000);

                });


            } else {
                Log.w(TAG, "Error getting documents.", task.getException());
            }
        });
    }


}
