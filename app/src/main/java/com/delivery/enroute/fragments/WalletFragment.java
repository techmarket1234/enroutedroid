package com.delivery.enroute.fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.delivery.enroute.R;
import com.delivery.enroute.activities.wallet.PayWithMomoActivity;
import com.delivery.enroute.activities.wallet.WalletTransactionHistoryActivity;
import com.delivery.enroute.enums.Collections;
import com.delivery.enroute.models.User;
import com.delivery.enroute.singletons.ApplicaitonUser;
import com.delivery.enroute.utils.Constants;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import spencerstudios.com.bungeelib.Bungee;


public class WalletFragment extends Fragment {

    private ApplicaitonUser applicaitonUser;
    private TextView noFundsLabel;
    private TextView balanceTv;
    private MaterialProgressBar progressBar;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private FirebaseUser firebaseUser;


    public WalletFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_wallet, container, false);

        progressBar = view.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
        noFundsLabel = view.findViewById(R.id.no_funds_label);
        balanceTv =  view.findViewById(R.id.balance);

        try {
            applicaitonUser = ApplicaitonUser.getInstance(getActivity());
            firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

            if (getActivity() != null){
                getActivity().setTitle("WALLET");

                if (firebaseUser != null){
                    Log.d(Constants.TAG, "onCreateView: " + firebaseUser.getUid());
                }

            }



//        add funds clicked
            view.findViewById(R.id.addFundsBtn).setOnClickListener(v -> {
                if (getActivity() != null){
                    startActivity(new Intent(getActivity(), PayWithMomoActivity.class));
                    Bungee.slideLeft(getActivity());
                }

            });



            Button viewTransactionHistory = view.findViewById(R.id.view_trans_btn);
            viewTransactionHistory.setOnClickListener(view1 -> {
                if (getActivity() != null){
                    startActivity(new Intent(getActivity(), WalletTransactionHistoryActivity.class));
                    Bungee.slideLeft(getActivity());
                }
            });


        }catch (Exception e){
            Log.d(Constants.TAG, "onCreateView: " + e.getMessage());
            Constants.openEmail(getActivity(), "APP CRUSHED", e.getMessage());
        }
       return view;

    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() != null){
            progressBar.setVisibility(View.VISIBLE);
            if (firebaseUser != null){
                db.collection(Collections.USERS.toString()).document(firebaseUser.getUid()).get().addOnSuccessListener(documentSnapshot -> {
                    progressBar.setVisibility(View.GONE);
                    if (documentSnapshot == null) return;
                    User user = documentSnapshot.toObject(User.class);
                    if (user == null) return;

                    if (getActivity() != null){
                        balanceTv.setText(getString(R.string.amount, String.valueOf(Constants.toWholeNumber(user.currentBalance))));

                        if (applicaitonUser != null){
                            applicaitonUser.currentBalance = Constants.toWholeNumber(user.currentBalance);

                            if (applicaitonUser.currentBalance <= 0.00){
                                showNoFunds(true);
                            }else{
                                showNoFunds(false);
                            }
                        }
                    }

                }).addOnFailureListener(e -> {
                    progressBar.setVisibility(View.GONE);
                });
            }
        }



    }

    private void showNoFunds(boolean isVisble){
        if (isVisble){
            noFundsLabel.setVisibility(View.VISIBLE);
        }else{
            noFundsLabel.setVisibility(View.GONE);
        }
    }

}
