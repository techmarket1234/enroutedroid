package com.delivery.enroute.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.delivery.enroute.R;
import com.delivery.enroute.singletons.ApplicaitonUser;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.Dialogs;
import com.delivery.enroute.utils.LoadingGif;
import com.delivery.enroute.webservices.enums.Collections;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public class ProfileFragment extends Fragment {

    private static final int SELECT_IMAGE = 2;
    private EditText username, phoneNumber, email;
    private RadioGroup radioSexGroup;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageRef = storage.getReference();
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private ImageView avatar;
    private View mainView;
    private ApplicaitonUser applicaitonUser;
    private FirebaseUser firebaseUser;
    private boolean isPhotoSet;
    private Uri mSelectedImageUri;
    private Button clearButton;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull  LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);


        if (getActivity() != null){
            getActivity().setTitle("Account Settings");
            applicaitonUser = ApplicaitonUser.getInstance(getActivity());
            firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        }


        TextView personName = view.findViewById(R.id.person_name);
         username = view.findViewById(R.id.username);
         phoneNumber = view.findViewById(R.id.phone_number);
         email = view.findViewById(R.id.email);
         radioSexGroup = view.findViewById(R.id.radioSex);
         clearButton = view.findViewById(R.id.clear_photo);
         clearButton.setVisibility(View.GONE);
         clearButton.setOnClickListener(clearPhoto);
        ProgressBar uploadImagePb = view.findViewById(R.id.uploadImage);
         uploadImagePb.setVisibility(View.GONE);

         RadioButton radioButtonMale = view.findViewById(R.id.radioMale);
         RadioButton radioButtonFemale = view.findViewById(R.id.radioFemale);

         avatar = view.findViewById(R.id.avatar);
         avatar.setOnClickListener(view1 -> {
             Intent intent = new Intent();
             intent.setType("image/*");
             intent.setAction(Intent.ACTION_GET_CONTENT);
             startActivityForResult(Intent.createChooser(intent, "Select Picture"),SELECT_IMAGE);
         });

         mainView = view;

         view.findViewById(R.id.updateDetails).setOnClickListener(updateProfile);

         if (getActivity() != null) {
             personName.setText(getActivity().getResources().getString(R.string.hi_username, applicaitonUser.fullName));
             username.setText(applicaitonUser.fullName);
             phoneNumber.setText(applicaitonUser.phoneNumber);
             email.setText(applicaitonUser.email);

             try {
                 imageLoader.displayImage(String.valueOf(applicaitonUser.photoUrl), avatar);
             }catch (Exception e){
                 Log.d(Constants.TAG, "onCreateView: " + e.getMessage());
             }

             if (applicaitonUser.gender != null){
                 if (applicaitonUser.gender.equals("Male")){
                     radioButtonMale.setChecked(true);
                 }else{
                     radioButtonFemale.setChecked(true);
                 }
             }
         }


        setPhoto();

        return view;
    }


    private View.OnClickListener updateProfile = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (TextUtils.isEmpty(username.getText())){
                Toast.makeText(getContext(), "Username field is required", Toast.LENGTH_SHORT).show();
                return;
            }

            if (TextUtils.isEmpty(phoneNumber.getText())){
                Toast.makeText(getContext(), "Your phone number required", Toast.LENGTH_SHORT).show();
                return;
            }


            // get selected radio button from radioGroup
            int selectedId = radioSexGroup.getCheckedRadioButtonId();
            if (selectedId == -1){
                Toast.makeText(getContext(), "Kindly select your gender", Toast.LENGTH_SHORT).show();
                return;
            }


            if (getContext() == null) return;

            RadioButton genderButton = mainView.findViewById(selectedId);
            String gender =  String.valueOf(genderButton.getText());

            String name = username.getText().toString();
            String phone  = phoneNumber.getText().toString();
            String uuid = firebaseUser.getUid();
            String emailtxt = email.getText().toString();

            Map<String, Object> data = new HashMap<>();
            data.put("email", emailtxt);
            data.put("fullName", name);
            data.put("phoneNumber", phone);
            data.put("gender", gender);

            LoadingGif.show(getActivity());

            db.collection("USERS").document(uuid).update(data).addOnSuccessListener(aVoid -> {

                LoadingGif.hide();

                setUserDisplayName(firebaseUser, name);

                ApplicaitonUser applicaitonUser = ApplicaitonUser.getInstance(getActivity());
                applicaitonUser.phoneNumber = phone;
                applicaitonUser.email = emailtxt;
                applicaitonUser.id = uuid;
                applicaitonUser.fullName = name;
                applicaitonUser.gender = gender;
                applicaitonUser.setAppUser(getActivity(),applicaitonUser);


                if(isPhotoSet && mSelectedImageUri != null && getActivity() != null){
                    uploadPic();
                }

                if (getActivity() != null){
                    Toast.makeText(getActivity(), "Profile updated", Toast.LENGTH_SHORT).show();
                }


            }).addOnFailureListener(e -> {
                LoadingGif.hide();
                Log.d(Constants.TAG, "profile update error: " + e.getMessage());
                Dialogs.alertWithSweet(getContext(), "Unable to update your profile." + e.getMessage());
            });

        }
    };

    private void setUserDisplayName(final FirebaseUser user,final String name){
        new Thread(() -> {
            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(name).build();
            firebaseUser.updateProfile(profileUpdates);
        }).start();
    }

    private void setUserPhotoUrl(final FirebaseUser user,final Uri url){
        new Thread(() -> {
            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setPhotoUri(url).build();
            firebaseUser.updateProfile(profileUpdates);
        }).start();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SELECT_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    try {
                        if (getActivity() == null)return;
                        isPhotoSet = true;
                        mSelectedImageUri = data.getData();
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                        avatar.setImageBitmap(bitmap);
                        clearButton.setVisibility(View.VISIBLE);

                        //todo upload to server

                        //setUserPhotoUrl();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED)  {
                Toast.makeText(getActivity(), "Canceled", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private View.OnClickListener clearPhoto = view -> {

        if (getActivity() == null) return;

        if (isPhotoSet){
            avatar.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.avatar2));
            clearButton.setVisibility(View.GONE);
            isPhotoSet = false;
            mSelectedImageUri = null;
        }


    };



    private void uploadPic(){


       if (getActivity() != null){
           //setUserPhotoUrl(firebaseUser, url);
           StorageReference imagesRef = storageRef.child("customerImages/" + firebaseUser.getUid());
           UploadTask uploadTask = imagesRef.putFile(mSelectedImageUri);

           LoadingGif.show(getActivity());
           uploadTask.continueWithTask(task -> {
               if (!task.isSuccessful()) {
                   throw Objects.requireNonNull(task.getException());
               }

               // Continue with the task to get the download URL
               return imagesRef.getDownloadUrl();
           }).addOnCompleteListener(task -> {
               LoadingGif.hide();

               if (task.isSuccessful()) {
                   Uri downloadUri = task.getResult();
                   Log.d(Constants.TAG, "uploadPic: downloadUri " + downloadUri);
                   setUserPhotoUrl(firebaseUser, downloadUri);

               } else {

                   Dialogs.alertWithSweet(getActivity(), "Please try uploading image again");
                   // Handle failures
                   // ...
               }
           });
       }

    }

    private void setPhoto(){
        if (getActivity() != null){
            new Thread(() ->{

                storageRef.child("customerImages/" + firebaseUser.getUid()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        // Got the download URL for 'users/me/profile.png'
                        Log.d(Constants.TAG, "onSuccess: uri " + uri);
                        if (uri == null) return;
                       if (getActivity() != null){
                           getActivity().runOnUiThread(() -> {
                               imageLoader.loadImage(String.valueOf(uri), new SimpleImageLoadingListener() {
                                   @Override
                                   public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                       // Do whatever you want with Bitmap
                                       if (loadedImage != null){
                                           avatar.setImageBitmap(loadedImage);
                                       }
                                   }
                               });
                           });
                       }

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle any errors
                    }
                });
            }).start();
        }
    }

//      LoadingGif.hide();
//                Dialogs.alertWithSweet(getActivity(), exception.getMessage());
//    @Override
//    public void onAttach(@NonNull Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }


}
