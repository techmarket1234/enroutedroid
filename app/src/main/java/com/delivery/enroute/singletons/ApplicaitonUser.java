package com.delivery.enroute.singletons;

// ref: https://medium.com/@kevalpatel2106/how-to-make-the-perfect-singleton-de6b951dfdb0

import android.content.Context;
import android.content.SharedPreferences;

import com.delivery.enroute.models.User;
import com.delivery.enroute.utils.Constants;
import com.google.gson.Gson;

import java.io.Serializable;

public class ApplicaitonUser extends User implements Serializable {

    private static volatile ApplicaitonUser appUser;


    private ApplicaitonUser(){
        //Prevent form the reflection api.
        if (appUser != null){
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    // synchronized mean that any thread calling the method will have to wait for other threads calling it to finish
    public static ApplicaitonUser getInstance(Context context){

        //Double check locking pattern
        if (appUser == null) {

            //Check for the first time

            synchronized (ApplicaitonUser.class) {   //Check for the second time.
                //if there is no instance available... create new one
                if (appUser == null) {

                    SharedPreferences sharedPreferences =  context.getSharedPreferences(Constants.APP, Context.MODE_PRIVATE);
                    String userString = sharedPreferences.getString(Constants.USER, null);

                    if (userString == null)
                        appUser = new ApplicaitonUser();
                    else
                        appUser = new Gson().fromJson(userString, ApplicaitonUser.class);
                }
            }
        }

        return appUser;
    }

    public void setAppUser(Context context, User user){

        fullName = user.fullName;
        email = user.email;
        phoneNumber = user.phoneNumber;
        authType = user.authType;
        userType = user.userType;
        address = user.address;
        dateAdded = user.dateAdded;
        lastLogin = user.lastLogin;
        gender = user.gender;
        photoUrl = user.photoUrl;
        currentBalance = user.currentBalance;
        outStandingBalance = user.outStandingBalance;
        id = user.id;
        device = "ANDROID";
        appReviewed = user.appReviewed;

        appUser = this;

        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.APP, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(Constants.USER, new Gson().toJson(this)).apply();

    }
}
