package com.delivery.enroute.singletons;

import android.content.Context;
import android.util.Log;

import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.utils.volley.HttpsTrustManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.transports.Polling;
import io.socket.engineio.client.transports.WebSocket;
import okhttp3.OkHttpClient;

public class SockIOConnection {

    private static SockIOConnection connection;

    private Socket mSocket;
    {
        try {
            IO.Options options = new IO.Options();

            OkHttpClient okHttpClient = null;
            try {
                okHttpClient = new OkHttpClient.Builder()
                        .hostnameVerifier(HttpsTrustManager.getHostnameVerifier())
                        .sslSocketFactory(HttpsTrustManager.getSSLSocketFactory())
                        .build();
            } catch (CertificateException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }

// default settings for all sockets
            IO.setDefaultOkHttpWebSocketFactory(okHttpClient);
            IO.setDefaultOkHttpCallFactory(okHttpClient);

            options.callFactory = okHttpClient;
            options.webSocketFactory = okHttpClient;

            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            if (firebaseUser != null){
                options.query = "customerId=" + firebaseUser.getUid() + "&customerName=" + firebaseUser.getDisplayName() + "&type=CUSTOMER";
//            mSocket = IO.socket("https://44b5e4ad.ngrok.io",options);
                options.transports = new String[] { WebSocket.NAME, Polling.NAME };
                mSocket = IO.socket(Constants.BaseUrl,options);
//            mSocket = IO.socket("https://realtime.elcentino.com",options);
            }

        } catch (URISyntaxException e) {
            Log.d(Constants.TAG, "instance initializer: URISyntaxException" + e.getMessage());
        }
    }


    public static Socket getSocketInstance(){

        if (connection == null){
            connection = new SockIOConnection();
        }

        Socket socket = connection.mSocket;



        if (socket != null){
            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d(Constants.TAG, "getSocketInstance: socket connected ");
                }

            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d(Constants.TAG, "getSocketInstance: socket disconnected ");

                }
            }).on("error", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d(Constants.TAG, "getSocketInstance: socket Error  " + args[0]);
                }
            });

            if (!socket.connected()){
                socket.connect();
            }
        }


        return socket;
    }


//    private Socket getmSocket(Context context){
//        try {
//            IO.Options options = new IO.Options();
//            ApplicaitonUser applicaitonUser = ApplicaitonUser.getInstance(context);
//            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//            if (firebaseUser != null){
//                options.query = "customerId=" + firebaseUser.getUid() + "&customerName=" +firebaseUser.getDisplayName() + "&type=CUSTOMER";
//                Log.d(Constants.TAG, "getmSocket: firebaseUser " + options.query);
//                return IO.socket(Constants.BaseUrl,options);
////            mSocket = IO.socket("https://realtime.elcentino.com",options);
//            }
////            if (applicaitonUser != null){
////                options.query = "customerId=" + applicaitonUser.id + "&customerName=" + applicaitonUser.fullName + "&type=CUSTOMER";
////                Log.d(Constants.TAG, "getmSocket: applicaitonUser " + options.query);
//////            mSocket = IO.socket("https://44b5e4ad.ngrok.io",options);
////                return IO.socket(Constants.BaseUrl,options);
//////            mSocket = IO.socket("https://realtime.elcentino.com",options);
////            }else
//
//        } catch (URISyntaxException e) {
//            Log.d(Constants.TAG, "instance initializer: URISyntaxException" + e.getMessage());
//        }
//        return null;
//    }


    private SockIOConnection(){}

//    public static Socket getSocketInstance(Context context){
//
//        if (connection == null){
//            connection = new SockIOConnection();
//        }
//
//        Socket mSocket = connection.getmSocket(context);
//
//        if (mSocket == null){
//            Log.d(Constants.TAG, "attempting to instantiate socket again ..... ");
//           return getSocketInstance(context);
//        }
//
//        mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                Log.d(Constants.TAG, "getSocketInstance: socket connected ");
//            }
//
//        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                Log.d(Constants.TAG, "getSocketInstance: socket disconnected ");
//
//            }
//        }).on("error", new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                Log.d(Constants.TAG, "getSocketInstance: socket Error  " + args[0]);
//            }
//        });
//
//
//        if (!mSocket.connected()){
//            mSocket.connect();
//        }
//
//        return mSocket;
//
//    }

}
