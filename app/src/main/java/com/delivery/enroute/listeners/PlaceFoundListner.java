package com.delivery.enroute.listeners;


import com.google.android.libraries.places.api.model.Place;

public interface PlaceFoundListner {
    void found(Place place);
}
