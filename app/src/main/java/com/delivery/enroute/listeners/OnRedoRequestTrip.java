package com.delivery.enroute.listeners;

import com.delivery.enroute.models.Trip;

public interface OnRedoRequestTrip {
    void requestTrip(Trip trip);
}
