package com.delivery.enroute.listeners;

public interface RowTappedLister<T> {
    void onRowTapped(T row);
}
