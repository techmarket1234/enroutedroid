package com.delivery.enroute.listeners;

import com.delivery.enroute.models.Coupon;

public interface PromoCodeAppliedListener {
    void onPromoCodeApplied(Coupon coupon);
}
