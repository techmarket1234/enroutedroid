package com.delivery.enroute.listeners;

import com.delivery.enroute.models.MyCurrentPlace;

public interface OnAddressRetrieved  {
    void addressFound(MyCurrentPlace address);
}
