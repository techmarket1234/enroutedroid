package com.delivery.enroute.listeners;

import com.delivery.enroute.singletons.ApplicaitonUser;

public interface OnGetServerUserCompleted {
    void userFound(ApplicaitonUser applicaitonUser);
    void userNotFound();
}
