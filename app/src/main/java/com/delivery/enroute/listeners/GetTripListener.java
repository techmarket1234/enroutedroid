package com.delivery.enroute.listeners;

import com.delivery.enroute.models.Trip;

public interface GetTripListener {
    void found(Trip trip);
    void notFound();
}
