package com.delivery.enroute.listeners;

import com.delivery.enroute.models.Trip;

public interface UnPaidTripListener {
    void unpaidTrip(Trip trip, boolean allowUserToRequestAnotherTrip);
    void notFound();
}
