package com.delivery.enroute.listeners;

public interface OnUpdateServerUserCompleted {
    void onComplete(boolean isSuccessful);
}
