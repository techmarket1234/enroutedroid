package com.delivery.enroute.listeners;

import com.delivery.enroute.models.User;
import com.delivery.enroute.models.Wallet;

public interface OnWalletBalanceFetched {
    void onFetch(User user);
}
