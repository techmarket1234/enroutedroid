package com.delivery.enroute.layoutdialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.delivery.enroute.R;
import com.delivery.enroute.listeners.PromoCodeAppliedListener;
import com.delivery.enroute.models.Coupon;
import com.delivery.enroute.singletons.ApplicaitonUser;
import com.delivery.enroute.utils.Constants;
import com.delivery.enroute.webservices.enums.Collections;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PromoCodeDialog extends AppCompatDialogFragment {

    private EditText promoCodeEt;
    private TextView errorMessage;
    private ProgressBar progressBar;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private ApplicaitonUser applicaitonUser;
    private FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private PromoCodeAppliedListener promoCodeAppliedListener;

    public void setPromoCodeAppliedListener(PromoCodeAppliedListener promoCodeAppliedListener) {
        this.promoCodeAppliedListener = promoCodeAppliedListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        if (getActivity() == null) return super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder =  new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View customDView = inflater.inflate(R.layout.dialog_add_promo_code,null);

        applicaitonUser = ApplicaitonUser.getInstance(getActivity());

        promoCodeEt = customDView.findViewById(R.id.promo_code_et);
        errorMessage = customDView.findViewById(R.id.error_message);
        Button applyCodeBtn = customDView.findViewById(R.id.apply_code_btn);
        progressBar = customDView.findViewById(R.id.progress_bar);

        ImageView closeBtn = customDView.findViewById(R.id.closeBtn);

        closeBtn.setOnClickListener(view -> dismiss());

        applyCodeBtn.setOnClickListener(view -> {
            checkIfCouponCanBeApplied();
        });

        builder.setView(customDView).setNegativeButton("Cancel", (dialog, which) -> dismiss());

        return builder.create();
    }

    void checkIfCouponCanBeApplied(){

        errorMessage.setVisibility(View.GONE);

        if (TextUtils.isEmpty(promoCodeEt.getText())){
            promoCodeEt.setError("Required");
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        String code = promoCodeEt.getText().toString();
        db.collection(Collections.COUPONS.toString()).whereEqualTo("code", code).get().addOnCompleteListener(task -> {

            if (!task.isSuccessful()){
                showError("Unable to connect. Please try again");
                return;
            }

            QuerySnapshot queryDocumentSnapshots = task.getResult();
            if (queryDocumentSnapshots == null) {
                showError("No connection.");
                return;
            }

            if (queryDocumentSnapshots.isEmpty()){
                showError("This coupon does not exist");
                return;
            }

            List<Coupon> coupons = task.getResult().toObjects(Coupon.class);

            if (coupons.isEmpty()){
                showError("The coupon you entered does not exist");
                return;
            }

            Coupon coupon =  coupons.get(0);
            if (coupon.status == null || "INACTIVE".equals(coupon.status) ){
                showError("This coupon is exhausted");
                return;
            }

            if (coupon.totalUsed >= coupon.limit){
                showError("This coupon is exhausted");
                return;
            }

            Constants.TEMP_COUPON = coupon;

            promoCodeAppliedListener.onPromoCodeApplied(coupon);

            dismiss();

        });

    }

    private void showError(String error){
        progressBar.setVisibility(View.GONE);
        errorMessage.setVisibility(View.VISIBLE);
        errorMessage.setText(error);
    }

}
