package com.delivery.enroute.layoutdialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.delivery.enroute.R;
import com.delivery.enroute.adapters.ItemCategoryAdapter;
import com.delivery.enroute.listeners.RowTappedLister;
import com.delivery.enroute.models.Item;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

public class ItemCategoriesDialog extends AppCompatDialogFragment {

    private RowTappedLister<Item> itemRowTappedLister;

    public void setItemRowTappedLister(RowTappedLister<Item> itemRowTappedLister) {
        this.itemRowTappedLister = itemRowTappedLister;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        if (getActivity() == null) return super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder =  new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View customDView = inflater.inflate(R.layout.item_category_list,null);

        GridView gridView = customDView.findViewById(R.id.gridview);
        ItemCategoryAdapter itemCategoryAdapter = new ItemCategoryAdapter(getActivity(), Item.getItems());
        gridView.setAdapter(itemCategoryAdapter);

        gridView.setOnItemClickListener((parent, view, position, id) -> {
            Item item = Item.getItems().get(position);
            itemRowTappedLister.onRowTapped(item);
            dismiss();
        });

        builder.setView(customDView).setNegativeButton("Cancel", (dialog, which) -> dismiss());

        return builder.create();
    }
}
