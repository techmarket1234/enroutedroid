package com.delivery.enroute.layoutdialogs;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.delivery.enroute.R;
import com.delivery.enroute.utils.Constants;
import com.jsibbold.zoomage.ZoomageView;

import java.io.ByteArrayOutputStream;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductEnlargeFragment extends AppCompatDialogFragment {

    private static ProductEnlargeFragment fragment;

    public ProductEnlargeFragment() {
        // Required empty public constructor
    }

    public static ProductEnlargeFragment newInstance(String productTitle, String productPrice, Bitmap bitmap) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        Bundle args = new Bundle();
        args.putByteArray(Constants.PRODUCT_IMAGE, byteArray);
        args.putString(Constants.PRODUCT_TITLE, productTitle);
        args.putString(Constants.PRODUCT_PRICE, productPrice);

        if(fragment == null) fragment = new ProductEnlargeFragment();

        fragment.setArguments(args);
        return fragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        if (getActivity() == null) return super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder =  new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();



        @SuppressLint("InflateParams")
        View customDView = inflater.inflate(R.layout.fragment_product_enlarge,null);

        ZoomageView zoomageView = customDView.findViewById(R.id.myZoomageView);
        TextView productTittle = customDView.findViewById(R.id.productTitle);
        TextView price = customDView.findViewById(R.id.price);

        if (getArguments() != null){
            productTittle.setText(getArguments().getString(Constants.PRODUCT_TITLE));
            price.setText(getArguments().getString(Constants.PRODUCT_PRICE));
            byte[] byteArray = getArguments().getByteArray(Constants.PRODUCT_IMAGE);
            if (byteArray != null){
                Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                zoomageView.setImageBitmap(bmp);
            }

        }

        builder.setView(customDView).setNegativeButton("close", (dialogInterface, i) -> dismiss());
        return builder.create();
    }

    @Override
    public void onPause() {
        if (getArguments() != null){
            setArguments(null);
        }
        super.onPause();
    }

    @Override
    public void show(@NonNull FragmentManager manager, @Nullable String tag) {
        if (!isAdded()){
            super.show(manager, tag);
        }

    }

}
