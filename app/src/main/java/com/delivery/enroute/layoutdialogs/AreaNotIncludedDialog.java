package com.delivery.enroute.layoutdialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.delivery.enroute.R;
import com.delivery.enroute.listeners.OnFragmentInteractedListener;

public class AreaNotIncludedDialog extends AppCompatDialogFragment {

    //private OnFragmentInteractedListener onFragmentInteractedListener;

//    public void setOnFragmentInteractedListener(OnFragmentInteractedListener onFragmentInteractedListener) {
//        this.onFragmentInteractedListener = onFragmentInteractedListener;
//    }

    public static AreaNotIncludedDialog newInstance(String description) {
        
        Bundle args = new Bundle();
        args.putString("description", description);

        AreaNotIncludedDialog fragment = new AreaNotIncludedDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        if (getActivity() == null) return super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder =  new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        @SuppressLint("InflateParams")
        View customDView = inflater.inflate(R.layout.fragment_area_not_included,null);


        TextView specify_location = customDView.findViewById(R.id.specify_location);
        if (getArguments() != null){
            String description = getArguments().getString("description");
            specify_location.setText(description);
        }
        // get active locations and set text to specify_location

        customDView.findViewById(R.id.search_another_location).setOnClickListener(view -> {
            dismiss();
//            if (onFragmentInteractedListener != null)
//                onFragmentInteractedListener.onFragmentInterecte(view);
        });

        builder.setView(customDView);
        return builder.create();
    }

    @Override
    public void onResume() {

        super.onResume();
    }



}
