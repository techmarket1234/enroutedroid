package com.delivery.enroute.layoutdialogs;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.delivery.enroute.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class VerficationCodeSentFragment extends AppCompatDialogFragment {


    public VerficationCodeSentFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_verfication_code_sent, container, false);
        if (getActivity() == null) return super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder =  new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        @SuppressLint("InflateParams")
        View customDView = inflater.inflate(R.layout.fragment_verfication_code_sent,null);

        customDView.findViewById(R.id.done).setOnClickListener(view -> {
//            if (onFragmentInteractedListener != null)
//                onFragmentInteractedListener.onFragmentInterecte(view);
            dismiss();
        });

        builder.setView(customDView);
        return builder.create();
    }
}
