package com.delivery.enroute.layoutdialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.delivery.enroute.R;

public class UpdateAppDialog extends AppCompatDialogFragment {

    private OnupdateappListener onupdateappListener;

    public void setOnupdateappListener(OnupdateappListener onupdateappListener) {
        this.onupdateappListener = onupdateappListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        if (getActivity() == null) return super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder =  new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        @SuppressLint("InflateParams")
        View customDView = inflater.inflate(R.layout.fragment_update_app,null);

        customDView.findViewById(R.id.update_now).setOnClickListener(view -> {
            dismiss();
            if (getActivity() != null){
                //                                                        debug mode --------
                if (onupdateappListener != null)
                    onupdateappListener.onUpdate(null);
            }
                
        });

        customDView.findViewById(R.id.cancel).setOnClickListener(view -> {
                dismiss();
        });

        builder.setView(customDView);
        return builder.create();
    }

    public interface OnupdateappListener{
        void onUpdate(String debugUrl);
        void onEverythinOk();
    }


}
