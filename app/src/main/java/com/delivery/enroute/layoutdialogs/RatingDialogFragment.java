package com.delivery.enroute.layoutdialogs;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.delivery.enroute.R;
import com.delivery.enroute.models.Trip;
import com.delivery.enroute.utils.Constants;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.ByteArrayOutputStream;

/**
 * A simple {@link Fragment} subclass.
 */
public class RatingDialogFragment extends AppCompatDialogFragment {

    private OnRatedListener onRatedListener;
    private RatingBar ratingBar;
    private EditText comment;
    private ImageView riderImage;


    public RatingDialogFragment() {
        // Required empty public constructor
    }

    private static RatingDialogFragment newInstance(Trip trip) {

        Bundle args = new Bundle();
        args.putSerializable(Constants.TRIP, trip);

        RatingDialogFragment fragment = new RatingDialogFragment();

        fragment.setArguments(args);
        return fragment;
    }

    public static RatingDialogFragment newInstance(Bitmap bitmap, Trip trip) {

        if (bitmap == null){
            return newInstance(trip);
        }

        Bundle args = new Bundle();
        args.putSerializable(Constants.TRIP, trip);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        args.putByteArray(Constants.RIDER_IMAGE, byteArray);

        RatingDialogFragment fragment = new RatingDialogFragment();

        fragment.setArguments(args);
        return fragment;
    }

    public void setOnRatedListener(OnRatedListener onRatedListener) {
        this.onRatedListener = onRatedListener;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        if (getActivity() == null) return super.onCreateDialog(savedInstanceState);

        ImageLoader imageLoader = ImageLoader.getInstance();


        Log.d(Constants.TAG, "onCreateDialog: RatingDialogFragment ");

        AlertDialog.Builder builder =  new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        @SuppressLint("InflateParams")
        View customDView = inflater.inflate(R.layout.fragment_rating_dialog,null);

        // private Trip trip;
        TextView rider_name = customDView.findViewById(R.id.rider_name);
        riderImage = customDView.findViewById(R.id.rider_image);

        try {
            if (getArguments() != null){
                Trip trip = (Trip) getArguments().getSerializable(Constants.TRIP);
                if (trip != null){
                    rider_name.setText(trip.riderName);
                    byte[] byteArray = getArguments().getByteArray(Constants.RIDER_IMAGE);
                    if (byteArray != null){
                            Log.d(Constants.TAG, "onCreateDialog: byteArray NOT null ");
                        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                        riderImage.setImageBitmap(bmp);
                    }else{
                        Log.d(Constants.TAG, "onCreateDialog: byteArray is null ");
                        if (trip.riderImage != null){
                            imageLoader.loadImage(trip.riderImage, new SimpleImageLoadingListener() {
                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                    // Do whatever you want with Bitmap
                                    if (loadedImage != null){
                                        riderImage.setImageBitmap(loadedImage);
                                    }
                                }
                            });
                        }
                    }


                }

            }
        }catch (Exception e){
            Log.d(Constants.TAG, "Rating dialog : Exception " + e.getMessage());
        }


        // message templates
        customDView.findViewById(R.id.remove_this_rider).setOnClickListener(messageTemplateTapped);
        customDView.findViewById(R.id.not_the_best).setOnClickListener(messageTemplateTapped);
        customDView.findViewById(R.id.below_average).setOnClickListener(messageTemplateTapped);
        customDView.findViewById(R.id.could_be_better).setOnClickListener(messageTemplateTapped);
        customDView.findViewById(R.id.awesome_delivery).setOnClickListener(messageTemplateTapped);

        ratingBar = customDView.findViewById(R.id.ratingBar);
        comment = customDView.findViewById(R.id.comment);

        builder.setView(customDView).setPositiveButton("Done", (dialogInterface, i) -> {
            double ratingValue = ratingBar.getRating();

           // Toast.makeText(getActivity(), "" + ratingValue, Toast.LENGTH_SHORT).show();

            if (onRatedListener != null){
                onRatedListener.onRated(ratingValue, String.valueOf(comment.getText()));
            }
        }).setNegativeButton("Cancel", (dialog, which) -> {
            if (onRatedListener != null){
                onRatedListener.onCancelled();
            }
            dismiss();
        });



        return builder.create();

    }

    private View.OnClickListener messageTemplateTapped = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            comment.setText(((TextView) view).getText());
        }
    };

    public interface OnRatedListener{
        void onRated(double mark, String message);
        void onCancelled();
    }

    @Override
    public void show(@NonNull FragmentManager manager, @Nullable String tag) {
        if (!isVisible() && !isAdded())
            super.show(manager, tag);

    }
}
