package com.delivery.enroute.layoutdialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import com.delivery.enroute.utils.Constants;



public class LocationSettingsDialog extends DialogFragment implements DialogInterface.OnClickListener {

    Activity activity;
    Context context;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Location Settings");
        builder.setMessage("Enable location Setting to continue");
        builder.setPositiveButton("OK",this);
        builder.setNegativeButton("Cancel",this);
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which){
            case DialogInterface.BUTTON_POSITIVE:
                openLocationSetting();
                activity.finish();
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                dialog.dismiss();
                break;
        }
    }

    public void addActivity(Activity activity){
        this.activity = activity;
    }

    private void openLocationSetting() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, Constants.LOCATION_SETTINGS_REQUEST);
    }




}
