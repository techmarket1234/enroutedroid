package com.delivery.enroute.layoutdialogs;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.delivery.enroute.R;
import com.delivery.enroute.utils.Constants;
import com.jsibbold.zoomage.ZoomageView;

import java.io.ByteArrayOutputStream;

/**
 * A simple {@link Fragment} subclass.
 */
    public class ImageZoomFragment extends AppCompatDialogFragment {

    private static ImageZoomFragment fragment;


    public ImageZoomFragment() {
        // Required empty public constructor
    }

    public static ImageZoomFragment newInstance(Bitmap bitmap) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        Bundle args = new Bundle();
        args.putByteArray(Constants.RIDER_IMAGE, byteArray);

         if(fragment == null) fragment = new ImageZoomFragment();

        fragment.setArguments(args);
        return fragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        if (getActivity() == null) return super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder =  new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();



        @SuppressLint("InflateParams")
        View customDView = inflater.inflate(R.layout.fragment_image_zoom,null);

       ZoomageView zoomageView = customDView.findViewById(R.id.myZoomageView);

       if (getArguments() != null){
           byte[] byteArray = getArguments().getByteArray(Constants.RIDER_IMAGE);
           if (byteArray != null){
               Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
               zoomageView.setImageBitmap(bmp);
           }

       }

        builder.setView(customDView).setNegativeButton("close", (dialogInterface, i) -> dismiss());
        return builder.create();
    }

}
