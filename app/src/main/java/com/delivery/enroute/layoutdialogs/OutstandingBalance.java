package com.delivery.enroute.layoutdialogs;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.delivery.enroute.R;
import com.delivery.enroute.listeners.OnFragmentInteractedListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class OutstandingBalance extends AppCompatDialogFragment {

    private OnFragmentInteractedListener onFragmentInteractedListener;

    public void setOnFragmentInteractedListener(OnFragmentInteractedListener onFragmentInteractedListener) {
        this.onFragmentInteractedListener = onFragmentInteractedListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        if (getActivity() == null) return super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder =  new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        @SuppressLint("InflateParams")
        View customDView = inflater.inflate(R.layout.fragment_outstanding_balance,null);

        customDView.findViewById(R.id.search_another_location).setOnClickListener(view -> {
            if (onFragmentInteractedListener != null)
                onFragmentInteractedListener.onFragmentInterecte(view);
        });

        builder.setView(customDView);
        return builder.create();
    }



//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_outstanding_balance, container, false);
//    }

}
