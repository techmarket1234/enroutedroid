package com.delivery.enroute.layoutdialogs;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.delivery.enroute.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewFragment extends AppCompatDialogFragment {

    private OnReviewListener onReviewListener;

    public void setOnReviewListener(OnReviewListener onReviewListener) {
        this.onReviewListener = onReviewListener;
    }

    public ReviewFragment() {
        // Required empty public constructor
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        if (getActivity() == null) return super.onCreateDialog(savedInstanceState);

        AlertDialog.Builder builder =  new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        @SuppressLint("InflateParams")
        View customDView = inflater.inflate(R.layout.fragment_review,null);

        customDView.findViewById(R.id.review_now).setOnClickListener(view -> {
            dismiss();
            if (getActivity() != null){
                //                                                        debug mode --------
                if (onReviewListener != null)
                    onReviewListener.onReviewTapped();
            }

        });

        customDView.findViewById(R.id.cancel).setOnClickListener(view -> {
            dismiss();
        });

        builder.setView(customDView);
        return builder.create();
    }

    public interface OnReviewListener {
        void onReviewTapped();
    }

    @Override
    public void show(@NonNull FragmentManager manager, @Nullable String tag) {
        if(!isAdded() && !isVisible()){
            super.show(manager, tag);
        }
    }
}
