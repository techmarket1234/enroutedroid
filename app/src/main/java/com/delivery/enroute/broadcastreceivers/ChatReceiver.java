package com.delivery.enroute.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.delivery.enroute.models.Chat;
import com.delivery.enroute.utils.Constants;
import com.google.gson.Gson;

public class ChatReceiver extends BroadcastReceiver {

    OnChatRecievedListener onChatRecievedListener;

    @Override
    public void onReceive(Context context, Intent intent) {

       Chat chat  = (Chat) intent.getSerializableExtra(Constants.CHAT);

       Log.d(Constants.TAG, "onReceive: message received" + new Gson().toJson(chat));

       if (onChatRecievedListener != null){
           onChatRecievedListener.onChatReceived(chat);
       }


    }

    public void setOnChatRecievedListener(OnChatRecievedListener onChatRecievedListener) {
        this.onChatRecievedListener = onChatRecievedListener;
    }


    public interface OnChatRecievedListener{
        void onChatReceived(Chat chat);
    }
}
