package com.delivery.enroute.broadcastreceivers;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.delivery.enroute.utils.Constants;


// Listen for SMS ----------------
public class SmsBroadcastReceiver extends BroadcastReceiver {
    static OnSmsReceived onSmsReceived;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(Constants.TAG, "onReceive: sms recived");
        if (intent.getAction() != null){
            if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
                Bundle bundle = intent.getExtras();           //---get the SMS message passed in---
                SmsMessage[] msgs = null;
                String msg_from;
                if (bundle != null){
                    //---retrieve the SMS message received---

                    try{
                        Object[] pdus = (Object[]) bundle.get("pdus");
                        if (pdus != null) {
                            msgs = new SmsMessage[pdus.length];

                            // 0 index is the first page of the sms
                            msgs[0] = SmsMessage.createFromPdu((byte[])pdus[0]);
                            msg_from = msgs[0].getOriginatingAddress();

                            if(msg_from != null && !msg_from.toLowerCase().equals("phone code")){
                                return;
                            }

                            if (msg_from != null){
                                String msgBody = msgs[0].getMessageBody();

                                Log.d(Constants.TAG, "onReceive: " + onSmsReceived);
                                if (onSmsReceived != null){
                                    onSmsReceived.recieved(extractNumber(msgBody));
                                }
                            }

//                           Intent i = new Intent(context, OtpActivity.class);
//                           i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                           i.putExtra("message", );
//                           context.startActivity(i);

                        }

                    }catch(Exception e){
//                            Log.d("Exception caught",e.getMessage());
                    }
                }
            }
        }
    }

    public void setSmsListener(OnSmsReceived smsReceived){
        onSmsReceived = smsReceived;
        Log.d(Constants.TAG, "setSmsListener: on receive set" + onSmsReceived);
    }

    public static String extractNumber(final String str) {

        if(str == null || str.isEmpty()) return "";

        StringBuilder sb = new StringBuilder();
        boolean found = false;
        for(char c : str.toCharArray()){
            if(Character.isDigit(c)){
                sb.append(c);
                found = true;
            } else if(found){
                // If we already found a digit before and this char is not a digit, stop looping
                break;
            }
        }

        return sb.toString();
    }

    public interface OnSmsReceived{
        void recieved(String code);
    }
}